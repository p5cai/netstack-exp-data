#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       80.7   432.5    26.8    39.1    42.8    65.0   111.8   131.1   196.0
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4719874.2 83654.9 4400742.6 4419727.1 4618720.9 4822441.2 5026161.5 5051626.5 5071998.6

Total QPS = 719865.8 (21595976 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 5277336440 bytes :  167.8 MB/s
TX  911870702 bytes :   29.0 MB/s
