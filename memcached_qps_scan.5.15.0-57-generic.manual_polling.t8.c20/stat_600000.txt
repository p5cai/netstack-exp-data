# started on Mon Apr  3 22:09:11 2023


 Performance counter stats for 'CPU(s) 0-7':

        88,775,149      LLC-load-misses           #   19.95% of all LL-cache accesses  (49.99%)
       170,432,296      LLC-load-misses           #    9.52% of all LL-cache accesses  (49.99%)
       444,923,855      LLC-loads                 #    1.539 M/sec                    (49.99%)
     1,790,425,177      LLC-loads                 #    6.193 M/sec                    (41.66%)
         2,592,056      LLC-store-misses          #    8.966 K/sec                    (25.01%)
        66,799,404      LLC-store-misses          #  231.051 K/sec                    (33.34%)
       180,952,701      LLC-stores                #  625.892 K/sec                    (25.00%)
       969,200,721      LLC-stores                #    3.352 M/sec                    (33.34%)
        289,111.50 msec task-clock                #    8.000 CPUs utilized          
    93,465,960,896      cycles:u                  #    0.323 GHz                      (41.67%)
   559,276,949,546      cycles:k                  #    1.934 GHz                      (41.67%)
    55,835,529,960      instructions:u            #    0.60  insn per cycle           (50.00%)
   601,295,351,826      instructions:k            #    1.08  insn per cycle           (49.99%)

      36.139711558 seconds time elapsed

