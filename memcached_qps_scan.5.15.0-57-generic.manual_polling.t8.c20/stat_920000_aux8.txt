# started on Tue Apr  4 00:56:54 2023


 Performance counter stats for 'CPU(s) 0-7':

       134,518,475      LLC-load-misses           #   20.30% of all LL-cache accesses  (49.98%)
       260,144,061      LLC-load-misses           #    8.68% of all LL-cache accesses  (49.99%)
       662,623,859      LLC-loads                 #    2.294 M/sec                    (50.01%)
     2,997,714,234      LLC-loads                 #   10.377 M/sec                    (41.69%)
         2,902,933      LLC-store-misses          #   10.049 K/sec                    (25.02%)
       107,129,429      LLC-store-misses          #  370.844 K/sec                    (33.35%)
       215,708,193      LLC-stores                #  746.704 K/sec                    (24.99%)
     1,618,703,924      LLC-stores                #    5.603 M/sec                    (33.32%)
        288,880.32 msec task-clock                #    8.000 CPUs utilized          
   121,009,011,157      cycles:u                  #    0.419 GHz                      (41.65%)
   533,883,097,086      cycles:k                  #    1.848 GHz                      (41.65%)
    81,011,842,036      instructions:u            #    0.67  insn per cycle           (49.97%)
   473,202,614,383      instructions:k            #    0.89  insn per cycle           (49.97%)

      36.111011174 seconds time elapsed

