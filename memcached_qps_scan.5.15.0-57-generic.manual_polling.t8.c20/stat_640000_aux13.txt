# started on Tue Apr  4 02:18:22 2023


 Performance counter stats for 'CPU(s) 0-7':

        93,971,375      LLC-load-misses           #   19.86% of all LL-cache accesses  (49.99%)
       183,280,022      LLC-load-misses           #    9.71% of all LL-cache accesses  (50.00%)
       473,094,831      LLC-loads                 #    1.634 M/sec                    (50.01%)
     1,888,220,432      LLC-loads                 #    6.520 M/sec                    (41.69%)
         2,568,180      LLC-store-misses          #    8.868 K/sec                    (25.01%)
        71,727,707      LLC-store-misses          #  247.678 K/sec                    (33.34%)
       183,790,447      LLC-stores                #  634.633 K/sec                    (24.99%)
     1,033,674,822      LLC-stores                #    3.569 M/sec                    (33.32%)
        289,600.99 msec task-clock                #    8.000 CPUs utilized          
    97,277,868,729      cycles:u                  #    0.336 GHz                      (41.65%)
   549,743,962,914      cycles:k                  #    1.898 GHz                      (41.65%)
    58,080,120,810      instructions:u            #    0.60  insn per cycle           (49.98%)
   564,661,855,918      instructions:k            #    1.03  insn per cycle           (49.98%)

      36.201166140 seconds time elapsed

