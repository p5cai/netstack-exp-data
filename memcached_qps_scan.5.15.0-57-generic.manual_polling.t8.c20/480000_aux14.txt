#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       56.8   581.9    26.8    33.6    35.4    45.9    66.4    77.5   115.7
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3111674.0 62986.6 3005766.4 2884678.5 2903471.2 3053812.8 3334986.1 3401354.0 3454448.3

Total QPS = 479828.2 (14394848 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 3487625532 bytes :  110.9 MB/s
TX  607764449 bytes :   19.3 MB/s
