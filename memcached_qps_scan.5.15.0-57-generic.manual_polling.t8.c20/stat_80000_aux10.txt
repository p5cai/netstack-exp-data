# started on Tue Apr  4 01:19:30 2023


 Performance counter stats for 'CPU(s) 0-7':

        12,577,090      LLC-load-misses           #   20.29% of all LL-cache accesses  (49.97%)
        34,360,708      LLC-load-misses           #   13.23% of all LL-cache accesses  (49.98%)
        61,982,022      LLC-loads                 #  214.484 K/sec                    (49.99%)
       259,704,623      LLC-loads                 #  898.689 K/sec                    (41.68%)
           115,124      LLC-store-misses          #  398.378 /sec                     (25.02%)
        10,390,630      LLC-store-misses          #   35.956 K/sec                    (33.36%)
        25,877,576      LLC-stores                #   89.547 K/sec                    (25.01%)
       135,922,249      LLC-stores                #  470.349 K/sec                    (33.34%)
        288,981.79 msec task-clock                #    8.000 CPUs utilized          
    14,876,654,568      cycles:u                  #    0.051 GHz                      (41.67%)
   631,646,569,096      cycles:k                  #    2.186 GHz                      (41.66%)
     7,656,170,349      instructions:u            #    0.51  insn per cycle           (49.98%)
 1,196,659,478,250      instructions:k            #    1.89  insn per cycle           (49.97%)

      36.121466154 seconds time elapsed

