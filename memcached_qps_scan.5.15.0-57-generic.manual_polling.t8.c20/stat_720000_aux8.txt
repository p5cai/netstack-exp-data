# started on Tue Apr  4 00:52:00 2023


 Performance counter stats for 'CPU(s) 0-7':

       105,666,250      LLC-load-misses           #   20.49% of all LL-cache accesses  (49.99%)
       203,128,211      LLC-load-misses           #    9.43% of all LL-cache accesses  (49.99%)
       515,793,607      LLC-loads                 #    1.786 M/sec                    (50.00%)
     2,154,701,212      LLC-loads                 #    7.463 M/sec                    (41.67%)
           758,423      LLC-store-misses          #    2.627 K/sec                    (25.01%)
        88,845,141      LLC-store-misses          #  307.706 K/sec                    (33.34%)
       187,187,741      LLC-stores                #  648.305 K/sec                    (25.00%)
     1,184,960,195      LLC-stores                #    4.104 M/sec                    (33.34%)
        288,733.93 msec task-clock                #    8.000 CPUs utilized          
   106,294,638,722      cycles:u                  #    0.368 GHz                      (41.67%)
   542,075,532,575      cycles:k                  #    1.877 GHz                      (41.67%)
    64,163,192,324      instructions:u            #    0.60  insn per cycle           (50.00%)
   512,049,163,926      instructions:k            #    0.94  insn per cycle           (49.99%)

      36.089940023 seconds time elapsed

