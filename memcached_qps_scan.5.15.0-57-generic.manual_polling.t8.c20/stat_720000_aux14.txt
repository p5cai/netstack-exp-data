# started on Tue Apr  4 02:38:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       104,768,004      LLC-load-misses           #   19.64% of all LL-cache accesses  (49.99%)
       202,016,940      LLC-load-misses           #    9.22% of all LL-cache accesses  (49.99%)
       533,473,038      LLC-loads                 #    1.848 M/sec                    (49.99%)
     2,191,073,254      LLC-loads                 #    7.590 M/sec                    (41.67%)
           683,603      LLC-store-misses          #    2.368 K/sec                    (25.01%)
        79,308,833      LLC-store-misses          #  274.714 K/sec                    (33.34%)
       199,772,432      LLC-stores                #  691.982 K/sec                    (25.01%)
     1,191,152,919      LLC-stores                #    4.126 M/sec                    (33.34%)
        288,696.15 msec task-clock                #    8.000 CPUs utilized          
   107,714,158,103      cycles:u                  #    0.373 GHz                      (41.67%)
   539,724,275,809      cycles:k                  #    1.870 GHz                      (41.67%)
    64,058,367,678      instructions:u            #    0.59  insn per cycle           (50.00%)
   491,811,506,685      instructions:k            #    0.91  insn per cycle           (49.99%)

      36.085054742 seconds time elapsed

