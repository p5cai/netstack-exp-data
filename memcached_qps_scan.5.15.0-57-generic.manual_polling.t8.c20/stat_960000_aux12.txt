# started on Tue Apr  4 02:08:44 2023


 Performance counter stats for 'CPU(s) 0-7':

       134,963,202      LLC-load-misses           #   19.66% of all LL-cache accesses  (49.99%)
       288,243,439      LLC-load-misses           #    9.38% of all LL-cache accesses  (49.99%)
       686,346,452      LLC-loads                 #    2.371 M/sec                    (50.00%)
     3,072,731,149      LLC-loads                 #   10.614 M/sec                    (41.66%)
         2,559,432      LLC-store-misses          #    8.841 K/sec                    (25.01%)
        79,730,821      LLC-store-misses          #  275.411 K/sec                    (33.34%)
       215,433,445      LLC-stores                #  744.164 K/sec                    (25.00%)
     1,702,490,329      LLC-stores                #    5.881 M/sec                    (33.34%)
        289,497.30 msec task-clock                #    8.000 CPUs utilized          
   123,112,568,365      cycles:u                  #    0.425 GHz                      (41.67%)
   524,107,125,531      cycles:k                  #    1.810 GHz                      (41.67%)
    81,529,512,918      instructions:u            #    0.66  insn per cycle           (50.00%)
   467,905,752,102      instructions:k            #    0.89  insn per cycle           (50.00%)

      36.188196164 seconds time elapsed

