# started on Mon Apr  3 23:34:41 2023


 Performance counter stats for 'CPU(s) 0-7':

        69,986,302      LLC-load-misses           #   19.18% of all LL-cache accesses  (49.98%)
       139,052,412      LLC-load-misses           #   10.00% of all LL-cache accesses  (49.99%)
       364,860,427      LLC-loads                 #    1.263 M/sec                    (50.00%)
     1,389,927,862      LLC-loads                 #    4.811 M/sec                    (41.69%)
         2,420,879      LLC-store-misses          #    8.380 K/sec                    (25.02%)
        58,548,330      LLC-store-misses          #  202.669 K/sec                    (33.35%)
       144,428,154      LLC-stores                #  499.948 K/sec                    (25.00%)
       759,455,400      LLC-stores                #    2.629 M/sec                    (33.33%)
        288,886.18 msec task-clock                #    8.000 CPUs utilized          
    80,282,925,014      cycles:u                  #    0.278 GHz                      (41.65%)
   566,908,248,219      cycles:k                  #    1.962 GHz                      (41.64%)
    44,565,027,820      instructions:u            #    0.56  insn per cycle           (49.97%)
   695,519,518,416      instructions:k            #    1.23  insn per cycle           (49.97%)

      36.111826560 seconds time elapsed

