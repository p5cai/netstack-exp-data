# started on Mon Apr  3 23:47:40 2023


 Performance counter stats for 'CPU(s) 0-7':

       135,547,239      LLC-load-misses           #   19.98% of all LL-cache accesses  (49.97%)
       274,262,456      LLC-load-misses           #    8.64% of all LL-cache accesses  (49.98%)
       678,513,916      LLC-loads                 #    2.348 M/sec                    (49.99%)
     3,175,444,826      LLC-loads                 #   10.987 M/sec                    (41.67%)
         2,611,276      LLC-store-misses          #    9.035 K/sec                    (25.01%)
        90,420,686      LLC-store-misses          #  312.861 K/sec                    (33.35%)
       215,498,768      LLC-stores                #  745.638 K/sec                    (25.01%)
     1,699,624,616      LLC-stores                #    5.881 M/sec                    (33.35%)
        289,012.41 msec task-clock                #    8.000 CPUs utilized          
   124,519,962,880      cycles:u                  #    0.431 GHz                      (41.68%)
   523,216,628,436      cycles:k                  #    1.810 GHz                      (41.66%)
    80,949,060,587      instructions:u            #    0.65  insn per cycle           (49.99%)
   466,919,636,082      instructions:k            #    0.89  insn per cycle           (49.98%)

      36.127653189 seconds time elapsed

