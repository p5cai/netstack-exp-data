#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       62.3   667.6    26.8    35.1    37.3    48.9    72.0    83.4   124.9
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3370394.1 67086.7 3306343.1 3169400.4 3186326.5 3321734.8 3457143.1 3562296.1 3764054.4

Total QPS = 520017.6 (15600530 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 3781719354 bytes :  120.2 MB/s
TX  658722273 bytes :   20.9 MB/s
