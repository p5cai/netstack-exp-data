# started on Tue Apr  4 03:45:13 2023


 Performance counter stats for 'CPU(s) 0-7':

        81,662,879      LLC-load-misses           #   19.35% of all LL-cache accesses  (49.98%)
       163,148,678      LLC-load-misses           #   10.02% of all LL-cache accesses  (49.99%)
       422,016,836      LLC-loads                 #    1.462 M/sec                    (49.99%)
     1,629,030,607      LLC-loads                 #    5.643 M/sec                    (41.66%)
           469,853      LLC-store-misses          #    1.628 K/sec                    (25.01%)
        62,807,827      LLC-store-misses          #  217.561 K/sec                    (33.34%)
       168,008,297      LLC-stores                #  581.966 K/sec                    (25.01%)
       891,378,439      LLC-stores                #    3.088 M/sec                    (33.34%)
        288,691.04 msec task-clock                #    8.000 CPUs utilized          
    85,745,472,683      cycles:u                  #    0.297 GHz                      (41.67%)
   561,086,036,322      cycles:k                  #    1.944 GHz                      (41.67%)
    51,178,342,620      instructions:u            #    0.60  insn per cycle           (50.00%)
   634,563,059,211      instructions:k            #    1.13  insn per cycle           (49.99%)

      36.084635552 seconds time elapsed

