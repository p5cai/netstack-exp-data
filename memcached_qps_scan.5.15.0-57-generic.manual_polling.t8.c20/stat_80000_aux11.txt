# started on Tue Apr  4 01:37:14 2023


 Performance counter stats for 'CPU(s) 0-7':

        12,411,855      LLC-load-misses           #   20.83% of all LL-cache accesses  (49.99%)
        34,312,408      LLC-load-misses           #   13.48% of all LL-cache accesses  (49.99%)
        59,596,327      LLC-loads                 #  205.884 K/sec                    (49.99%)
       254,533,665      LLC-loads                 #  879.322 K/sec                    (41.67%)
           106,194      LLC-store-misses          #  366.862 /sec                     (25.01%)
         9,946,109      LLC-store-misses          #   34.360 K/sec                    (33.34%)
        27,763,698      LLC-stores                #   95.914 K/sec                    (25.01%)
       134,679,141      LLC-stores                #  465.268 K/sec                    (33.34%)
        289,465.84 msec task-clock                #    8.000 CPUs utilized          
    13,746,555,039      cycles:u                  #    0.047 GHz                      (41.67%)
   631,718,678,939      cycles:k                  #    2.182 GHz                      (41.67%)
     7,658,047,093      instructions:u            #    0.56  insn per cycle           (50.00%)
 1,199,139,683,534      instructions:k            #    1.90  insn per cycle           (49.99%)

      36.182761603 seconds time elapsed

