# started on Mon Apr  3 22:40:39 2023


 Performance counter stats for 'CPU(s) 0-7':

        47,373,087      LLC-load-misses           #   19.82% of all LL-cache accesses  (49.99%)
        99,760,602      LLC-load-misses           #   10.49% of all LL-cache accesses  (50.00%)
       238,999,387      LLC-loads                 #  827.360 K/sec                    (50.01%)
       950,975,545      LLC-loads                 #    3.292 M/sec                    (41.69%)
         2,272,558      LLC-store-misses          #    7.867 K/sec                    (25.01%)
        38,243,180      LLC-store-misses          #  132.389 K/sec                    (33.34%)
       103,898,219      LLC-stores                #  359.671 K/sec                    (24.99%)
       510,389,435      LLC-stores                #    1.767 M/sec                    (33.32%)
        288,870.00 msec task-clock                #    8.000 CPUs utilized          
    54,146,790,205      cycles:u                  #    0.187 GHz                      (41.65%)
   596,406,134,257      cycles:k                  #    2.065 GHz                      (41.65%)
    31,214,142,887      instructions:u            #    0.58  insn per cycle           (49.98%)
   884,932,447,471      instructions:k            #    1.48  insn per cycle           (49.98%)

      36.107862247 seconds time elapsed

