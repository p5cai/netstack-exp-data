# started on Tue Apr  4 01:28:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       113,185,563      LLC-load-misses           #   20.07% of all LL-cache accesses  (49.99%)
       217,977,954      LLC-load-misses           #    9.44% of all LL-cache accesses  (49.99%)
       563,933,569      LLC-loads                 #    1.948 M/sec                    (49.99%)
     2,309,618,856      LLC-loads                 #    7.978 M/sec                    (41.66%)
         2,917,658      LLC-store-misses          #   10.078 K/sec                    (25.01%)
        97,566,719      LLC-store-misses          #  337.025 K/sec                    (33.34%)
       201,291,732      LLC-stores                #  695.323 K/sec                    (25.00%)
     1,258,744,545      LLC-stores                #    4.348 M/sec                    (33.34%)
        289,493.92 msec task-clock                #    8.000 CPUs utilized          
   110,284,865,255      cycles:u                  #    0.381 GHz                      (41.67%)
   543,011,040,386      cycles:k                  #    1.876 GHz                      (41.67%)
    68,810,488,119      instructions:u            #    0.62  insn per cycle           (50.00%)
   496,957,828,323      instructions:k            #    0.92  insn per cycle           (50.00%)

      36.187764974 seconds time elapsed

