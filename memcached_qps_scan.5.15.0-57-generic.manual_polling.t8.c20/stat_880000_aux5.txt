# started on Tue Apr  4 00:02:52 2023


 Performance counter stats for 'CPU(s) 0-7':

       129,775,314      LLC-load-misses           #   20.67% of all LL-cache accesses  (49.99%)
       240,108,012      LLC-load-misses           #    8.76% of all LL-cache accesses  (49.99%)
       627,982,884      LLC-loads                 #    2.175 M/sec                    (49.99%)
     2,740,171,930      LLC-loads                 #    9.491 M/sec                    (41.66%)
         1,010,436      LLC-store-misses          #    3.500 K/sec                    (25.01%)
       101,195,288      LLC-store-misses          #  350.502 K/sec                    (33.34%)
       204,778,305      LLC-stores                #  709.274 K/sec                    (25.00%)
     1,506,361,491      LLC-stores                #    5.217 M/sec                    (33.34%)
        288,715.35 msec task-clock                #    8.000 CPUs utilized          
   117,729,045,872      cycles:u                  #    0.408 GHz                      (41.67%)
   529,821,019,277      cycles:k                  #    1.835 GHz                      (41.67%)
    76,153,628,980      instructions:u            #    0.65  insn per cycle           (50.00%)
   461,399,040,927      instructions:k            #    0.87  insn per cycle           (49.99%)

      36.087560682 seconds time elapsed

