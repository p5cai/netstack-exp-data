#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       76.0   605.9    26.8    36.8    39.5    57.6    95.3   113.7   205.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4166765.2 71810.6 4000675.1 3842490.3 3870486.6 4094456.9 4483900.3 4549719.1 4602374.0

Total QPS = 640176.6 (19205301 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4677825277 bytes :  148.7 MB/s
TX  810896007 bytes :   25.8 MB/s
