# started on Tue Apr  4 03:07:30 2023


 Performance counter stats for 'CPU(s) 0-7':

        71,236,107      LLC-load-misses           #   19.51% of all LL-cache accesses  (49.98%)
       142,472,565      LLC-load-misses           #   10.19% of all LL-cache accesses  (49.98%)
       365,178,758      LLC-loads                 #    1.265 M/sec                    (49.99%)
     1,397,570,450      LLC-loads                 #    4.842 M/sec                    (41.68%)
         2,450,592      LLC-store-misses          #    8.490 K/sec                    (25.01%)
        58,874,558      LLC-store-misses          #  203.975 K/sec                    (33.35%)
       144,692,971      LLC-stores                #  501.299 K/sec                    (25.01%)
       757,779,033      LLC-stores                #    2.625 M/sec                    (33.35%)
        288,635.84 msec task-clock                #    8.000 CPUs utilized          
    77,385,496,001      cycles:u                  #    0.268 GHz                      (41.67%)
   571,020,446,442      cycles:k                  #    1.978 GHz                      (41.66%)
    44,570,192,385      instructions:u            #    0.58  insn per cycle           (49.99%)
   711,225,432,028      instructions:k            #    1.25  insn per cycle           (49.98%)

      36.080535789 seconds time elapsed

