# started on Tue Apr  4 02:52:08 2023


 Performance counter stats for 'CPU(s) 0-7':

        82,033,284      LLC-load-misses           #   20.62% of all LL-cache accesses  (49.97%)
       163,256,865      LLC-load-misses           #    9.86% of all LL-cache accesses  (49.98%)
       397,823,334      LLC-loads                 #    1.379 M/sec                    (50.00%)
     1,655,264,785      LLC-loads                 #    5.736 M/sec                    (41.68%)
           586,568      LLC-store-misses          #    2.033 K/sec                    (25.02%)
        63,468,443      LLC-store-misses          #  219.950 K/sec                    (33.36%)
       167,619,331      LLC-stores                #  580.884 K/sec                    (25.01%)
       892,264,402      LLC-stores                #    3.092 M/sec                    (33.34%)
        288,559.14 msec task-clock                #    8.000 CPUs utilized          
    86,003,616,605      cycles:u                  #    0.298 GHz                      (41.66%)
   562,137,668,097      cycles:k                  #    1.948 GHz                      (41.65%)
    51,232,561,033      instructions:u            #    0.60  insn per cycle           (49.98%)
   634,242,408,097      instructions:k            #    1.13  insn per cycle           (49.97%)

      36.069997627 seconds time elapsed

