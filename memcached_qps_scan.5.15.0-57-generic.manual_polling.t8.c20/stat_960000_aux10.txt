# started on Tue Apr  4 01:33:21 2023


 Performance counter stats for 'CPU(s) 0-7':

       134,598,790      LLC-load-misses           #   19.81% of all LL-cache accesses  (49.99%)
       272,814,311      LLC-load-misses           #    9.09% of all LL-cache accesses  (50.00%)
       679,424,255      LLC-loads                 #    2.349 M/sec                    (50.01%)
     3,000,955,175      LLC-loads                 #   10.376 M/sec                    (41.69%)
         2,684,965      LLC-store-misses          #    9.284 K/sec                    (25.01%)
        95,302,209      LLC-store-misses          #  329.516 K/sec                    (33.34%)
       216,429,037      LLC-stores                #  748.324 K/sec                    (24.99%)
     1,645,654,285      LLC-stores                #    5.690 M/sec                    (33.32%)
        289,218.49 msec task-clock                #    8.000 CPUs utilized          
   121,856,116,299      cycles:u                  #    0.421 GHz                      (41.65%)
   526,177,155,618      cycles:k                  #    1.819 GHz                      (41.65%)
    80,363,992,772      instructions:u            #    0.66  insn per cycle           (49.98%)
   465,433,056,998      instructions:k            #    0.88  insn per cycle           (49.98%)

      36.153361278 seconds time elapsed

