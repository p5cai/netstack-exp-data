# started on Tue Apr  4 01:07:27 2023


 Performance counter stats for 'CPU(s) 0-7':

        96,372,691      LLC-load-misses           #   20.50% of all LL-cache accesses  (49.97%)
       190,826,639      LLC-load-misses           #   10.14% of all LL-cache accesses  (49.99%)
       470,074,033      LLC-loads                 #    1.627 M/sec                    (50.00%)
     1,882,651,773      LLC-loads                 #    6.516 M/sec                    (41.68%)
         2,765,554      LLC-store-misses          #    9.572 K/sec                    (25.02%)
        82,293,171      LLC-store-misses          #  284.839 K/sec                    (33.36%)
       179,769,633      LLC-stores                #  622.232 K/sec                    (25.00%)
     1,036,686,654      LLC-stores                #    3.588 M/sec                    (33.33%)
        288,910.82 msec task-clock                #    8.000 CPUs utilized          
    96,979,029,341      cycles:u                  #    0.336 GHz                      (41.66%)
   551,162,007,017      cycles:k                  #    1.908 GHz                      (41.65%)
    58,070,478,658      instructions:u            #    0.60  insn per cycle           (49.97%)
   563,600,667,554      instructions:k            #    1.02  insn per cycle           (49.97%)

      36.114889308 seconds time elapsed

