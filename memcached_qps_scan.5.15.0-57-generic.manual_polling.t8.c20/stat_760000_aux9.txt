# started on Tue Apr  4 01:10:39 2023


 Performance counter stats for 'CPU(s) 0-7':

       110,927,346      LLC-load-misses           #   19.49% of all LL-cache accesses  (49.99%)
       213,611,124      LLC-load-misses           #    9.30% of all LL-cache accesses  (50.00%)
       569,163,121      LLC-loads                 #    1.965 M/sec                    (50.01%)
     2,296,693,559      LLC-loads                 #    7.930 M/sec                    (41.69%)
         2,691,786      LLC-store-misses          #    9.294 K/sec                    (25.01%)
        85,749,621      LLC-store-misses          #  296.064 K/sec                    (33.34%)
       206,626,616      LLC-stores                #  713.411 K/sec                    (24.99%)
     1,266,377,674      LLC-stores                #    4.372 M/sec                    (33.32%)
        289,632.06 msec task-clock                #    8.000 CPUs utilized          
   112,181,495,545      cycles:u                  #    0.387 GHz                      (41.65%)
   538,923,979,016      cycles:k                  #    1.861 GHz                      (41.65%)
    68,206,688,763      instructions:u            #    0.61  insn per cycle           (49.98%)
   483,524,373,644      instructions:k            #    0.90  insn per cycle           (49.98%)

      36.205023868 seconds time elapsed

