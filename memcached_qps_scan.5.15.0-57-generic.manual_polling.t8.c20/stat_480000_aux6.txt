# started on Tue Apr  4 00:10:11 2023


 Performance counter stats for 'CPU(s) 0-7':

        70,645,395      LLC-load-misses           #   20.49% of all LL-cache accesses  (50.00%)
       139,939,345      LLC-load-misses           #    9.94% of all LL-cache accesses  (50.01%)
       344,781,162      LLC-loads                 #    1.194 M/sec                    (50.01%)
     1,408,301,726      LLC-loads                 #    4.876 M/sec                    (41.68%)
         2,415,280      LLC-store-misses          #    8.363 K/sec                    (25.00%)
        56,404,936      LLC-store-misses          #  195.307 K/sec                    (33.33%)
       149,227,037      LLC-stores                #  516.712 K/sec                    (25.00%)
       763,284,534      LLC-stores                #    2.643 M/sec                    (33.33%)
        288,801.39 msec task-clock                #    8.000 CPUs utilized          
    80,182,231,366      cycles:u                  #    0.278 GHz                      (41.66%)
   566,495,864,794      cycles:k                  #    1.962 GHz                      (41.66%)
    44,440,040,791      instructions:u            #    0.55  insn per cycle           (49.99%)
   683,146,944,199      instructions:k            #    1.21  insn per cycle           (49.99%)

      36.101248659 seconds time elapsed

