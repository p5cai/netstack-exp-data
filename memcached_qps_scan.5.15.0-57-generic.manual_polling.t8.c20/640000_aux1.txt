#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       71.8   595.7    26.8    35.8    38.5    55.3    90.3   107.8   169.0
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4175924.6 77640.8 4000675.1 3845953.8 3877413.5 4129091.5 4508974.1 4562256.0 4604881.4

Total QPS = 640047.0 (19201411 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4671389382 bytes :  148.5 MB/s
TX  810695870 bytes :   25.8 MB/s
