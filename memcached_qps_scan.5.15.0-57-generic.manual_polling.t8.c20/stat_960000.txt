# started on Mon Apr  3 22:18:07 2023


 Performance counter stats for 'CPU(s) 0-7':

       138,820,492      LLC-load-misses           #   20.52% of all LL-cache accesses  (49.98%)
       282,151,156      LLC-load-misses           #    8.49% of all LL-cache accesses  (49.98%)
       676,416,791      LLC-loads                 #    2.340 M/sec                    (49.99%)
     3,323,474,322      LLC-loads                 #   11.497 M/sec                    (41.66%)
         3,015,735      LLC-store-misses          #   10.433 K/sec                    (25.01%)
        95,443,536      LLC-store-misses          #  330.177 K/sec                    (33.35%)
       203,106,242      LLC-stores                #  702.626 K/sec                    (25.01%)
     1,732,576,612      LLC-stores                #    5.994 M/sec                    (33.34%)
        289,067.55 msec task-clock                #    8.000 CPUs utilized          
   124,727,660,543      cycles:u                  #    0.431 GHz                      (41.68%)
   523,248,480,722      cycles:k                  #    1.810 GHz                      (41.67%)
    80,687,680,925      instructions:u            #    0.65  insn per cycle           (50.00%)
   465,117,564,823      instructions:k            #    0.89  insn per cycle           (49.99%)

      36.134602882 seconds time elapsed

