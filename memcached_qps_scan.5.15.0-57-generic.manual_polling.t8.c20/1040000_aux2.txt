#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1135.0   435.7    35.7   753.9   977.4  1177.2  1263.8  1298.9  1386.4
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6261833.5 326325.0 5857388.4 5638630.1 5692459.5 6123094.8 6864308.0 7148838.9 7376463.6

Total QPS = 959063.5 (28771907 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 7034853141 bytes :  223.6 MB/s
TX 1214787649 bytes :   38.6 MB/s
