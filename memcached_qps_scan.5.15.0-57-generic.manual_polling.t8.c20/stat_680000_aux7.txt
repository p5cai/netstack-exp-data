# started on Tue Apr  4 00:32:47 2023


 Performance counter stats for 'CPU(s) 0-7':

        99,824,530      LLC-load-misses           #   20.09% of all LL-cache accesses  (49.98%)
       194,197,960      LLC-load-misses           #    9.67% of all LL-cache accesses  (49.99%)
       496,943,299      LLC-loads                 #    1.720 M/sec                    (50.01%)
     2,008,412,193      LLC-loads                 #    6.952 M/sec                    (41.69%)
         2,557,132      LLC-store-misses          #    8.852 K/sec                    (25.02%)
        77,417,448      LLC-store-misses          #  267.986 K/sec                    (33.35%)
       199,706,620      LLC-stores                #  691.298 K/sec                    (24.99%)
     1,111,878,987      LLC-stores                #    3.849 M/sec                    (33.32%)
        288,886.49 msec task-clock                #    8.000 CPUs utilized          
   102,498,273,159      cycles:u                  #    0.355 GHz                      (41.65%)
   550,344,153,951      cycles:k                  #    1.905 GHz                      (41.64%)
    62,270,119,000      instructions:u            #    0.61  insn per cycle           (49.97%)
   546,672,267,345      instructions:k            #    0.99  insn per cycle           (49.97%)

      36.111861939 seconds time elapsed

