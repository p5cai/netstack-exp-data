# started on Mon Apr  3 23:52:26 2023


 Performance counter stats for 'CPU(s) 0-7':

        71,785,589      LLC-load-misses           #   18.93% of all LL-cache accesses  (49.98%)
       140,340,920      LLC-load-misses           #    9.95% of all LL-cache accesses  (49.99%)
       379,116,874      LLC-loads                 #    1.311 M/sec                    (50.01%)
     1,410,743,553      LLC-loads                 #    4.877 M/sec                    (41.69%)
         2,472,390      LLC-store-misses          #    8.546 K/sec                    (25.01%)
        59,992,746      LLC-store-misses          #  207.381 K/sec                    (33.35%)
       151,805,270      LLC-stores                #  524.756 K/sec                    (25.00%)
       758,661,212      LLC-stores                #    2.623 M/sec                    (33.33%)
        289,287.44 msec task-clock                #    8.001 CPUs utilized          
    78,539,253,410      cycles:u                  #    0.271 GHz                      (41.65%)
   577,026,295,408      cycles:k                  #    1.995 GHz                      (41.65%)
    45,554,774,147      instructions:u            #    0.58  insn per cycle           (49.98%)
   710,874,414,432      instructions:k            #    1.23  insn per cycle           (49.97%)

      36.157744958 seconds time elapsed

