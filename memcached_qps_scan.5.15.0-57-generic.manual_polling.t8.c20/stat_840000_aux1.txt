# started on Mon Apr  3 22:50:26 2023


 Performance counter stats for 'CPU(s) 0-7':

       126,098,899      LLC-load-misses           #   20.40% of all LL-cache accesses  (49.97%)
       234,710,930      LLC-load-misses           #    9.02% of all LL-cache accesses  (49.98%)
       618,179,265      LLC-loads                 #    2.137 M/sec                    (50.00%)
     2,602,207,405      LLC-loads                 #    8.994 M/sec                    (41.68%)
         2,981,143      LLC-store-misses          #   10.303 K/sec                    (25.02%)
       107,426,989      LLC-store-misses          #  371.291 K/sec                    (33.36%)
       220,142,942      LLC-stores                #  760.863 K/sec                    (25.01%)
     1,422,826,882      LLC-stores                #    4.918 M/sec                    (33.34%)
        289,333.37 msec task-clock                #    8.000 CPUs utilized          
   116,321,948,331      cycles:u                  #    0.402 GHz                      (41.66%)
   531,209,583,015      cycles:k                  #    1.836 GHz                      (41.65%)
    73,515,185,646      instructions:u            #    0.63  insn per cycle           (49.98%)
   462,746,740,974      instructions:k            #    0.87  insn per cycle           (49.97%)

      36.167701890 seconds time elapsed

