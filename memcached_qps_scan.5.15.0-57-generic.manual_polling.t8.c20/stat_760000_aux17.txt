# started on Tue Apr  4 03:32:23 2023


 Performance counter stats for 'CPU(s) 0-7':

       111,552,255      LLC-load-misses           #   20.08% of all LL-cache accesses  (49.98%)
       213,826,626      LLC-load-misses           #    9.31% of all LL-cache accesses  (49.99%)
       555,543,653      LLC-loads                 #    1.925 M/sec                    (50.00%)
     2,296,009,611      LLC-loads                 #    7.958 M/sec                    (41.69%)
         2,665,852      LLC-store-misses          #    9.239 K/sec                    (25.01%)
        87,128,722      LLC-store-misses          #  301.973 K/sec                    (33.35%)
       208,946,969      LLC-stores                #  724.174 K/sec                    (25.00%)
     1,261,847,785      LLC-stores                #    4.373 M/sec                    (33.33%)
        288,531.29 msec task-clock                #    8.001 CPUs utilized          
   111,469,154,166      cycles:u                  #    0.386 GHz                      (41.66%)
   544,306,994,371      cycles:k                  #    1.886 GHz                      (41.65%)
    69,351,137,162      instructions:u            #    0.62  insn per cycle           (49.98%)
   495,516,993,214      instructions:k            #    0.91  insn per cycle           (49.98%)

      36.061684615 seconds time elapsed

