# started on Mon Apr  3 22:10:54 2023


 Performance counter stats for 'CPU(s) 0-7':

       100,133,922      LLC-load-misses           #   19.86% of all LL-cache accesses  (49.99%)
       191,157,298      LLC-load-misses           #    9.27% of all LL-cache accesses  (49.99%)
       504,318,473      LLC-loads                 #    1.747 M/sec                    (50.00%)
     2,061,827,928      LLC-loads                 #    7.141 M/sec                    (41.66%)
         2,619,236      LLC-store-misses          #    9.072 K/sec                    (25.01%)
        77,403,346      LLC-store-misses          #  268.081 K/sec                    (33.34%)
       192,992,666      LLC-stores                #  668.416 K/sec                    (25.00%)
     1,114,234,699      LLC-stores                #    3.859 M/sec                    (33.34%)
        288,731.46 msec task-clock                #    8.000 CPUs utilized          
   102,972,422,142      cycles:u                  #    0.357 GHz                      (41.67%)
   547,982,045,071      cycles:k                  #    1.898 GHz                      (41.67%)
    62,028,813,660      instructions:u            #    0.60  insn per cycle           (50.00%)
   534,959,907,321      instructions:k            #    0.98  insn per cycle           (50.00%)

      36.092572309 seconds time elapsed

