#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       79.9   620.3    26.8    36.8    39.7    58.9   100.7   120.0   629.4
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4451609.0 70123.8 4400742.6 4217324.0 4238704.6 4409749.5 4580794.3 4602174.9 4830929.5

Total QPS = 679807.4 (20394234 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4988275214 bytes :  158.6 MB/s
TX  861114308 bytes :   27.4 MB/s
