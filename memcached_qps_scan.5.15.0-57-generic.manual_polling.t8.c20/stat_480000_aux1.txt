# started on Mon Apr  3 22:41:30 2023


 Performance counter stats for 'CPU(s) 0-7':

        70,509,276      LLC-load-misses           #   19.96% of all LL-cache accesses  (49.98%)
       140,575,585      LLC-load-misses           #    9.89% of all LL-cache accesses  (49.98%)
       353,329,147      LLC-loads                 #    1.221 M/sec                    (49.99%)
     1,421,420,026      LLC-loads                 #    4.911 M/sec                    (41.67%)
         2,460,899      LLC-store-misses          #    8.502 K/sec                    (25.01%)
        56,667,758      LLC-store-misses          #  195.783 K/sec                    (33.35%)
       145,335,031      LLC-stores                #  502.121 K/sec                    (25.01%)
       766,414,157      LLC-stores                #    2.648 M/sec                    (33.34%)
        289,442.24 msec task-clock                #    8.000 CPUs utilized          
    77,565,514,946      cycles:u                  #    0.268 GHz                      (41.68%)
   570,958,731,911      cycles:k                  #    1.973 GHz                      (41.67%)
    44,611,368,624      instructions:u            #    0.58  insn per cycle           (50.00%)
   698,722,251,792      instructions:k            #    1.22  insn per cycle           (49.98%)

      36.181402416 seconds time elapsed

