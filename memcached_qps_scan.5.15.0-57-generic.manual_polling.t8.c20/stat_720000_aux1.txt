# started on Mon Apr  3 22:47:53 2023


 Performance counter stats for 'CPU(s) 0-7':

       105,844,344      LLC-load-misses           #   19.95% of all LL-cache accesses  (49.97%)
       203,649,880      LLC-load-misses           #    9.34% of all LL-cache accesses  (49.98%)
       530,667,303      LLC-loads                 #    1.837 M/sec                    (50.00%)
     2,181,029,889      LLC-loads                 #    7.548 M/sec                    (41.68%)
           697,950      LLC-store-misses          #    2.416 K/sec                    (25.02%)
        83,722,639      LLC-store-misses          #  289.759 K/sec                    (33.36%)
       204,592,232      LLC-stores                #  708.081 K/sec                    (25.01%)
     1,182,809,189      LLC-stores                #    4.094 M/sec                    (33.34%)
        288,939.17 msec task-clock                #    8.000 CPUs utilized          
   104,803,226,117      cycles:u                  #    0.363 GHz                      (41.66%)
   543,950,764,316      cycles:k                  #    1.883 GHz                      (41.65%)
    64,217,389,022      instructions:u            #    0.61  insn per cycle           (49.98%)
   515,302,183,569      instructions:k            #    0.95  insn per cycle           (49.97%)

      36.116012846 seconds time elapsed

