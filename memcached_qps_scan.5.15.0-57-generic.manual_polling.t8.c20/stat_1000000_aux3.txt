# started on Mon Apr  3 23:29:56 2023


 Performance counter stats for 'CPU(s) 0-7':

       134,177,247      LLC-load-misses           #   19.26% of all LL-cache accesses  (49.98%)
       278,537,369      LLC-load-misses           #    9.15% of all LL-cache accesses  (49.99%)
       696,538,327      LLC-loads                 #    2.413 M/sec                    (49.99%)
     3,043,718,361      LLC-loads                 #   10.544 M/sec                    (41.67%)
         2,520,201      LLC-store-misses          #    8.731 K/sec                    (25.01%)
        70,721,682      LLC-store-misses          #  245.001 K/sec                    (33.35%)
       202,228,089      LLC-stores                #  700.578 K/sec                    (25.01%)
     1,694,872,603      LLC-stores                #    5.872 M/sec                    (33.34%)
        288,658.72 msec task-clock                #    8.001 CPUs utilized          
   127,339,712,423      cycles:u                  #    0.441 GHz                      (41.67%)
   525,260,819,518      cycles:k                  #    1.820 GHz                      (41.66%)
    81,726,200,712      instructions:u            #    0.64  insn per cycle           (49.99%)
   471,324,436,112      instructions:k            #    0.90  insn per cycle           (49.98%)

      36.075648836 seconds time elapsed

