# started on Mon Apr  3 23:20:59 2023


 Performance counter stats for 'CPU(s) 0-7':

        94,288,707      LLC-load-misses           #   19.19% of all LL-cache accesses  (50.00%)
       183,246,105      LLC-load-misses           #    9.52% of all LL-cache accesses  (50.00%)
       491,451,169      LLC-loads                 #    1.704 M/sec                    (50.00%)
     1,925,232,499      LLC-loads                 #    6.676 M/sec                    (41.67%)
         2,666,611      LLC-store-misses          #    9.247 K/sec                    (25.00%)
        71,831,205      LLC-store-misses          #  249.083 K/sec                    (33.34%)
       185,536,930      LLC-stores                #  643.371 K/sec                    (25.00%)
     1,044,600,791      LLC-stores                #    3.622 M/sec                    (33.33%)
        288,382.33 msec task-clock                #    8.000 CPUs utilized          
    99,374,001,591      cycles:u                  #    0.345 GHz                      (41.66%)
   548,780,535,556      cycles:k                  #    1.903 GHz                      (41.66%)
    57,961,268,634      instructions:u            #    0.58  insn per cycle           (49.99%)
   551,476,884,919      instructions:k            #    1.00  insn per cycle           (49.99%)

      36.048935723 seconds time elapsed

