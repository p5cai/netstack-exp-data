# started on Tue Apr  4 01:46:12 2023


 Performance counter stats for 'CPU(s) 0-7':

       112,995,372      LLC-load-misses           #   20.17% of all LL-cache accesses  (49.99%)
       216,092,578      LLC-load-misses           #    9.30% of all LL-cache accesses  (49.99%)
       560,278,201      LLC-loads                 #    1.941 M/sec                    (49.99%)
     2,324,007,959      LLC-loads                 #    8.050 M/sec                    (41.66%)
         2,976,859      LLC-store-misses          #   10.311 K/sec                    (25.01%)
        86,870,113      LLC-store-misses          #  300.897 K/sec                    (33.34%)
       195,975,591      LLC-stores                #  678.812 K/sec                    (25.01%)
     1,272,390,014      LLC-stores                #    4.407 M/sec                    (33.34%)
        288,703.57 msec task-clock                #    8.000 CPUs utilized          
   114,568,478,545      cycles:u                  #    0.397 GHz                      (41.67%)
   539,291,870,783      cycles:k                  #    1.868 GHz                      (41.67%)
    68,516,482,461      instructions:u            #    0.60  insn per cycle           (50.00%)
   473,510,928,826      instructions:k            #    0.88  insn per cycle           (49.99%)

      36.088999292 seconds time elapsed

