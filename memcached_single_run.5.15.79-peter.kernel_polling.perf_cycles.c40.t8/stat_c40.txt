# started on Mon Mar 27 16:59:49 2023

#           time             counts unit events
     5.002609701          6,261,115      LLC-load-misses           #   25.94% of all LL-cache accesses  (46.18%)
     5.002609701         16,783,156      LLC-load-misses           #   14.99% of all LL-cache accesses  (46.24%)
     5.002609701         24,141,323      LLC-loads                 #  603.179 K/sec                    (46.32%)
     5.002609701        111,969,789      LLC-loads                 #    2.798 M/sec                    (38.65%)
     5.002609701            190,302      LLC-store-misses          #    4.755 K/sec                    (23.27%)
     5.002609701          7,105,657      LLC-store-misses          #  177.537 K/sec                    (31.03%)
     5.002609701          7,096,974      LLC-stores                #  177.320 K/sec                    (23.27%)
     5.002609701         54,962,310      LLC-stores                #    1.373 M/sec                    (31.03%)
     5.002609701          40,023.49 msec task-clock                #    8.005 CPUs utilized          
     5.002609701      4,303,983,532      cycles:u                  #    0.108 GHz                      (38.73%)
     5.002609701     21,874,667,166      cycles:k                  #    0.547 GHz                      (38.64%)
     5.002609701      2,708,152,045      instructions:u            #    0.63  insn per cycle           (46.31%)
     5.002609701     19,061,098,181      instructions:k            #    0.87  insn per cycle           (46.23%)
    10.006621573         28,850,445      LLC-load-misses           #   25.73% of all LL-cache accesses  (46.28%)
    10.006621573         71,689,345      LLC-load-misses           #   14.44% of all LL-cache accesses  (46.23%)
    10.006621573        112,126,284      LLC-loads                 #    2.801 M/sec                    (46.15%)
    10.006621573        496,564,463      LLC-loads                 #   12.404 M/sec                    (38.37%)
    10.006621573            817,122      LLC-store-misses          #   20.412 K/sec                    (23.02%)
    10.006621573         24,298,028      LLC-store-misses          #  606.976 K/sec                    (30.70%)
    10.006621573         33,207,199      LLC-stores                #  829.531 K/sec                    (23.02%)
    10.006621573        244,426,982      LLC-stores                #    6.106 M/sec                    (30.70%)
    10.006621573          40,031.31 msec task-clock                #    8.006 CPUs utilized          
    10.006621573     19,924,362,818      cycles:u                  #    0.498 GHz                      (38.42%)
    10.006621573     87,268,727,682      cycles:k                  #    2.180 GHz                      (38.51%)
    10.006621573     12,804,774,605      instructions:u            #    0.64  insn per cycle           (46.28%)
    10.006621573     71,719,676,631      instructions:k            #    0.82  insn per cycle           (46.28%)
    15.010611491         28,940,660      LLC-load-misses           #   25.91% of all LL-cache accesses  (46.25%)
    15.010611491         72,597,539      LLC-load-misses           #   14.72% of all LL-cache accesses  (46.28%)
    15.010611491        111,691,656      LLC-loads                 #    2.790 M/sec                    (46.28%)
    15.010611491        493,039,303      LLC-loads                 #   12.317 M/sec                    (38.56%)
    15.010611491            831,917      LLC-store-misses          #   20.782 K/sec                    (23.02%)
    15.010611491         25,158,357      LLC-store-misses          #  628.479 K/sec                    (30.70%)
    15.010611491         33,464,850      LLC-stores                #  835.983 K/sec                    (23.02%)
    15.010611491        244,224,976      LLC-stores                #    6.101 M/sec                    (30.70%)
    15.010611491          40,030.53 msec task-clock                #    8.006 CPUs utilized          
    15.010611491     19,932,878,437      cycles:u                  #    0.498 GHz                      (38.37%)
    15.010611491     87,272,369,280      cycles:k                  #    2.180 GHz                      (38.37%)
    15.010611491     12,772,747,699      instructions:u            #    0.64  insn per cycle           (46.09%)
    15.010611491     71,679,906,218      instructions:k            #    0.82  insn per cycle           (46.17%)
    20.014622956         28,672,601      LLC-load-misses           #   25.66% of all LL-cache accesses  (46.04%)
    20.014622956         71,610,476      LLC-load-misses           #   14.51% of all LL-cache accesses  (46.09%)
    20.014622956        111,748,529      LLC-loads                 #    2.792 M/sec                    (46.17%)
    20.014622956        493,433,159      LLC-loads                 #   12.326 M/sec                    (38.58%)
    20.014622956            806,682      LLC-store-misses          #   20.151 K/sec                    (23.22%)
    20.014622956         24,788,321      LLC-store-misses          #  619.223 K/sec                    (30.89%)
    20.014622956         33,502,608      LLC-stores                #  836.910 K/sec                    (23.05%)
    20.014622956        244,101,816      LLC-stores                #    6.098 M/sec                    (30.73%)
    20.014622956          40,031.33 msec task-clock                #    8.006 CPUs utilized          
    20.014622956     19,915,102,356      cycles:u                  #    0.497 GHz                      (38.40%)
    20.014622956     87,268,185,011      cycles:k                  #    2.180 GHz                      (38.37%)
    20.014622956     12,783,288,391      instructions:u            #    0.64  insn per cycle           (46.04%)
    20.014622956     71,819,833,555      instructions:k            #    0.82  insn per cycle           (46.04%)
    25.018620186         28,769,729      LLC-load-misses           #   25.23% of all LL-cache accesses  (46.04%)
    25.018620186         72,346,354      LLC-load-misses           #   14.64% of all LL-cache accesses  (46.04%)
    25.018620186        114,023,955      LLC-loads                 #    2.848 M/sec                    (46.04%)
    25.018620186        494,105,073      LLC-loads                 #   12.343 M/sec                    (38.37%)
    25.018620186          1,386,785      LLC-store-misses          #   34.643 K/sec                    (23.07%)
    25.018620186         25,196,173      LLC-store-misses          #  629.422 K/sec                    (30.83%)
    25.018620186         35,343,320      LLC-stores                #  882.907 K/sec                    (23.23%)
    25.018620186        243,008,760      LLC-stores                #    6.071 M/sec                    (30.93%)
    25.018620186          40,030.62 msec task-clock                #    8.006 CPUs utilized          
    25.018620186     20,100,689,467      cycles:u                  #    0.502 GHz                      (38.61%)
    25.018620186     87,088,228,894      cycles:k                  #    2.176 GHz                      (38.55%)
    25.018620186     12,736,313,002      instructions:u            #    0.63  insn per cycle           (46.15%)
    25.018620186     71,349,523,024      instructions:k            #    0.82  insn per cycle           (46.07%)
    30.022642881         29,130,795      LLC-load-misses           #   24.83% of all LL-cache accesses  (46.23%)
    30.022642881         72,481,300      LLC-load-misses           #   14.71% of all LL-cache accesses  (46.15%)
    30.022642881        117,328,213      LLC-loads                 #    2.931 M/sec                    (46.07%)
    30.022642881        492,872,039      LLC-loads                 #   12.312 M/sec                    (38.37%)
    30.022642881          2,124,271      LLC-store-misses          #   53.066 K/sec                    (23.03%)
    30.022642881         24,851,993      LLC-store-misses          #  620.823 K/sec                    (30.70%)
    30.022642881         37,925,464      LLC-stores                #  947.408 K/sec                    (23.02%)
    30.022642881        242,295,294      LLC-stores                #    6.053 M/sec                    (30.74%)
    30.022642881          40,030.75 msec task-clock                #    8.006 CPUs utilized          
    30.022642881     20,408,474,760      cycles:u                  #    0.510 GHz                      (38.50%)
    30.022642881     86,768,071,965      cycles:k                  #    2.168 GHz                      (38.58%)
    30.022642881     12,764,710,068      instructions:u            #    0.63  insn per cycle           (46.28%)
    30.022642881     71,039,881,502      instructions:k            #    0.82  insn per cycle           (46.28%)
    35.026679649         22,391,797      LLC-load-misses           #   25.49% of all LL-cache accesses  (46.28%)
    35.026679649         55,330,616      LLC-load-misses           #   14.32% of all LL-cache accesses  (46.28%)
    35.026679649         87,834,469      LLC-loads                 #    2.194 M/sec                    (46.28%)
    35.026679649        386,461,136      LLC-loads                 #    9.654 M/sec                    (38.49%)
    35.026679649            751,686      LLC-store-misses          #   18.777 K/sec                    (23.03%)
    35.026679649         18,426,210      LLC-store-misses          #  460.289 K/sec                    (30.70%)
    35.026679649         26,615,130      LLC-stores                #  664.850 K/sec                    (23.02%)
    35.026679649        192,189,527      LLC-stores                #    4.801 M/sec                    (30.70%)
    35.026679649          40,031.80 msec task-clock                #    8.006 CPUs utilized          
    35.026679649     15,680,837,451      cycles:u                  #    0.392 GHz                      (38.37%)
    35.026679649     68,480,162,690      cycles:k                  #    1.711 GHz                      (38.37%)
    35.026679649      9,995,248,633      instructions:u            #    0.64  insn per cycle           (46.17%)
    35.026679649     56,713,824,167      instructions:k            #    0.83  insn per cycle           (46.25%)
    36.135675185             34,216      LLC-load-misses           #   91.85% of all LL-cache accesses  (45.65%)
    36.135675185            259,487      LLC-load-misses           #   24.07% of all LL-cache accesses  (46.02%)
    36.135675185             37,253      LLC-loads                 #    4.199 K/sec                    (46.39%)
    36.135675185          1,078,264      LLC-loads                 #  121.543 K/sec                    (39.18%)
    36.135675185                  0      LLC-store-misses          #    0.000 /sec                     (23.68%)
    36.135675185            349,389      LLC-store-misses          #   39.383 K/sec                    (31.26%)
    36.135675185                587      LLC-stores                #   66.167 /sec                     (22.96%)
    36.135675185            534,160      LLC-stores                #   60.211 K/sec                    (30.54%)
    36.135675185           8,871.47 msec task-clock                #    1.774 CPUs utilized          
    36.135675185            254,100      cycles:u                  #    0.000 GHz                      (38.11%)
    36.135675185        268,081,469      cycles:k                  #    0.030 GHz                      (37.87%)
    36.135675185              3,239      instructions:u            #    0.01  insn per cycle           (45.44%)
    36.135675185         26,206,956      instructions:k            #    0.10  insn per cycle           (45.43%)
