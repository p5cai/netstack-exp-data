# started on Fri Mar 31 13:32:13 2023


 Performance counter stats for 'CPU(s) 0-7':

        98,295,480      LLC-load-misses           #   16.70% of all LL-cache accesses  (49.98%)
       175,017,013      LLC-load-misses           #    4.91% of all LL-cache accesses  (49.99%)
       588,759,301      LLC-loads                 #    2.044 M/sec                    (50.01%)
     3,564,096,311      LLC-loads                 #   12.371 M/sec                    (41.69%)
         2,401,055      LLC-store-misses          #    8.334 K/sec                    (25.02%)
        82,586,356      LLC-store-misses          #  286.666 K/sec                    (33.35%)
       218,366,376      LLC-stores                #  757.972 K/sec                    (24.99%)
     1,868,560,470      LLC-stores                #    6.486 M/sec                    (33.32%)
        288,092.86 msec task-clock                #    8.000 CPUs utilized          
   101,013,668,600      cycles:u                  #    0.351 GHz                      (41.65%)
   544,222,106,889      cycles:k                  #    1.889 GHz                      (41.65%)
    60,468,248,821      instructions:u            #    0.60  insn per cycle           (49.98%)
   349,734,050,293      instructions:k            #    0.64  insn per cycle           (49.98%)

      36.012595852 seconds time elapsed

