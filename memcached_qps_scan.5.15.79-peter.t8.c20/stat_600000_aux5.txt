# started on Fri Mar 31 12:26:36 2023


 Performance counter stats for 'CPU(s) 0-7':

        86,337,992      LLC-load-misses           #   16.07% of all LL-cache accesses  (49.97%)
       156,395,405      LLC-load-misses           #    4.75% of all LL-cache accesses  (49.98%)
       537,115,557      LLC-loads                 #    1.856 M/sec                    (50.00%)
     3,290,962,205      LLC-loads                 #   11.374 M/sec                    (41.68%)
         2,412,649      LLC-store-misses          #    8.338 K/sec                    (25.02%)
        68,530,108      LLC-store-misses          #  236.849 K/sec                    (33.36%)
       191,584,274      LLC-stores                #  662.139 K/sec                    (25.01%)
     1,739,539,452      LLC-stores                #    6.012 M/sec                    (33.34%)
        289,341.29 msec task-clock                #    8.000 CPUs utilized          
    96,620,285,324      cycles:u                  #    0.334 GHz                      (41.67%)
   529,260,769,923      cycles:k                  #    1.829 GHz                      (41.65%)
    54,650,849,199      instructions:u            #    0.57  insn per cycle           (49.98%)
   328,867,401,901      instructions:k            #    0.62  insn per cycle           (49.97%)

      36.168940144 seconds time elapsed

