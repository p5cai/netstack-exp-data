# started on Fri Mar 31 13:48:11 2023


 Performance counter stats for 'CPU(s) 0-7':

        69,349,420      LLC-load-misses           #   15.16% of all LL-cache accesses  (49.99%)
       129,098,997      LLC-load-misses           #    4.42% of all LL-cache accesses  (49.99%)
       457,429,138      LLC-loads                 #    1.584 M/sec                    (49.99%)
     2,924,017,785      LLC-loads                 #   10.128 M/sec                    (41.66%)
         2,350,701      LLC-store-misses          #    8.142 K/sec                    (25.01%)
        54,086,631      LLC-store-misses          #  187.339 K/sec                    (33.34%)
       175,487,548      LLC-stores                #  607.832 K/sec                    (25.00%)
     1,484,074,069      LLC-stores                #    5.140 M/sec                    (33.34%)
        288,710.52 msec task-clock                #    8.000 CPUs utilized          
    88,491,370,221      cycles:u                  #    0.307 GHz                      (41.67%)
   492,286,957,370      cycles:k                  #    1.705 GHz                      (41.67%)
    46,119,315,244      instructions:u            #    0.52  insn per cycle           (50.00%)
   291,088,336,671      instructions:k            #    0.59  insn per cycle           (49.99%)

      36.089949094 seconds time elapsed

