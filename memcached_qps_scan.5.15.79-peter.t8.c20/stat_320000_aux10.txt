# started on Fri Mar 31 13:16:03 2023


 Performance counter stats for 'CPU(s) 0-7':

        46,300,283      LLC-load-misses           #   13.17% of all LL-cache accesses  (49.98%)
        91,995,756      LLC-load-misses           #    4.06% of all LL-cache accesses  (49.98%)
       351,679,991      LLC-loads                 #    1.217 M/sec                    (49.98%)
     2,264,774,022      LLC-loads                 #    7.835 M/sec                    (41.66%)
         2,294,853      LLC-store-misses          #    7.939 K/sec                    (25.01%)
        37,726,694      LLC-store-misses          #  130.522 K/sec                    (33.35%)
       132,633,765      LLC-stores                #  458.870 K/sec                    (25.01%)
     1,091,914,722      LLC-stores                #    3.778 M/sec                    (33.34%)
        289,044.42 msec task-clock                #    8.000 CPUs utilized          
    67,760,963,727      cycles:u                  #    0.234 GHz                      (41.68%)
   409,207,445,972      cycles:k                  #    1.416 GHz                      (41.68%)
    30,141,759,519      instructions:u            #    0.44  insn per cycle           (50.00%)
   230,946,925,223      instructions:k            #    0.56  insn per cycle           (49.99%)

      36.131754862 seconds time elapsed

