# started on Fri Mar 31 13:41:47 2023


 Performance counter stats for 'CPU(s) 0-7':

        92,363,898      LLC-load-misses           #   16.54% of all LL-cache accesses  (50.00%)
       164,462,369      LLC-load-misses           #    4.85% of all LL-cache accesses  (50.00%)
       558,507,929      LLC-loads                 #    1.934 M/sec                    (50.00%)
     3,394,210,705      LLC-loads                 #   11.754 M/sec                    (41.67%)
         2,523,578      LLC-store-misses          #    8.739 K/sec                    (25.00%)
        73,625,672      LLC-store-misses          #  254.967 K/sec                    (33.34%)
       212,000,241      LLC-stores                #  734.162 K/sec                    (25.00%)
     1,807,535,708      LLC-stores                #    6.260 M/sec                    (33.33%)
        288,765.01 msec task-clock                #    8.000 CPUs utilized          
   100,267,778,907      cycles:u                  #    0.347 GHz                      (41.66%)
   536,434,893,190      cycles:k                  #    1.858 GHz                      (41.66%)
    58,518,557,629      instructions:u            #    0.58  insn per cycle           (49.99%)
   338,931,901,585      instructions:k            #    0.63  insn per cycle           (49.99%)

      36.096802252 seconds time elapsed

