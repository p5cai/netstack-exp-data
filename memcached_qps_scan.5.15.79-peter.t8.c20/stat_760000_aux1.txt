# started on Wed Mar 29 12:15:46 2023


 Performance counter stats for 'CPU(s) 0-7':

        97,225,857      LLC-load-misses           #   17.53% of all LL-cache accesses  (49.97%)
       183,775,886      LLC-load-misses           #    5.34% of all LL-cache accesses  (49.98%)
       554,523,933      LLC-loads                 #    1.914 M/sec                    (50.00%)
     3,441,698,950      LLC-loads                 #   11.880 M/sec                    (41.68%)
         2,249,457      LLC-store-misses          #    7.764 K/sec                    (25.02%)
        38,621,985      LLC-store-misses          #  133.310 K/sec                    (33.36%)
       234,863,645      LLC-stores                #  810.670 K/sec                    (25.01%)
     1,832,428,260      LLC-stores                #    6.325 M/sec                    (33.34%)
        289,715.45 msec task-clock                #    8.000 CPUs utilized          
   100,192,271,696      cycles:u                  #    0.346 GHz                      (41.67%)
   547,413,378,100      cycles:k                  #    1.889 GHz                      (41.65%)
    61,683,163,977      instructions:u            #    0.62  insn per cycle           (49.98%)
   352,778,728,776      instructions:k            #    0.64  insn per cycle           (49.97%)

      36.215654846 seconds time elapsed

