# started on Fri Mar 31 12:37:01 2023


 Performance counter stats for 'CPU(s) 0-7':

        86,431,226      LLC-load-misses           #   15.90% of all LL-cache accesses  (49.97%)
       153,726,415      LLC-load-misses           #    4.69% of all LL-cache accesses  (49.97%)
       543,750,356      LLC-loads                 #    1.882 M/sec                    (49.98%)
     3,278,674,855      LLC-loads                 #   11.345 M/sec                    (41.67%)
         2,410,066      LLC-store-misses          #    8.340 K/sec                    (25.02%)
        67,660,061      LLC-store-misses          #  234.124 K/sec                    (33.35%)
       204,003,259      LLC-stores                #  705.913 K/sec                    (25.01%)
     1,734,849,244      LLC-stores                #    6.003 M/sec                    (33.35%)
        288,992.02 msec task-clock                #    8.000 CPUs utilized          
    97,120,779,269      cycles:u                  #    0.336 GHz                      (41.68%)
   530,347,582,120      cycles:k                  #    1.835 GHz                      (41.66%)
    54,606,667,990      instructions:u            #    0.56  insn per cycle           (49.99%)
   327,507,270,522      instructions:k            #    0.62  insn per cycle           (49.98%)

      36.125194694 seconds time elapsed

