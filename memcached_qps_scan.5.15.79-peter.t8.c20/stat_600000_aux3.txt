# started on Wed Mar 29 12:32:38 2023


 Performance counter stats for 'CPU(s) 0-7':

        86,983,565      LLC-load-misses           #   15.96% of all LL-cache accesses  (49.98%)
       157,722,123      LLC-load-misses           #    4.83% of all LL-cache accesses  (49.98%)
       544,919,795      LLC-loads                 #    1.890 M/sec                    (49.98%)
     3,266,045,230      LLC-loads                 #   11.330 M/sec                    (41.67%)
         2,493,753      LLC-store-misses          #    8.651 K/sec                    (25.01%)
        64,362,753      LLC-store-misses          #  223.274 K/sec                    (33.35%)
       212,436,899      LLC-stores                #  736.942 K/sec                    (25.01%)
     1,751,715,896      LLC-stores                #    6.077 M/sec                    (33.35%)
        288,268.27 msec task-clock                #    8.000 CPUs utilized          
    98,365,901,744      cycles:u                  #    0.341 GHz                      (41.68%)
   529,004,404,346      cycles:k                  #    1.835 GHz                      (41.67%)
    54,374,823,397      instructions:u            #    0.55  insn per cycle           (50.00%)
   326,374,947,268      instructions:k            #    0.62  insn per cycle           (49.99%)

      36.034890856 seconds time elapsed

