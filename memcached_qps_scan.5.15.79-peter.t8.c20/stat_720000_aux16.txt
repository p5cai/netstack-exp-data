# started on Fri Mar 31 14:25:47 2023


 Performance counter stats for 'CPU(s) 0-7':

       100,411,835      LLC-load-misses           #   17.14% of all LL-cache accesses  (49.98%)
       184,485,316      LLC-load-misses           #    5.18% of all LL-cache accesses  (49.98%)
       585,780,139      LLC-loads                 #    2.024 M/sec                    (49.98%)
     3,560,577,451      LLC-loads                 #   12.302 M/sec                    (41.67%)
           366,658      LLC-store-misses          #    1.267 K/sec                    (25.01%)
        65,797,644      LLC-store-misses          #  227.331 K/sec                    (33.35%)
       223,923,683      LLC-stores                #  773.657 K/sec                    (25.01%)
     1,880,260,850      LLC-stores                #    6.496 M/sec                    (33.34%)
        289,435.29 msec task-clock                #    8.000 CPUs utilized          
    99,289,986,295      cycles:u                  #    0.343 GHz                      (41.68%)
   547,509,756,245      cycles:k                  #    1.892 GHz                      (41.67%)
    62,585,032,020      instructions:u            #    0.63  insn per cycle           (50.00%)
   358,601,768,151      instructions:k            #    0.65  insn per cycle           (49.99%)

      36.180520278 seconds time elapsed

