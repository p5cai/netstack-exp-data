#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       48.7     9.9    29.5    38.7    40.5    47.7    57.8    60.3    70.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      521101.0 29137.3 335678.5 472199.8 477967.1 523091.1 563818.4 582463.9 615453.0

Total QPS = 79961.5 (2398844 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX  588626063 bytes :   18.7 MB/s
TX  101268162 bytes :    3.2 MB/s
