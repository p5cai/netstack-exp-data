#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1542.2   207.2   180.3  1407.7  1417.7  1497.1  1647.6  1676.4  3027.4
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4746447.4 110323.9 4400742.6 4363781.2 4531618.9 4813346.5 5024342.6 5050717.1 5071816.7

Total QPS = 727007.7 (21810235 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 5328727682 bytes :  169.4 MB/s
TX  920929683 bytes :   29.3 MB/s
