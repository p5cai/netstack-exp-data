# started on Fri Mar 31 13:44:57 2023


 Performance counter stats for 'CPU(s) 0-7':

       100,791,255      LLC-load-misses           #   17.14% of all LL-cache accesses  (50.00%)
       183,638,731      LLC-load-misses           #    5.22% of all LL-cache accesses  (50.00%)
       588,052,446      LLC-loads                 #    2.031 M/sec                    (50.00%)
     3,520,083,121      LLC-loads                 #   12.158 M/sec                    (41.67%)
         2,209,604      LLC-store-misses          #    7.632 K/sec                    (25.00%)
        39,559,064      LLC-store-misses          #  136.633 K/sec                    (33.34%)
       223,943,925      LLC-stores                #  773.479 K/sec                    (25.00%)
     1,876,200,543      LLC-stores                #    6.480 M/sec                    (33.33%)
        289,528.15 msec task-clock                #    8.000 CPUs utilized          
    99,845,650,296      cycles:u                  #    0.345 GHz                      (41.67%)
   548,174,417,274      cycles:k                  #    1.893 GHz                      (41.66%)
    63,916,990,236      instructions:u            #    0.64  insn per cycle           (49.99%)
   362,903,715,721      instructions:k            #    0.66  insn per cycle           (50.00%)

      36.191250295 seconds time elapsed

