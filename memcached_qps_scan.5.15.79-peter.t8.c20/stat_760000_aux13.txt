# started on Fri Mar 31 13:55:23 2023


 Performance counter stats for 'CPU(s) 0-7':

       100,550,379      LLC-load-misses           #   17.34% of all LL-cache accesses  (49.99%)
       180,939,212      LLC-load-misses           #    5.18% of all LL-cache accesses  (49.99%)
       579,724,111      LLC-loads                 #    2.013 M/sec                    (50.00%)
     3,494,054,032      LLC-loads                 #   12.133 M/sec                    (41.67%)
         2,143,150      LLC-store-misses          #    7.442 K/sec                    (25.01%)
        36,605,911      LLC-store-misses          #  127.117 K/sec                    (33.34%)
       229,003,491      LLC-stores                #  795.234 K/sec                    (25.00%)
     1,862,762,367      LLC-stores                #    6.469 M/sec                    (33.33%)
        287,969.95 msec task-clock                #    8.000 CPUs utilized          
    98,682,936,871      cycles:u                  #    0.343 GHz                      (41.67%)
   548,863,075,929      cycles:k                  #    1.906 GHz                      (41.67%)
    63,090,473,308      instructions:u            #    0.64  insn per cycle           (50.00%)
   363,329,622,129      instructions:k            #    0.66  insn per cycle           (50.00%)

      35.997234026 seconds time elapsed

