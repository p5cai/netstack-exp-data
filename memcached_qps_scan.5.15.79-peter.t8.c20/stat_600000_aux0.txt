# started on Wed Mar 29 12:01:19 2023


 Performance counter stats for 'CPU(s) 0-7':

        85,811,269      LLC-load-misses           #   15.44% of all LL-cache accesses  (50.00%)
       158,643,641      LLC-load-misses           #    4.84% of all LL-cache accesses  (50.00%)
       555,726,050      LLC-loads                 #    1.922 M/sec                    (50.00%)
     3,276,267,808      LLC-loads                 #   11.331 M/sec                    (41.67%)
         2,473,608      LLC-store-misses          #    8.555 K/sec                    (25.00%)
        67,571,188      LLC-store-misses          #  233.692 K/sec                    (33.34%)
       204,867,313      LLC-stores                #  708.523 K/sec                    (25.00%)
     1,737,135,405      LLC-stores                #    6.008 M/sec                    (33.33%)
        289,146.92 msec task-clock                #    8.000 CPUs utilized          
    98,986,676,813      cycles:u                  #    0.342 GHz                      (41.66%)
   529,877,489,453      cycles:k                  #    1.833 GHz                      (41.66%)
    53,644,702,927      instructions:u            #    0.54  insn per cycle           (49.99%)
   326,123,954,803      instructions:k            #    0.62  insn per cycle           (50.00%)

      36.144604564 seconds time elapsed

