# started on Fri Mar 31 13:20:05 2023


 Performance counter stats for 'CPU(s) 0-7':

        86,396,463      LLC-load-misses           #   16.03% of all LL-cache accesses  (49.97%)
       156,938,147      LLC-load-misses           #    4.76% of all LL-cache accesses  (49.98%)
       538,867,148      LLC-loads                 #    1.865 M/sec                    (49.99%)
     3,294,707,185      LLC-loads                 #   11.401 M/sec                    (41.68%)
         2,450,419      LLC-store-misses          #    8.479 K/sec                    (25.02%)
        68,026,582      LLC-store-misses          #  235.390 K/sec                    (33.35%)
       208,815,846      LLC-stores                #  722.558 K/sec                    (25.01%)
     1,737,942,617      LLC-stores                #    6.014 M/sec                    (33.35%)
        288,995.28 msec task-clock                #    8.000 CPUs utilized          
    97,496,778,753      cycles:u                  #    0.337 GHz                      (41.67%)
   528,861,919,472      cycles:k                  #    1.830 GHz                      (41.66%)
    53,515,410,418      instructions:u            #    0.55  insn per cycle           (49.99%)
   326,879,034,471      instructions:k            #    0.62  insn per cycle           (49.97%)

      36.125185717 seconds time elapsed

