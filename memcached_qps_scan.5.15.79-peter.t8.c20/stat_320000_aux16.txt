# started on Fri Mar 31 14:18:36 2023


 Performance counter stats for 'CPU(s) 0-7':

        46,113,289      LLC-load-misses           #   12.71% of all LL-cache accesses  (49.97%)
        90,907,689      LLC-load-misses           #    3.98% of all LL-cache accesses  (49.98%)
       362,748,612      LLC-loads                 #    1.257 M/sec                    (50.00%)
     2,283,185,096      LLC-loads                 #    7.912 M/sec                    (41.68%)
         2,206,177      LLC-store-misses          #    7.645 K/sec                    (25.02%)
        36,877,575      LLC-store-misses          #  127.792 K/sec                    (33.36%)
       131,483,846      LLC-stores                #  455.633 K/sec                    (25.01%)
     1,100,008,306      LLC-stores                #    3.812 M/sec                    (33.34%)
        288,574.01 msec task-clock                #    8.000 CPUs utilized          
    69,768,568,868      cycles:u                  #    0.242 GHz                      (41.66%)
   411,752,517,324      cycles:k                  #    1.427 GHz                      (41.65%)
    31,531,122,535      instructions:u            #    0.45  insn per cycle           (49.98%)
   230,679,247,950      instructions:k            #    0.56  insn per cycle           (49.97%)

      36.072854981 seconds time elapsed

