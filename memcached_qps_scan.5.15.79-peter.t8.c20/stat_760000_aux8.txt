# started on Fri Mar 31 13:01:54 2023


 Performance counter stats for 'CPU(s) 0-7':

       100,103,978      LLC-load-misses           #   17.06% of all LL-cache accesses  (49.99%)
       182,282,229      LLC-load-misses           #    5.21% of all LL-cache accesses  (49.99%)
       586,617,818      LLC-loads                 #    2.029 M/sec                    (49.99%)
     3,496,247,378      LLC-loads                 #   12.094 M/sec                    (41.66%)
         2,221,844      LLC-store-misses          #    7.686 K/sec                    (25.01%)
        43,051,788      LLC-store-misses          #  148.920 K/sec                    (33.34%)
       230,441,097      LLC-stores                #  797.115 K/sec                    (25.00%)
     1,860,702,782      LLC-stores                #    6.436 M/sec                    (33.34%)
        289,093.83 msec task-clock                #    8.000 CPUs utilized          
   101,790,882,645      cycles:u                  #    0.352 GHz                      (41.67%)
   545,584,080,518      cycles:k                  #    1.887 GHz                      (41.67%)
    62,945,836,442      instructions:u            #    0.62  insn per cycle           (50.00%)
   359,008,258,796      instructions:k            #    0.66  insn per cycle           (49.99%)

      36.137897951 seconds time elapsed

