# started on Wed Mar 29 11:57:17 2023


 Performance counter stats for 'CPU(s) 0-7':

        46,507,651      LLC-load-misses           #   12.71% of all LL-cache accesses  (49.99%)
        95,020,854      LLC-load-misses           #    4.17% of all LL-cache accesses  (50.00%)
       365,806,303      LLC-loads                 #    1.266 M/sec                    (50.01%)
     2,280,708,478      LLC-loads                 #    7.896 M/sec                    (41.69%)
         2,310,111      LLC-store-misses          #    7.997 K/sec                    (25.01%)
        38,047,344      LLC-store-misses          #  131.717 K/sec                    (33.34%)
       139,001,764      LLC-stores                #  481.212 K/sec                    (24.99%)
     1,116,362,137      LLC-stores                #    3.865 M/sec                    (33.32%)
        288,857.56 msec task-clock                #    8.000 CPUs utilized          
    69,818,166,784      cycles:u                  #    0.242 GHz                      (41.65%)
   415,376,042,691      cycles:k                  #    1.438 GHz                      (41.65%)
    30,294,316,159      instructions:u            #    0.43  insn per cycle           (49.98%)
   229,111,889,441      instructions:k            #    0.55  insn per cycle           (49.98%)

      36.108353651 seconds time elapsed

