# started on Wed Mar 29 11:45:59 2023


 Performance counter stats for 'CPU(s) 0-7':

        10,994,760      LLC-load-misses           #   11.23% of all LL-cache accesses  (49.97%)
        28,014,230      LLC-load-misses           #    3.45% of all LL-cache accesses  (49.98%)
        97,880,089      LLC-loads                 #  335.555 K/sec                    (49.99%)
       811,742,211      LLC-loads                 #    2.783 M/sec                    (41.67%)
            78,940      LLC-store-misses          #  270.624 /sec                     (25.01%)
         5,330,680      LLC-store-misses          #   18.275 K/sec                    (33.35%)
        42,075,490      LLC-stores                #  144.244 K/sec                    (25.01%)
       346,770,777      LLC-stores                #    1.189 M/sec                    (33.35%)
        291,696.38 msec task-clock                #    8.000 CPUs utilized          
    23,655,513,976      cycles:u                  #    0.081 GHz                      (41.68%)
   163,432,255,400      cycles:k                  #    0.560 GHz                      (41.66%)
     9,693,624,880      instructions:u            #    0.41  insn per cycle           (49.99%)
    87,154,280,544      instructions:k            #    0.53  insn per cycle           (49.98%)

      36.463292389 seconds time elapsed

