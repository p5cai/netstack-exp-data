# started on Fri Mar 31 12:42:35 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,100,139      LLC-load-misses           #   11.44% of all LL-cache accesses  (50.00%)
        28,304,317      LLC-load-misses           #    3.52% of all LL-cache accesses  (50.00%)
        97,071,148      LLC-loads                 #  336.158 K/sec                    (50.00%)
       804,211,262      LLC-loads                 #    2.785 M/sec                    (41.67%)
            98,221      LLC-store-misses          #  340.140 /sec                     (25.00%)
         6,338,521      LLC-store-misses          #   21.950 K/sec                    (33.34%)
        39,324,386      LLC-stores                #  136.181 K/sec                    (25.00%)
       339,012,424      LLC-stores                #    1.174 M/sec                    (33.33%)
        288,766.19 msec task-clock                #    8.000 CPUs utilized          
    20,963,596,344      cycles:u                  #    0.073 GHz                      (41.66%)
   158,959,343,115      cycles:k                  #    0.550 GHz                      (41.66%)
     7,696,111,874      instructions:u            #    0.37  insn per cycle           (49.99%)
    87,279,843,767      instructions:k            #    0.55  insn per cycle           (49.99%)

      36.097009367 seconds time elapsed

