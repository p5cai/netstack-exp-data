# started on Fri Mar 10 14:46:30 2023


 Performance counter stats for 'system wide':

       132,451,232      LLC-load-misses           #   28.38% of all LL-cache accesses  (49.97%)
       477,629,379      LLC-load-misses           #   13.62% of all LL-cache accesses  (49.97%)
       466,717,422      LLC-loads                 #  457.851 K/sec                    (49.98%)
     3,507,121,977      LLC-loads                 #    3.440 M/sec                    (41.67%)
        13,625,915      LLC-store-misses          #   13.367 K/sec                    (25.02%)
       182,538,966      LLC-store-misses          #  179.071 K/sec                    (33.36%)
       178,794,101      LLC-stores                #  175.397 K/sec                    (25.01%)
     1,705,640,075      LLC-stores                #    1.673 M/sec                    (33.35%)
      1,019,365.22 msec task-clock                #   31.999 CPUs utilized          
   120,915,786,026      cycles:u                  #    0.119 GHz                      (41.68%)
   831,411,670,564      cycles:k                  #    0.816 GHz                      (41.66%)
    37,529,107,206      instructions:u            #    0.31  insn per cycle           (49.99%)
   339,432,048,004      instructions:k            #    0.41  insn per cycle           (49.97%)

      31.855869309 seconds time elapsed

