# started on Fri Mar 10 14:54:38 2023


 Performance counter stats for 'system wide':

       334,064,783      LLC-load-misses           #   55.96% of all LL-cache accesses  (49.97%)
     1,498,911,477      LLC-load-misses           #   37.06% of all LL-cache accesses  (49.99%)
       596,932,418      LLC-loads                 #  584.995 K/sec                    (50.01%)
     4,044,076,814      LLC-loads                 #    3.963 M/sec                    (41.70%)
        42,128,854      LLC-store-misses          #   41.286 K/sec                    (25.03%)
     1,189,236,398      LLC-store-misses          #    1.165 M/sec                    (33.36%)
       231,656,911      LLC-stores                #  227.024 K/sec                    (25.00%)
     2,559,255,322      LLC-stores                #    2.508 M/sec                    (33.32%)
      1,020,405.44 msec task-clock                #   31.999 CPUs utilized          
   159,482,076,782      cycles:u                  #    0.156 GHz                      (41.65%)
   976,625,144,760      cycles:k                  #    0.957 GHz                      (41.64%)
    56,694,570,227      instructions:u            #    0.36  insn per cycle           (49.96%)
   365,922,550,368      instructions:k            #    0.37  insn per cycle           (49.96%)

      31.888765116 seconds time elapsed

