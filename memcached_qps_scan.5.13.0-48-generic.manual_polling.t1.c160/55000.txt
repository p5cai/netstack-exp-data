#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       71.6   347.5    29.5    37.5    39.1    51.9    81.8    98.9   146.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      44889.2  7325.4     1.0 33983.3 36242.6 44388.3 54757.0 57479.9 65739.3

Total QPS = 54994.1 (1649822 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX  400466577 bytes :   12.7 MB/s
TX   69666163 bytes :    2.2 MB/s
