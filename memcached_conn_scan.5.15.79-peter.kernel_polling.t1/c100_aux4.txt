#type       avg     std     min     5th    10th    50th    90th    95th    99th
read    59409.3  3229.6  1102.5 57789.8 58083.1 60428.9 62774.7 63067.9 63302.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      123799.6 10637.2 97234.1 105852.6 111016.2 122718.0 137753.0 145090.1 158907.3

Total QPS = 93864.8 (2815943 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX  692662772 bytes :   22.0 MB/s
TX  119113969 bytes :    3.8 MB/s
