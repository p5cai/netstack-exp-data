# started on Sun Apr  2 11:25:13 2023

#           time             counts unit events
     5.002472872          1,032,615      LLC-load-misses           #   38.26% of all LL-cache accesses  (49.94%)
     5.002472872          3,729,410      LLC-load-misses           #   30.02% of all LL-cache accesses  (50.02%)
     5.002472872          2,699,119      LLC-loads                 #  539.563 K/sec                    (50.10%)
     5.002472872         12,422,942      LLC-loads                 #    2.483 M/sec                    (41.79%)
     5.002472872            153,388      LLC-store-misses          #   30.663 K/sec                    (25.03%)
     5.002472872          2,001,928      LLC-store-misses          #  400.192 K/sec                    (33.35%)
     5.002472872            991,008      LLC-stores                #  198.106 K/sec                    (24.95%)
     5.002472872          6,550,825      LLC-stores                #    1.310 M/sec                    (33.26%)
     5.002472872           5,002.42 msec task-clock                #    1.000 CPUs utilized          
     5.002472872        534,043,209      cycles:u                  #    0.107 GHz                      (41.58%)
     5.002472872      2,656,857,249      cycles:k                  #    0.531 GHz                      (41.58%)
     5.002472872        305,472,788      instructions:u            #    0.57  insn per cycle           (49.89%)
     5.002472872      2,184,192,157      instructions:k            #    0.82  insn per cycle           (49.89%)
    10.006473703          5,045,967      LLC-load-misses           #   38.33% of all LL-cache accesses  (49.88%)
    10.006473703         17,384,923      LLC-load-misses           #   29.97% of all LL-cache accesses  (49.88%)
    10.006473703         13,163,078      LLC-loads                 #    2.631 M/sec                    (49.88%)
    10.006473703         58,014,460      LLC-loads                 #   11.594 M/sec                    (41.65%)
    10.006473703            620,748      LLC-store-misses          #  124.055 K/sec                    (25.10%)
    10.006473703          8,221,004      LLC-store-misses          #    1.643 M/sec                    (33.49%)
    10.006473703          4,786,811      LLC-stores                #  956.633 K/sec                    (25.10%)
    10.006473703         27,832,651      LLC-stores                #    5.562 M/sec                    (33.42%)
    10.006473703           5,003.81 msec task-clock                #    1.001 CPUs utilized          
    10.006473703      2,549,100,412      cycles:u                  #    0.509 GHz                      (41.73%)
    10.006473703     10,896,992,235      cycles:k                  #    2.178 GHz                      (41.65%)
    10.006473703      1,465,246,559      instructions:u            #    0.57  insn per cycle           (49.96%)
    10.006473703      8,161,265,681      instructions:k            #    0.75  insn per cycle           (49.88%)
    15.010470517          5,025,873      LLC-load-misses           #   38.79% of all LL-cache accesses  (50.04%)
    15.010470517         17,450,634      LLC-load-misses           #   29.79% of all LL-cache accesses  (49.96%)
    15.010470517         12,956,636      LLC-loads                 #    2.589 M/sec                    (49.88%)
    15.010470517         58,584,005      LLC-loads                 #   11.708 M/sec                    (41.57%)
    15.010470517            500,809      LLC-store-misses          #  100.084 K/sec                    (24.94%)
    15.010470517          8,191,945      LLC-store-misses          #    1.637 M/sec                    (33.26%)
    15.010470517          4,609,022      LLC-stores                #  921.091 K/sec                    (25.02%)
    15.010470517         27,943,526      LLC-stores                #    5.584 M/sec                    (33.41%)
    15.010470517           5,003.87 msec task-clock                #    1.001 CPUs utilized          
    15.010470517      2,515,442,582      cycles:u                  #    0.503 GHz                      (41.80%)
    15.010470517     10,927,876,331      cycles:k                  #    2.184 GHz                      (41.80%)
    15.010470517      1,459,307,800      instructions:u            #    0.58  insn per cycle           (50.12%)
    15.010470517      8,204,623,837      instructions:k            #    0.75  insn per cycle           (50.12%)
    20.014473017          5,012,200      LLC-load-misses           #   39.09% of all LL-cache accesses  (50.12%)
    20.014473017         17,517,420      LLC-load-misses           #   29.77% of all LL-cache accesses  (50.12%)
    20.014473017         12,823,128      LLC-loads                 #    2.563 M/sec                    (50.12%)
    20.014473017         58,839,836      LLC-loads                 #   11.759 M/sec                    (41.65%)
    20.014473017            431,563      LLC-store-misses          #   86.247 K/sec                    (24.94%)
    20.014473017          8,148,052      LLC-store-misses          #    1.628 M/sec                    (33.26%)
    20.014473017          4,505,384      LLC-stores                #  900.390 K/sec                    (24.94%)
    20.014473017         28,041,512      LLC-stores                #    5.604 M/sec                    (33.25%)
    20.014473017           5,003.81 msec task-clock                #    1.001 CPUs utilized          
    20.014473017      2,499,018,788      cycles:u                  #    0.499 GHz                      (41.57%)
    20.014473017     10,944,679,815      cycles:k                  #    2.187 GHz                      (41.64%)
    20.014473017      1,458,267,275      instructions:u            #    0.58  insn per cycle           (50.03%)
    20.014473017      8,224,812,591      instructions:k            #    0.75  insn per cycle           (50.11%)
    25.018470339          5,004,231      LLC-load-misses           #   39.24% of all LL-cache accesses  (49.96%)
    25.018470339         17,501,295      LLC-load-misses           #   29.66% of all LL-cache accesses  (50.04%)
    25.018470339         12,751,956      LLC-loads                 #    2.548 M/sec                    (50.12%)
    25.018470339         59,003,792      LLC-loads                 #   11.792 M/sec                    (41.81%)
    25.018470339            404,889      LLC-store-misses          #   80.915 K/sec                    (25.03%)
    25.018470339          8,117,478      LLC-store-misses          #    1.622 M/sec                    (33.34%)
    25.018470339          4,461,630      LLC-stores                #  891.637 K/sec                    (24.94%)
    25.018470339         28,006,993      LLC-stores                #    5.597 M/sec                    (33.25%)
    25.018470339           5,003.86 msec task-clock                #    1.001 CPUs utilized          
    25.018470339      2,494,279,333      cycles:u                  #    0.498 GHz                      (41.57%)
    25.018470339     10,947,614,261      cycles:k                  #    2.188 GHz                      (41.56%)
    25.018470339      1,458,086,411      instructions:u            #    0.58  insn per cycle           (49.88%)
    25.018470339      8,242,835,696      instructions:k            #    0.75  insn per cycle           (49.88%)
    30.022476981          4,987,267      LLC-load-misses           #   39.21% of all LL-cache accesses  (49.88%)
    30.022476981         17,473,944      LLC-load-misses           #   29.62% of all LL-cache accesses  (49.88%)
    30.022476981         12,720,825      LLC-loads                 #    2.542 M/sec                    (49.88%)
    30.022476981         58,995,825      LLC-loads                 #   11.790 M/sec                    (41.65%)
    30.022476981            382,879      LLC-store-misses          #   76.517 K/sec                    (25.10%)
    30.022476981          8,085,229      LLC-store-misses          #    1.616 M/sec                    (33.49%)
    30.022476981          4,438,502      LLC-stores                #  887.022 K/sec                    (25.10%)
    30.022476981         28,045,980      LLC-stores                #    5.605 M/sec                    (33.42%)
    30.022476981           5,003.83 msec task-clock                #    1.001 CPUs utilized          
    30.022476981      2,491,891,783      cycles:u                  #    0.498 GHz                      (41.73%)
    30.022476981     10,952,808,187      cycles:k                  #    2.189 GHz                      (41.65%)
    30.022476981      1,460,841,235      instructions:u            #    0.59  insn per cycle           (49.96%)
    30.022476981      8,259,633,200      instructions:k            #    0.75  insn per cycle           (49.88%)
    35.026493916          4,003,962      LLC-load-misses           #   39.26% of all LL-cache accesses  (50.04%)
    35.026493916         14,343,519      LLC-load-misses           #   29.89% of all LL-cache accesses  (49.96%)
    35.026493916         10,198,621      LLC-loads                 #    2.038 M/sec                    (49.88%)
    35.026493916         47,992,371      LLC-loads                 #    9.591 M/sec                    (41.57%)
    35.026493916            290,501      LLC-store-misses          #   58.056 K/sec                    (24.94%)
    35.026493916          6,645,164      LLC-store-misses          #    1.328 M/sec                    (33.26%)
    35.026493916          3,522,130      LLC-stores                #  703.886 K/sec                    (25.02%)
    35.026493916         22,834,297      LLC-stores                #    4.563 M/sec                    (33.41%)
    35.026493916           5,003.84 msec task-clock                #    1.001 CPUs utilized          
    35.026493916      1,994,613,225      cycles:u                  #    0.399 GHz                      (41.80%)
    35.026493916      8,928,122,248      cycles:k                  #    1.784 GHz                      (41.80%)
    35.026493916      1,173,319,773      instructions:u            #    0.59  insn per cycle           (50.12%)
    35.026493916      6,782,446,417      instructions:k            #    0.76  insn per cycle           (50.12%)
    36.128434759                517      LLC-load-misses           #   54.42% of all LL-cache accesses  (49.90%)
    36.128434759             36,507      LLC-load-misses           #   25.04% of all LL-cache accesses  (49.90%)
    36.128434759                950      LLC-loads                 #  862.152 /sec                     (50.09%)
    36.128434759            145,778      LLC-loads                 #  132.298 K/sec                    (41.75%)
    36.128434759                  0      LLC-store-misses          #    0.000 /sec                     (25.05%)
    36.128434759             45,320      LLC-store-misses          #   41.129 K/sec                    (33.40%)
    36.128434759                  0      LLC-stores                #    0.000 /sec                     (25.05%)
    36.128434759             58,719      LLC-stores                #   53.289 K/sec                    (33.40%)
    36.128434759           1,101.89 msec task-clock                #    0.220 CPUs utilized          
    36.128434759             36,540      cycles:u                  #    0.000 GHz                      (41.56%)
    36.128434759         28,678,030      cycles:k                  #    0.026 GHz                      (41.55%)
    36.128434759            350,525      instructions:u            #    9.59  insn per cycle           (49.90%)
    36.128434759          3,340,003      instructions:k            #    0.12  insn per cycle           (49.90%)
