# started on Fri Mar 31 09:09:42 2023

#           time             counts unit events
     5.004328795          5,100,319      LLC-load-misses           #   16.29% of all LL-cache accesses  (49.92%)
     5.004328795         10,865,073      LLC-load-misses           #    9.30% of all LL-cache accesses  (50.01%)
     5.004328795         31,317,550      LLC-loads                 #  782.209 K/sec                    (50.09%)
     5.004328795        116,890,890      LLC-loads                 #    2.920 M/sec                    (41.82%)
     5.004328795          1,230,676      LLC-store-misses          #   30.738 K/sec                    (25.06%)
     5.004328795          2,532,797      LLC-store-misses          #   63.261 K/sec                    (33.37%)
     5.004328795         11,714,133      LLC-stores                #  292.580 K/sec                    (24.94%)
     5.004328795         59,895,857      LLC-stores                #    1.496 M/sec                    (33.25%)
     5.004328795          40,037.32 msec task-clock                #    8.007 CPUs utilized          
     5.004328795      5,160,327,208      cycles:u                  #    0.129 GHz                      (41.56%)
     5.004328795     20,932,221,270      cycles:k                  #    0.523 GHz                      (41.55%)
     5.004328795      3,268,850,545      instructions:u            #    0.63  insn per cycle           (49.86%)
     5.004328795     19,615,581,829      instructions:k            #    0.94  insn per cycle           (49.86%)
    10.008330629         21,947,177      LLC-load-misses           #   18.15% of all LL-cache accesses  (49.88%)
    10.008330629         46,163,323      LLC-load-misses           #    8.92% of all LL-cache accesses  (49.88%)
    10.008330629        120,938,957      LLC-loads                 #    3.021 M/sec                    (49.88%)
    10.008330629        517,397,383      LLC-loads                 #   12.925 M/sec                    (41.61%)
    10.008330629            760,617      LLC-store-misses          #   19.001 K/sec                    (25.06%)
    10.008330629          3,768,772      LLC-store-misses          #   94.148 K/sec                    (33.46%)
    10.008330629         33,152,246      LLC-stores                #  828.179 K/sec                    (25.13%)
    10.008330629        263,262,956      LLC-stores                #    6.577 M/sec                    (33.45%)
    10.008330629          40,030.31 msec task-clock                #    8.006 CPUs utilized          
    10.008330629     21,016,154,629      cycles:u                  #    0.525 GHz                      (41.76%)
    10.008330629     86,473,656,195      cycles:k                  #    2.160 GHz                      (41.68%)
    10.008330629     13,612,491,046      instructions:u            #    0.65  insn per cycle           (50.00%)
    10.008330629     74,727,137,008      instructions:k            #    0.86  insn per cycle           (49.92%)
    15.012335269         21,905,713      LLC-load-misses           #   18.55% of all LL-cache accesses  (50.07%)
    15.012335269         47,412,183      LLC-load-misses           #    9.16% of all LL-cache accesses  (50.00%)
    15.012335269        118,092,380      LLC-loads                 #    2.950 M/sec                    (49.92%)
    15.012335269        517,660,105      LLC-loads                 #   12.932 M/sec                    (41.57%)
    15.012335269             75,315      LLC-store-misses          #    1.881 K/sec                    (24.94%)
    15.012335269          3,832,937      LLC-store-misses          #   95.750 K/sec                    (33.26%)
    15.012335269         30,836,628      LLC-stores                #  770.324 K/sec                    (24.99%)
    15.012335269        263,446,289      LLC-stores                #    6.581 M/sec                    (33.37%)
    15.012335269          40,030.75 msec task-clock                #    8.006 CPUs utilized          
    15.012335269     20,759,252,421      cycles:u                  #    0.519 GHz                      (41.77%)
    15.012335269     86,707,891,227      cycles:k                  #    2.166 GHz                      (41.80%)
    15.012335269     13,579,590,010      instructions:u            #    0.65  insn per cycle           (50.12%)
    15.012335269     75,056,624,488      instructions:k            #    0.87  insn per cycle           (50.12%)
    20.016331330         21,896,828      LLC-load-misses           #   18.55% of all LL-cache accesses  (50.11%)
    20.016331330         47,084,091      LLC-load-misses           #    9.09% of all LL-cache accesses  (50.12%)
    20.016331330        118,068,879      LLC-loads                 #    2.949 M/sec                    (50.12%)
    20.016331330        517,989,335      LLC-loads                 #   12.940 M/sec                    (41.69%)
    20.016331330             66,106      LLC-store-misses          #    1.651 K/sec                    (24.94%)
    20.016331330          3,753,228      LLC-store-misses          #   93.759 K/sec                    (33.26%)
    20.016331330         30,866,263      LLC-stores                #  771.066 K/sec                    (24.94%)
    20.016331330        264,125,260      LLC-stores                #    6.598 M/sec                    (33.25%)
    20.016331330          40,030.66 msec task-clock                #    8.006 CPUs utilized          
    20.016331330     20,787,237,227      cycles:u                  #    0.519 GHz                      (41.57%)
    20.016331330     86,699,488,228      cycles:k                  #    2.166 GHz                      (41.61%)
    20.016331330     13,582,535,702      instructions:u            #    0.65  insn per cycle           (50.00%)
    20.016331330     75,073,038,930      instructions:k            #    0.87  insn per cycle           (50.08%)
    25.020333470         21,835,574      LLC-load-misses           #   18.50% of all LL-cache accesses  (49.92%)
    25.020333470         46,256,316      LLC-load-misses           #    8.94% of all LL-cache accesses  (50.00%)
    25.020333470        118,061,349      LLC-loads                 #    2.949 M/sec                    (50.08%)
    25.020333470        517,586,641      LLC-loads                 #   12.930 M/sec                    (41.80%)
    25.020333470             63,844      LLC-store-misses          #    1.595 K/sec                    (25.06%)
    25.020333470          3,693,426      LLC-store-misses          #   92.265 K/sec                    (33.38%)
    25.020333470         30,841,593      LLC-stores                #  770.450 K/sec                    (24.94%)
    25.020333470        263,934,277      LLC-stores                #    6.593 M/sec                    (33.26%)
    25.020333470          40,030.65 msec task-clock                #    8.006 CPUs utilized          
    25.020333470     20,787,779,791      cycles:u                  #    0.519 GHz                      (41.57%)
    25.020333470     86,705,308,770      cycles:k                  #    2.166 GHz                      (41.56%)
    25.020333470     13,578,395,383      instructions:u            #    0.65  insn per cycle           (49.88%)
    25.020333470     75,039,029,597      instructions:k            #    0.87  insn per cycle           (49.88%)
    30.024333650         21,819,426      LLC-load-misses           #   18.50% of all LL-cache accesses  (49.88%)
    30.024333650         46,909,020      LLC-load-misses           #    9.07% of all LL-cache accesses  (49.88%)
    30.024333650        117,929,025      LLC-loads                 #    2.946 M/sec                    (49.88%)
    30.024333650        517,045,916      LLC-loads                 #   12.916 M/sec                    (41.61%)
    30.024333650             64,974      LLC-store-misses          #    1.623 K/sec                    (25.06%)
    30.024333650          3,688,791      LLC-store-misses          #   92.149 K/sec                    (33.46%)
    30.024333650         30,779,068      LLC-stores                #  768.886 K/sec                    (25.13%)
    30.024333650        263,049,961      LLC-stores                #    6.571 M/sec                    (33.45%)
    30.024333650          40,030.71 msec task-clock                #    8.006 CPUs utilized          
    30.024333650     20,785,322,925      cycles:u                  #    0.519 GHz                      (41.76%)
    30.024333650     86,705,583,161      cycles:k                  #    2.166 GHz                      (41.68%)
    30.024333650     13,563,404,040      instructions:u            #    0.65  insn per cycle           (50.00%)
    30.024333650     74,985,493,082      instructions:k            #    0.86  insn per cycle           (49.92%)
    35.028324052         16,983,132      LLC-load-misses           #   18.49% of all LL-cache accesses  (50.07%)
    35.028324052         36,764,843      LLC-load-misses           #    9.10% of all LL-cache accesses  (50.00%)
    35.028324052         91,853,185      LLC-loads                 #    2.295 M/sec                    (49.92%)
    35.028324052        404,057,302      LLC-loads                 #   10.094 M/sec                    (41.57%)
    35.028324052             51,489      LLC-store-misses          #    1.286 K/sec                    (24.94%)
    35.028324052          3,438,610      LLC-store-misses          #   85.899 K/sec                    (33.26%)
    35.028324052         23,746,782      LLC-stores                #  593.213 K/sec                    (24.98%)
    35.028324052        203,737,016      LLC-stores                #    5.090 M/sec                    (33.37%)
    35.028324052          40,030.79 msec task-clock                #    8.006 CPUs utilized          
    35.028324052     16,046,414,605      cycles:u                  #    0.401 GHz                      (41.77%)
    35.028324052     67,963,672,947      cycles:k                  #    1.698 GHz                      (41.80%)
    35.028324052     10,492,317,967      instructions:u            #    0.65  insn per cycle           (50.12%)
    35.028324052     59,024,232,491      instructions:k            #    0.87  insn per cycle           (50.12%)
    36.079830718             23,771      LLC-load-misses           #   84.07% of all LL-cache accesses  (49.77%)
    36.079830718            250,293      LLC-load-misses           #   27.18% of all LL-cache accesses  (49.78%)
    36.079830718             28,275      LLC-loads                 #    3.362 K/sec                    (50.04%)
    36.079830718            920,806      LLC-loads                 #  109.471 K/sec                    (41.85%)
    36.079830718                  7      LLC-store-misses          #    0.832 /sec                     (25.11%)
    36.079830718            325,666      LLC-store-misses          #   38.717 K/sec                    (33.48%)
    36.079830718                187      LLC-stores                #   22.232 /sec                     (25.10%)
    36.079830718            440,913      LLC-stores                #   52.419 K/sec                    (33.47%)
    36.079830718           8,411.38 msec task-clock                #    1.682 CPUs utilized          
    36.079830718            465,478      cycles:u                  #    0.000 GHz                      (41.59%)
    36.079830718        221,776,107      cycles:k                  #    0.026 GHz                      (41.44%)
    36.079830718            478,594      instructions:u            #    1.03  insn per cycle           (49.79%)
    36.079830718         21,846,194      instructions:k            #    0.10  insn per cycle           (49.78%)
