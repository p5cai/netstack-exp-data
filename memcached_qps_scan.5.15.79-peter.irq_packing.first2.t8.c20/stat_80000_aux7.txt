# started on Wed Apr  5 23:51:02 2023


 Performance counter stats for 'CPU(s) 2-9':

        10,405,292      LLC-load-misses           #   18.98% of all LL-cache accesses  (49.98%)
       267,379,076      LLC-load-misses           #   38.40% of all LL-cache accesses  (49.98%)
        54,828,983      LLC-loads                 #  189.952 K/sec                    (49.98%)
       696,355,078      LLC-loads                 #    2.412 M/sec                    (41.67%)
            13,548      LLC-store-misses          #   46.936 /sec                     (25.01%)
       113,955,550      LLC-store-misses          #  394.793 K/sec                    (33.35%)
        24,580,144      LLC-stores                #   85.157 K/sec                    (25.01%)
       243,693,409      LLC-stores                #  844.263 K/sec                    (33.35%)
        288,646.43 msec task-clock                #    8.000 CPUs utilized          
    16,994,476,459      cycles:u                  #    0.059 GHz                      (41.68%)
   171,461,113,660      cycles:k                  #    0.594 GHz                      (41.67%)
     7,770,490,585      instructions:u            #    0.46  insn per cycle           (50.00%)
    86,902,521,469      instructions:k            #    0.51  insn per cycle           (49.98%)

      36.081878939 seconds time elapsed

