#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       64.0    17.7    29.5    44.4    47.6    61.5    82.1    89.3   106.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      2087531.6 52143.6 1866344.5 1967225.8 1978411.1 2067894.0 2187180.1 2277840.4 2350368.6

Total QPS = 319736.0 (9592092 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 2343513688 bytes :   74.5 MB/s
TX  405002137 bytes :   12.9 MB/s
