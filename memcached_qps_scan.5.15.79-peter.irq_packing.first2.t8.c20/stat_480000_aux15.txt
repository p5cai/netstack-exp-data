# started on Thu Apr  6 01:09:35 2023


 Performance counter stats for 'CPU(s) 2-9':

        69,166,389      LLC-load-misses           #   20.60% of all LL-cache accesses  (49.98%)
     1,147,366,682      LLC-load-misses           #   51.38% of all LL-cache accesses  (49.98%)
       335,826,037      LLC-loads                 #    1.160 M/sec                    (49.98%)
     2,233,254,445      LLC-loads                 #    7.716 M/sec                    (41.67%)
         1,995,665      LLC-store-misses          #    6.895 K/sec                    (25.01%)
       505,877,663      LLC-store-misses          #    1.748 M/sec                    (33.35%)
       125,817,881      LLC-stores                #  434.694 K/sec                    (25.01%)
     1,106,986,781      LLC-stores                #    3.825 M/sec                    (33.34%)
        289,440.43 msec task-clock                #    8.000 CPUs utilized          
    77,737,961,160      cycles:u                  #    0.269 GHz                      (41.68%)
   470,485,670,070      cycles:k                  #    1.626 GHz                      (41.67%)
    44,420,487,425      instructions:u            #    0.57  insn per cycle           (49.99%)
   273,571,096,830      instructions:k            #    0.58  insn per cycle           (49.98%)

      36.181162947 seconds time elapsed

