# started on Wed Apr  5 23:14:19 2023


 Performance counter stats for 'CPU(s) 2-9':

        70,522,338      LLC-load-misses           #   20.84% of all LL-cache accesses  (49.99%)
     1,154,113,246      LLC-load-misses           #   50.57% of all LL-cache accesses  (50.00%)
       338,394,021      LLC-loads                 #    1.173 M/sec                    (50.01%)
     2,282,366,040      LLC-loads                 #    7.914 M/sec                    (41.67%)
         2,002,374      LLC-store-misses          #    6.943 K/sec                    (25.00%)
       514,531,373      LLC-store-misses          #    1.784 M/sec                    (33.33%)
       126,932,131      LLC-stores                #  440.120 K/sec                    (25.00%)
     1,122,903,279      LLC-stores                #    3.894 M/sec                    (33.33%)
        288,403.14 msec task-clock                #    8.000 CPUs utilized          
    76,857,082,634      cycles:u                  #    0.266 GHz                      (41.66%)
   467,583,754,096      cycles:k                  #    1.621 GHz                      (41.66%)
    44,614,844,412      instructions:u            #    0.58  insn per cycle           (49.99%)
   274,347,672,787      instructions:k            #    0.59  insn per cycle           (49.99%)

      36.051497307 seconds time elapsed

