# started on Thu Apr  6 01:36:42 2023


 Performance counter stats for 'CPU(s) 2-9':

        10,361,983      LLC-load-misses           #   18.00% of all LL-cache accesses  (49.97%)
       265,875,792      LLC-load-misses           #   38.11% of all LL-cache accesses  (49.98%)
        57,556,962      LLC-loads                 #  199.681 K/sec                    (49.98%)
       697,686,167      LLC-loads                 #    2.420 M/sec                    (41.67%)
            14,363      LLC-store-misses          #   49.829 /sec                     (25.01%)
       115,122,962      LLC-store-misses          #  399.393 K/sec                    (33.35%)
        24,262,940      LLC-stores                #   84.175 K/sec                    (25.01%)
       245,694,710      LLC-stores                #  852.383 K/sec                    (33.35%)
        288,244.50 msec task-clock                #    8.000 CPUs utilized          
    17,590,533,326      cycles:u                  #    0.061 GHz                      (41.68%)
   171,649,560,708      cycles:k                  #    0.595 GHz                      (41.66%)
     8,811,575,368      instructions:u            #    0.50  insn per cycle           (49.99%)
    87,212,666,102      instructions:k            #    0.51  insn per cycle           (49.98%)

      36.031674409 seconds time elapsed

