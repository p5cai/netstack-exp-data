# started on Thu Apr  6 01:22:22 2023


 Performance counter stats for 'CPU(s) 2-9':

        78,360,577      LLC-load-misses           #   21.16% of all LL-cache accesses  (49.99%)
     1,192,969,058      LLC-load-misses           #   50.52% of all LL-cache accesses  (49.99%)
       370,341,954      LLC-loads                 #    1.283 M/sec                    (49.99%)
     2,361,426,438      LLC-loads                 #    8.179 M/sec                    (41.66%)
         2,038,120      LLC-store-misses          #    7.059 K/sec                    (25.01%)
       530,403,294      LLC-store-misses          #    1.837 M/sec                    (33.34%)
       138,034,407      LLC-stores                #  478.082 K/sec                    (25.00%)
     1,332,004,142      LLC-stores                #    4.613 M/sec                    (33.34%)
        288,725.43 msec task-clock                #    8.000 CPUs utilized          
    82,277,027,078      cycles:u                  #    0.285 GHz                      (41.67%)
   485,828,430,067      cycles:k                  #    1.683 GHz                      (41.67%)
    48,407,188,915      instructions:u            #    0.59  insn per cycle           (50.00%)
   288,500,618,058      instructions:k            #    0.59  insn per cycle           (50.00%)

      36.091770973 seconds time elapsed

