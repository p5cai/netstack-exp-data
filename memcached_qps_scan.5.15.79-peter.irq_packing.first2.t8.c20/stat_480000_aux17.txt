# started on Thu Apr  6 01:28:48 2023


 Performance counter stats for 'CPU(s) 2-9':

        70,818,138      LLC-load-misses           #   20.69% of all LL-cache accesses  (49.99%)
     1,151,788,945      LLC-load-misses           #   50.32% of all LL-cache accesses  (50.00%)
       342,202,585      LLC-loads                 #    1.185 M/sec                    (50.01%)
     2,288,744,164      LLC-loads                 #    7.924 M/sec                    (41.68%)
         2,022,269      LLC-store-misses          #    7.002 K/sec                    (25.01%)
       509,280,228      LLC-store-misses          #    1.763 M/sec                    (33.34%)
       138,829,302      LLC-stores                #  480.666 K/sec                    (24.99%)
     1,125,101,210      LLC-stores                #    3.895 M/sec                    (33.33%)
        288,826.93 msec task-clock                #    8.000 CPUs utilized          
    74,920,088,242      cycles:u                  #    0.259 GHz                      (41.66%)
   468,833,867,389      cycles:k                  #    1.623 GHz                      (41.65%)
    44,941,522,329      instructions:u            #    0.60  insn per cycle           (49.98%)
   274,873,521,135      instructions:k            #    0.59  insn per cycle           (49.98%)

      36.104465772 seconds time elapsed

