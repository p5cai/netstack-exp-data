#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1096.2   934.3    35.7    90.7   119.5   532.2  2204.4  2229.2  2249.0
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3392237.1 73460.4 3306343.1 3171295.2 3190115.9 3340681.8 3601095.8 3707794.9 3793154.2

Total QPS = 516228.5 (15486856 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 3807944148 bytes :  121.1 MB/s
TX  653873024 bytes :   20.8 MB/s
