# started on Thu Apr  6 01:13:37 2023


 Performance counter stats for 'CPU(s) 2-9':

        76,897,833      LLC-load-misses           #   20.54% of all LL-cache accesses  (50.00%)
     1,197,119,632      LLC-load-misses           #   50.43% of all LL-cache accesses  (50.00%)
       374,457,501      LLC-loads                 #    1.295 M/sec                    (50.01%)
     2,373,945,665      LLC-loads                 #    8.209 M/sec                    (41.68%)
         1,972,649      LLC-store-misses          #    6.822 K/sec                    (25.00%)
       515,604,913      LLC-store-misses          #    1.783 M/sec                    (33.33%)
       137,324,126      LLC-stores                #  474.878 K/sec                    (25.00%)
     1,234,531,090      LLC-stores                #    4.269 M/sec                    (33.33%)
        289,177.89 msec task-clock                #    8.000 CPUs utilized          
    82,633,678,310      cycles:u                  #    0.286 GHz                      (41.66%)
   487,666,891,001      cycles:k                  #    1.686 GHz                      (41.66%)
    48,334,014,641      instructions:u            #    0.58  insn per cycle           (49.99%)
   288,428,870,661      instructions:k            #    0.59  insn per cycle           (49.99%)

      36.148353406 seconds time elapsed

