# started on Wed Apr  5 23:47:10 2023


 Performance counter stats for 'CPU(s) 2-9':

        77,188,959      LLC-load-misses           #   21.22% of all LL-cache accesses  (49.97%)
     1,200,029,084      LLC-load-misses           #   51.40% of all LL-cache accesses  (49.98%)
       363,747,178      LLC-loads                 #    1.261 M/sec                    (50.00%)
     2,334,872,036      LLC-loads                 #    8.091 M/sec                    (41.68%)
         1,968,862      LLC-store-misses          #    6.823 K/sec                    (25.02%)
       520,587,160      LLC-store-misses          #    1.804 M/sec                    (33.36%)
       127,139,246      LLC-stores                #  440.597 K/sec                    (25.01%)
     1,264,394,712      LLC-stores                #    4.382 M/sec                    (33.34%)
        288,560.99 msec task-clock                #    8.000 CPUs utilized          
    81,149,491,130      cycles:u                  #    0.281 GHz                      (41.67%)
   484,296,766,603      cycles:k                  #    1.678 GHz                      (41.65%)
    48,358,330,943      instructions:u            #    0.60  insn per cycle           (49.98%)
   290,271,407,266      instructions:k            #    0.60  insn per cycle           (49.97%)

      36.071215867 seconds time elapsed

