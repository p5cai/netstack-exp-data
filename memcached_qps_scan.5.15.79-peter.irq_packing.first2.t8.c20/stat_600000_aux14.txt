# started on Thu Apr  6 01:03:09 2023


 Performance counter stats for 'CPU(s) 2-9':

        76,405,732      LLC-load-misses           #   21.19% of all LL-cache accesses  (49.99%)
     1,201,436,240      LLC-load-misses           #   51.09% of all LL-cache accesses  (50.00%)
       360,630,504      LLC-loads                 #    1.249 M/sec                    (50.01%)
     2,351,509,672      LLC-loads                 #    8.142 M/sec                    (41.68%)
         1,973,085      LLC-store-misses          #    6.831 K/sec                    (25.01%)
       526,498,327      LLC-store-misses          #    1.823 M/sec                    (33.34%)
       140,612,000      LLC-stores                #  486.839 K/sec                    (24.99%)
     1,184,871,959      LLC-stores                #    4.102 M/sec                    (33.33%)
        288,826.29 msec task-clock                #    8.000 CPUs utilized          
    80,915,140,379      cycles:u                  #    0.280 GHz                      (41.66%)
   485,575,255,549      cycles:k                  #    1.681 GHz                      (41.65%)
    48,592,329,707      instructions:u            #    0.60  insn per cycle           (49.98%)
   288,673,172,679      instructions:k            #    0.59  insn per cycle           (49.98%)

      36.104405293 seconds time elapsed

