# started on Wed Apr  5 22:47:50 2023


 Performance counter stats for 'CPU(s) 2-9':

        75,755,014      LLC-load-misses           #   21.57% of all LL-cache accesses  (49.97%)
     1,211,382,413      LLC-load-misses           #   51.85% of all LL-cache accesses  (49.97%)
       351,232,910      LLC-loads                 #    1.219 M/sec                    (49.98%)
     2,336,164,000      LLC-loads                 #    8.105 M/sec                    (41.67%)
            98,922      LLC-store-misses          #  343.199 /sec                     (25.02%)
       530,168,270      LLC-store-misses          #    1.839 M/sec                    (33.35%)
       119,559,988      LLC-stores                #  414.801 K/sec                    (25.01%)
     1,175,104,557      LLC-stores                #    4.077 M/sec                    (33.35%)
        288,234.73 msec task-clock                #    8.000 CPUs utilized          
    79,527,635,713      cycles:u                  #    0.276 GHz                      (41.68%)
   486,169,425,671      cycles:k                  #    1.687 GHz                      (41.67%)
    47,808,806,009      instructions:u            #    0.60  insn per cycle           (49.99%)
   286,516,134,123      instructions:k            #    0.59  insn per cycle           (49.98%)

      36.030382396 seconds time elapsed

