# started on Thu Apr  6 00:52:42 2023


 Performance counter stats for 'CPU(s) 2-9':

        74,631,866      LLC-load-misses           #   21.25% of all LL-cache accesses  (49.97%)
     1,194,266,965      LLC-load-misses           #   51.54% of all LL-cache accesses  (49.97%)
       351,160,527      LLC-loads                 #    1.213 M/sec                    (49.98%)
     2,317,103,234      LLC-loads                 #    8.007 M/sec                    (41.67%)
            96,560      LLC-store-misses          #  333.670 /sec                     (25.02%)
       536,421,869      LLC-store-misses          #    1.854 M/sec                    (33.35%)
       130,370,038      LLC-stores                #  450.503 K/sec                    (25.01%)
     1,161,613,677      LLC-stores                #    4.014 M/sec                    (33.35%)
        289,387.62 msec task-clock                #    8.000 CPUs utilized          
    78,974,042,023      cycles:u                  #    0.273 GHz                      (41.68%)
   480,011,929,441      cycles:k                  #    1.659 GHz                      (41.67%)
    47,090,678,345      instructions:u            #    0.60  insn per cycle           (49.99%)
   284,608,323,672      instructions:k            #    0.59  insn per cycle           (49.98%)

      36.174534110 seconds time elapsed

