# started on Wed Apr  5 23:07:53 2023


 Performance counter stats for 'CPU(s) 2-9':

        77,401,865      LLC-load-misses           #   21.59% of all LL-cache accesses  (49.98%)
     1,200,666,381      LLC-load-misses           #   50.89% of all LL-cache accesses  (49.98%)
       358,516,206      LLC-loads                 #    1.244 M/sec                    (49.98%)
     2,359,227,874      LLC-loads                 #    8.184 M/sec                    (41.67%)
         2,010,217      LLC-store-misses          #    6.973 K/sec                    (25.01%)
       523,876,317      LLC-store-misses          #    1.817 M/sec                    (33.35%)
       136,721,141      LLC-stores                #  474.272 K/sec                    (25.01%)
     1,296,685,291      LLC-stores                #    4.498 M/sec                    (33.34%)
        288,276.00 msec task-clock                #    8.000 CPUs utilized          
    80,385,460,656      cycles:u                  #    0.279 GHz                      (41.68%)
   484,439,699,144      cycles:k                  #    1.680 GHz                      (41.67%)
    48,415,135,491      instructions:u            #    0.60  insn per cycle           (50.00%)
   290,108,887,083      instructions:k            #    0.60  insn per cycle           (49.98%)

      36.035583173 seconds time elapsed

