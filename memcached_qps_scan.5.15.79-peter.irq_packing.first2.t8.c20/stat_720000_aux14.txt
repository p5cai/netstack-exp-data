# started on Thu Apr  6 01:06:20 2023


 Performance counter stats for 'CPU(s) 2-9':

        75,664,202      LLC-load-misses           #   21.33% of all LL-cache accesses  (49.98%)
     1,193,938,546      LLC-load-misses           #   50.83% of all LL-cache accesses  (50.00%)
       354,665,280      LLC-loads                 #    1.228 M/sec                    (50.01%)
     2,348,940,644      LLC-loads                 #    8.131 M/sec                    (41.69%)
            58,451      LLC-store-misses          #  202.342 /sec                     (25.01%)
       510,870,610      LLC-store-misses          #    1.769 M/sec                    (33.34%)
       131,241,823      LLC-stores                #  454.325 K/sec                    (24.99%)
     1,230,967,215      LLC-stores                #    4.261 M/sec                    (33.32%)
        288,871.91 msec task-clock                #    8.000 CPUs utilized          
    78,848,822,325      cycles:u                  #    0.273 GHz                      (41.65%)
   485,572,353,471      cycles:k                  #    1.681 GHz                      (41.65%)
    47,882,590,275      instructions:u            #    0.61  insn per cycle           (49.98%)
   288,729,651,608      instructions:k            #    0.59  insn per cycle           (49.98%)

      36.110089442 seconds time elapsed

