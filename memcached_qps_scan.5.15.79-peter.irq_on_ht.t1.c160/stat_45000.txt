# started on Fri Mar 10 15:02:48 2023


 Performance counter stats for 'system wide':

        14,746,432      LLC-load-misses           #   22.54% of all LL-cache accesses  (49.98%)
        70,259,955      LLC-load-misses           #   15.07% of all LL-cache accesses  (50.00%)
        65,437,781      LLC-loads                 #   61.460 K/sec                    (50.01%)
       466,353,037      LLC-loads                 #  438.002 K/sec                    (41.69%)
         2,521,758      LLC-store-misses          #    2.368 K/sec                    (25.01%)
        49,706,951      LLC-store-misses          #   46.685 K/sec                    (33.34%)
        24,113,260      LLC-stores                #   22.647 K/sec                    (24.99%)
       179,104,940      LLC-stores                #  168.216 K/sec                    (33.32%)
      1,064,729.28 msec task-clock                #   31.999 CPUs utilized          
    14,819,640,661      cycles:u                  #    0.014 GHz                      (41.65%)
   117,043,946,281      cycles:k                  #    0.110 GHz                      (41.65%)
     4,515,984,251      instructions:u            #    0.30  insn per cycle           (49.98%)
    42,217,075,677      instructions:k            #    0.36  insn per cycle           (49.98%)

      33.273373470 seconds time elapsed

