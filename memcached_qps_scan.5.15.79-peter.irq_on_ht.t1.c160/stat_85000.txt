# started on Fri Mar 10 15:11:56 2023


 Performance counter stats for 'system wide':

        33,627,245      LLC-load-misses           #   44.07% of all LL-cache accesses  (49.99%)
       200,711,164      LLC-load-misses           #   33.99% of all LL-cache accesses  (50.00%)
        76,304,616      LLC-loads                 #   71.678 K/sec                    (50.01%)
       590,491,551      LLC-loads                 #  554.686 K/sec                    (41.68%)
         4,465,952      LLC-store-misses          #    4.195 K/sec                    (25.00%)
       158,671,651      LLC-store-misses          #  149.050 K/sec                    (33.34%)
        34,409,589      LLC-stores                #   32.323 K/sec                    (25.00%)
       331,287,648      LLC-stores                #  311.200 K/sec                    (33.33%)
      1,064,550.68 msec task-clock                #   32.000 CPUs utilized          
    18,401,936,045      cycles:u                  #    0.017 GHz                      (41.66%)
   150,290,866,682      cycles:k                  #    0.141 GHz                      (41.65%)
     6,550,469,197      instructions:u            #    0.36  insn per cycle           (49.99%)
    54,537,359,723      instructions:k            #    0.36  insn per cycle           (49.99%)

      33.267626999 seconds time elapsed

