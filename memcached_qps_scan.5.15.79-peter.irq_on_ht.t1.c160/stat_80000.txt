# started on Fri Mar 10 15:09:55 2023


 Performance counter stats for 'system wide':

        34,634,549      LLC-load-misses           #   43.75% of all LL-cache accesses  (49.99%)
       216,163,555      LLC-load-misses           #   36.67% of all LL-cache accesses  (50.00%)
        79,160,339      LLC-loads                 #   66.065 K/sec                    (50.01%)
       589,429,093      LLC-loads                 #  491.924 K/sec                    (41.68%)
         5,645,421      LLC-store-misses          #    4.712 K/sec                    (25.00%)
       173,430,281      LLC-store-misses          #  144.741 K/sec                    (33.34%)
        32,851,360      LLC-stores                #   27.417 K/sec                    (25.00%)
       341,585,619      LLC-stores                #  285.080 K/sec                    (33.33%)
      1,198,210.63 msec task-clock                #   31.999 CPUs utilized          
    19,048,166,611      cycles:u                  #    0.016 GHz                      (41.66%)
   150,751,994,836      cycles:k                  #    0.126 GHz                      (41.65%)
     6,575,406,753      instructions:u            #    0.35  insn per cycle           (49.98%)
    54,086,002,303      instructions:k            #    0.36  insn per cycle           (49.98%)

      37.445128385 seconds time elapsed

