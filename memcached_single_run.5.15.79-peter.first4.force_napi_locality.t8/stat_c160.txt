# started on Thu Mar 23 08:46:52 2023


 Performance counter stats for 'CPU(s) 4-11':

       228,243,707      LLC-load-misses           #   50.33% of all LL-cache accesses  (49.97%)
     1,424,920,204      LLC-load-misses           #   50.91% of all LL-cache accesses  (49.97%)
       453,523,034      LLC-loads                 #    1.567 M/sec                    (49.99%)
     2,798,868,337      LLC-loads                 #    9.673 M/sec                    (41.67%)
        31,819,004      LLC-store-misses          #  109.964 K/sec                    (25.02%)
       929,671,779      LLC-store-misses          #    3.213 M/sec                    (33.36%)
       166,434,169      LLC-stores                #  575.186 K/sec                    (25.01%)
     1,742,820,411      LLC-stores                #    6.023 M/sec                    (33.35%)
        289,357.23 msec task-clock                #    8.000 CPUs utilized          
    90,245,263,632      cycles:u                  #    0.312 GHz                      (41.67%)
   557,251,929,546      cycles:k                  #    1.926 GHz                      (41.66%)
    42,806,692,207      instructions:u            #    0.47  insn per cycle           (49.99%)
   267,992,304,723      instructions:k            #    0.48  insn per cycle           (49.98%)

      36.170779087 seconds time elapsed

