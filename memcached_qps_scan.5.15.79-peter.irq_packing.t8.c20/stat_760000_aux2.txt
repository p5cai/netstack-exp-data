# started on Fri Mar 31 22:58:29 2023


 Performance counter stats for 'CPU(s) 0-7':

       106,112,112      LLC-load-misses           #   22.03% of all LL-cache accesses  (49.99%)
       130,085,706      LLC-load-misses           #    4.09% of all LL-cache accesses  (50.01%)
       481,573,050      LLC-loads                 #    1.667 M/sec                    (50.01%)
     3,180,677,746      LLC-loads                 #   11.012 M/sec                    (41.68%)
         2,479,447      LLC-store-misses          #    8.584 K/sec                    (25.00%)
       119,897,458      LLC-store-misses          #  415.101 K/sec                    (33.33%)
       186,329,057      LLC-stores                #  645.096 K/sec                    (24.99%)
     2,256,440,522      LLC-stores                #    7.812 M/sec                    (33.32%)
        288,839.28 msec task-clock                #    8.000 CPUs utilized          
   109,299,680,479      cycles:u                  #    0.378 GHz                      (41.65%)
   504,894,218,503      cycles:k                  #    1.748 GHz                      (41.65%)
    70,270,935,137      instructions:u            #    0.64  insn per cycle           (49.98%)
   394,550,917,354      instructions:k            #    0.78  insn per cycle           (49.98%)

      36.105941362 seconds time elapsed

