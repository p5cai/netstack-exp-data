# started on Fri Mar 31 23:58:36 2023


 Performance counter stats for 'CPU(s) 0-7':

       124,265,702      LLC-load-misses           #   22.85% of all LL-cache accesses  (49.98%)
       155,248,947      LLC-load-misses           #    4.57% of all LL-cache accesses  (49.98%)
       543,782,146      LLC-loads                 #    1.881 M/sec                    (49.99%)
     3,396,587,987      LLC-loads                 #   11.752 M/sec                    (41.68%)
           545,168      LLC-store-misses          #    1.886 K/sec                    (25.01%)
       267,858,534      LLC-store-misses          #  926.760 K/sec                    (33.35%)
       178,096,245      LLC-stores                #  616.193 K/sec                    (25.01%)
     2,656,511,954      LLC-stores                #    9.191 M/sec                    (33.35%)
        289,026.79 msec task-clock                #    8.000 CPUs utilized          
   114,400,715,079      cycles:u                  #    0.396 GHz                      (41.68%)
   522,335,529,695      cycles:k                  #    1.807 GHz                      (41.66%)
    75,830,410,641      instructions:u            #    0.66  insn per cycle           (49.99%)
   421,324,261,721      instructions:k            #    0.81  insn per cycle           (49.98%)

      36.129426432 seconds time elapsed

