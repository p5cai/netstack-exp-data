# started on Fri Mar 31 23:13:48 2023


 Performance counter stats for 'CPU(s) 0-7':

        94,454,865      LLC-load-misses           #   21.00% of all LL-cache accesses  (49.97%)
       115,456,974      LLC-load-misses           #    3.88% of all LL-cache accesses  (49.97%)
       449,831,395      LLC-loads                 #    1.554 M/sec                    (49.98%)
     2,972,571,175      LLC-loads                 #   10.272 M/sec                    (41.67%)
         2,303,923      LLC-store-misses          #    7.961 K/sec                    (25.02%)
       109,691,731      LLC-store-misses          #  379.042 K/sec                    (33.35%)
       166,236,356      LLC-stores                #  574.433 K/sec                    (25.01%)
     2,090,060,790      LLC-stores                #    7.222 M/sec                    (33.35%)
        289,392.28 msec task-clock                #    8.000 CPUs utilized          
   100,610,555,579      cycles:u                  #    0.348 GHz                      (41.68%)
   489,014,963,259      cycles:k                  #    1.690 GHz                      (41.66%)
    62,205,621,587      instructions:u            #    0.62  insn per cycle           (49.99%)
   371,550,295,303      instructions:k            #    0.76  insn per cycle           (49.98%)

      36.175104749 seconds time elapsed

