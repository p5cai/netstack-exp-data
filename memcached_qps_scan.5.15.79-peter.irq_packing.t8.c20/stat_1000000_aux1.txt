# started on Fri Mar 31 22:46:34 2023


 Performance counter stats for 'CPU(s) 0-7':

       121,815,717      LLC-load-misses           #   21.75% of all LL-cache accesses  (49.97%)
       153,155,424      LLC-load-misses           #    4.51% of all LL-cache accesses  (49.98%)
       559,997,238      LLC-loads                 #    1.940 M/sec                    (49.99%)
     3,397,798,625      LLC-loads                 #   11.774 M/sec                    (41.68%)
         2,371,775      LLC-store-misses          #    8.218 K/sec                    (25.02%)
       244,349,428      LLC-store-misses          #  846.681 K/sec                    (33.36%)
       206,145,622      LLC-stores                #  714.303 K/sec                    (25.01%)
     2,562,616,699      LLC-stores                #    8.880 M/sec                    (33.35%)
        288,596.80 msec task-clock                #    8.000 CPUs utilized          
   114,282,976,574      cycles:u                  #    0.396 GHz                      (41.67%)
   522,632,054,045      cycles:k                  #    1.811 GHz                      (41.66%)
    75,831,223,287      instructions:u            #    0.66  insn per cycle           (49.98%)
   420,360,528,829      instructions:k            #    0.80  insn per cycle           (49.97%)

      36.075630713 seconds time elapsed

