#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1313.4   342.4   319.3  1218.8  1273.0  1329.6  1386.2  1393.2  1398.9
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5602228.4 85371.3 5324898.6 5131979.0 5186866.5 5623764.4 6039377.5 6091329.2 6132890.5

Total QPS = 852072.4 (25562177 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6264711255 bytes :  199.1 MB/s
TX 1079367696 bytes :   34.3 MB/s
