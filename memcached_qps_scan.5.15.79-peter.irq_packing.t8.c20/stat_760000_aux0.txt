# started on Fri Mar 31 22:23:11 2023


 Performance counter stats for 'CPU(s) 0-7':

       105,687,788      LLC-load-misses           #   22.44% of all LL-cache accesses  (49.99%)
       124,585,367      LLC-load-misses           #    3.93% of all LL-cache accesses  (49.99%)
       471,054,119      LLC-loads                 #    1.632 M/sec                    (49.99%)
     3,169,378,274      LLC-loads                 #   10.977 M/sec                    (41.66%)
         2,379,812      LLC-store-misses          #    8.243 K/sec                    (25.01%)
       117,593,084      LLC-store-misses          #  407.296 K/sec                    (33.34%)
       177,543,243      LLC-stores                #  614.940 K/sec                    (25.00%)
     2,254,525,372      LLC-stores                #    7.809 M/sec                    (33.34%)
        288,716.29 msec task-clock                #    8.000 CPUs utilized          
   115,291,764,386      cycles:u                  #    0.399 GHz                      (41.67%)
   501,817,898,466      cycles:k                  #    1.738 GHz                      (41.67%)
    68,608,167,418      instructions:u            #    0.60  insn per cycle           (50.00%)
   392,017,267,402      instructions:k            #    0.78  insn per cycle           (49.99%)

      36.090591285 seconds time elapsed

