#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       49.5    10.9    29.5    38.2    39.9    48.3    59.6    64.4    76.7
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      2101370.5 51590.0 2052978.9 1968952.9 1980467.3 2072582.0 2224955.2 2296727.9 2354146.1

Total QPS = 319980.9 (9599428 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 2354753857 bytes :   74.9 MB/s
TX  405272255 bytes :   12.9 MB/s
