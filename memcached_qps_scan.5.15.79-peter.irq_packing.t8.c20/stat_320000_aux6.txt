# started on Sat Apr  1 00:01:00 2023


 Performance counter stats for 'CPU(s) 0-7':

        44,510,357      LLC-load-misses           #   19.21% of all LL-cache accesses  (49.97%)
        62,954,182      LLC-load-misses           #    3.71% of all LL-cache accesses  (49.97%)
       231,715,793      LLC-loads                 #  802.865 K/sec                    (49.99%)
     1,697,610,960      LLC-loads                 #    5.882 M/sec                    (41.68%)
         2,200,314      LLC-store-misses          #    7.624 K/sec                    (25.02%)
        43,589,556      LLC-store-misses          #  151.032 K/sec                    (33.35%)
        96,810,239      LLC-stores                #  335.435 K/sec                    (25.01%)
     1,143,816,095      LLC-stores                #    3.963 M/sec                    (33.35%)
        288,610.99 msec task-clock                #    8.000 CPUs utilized          
    59,748,744,101      cycles:u                  #    0.207 GHz                      (41.67%)
   363,271,339,632      cycles:k                  #    1.259 GHz                      (41.66%)
    30,733,362,355      instructions:u            #    0.51  insn per cycle           (49.99%)
   241,343,903,948      instructions:k            #    0.66  insn per cycle           (49.98%)

      36.077448064 seconds time elapsed

