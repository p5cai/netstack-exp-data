# started on Sat Apr  1 00:19:30 2023


 Performance counter stats for 'CPU(s) 0-7':

        66,242,208      LLC-load-misses           #   20.27% of all LL-cache accesses  (49.98%)
        90,282,667      LLC-load-misses           #    3.80% of all LL-cache accesses  (49.98%)
       326,746,086      LLC-loads                 #    1.127 M/sec                    (49.98%)
     2,378,968,757      LLC-loads                 #    8.208 M/sec                    (41.67%)
         2,188,738      LLC-store-misses          #    7.552 K/sec                    (25.01%)
       103,613,661      LLC-store-misses          #  357.509 K/sec                    (33.35%)
       141,230,964      LLC-stores                #  487.304 K/sec                    (25.01%)
     1,619,431,083      LLC-stores                #    5.588 M/sec                    (33.34%)
        289,821.35 msec task-clock                #    8.000 CPUs utilized          
    80,287,559,916      cycles:u                  #    0.277 GHz                      (41.68%)
   431,535,079,493      cycles:k                  #    1.489 GHz                      (41.67%)
    44,870,285,408      instructions:u            #    0.56  insn per cycle           (50.00%)
   301,404,305,735      instructions:k            #    0.70  insn per cycle           (49.99%)

      36.228747158 seconds time elapsed

