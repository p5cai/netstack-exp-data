# started on Sat Apr  1 02:04:30 2023


 Performance counter stats for 'CPU(s) 0-7':

        43,761,694      LLC-load-misses           #   19.46% of all LL-cache accesses  (49.98%)
        63,982,194      LLC-load-misses           #    3.81% of all LL-cache accesses  (49.98%)
       224,859,027      LLC-loads                 #  776.854 K/sec                    (49.99%)
     1,677,279,555      LLC-loads                 #    5.795 M/sec                    (41.66%)
         2,128,119      LLC-store-misses          #    7.352 K/sec                    (25.01%)
        44,661,572      LLC-store-misses          #  154.299 K/sec                    (33.35%)
       101,621,129      LLC-stores                #  351.086 K/sec                    (25.01%)
     1,139,011,659      LLC-stores                #    3.935 M/sec                    (33.34%)
        289,448.32 msec task-clock                #    8.000 CPUs utilized          
    58,747,189,112      cycles:u                  #    0.203 GHz                      (41.68%)
   359,219,406,482      cycles:k                  #    1.241 GHz                      (41.67%)
    30,615,448,082      instructions:u            #    0.52  insn per cycle           (50.00%)
   241,728,591,246      instructions:k            #    0.67  insn per cycle           (49.99%)

      36.182126794 seconds time elapsed

