# started on Fri Mar 31 22:05:33 2023


 Performance counter stats for 'CPU(s) 0-7':

       107,161,052      LLC-load-misses           #   22.92% of all LL-cache accesses  (49.97%)
       124,941,606      LLC-load-misses           #    3.98% of all LL-cache accesses  (49.98%)
       467,581,880      LLC-loads                 #    1.618 M/sec                    (50.00%)
     3,135,931,372      LLC-loads                 #   10.854 M/sec                    (41.68%)
         2,512,550      LLC-store-misses          #    8.697 K/sec                    (25.02%)
       133,868,320      LLC-store-misses          #  463.356 K/sec                    (33.36%)
       179,699,585      LLC-stores                #  621.991 K/sec                    (25.01%)
     2,237,879,054      LLC-stores                #    7.746 M/sec                    (33.33%)
        288,910.33 msec task-clock                #    8.000 CPUs utilized          
   121,876,960,503      cycles:u                  #    0.422 GHz                      (41.66%)
   512,444,390,152      cycles:k                  #    1.774 GHz                      (41.65%)
    67,535,004,327      instructions:u            #    0.55  insn per cycle           (49.98%)
   380,717,121,984      instructions:k            #    0.74  insn per cycle           (49.97%)

      36.114868141 seconds time elapsed

