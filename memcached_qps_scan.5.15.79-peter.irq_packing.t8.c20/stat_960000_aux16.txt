# started on Sat Apr  1 03:10:23 2023


 Performance counter stats for 'CPU(s) 0-7':

       120,957,639      LLC-load-misses           #   22.35% of all LL-cache accesses  (49.99%)
       148,910,093      LLC-load-misses           #    4.41% of all LL-cache accesses  (50.01%)
       541,241,332      LLC-loads                 #    1.869 M/sec                    (50.01%)
     3,374,345,896      LLC-loads                 #   11.652 M/sec                    (41.68%)
         2,387,614      LLC-store-misses          #    8.245 K/sec                    (25.00%)
       243,211,409      LLC-store-misses          #  839.869 K/sec                    (33.33%)
       205,842,787      LLC-stores                #  710.826 K/sec                    (25.00%)
     2,583,548,958      LLC-stores                #    8.922 M/sec                    (33.33%)
        289,582.50 msec task-clock                #    8.000 CPUs utilized          
   115,431,396,243      cycles:u                  #    0.399 GHz                      (41.66%)
   521,434,033,707      cycles:k                  #    1.801 GHz                      (41.65%)
    75,331,993,687      instructions:u            #    0.65  insn per cycle           (49.99%)
   417,453,133,705      instructions:k            #    0.80  insn per cycle           (49.99%)

      36.198905737 seconds time elapsed

