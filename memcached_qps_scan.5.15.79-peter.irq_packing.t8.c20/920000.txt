#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1326.8   117.9   263.9  1272.3  1279.1  1333.7  1388.3  1395.1  1482.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5534628.8 100836.3 5324898.6 5113683.2 5150274.9 5443008.1 5960919.9 6052100.4 6125044.7

Total QPS = 843440.6 (25303230 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6200788103 bytes :  197.1 MB/s
TX 1068438844 bytes :   34.0 MB/s
