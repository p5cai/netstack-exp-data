# started on Fri Mar 31 23:55:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       125,140,452      LLC-load-misses           #   23.75% of all LL-cache accesses  (49.97%)
       157,408,105      LLC-load-misses           #    4.70% of all LL-cache accesses  (49.98%)
       526,998,589      LLC-loads                 #    1.824 M/sec                    (49.99%)
     3,348,891,703      LLC-loads                 #   11.589 M/sec                    (41.68%)
         2,510,199      LLC-store-misses          #    8.687 K/sec                    (25.02%)
       302,693,750      LLC-store-misses          #    1.047 M/sec                    (33.36%)
       196,049,319      LLC-stores                #  678.440 K/sec                    (25.01%)
     2,633,615,068      LLC-stores                #    9.114 M/sec                    (33.34%)
        288,970.90 msec task-clock                #    8.000 CPUs utilized          
   120,275,960,566      cycles:u                  #    0.416 GHz                      (41.67%)
   523,111,094,736      cycles:k                  #    1.810 GHz                      (41.66%)
    75,987,153,153      instructions:u            #    0.63  insn per cycle           (49.98%)
   414,100,501,592      instructions:k            #    0.79  insn per cycle           (49.97%)

      36.122436624 seconds time elapsed

