# started on Sat Apr  1 03:58:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       105,851,927      LLC-load-misses           #   22.02% of all LL-cache accesses  (49.99%)
       124,691,493      LLC-load-misses           #    3.89% of all LL-cache accesses  (50.00%)
       480,602,851      LLC-loads                 #    1.666 M/sec                    (50.01%)
     3,208,091,118      LLC-loads                 #   11.123 M/sec                    (41.68%)
         2,515,987      LLC-store-misses          #    8.723 K/sec                    (25.00%)
       114,272,987      LLC-store-misses          #  396.189 K/sec                    (33.34%)
       186,549,654      LLC-stores                #  646.775 K/sec                    (24.99%)
     2,263,605,712      LLC-stores                #    7.848 M/sec                    (33.33%)
        288,430.47 msec task-clock                #    8.000 CPUs utilized          
   109,348,554,897      cycles:u                  #    0.379 GHz                      (41.66%)
   504,564,673,608      cycles:k                  #    1.749 GHz                      (41.65%)
    70,062,136,868      instructions:u            #    0.64  insn per cycle           (49.99%)
   394,621,131,938      instructions:k            #    0.78  insn per cycle           (49.99%)

      36.054862069 seconds time elapsed

