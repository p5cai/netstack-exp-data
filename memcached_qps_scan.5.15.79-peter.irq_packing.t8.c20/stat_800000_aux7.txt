# started on Sat Apr  1 00:27:32 2023


 Performance counter stats for 'CPU(s) 0-7':

       112,920,785      LLC-load-misses           #   21.59% of all LL-cache accesses  (49.98%)
       130,307,819      LLC-load-misses           #    3.92% of all LL-cache accesses  (49.99%)
       522,932,492      LLC-loads                 #    1.815 M/sec                    (50.01%)
     3,323,903,899      LLC-loads                 #   11.537 M/sec                    (41.69%)
         2,571,826      LLC-store-misses          #    8.927 K/sec                    (25.02%)
       118,975,323      LLC-store-misses          #  412.955 K/sec                    (33.35%)
       190,675,577      LLC-stores                #  661.821 K/sec                    (24.99%)
     2,345,180,932      LLC-stores                #    8.140 M/sec                    (33.32%)
        288,107.53 msec task-clock                #    8.000 CPUs utilized          
   110,305,478,067      cycles:u                  #    0.383 GHz                      (41.65%)
   512,073,019,682      cycles:k                  #    1.777 GHz                      (41.65%)
    72,398,560,193      instructions:u            #    0.66  insn per cycle           (49.98%)
   406,015,077,985      instructions:k            #    0.79  insn per cycle           (49.98%)

      36.014493166 seconds time elapsed

