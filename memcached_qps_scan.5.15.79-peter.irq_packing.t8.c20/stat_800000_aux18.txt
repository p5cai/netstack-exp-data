# started on Sat Apr  1 03:41:38 2023


 Performance counter stats for 'CPU(s) 0-7':

       112,276,780      LLC-load-misses           #   21.87% of all LL-cache accesses  (49.99%)
       126,971,188      LLC-load-misses           #    3.83% of all LL-cache accesses  (50.00%)
       513,317,983      LLC-loads                 #    1.770 M/sec                    (50.02%)
     3,313,731,731      LLC-loads                 #   11.427 M/sec                    (41.69%)
         2,448,826      LLC-store-misses          #    8.444 K/sec                    (25.01%)
       116,670,795      LLC-store-misses          #  402.316 K/sec                    (33.34%)
       198,464,686      LLC-stores                #  684.366 K/sec                    (24.99%)
     2,342,809,375      LLC-stores                #    8.079 M/sec                    (33.32%)
        289,998.08 msec task-clock                #    8.000 CPUs utilized          
   111,033,250,865      cycles:u                  #    0.383 GHz                      (41.65%)
   510,757,299,637      cycles:k                  #    1.761 GHz                      (41.65%)
    71,861,969,199      instructions:u            #    0.65  insn per cycle           (49.98%)
   405,538,582,455      instructions:k            #    0.79  insn per cycle           (49.98%)

      36.250855597 seconds time elapsed

