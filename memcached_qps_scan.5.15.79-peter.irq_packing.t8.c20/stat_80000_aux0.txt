# started on Fri Mar 31 22:14:17 2023


 Performance counter stats for 'CPU(s) 0-7':

        10,762,966      LLC-load-misses           #   20.10% of all LL-cache accesses  (49.99%)
        19,506,165      LLC-load-misses           #    3.26% of all LL-cache accesses  (49.99%)
        53,536,623      LLC-loads                 #  184.949 K/sec                    (49.99%)
       598,773,943      LLC-loads                 #    2.069 M/sec                    (41.66%)
            48,730      LLC-store-misses          #  168.344 /sec                     (25.01%)
         9,204,800      LLC-store-misses          #   31.799 K/sec                    (33.34%)
        24,210,553      LLC-stores                #   83.638 K/sec                    (25.00%)
       334,285,191      LLC-stores                #    1.155 M/sec                    (33.34%)
        289,467.17 msec task-clock                #    8.000 CPUs utilized          
    17,221,795,358      cycles:u                  #    0.059 GHz                      (41.67%)
   142,348,971,719      cycles:k                  #    0.492 GHz                      (41.67%)
     7,689,306,257      instructions:u            #    0.45  insn per cycle           (50.00%)
    90,464,215,032      instructions:k            #    0.64  insn per cycle           (49.99%)

      36.184296277 seconds time elapsed

