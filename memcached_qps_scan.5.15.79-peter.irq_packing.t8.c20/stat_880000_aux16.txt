# started on Sat Apr  1 03:08:40 2023


 Performance counter stats for 'CPU(s) 0-7':

       121,656,806      LLC-load-misses           #   22.98% of all LL-cache accesses  (49.97%)
       151,429,906      LLC-load-misses           #    4.49% of all LL-cache accesses  (49.97%)
       529,410,023      LLC-loads                 #    1.832 M/sec                    (49.98%)
     3,371,332,512      LLC-loads                 #   11.665 M/sec                    (41.67%)
           551,726      LLC-store-misses          #    1.909 K/sec                    (25.02%)
       258,602,068      LLC-store-misses          #  894.786 K/sec                    (33.35%)
       196,265,698      LLC-stores                #  679.097 K/sec                    (25.01%)
     2,594,495,578      LLC-stores                #    8.977 M/sec                    (33.35%)
        289,009.91 msec task-clock                #    8.000 CPUs utilized          
   114,573,517,332      cycles:u                  #    0.396 GHz                      (41.68%)
   521,688,362,512      cycles:k                  #    1.805 GHz                      (41.67%)
    74,876,907,221      instructions:u            #    0.65  insn per cycle           (49.99%)
   416,440,432,239      instructions:k            #    0.80  insn per cycle           (49.98%)

      36.127217343 seconds time elapsed

