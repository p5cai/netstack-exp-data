# started on Sat Apr  1 03:05:30 2023


 Performance counter stats for 'CPU(s) 0-7':

       107,055,136      LLC-load-misses           #   21.67% of all LL-cache accesses  (50.00%)
       123,947,285      LLC-load-misses           #    3.86% of all LL-cache accesses  (50.00%)
       494,093,990      LLC-loads                 #    1.713 M/sec                    (50.00%)
     3,207,272,617      LLC-loads                 #   11.121 M/sec                    (41.67%)
         2,459,043      LLC-store-misses          #    8.527 K/sec                    (25.00%)
       112,522,347      LLC-store-misses          #  390.171 K/sec                    (33.34%)
       174,943,099      LLC-stores                #  606.615 K/sec                    (25.00%)
     2,260,225,680      LLC-stores                #    7.837 M/sec                    (33.33%)
        288,392.15 msec task-clock                #    8.000 CPUs utilized          
   107,763,271,959      cycles:u                  #    0.374 GHz                      (41.66%)
   506,209,519,301      cycles:k                  #    1.755 GHz                      (41.66%)
    69,493,149,392      instructions:u            #    0.64  insn per cycle           (49.99%)
   393,966,496,854      instructions:k            #    0.78  insn per cycle           (49.99%)

      36.050104384 seconds time elapsed

