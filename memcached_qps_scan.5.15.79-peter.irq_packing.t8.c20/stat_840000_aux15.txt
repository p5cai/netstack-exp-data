# started on Sat Apr  1 02:49:34 2023


 Performance counter stats for 'CPU(s) 0-7':

       116,241,119      LLC-load-misses           #   21.45% of all LL-cache accesses  (49.97%)
       136,647,386      LLC-load-misses           #    4.01% of all LL-cache accesses  (49.98%)
       542,030,546      LLC-loads                 #    1.883 M/sec                    (49.99%)
     3,406,661,521      LLC-loads                 #   11.836 M/sec                    (41.68%)
         2,411,462      LLC-store-misses          #    8.378 K/sec                    (25.02%)
       117,463,025      LLC-store-misses          #  408.094 K/sec                    (33.35%)
       187,023,384      LLC-stores                #  649.763 K/sec                    (25.01%)
     2,400,325,023      LLC-stores                #    8.339 M/sec                    (33.34%)
        287,833.39 msec task-clock                #    8.000 CPUs utilized          
   111,778,984,012      cycles:u                  #    0.388 GHz                      (41.67%)
   519,510,425,733      cycles:k                  #    1.805 GHz                      (41.66%)
    74,857,566,442      instructions:u            #    0.67  insn per cycle           (49.98%)
   417,660,730,930      instructions:k            #    0.80  insn per cycle           (49.97%)

      35.980276927 seconds time elapsed

