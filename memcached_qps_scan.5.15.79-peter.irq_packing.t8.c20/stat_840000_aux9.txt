# started on Sat Apr  1 01:03:40 2023


 Performance counter stats for 'CPU(s) 0-7':

       118,212,986      LLC-load-misses           #   21.91% of all LL-cache accesses  (49.99%)
       135,607,953      LLC-load-misses           #    3.98% of all LL-cache accesses  (50.00%)
       539,422,934      LLC-loads                 #    1.861 M/sec                    (50.00%)
     3,406,103,731      LLC-loads                 #   11.749 M/sec                    (41.67%)
         2,564,848      LLC-store-misses          #    8.847 K/sec                    (25.01%)
       120,732,370      LLC-store-misses          #  416.462 K/sec                    (33.34%)
       197,941,872      LLC-stores                #  682.794 K/sec                    (25.00%)
     2,411,003,506      LLC-stores                #    8.317 M/sec                    (33.33%)
        289,899.86 msec task-clock                #    8.000 CPUs utilized          
   114,230,310,130      cycles:u                  #    0.394 GHz                      (41.67%)
   519,321,909,561      cycles:k                  #    1.791 GHz                      (41.66%)
    75,398,178,983      instructions:u            #    0.66  insn per cycle           (50.00%)
   417,489,701,939      instructions:k            #    0.80  insn per cycle           (50.00%)

      36.238527475 seconds time elapsed

