# started on Sat Apr  1 00:53:04 2023


 Performance counter stats for 'CPU(s) 0-7':

        10,727,801      LLC-load-misses           #   18.08% of all LL-cache accesses  (50.00%)
        19,156,075      LLC-load-misses           #    3.15% of all LL-cache accesses  (50.01%)
        59,343,246      LLC-loads                 #  205.483 K/sec                    (50.01%)
       609,063,743      LLC-loads                 #    2.109 M/sec                    (41.68%)
            91,006      LLC-store-misses          #  315.118 /sec                     (25.00%)
         9,206,077      LLC-store-misses          #   31.877 K/sec                    (33.33%)
        25,610,361      LLC-stores                #   88.679 K/sec                    (25.00%)
       336,757,825      LLC-stores                #    1.166 M/sec                    (33.33%)
        288,799.43 msec task-clock                #    8.000 CPUs utilized          
    17,388,958,040      cycles:u                  #    0.060 GHz                      (41.66%)
   142,693,832,247      cycles:k                  #    0.494 GHz                      (41.66%)
     7,704,973,538      instructions:u            #    0.44  insn per cycle           (49.99%)
    90,334,057,695      instructions:k            #    0.63  insn per cycle           (49.99%)

      36.100994584 seconds time elapsed

