# started on Sat Apr  1 00:26:41 2023


 Performance counter stats for 'CPU(s) 0-7':

       106,704,529      LLC-load-misses           #   21.76% of all LL-cache accesses  (50.00%)
       125,658,088      LLC-load-misses           #    3.90% of all LL-cache accesses  (50.00%)
       490,312,899      LLC-loads                 #    1.696 M/sec                    (50.01%)
     3,223,893,282      LLC-loads                 #   11.149 M/sec                    (41.68%)
         2,552,813      LLC-store-misses          #    8.828 K/sec                    (25.00%)
       116,186,151      LLC-store-misses          #  401.787 K/sec                    (33.33%)
       184,025,346      LLC-stores                #  636.384 K/sec                    (25.00%)
     2,258,573,165      LLC-stores                #    7.810 M/sec                    (33.33%)
        289,173.56 msec task-clock                #    8.000 CPUs utilized          
   107,052,705,651      cycles:u                  #    0.370 GHz                      (41.66%)
   505,286,889,372      cycles:k                  #    1.747 GHz                      (41.66%)
    68,689,390,720      instructions:u            #    0.64  insn per cycle           (49.99%)
   394,144,558,907      instructions:k            #    0.78  insn per cycle           (49.99%)

      36.147734709 seconds time elapsed

