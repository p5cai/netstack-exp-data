#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1340.3    43.9   239.9  1275.4  1282.4  1338.4  1394.5  1436.3  1524.0
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5505589.6 163037.3 5324898.6 5110113.3 5143135.0 5407308.9 5929744.3 6061151.4 6429967.2

Total QPS = 834410.6 (25032324 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6167464254 bytes :  196.1 MB/s
TX 1057051468 bytes :   33.6 MB/s
