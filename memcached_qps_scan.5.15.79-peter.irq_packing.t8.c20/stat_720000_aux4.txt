# started on Fri Mar 31 23:32:54 2023


 Performance counter stats for 'CPU(s) 0-7':

        99,905,917      LLC-load-misses           #   21.81% of all LL-cache accesses  (49.97%)
       120,157,235      LLC-load-misses           #    3.90% of all LL-cache accesses  (49.98%)
       458,148,947      LLC-loads                 #    1.588 M/sec                    (50.00%)
     3,081,704,292      LLC-loads                 #   10.681 M/sec                    (41.68%)
           482,724      LLC-store-misses          #    1.673 K/sec                    (25.02%)
       112,603,427      LLC-store-misses          #  390.261 K/sec                    (33.36%)
       170,298,508      LLC-stores                #  590.220 K/sec                    (25.01%)
     2,171,083,943      LLC-stores                #    7.525 M/sec                    (33.34%)
        288,533.72 msec task-clock                #    8.000 CPUs utilized          
   106,113,974,467      cycles:u                  #    0.368 GHz                      (41.66%)
   497,975,067,640      cycles:k                  #    1.726 GHz                      (41.65%)
    65,168,663,900      instructions:u            #    0.61  insn per cycle           (49.98%)
   381,827,311,574      instructions:k            #    0.77  insn per cycle           (49.97%)

      36.067787563 seconds time elapsed

