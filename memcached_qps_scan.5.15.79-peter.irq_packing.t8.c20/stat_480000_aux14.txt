# started on Sat Apr  1 02:23:01 2023


 Performance counter stats for 'CPU(s) 0-7':

        66,787,511      LLC-load-misses           #   20.22% of all LL-cache accesses  (49.99%)
        91,378,940      LLC-load-misses           #    3.88% of all LL-cache accesses  (49.99%)
       330,262,002      LLC-loads                 #    1.144 M/sec                    (49.99%)
     2,353,301,657      LLC-loads                 #    8.151 M/sec                    (41.66%)
         2,240,287      LLC-store-misses          #    7.760 K/sec                    (25.01%)
       104,268,794      LLC-store-misses          #  361.158 K/sec                    (33.34%)
       132,452,278      LLC-stores                #  458.778 K/sec                    (25.00%)
     1,617,486,874      LLC-stores                #    5.603 M/sec                    (33.34%)
        288,706.97 msec task-clock                #    8.000 CPUs utilized          
    82,343,827,976      cycles:u                  #    0.285 GHz                      (41.67%)
   434,239,095,917      cycles:k                  #    1.504 GHz                      (41.67%)
    45,052,684,459      instructions:u            #    0.55  insn per cycle           (50.00%)
   300,397,074,933      instructions:k            #    0.69  insn per cycle           (49.99%)

      36.089446375 seconds time elapsed

