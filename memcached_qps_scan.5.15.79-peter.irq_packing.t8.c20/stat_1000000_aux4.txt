# started on Fri Mar 31 23:39:29 2023


 Performance counter stats for 'CPU(s) 0-7':

       123,083,633      LLC-load-misses           #   22.63% of all LL-cache accesses  (49.99%)
       154,455,371      LLC-load-misses           #    4.56% of all LL-cache accesses  (49.99%)
       543,824,964      LLC-loads                 #    1.879 M/sec                    (49.99%)
     3,386,333,222      LLC-loads                 #   11.698 M/sec                    (41.66%)
         2,422,406      LLC-store-misses          #    8.368 K/sec                    (25.01%)
       269,938,782      LLC-store-misses          #  932.499 K/sec                    (33.34%)
       192,259,705      LLC-stores                #  664.158 K/sec                    (25.00%)
     2,593,005,228      LLC-stores                #    8.957 M/sec                    (33.34%)
        289,478.78 msec task-clock                #    8.000 CPUs utilized          
   113,743,485,241      cycles:u                  #    0.393 GHz                      (41.67%)
   523,706,627,386      cycles:k                  #    1.809 GHz                      (41.67%)
    76,534,962,991      instructions:u            #    0.67  insn per cycle           (50.00%)
   419,209,880,840      instructions:k            #    0.80  insn per cycle           (50.00%)

      36.185904917 seconds time elapsed

