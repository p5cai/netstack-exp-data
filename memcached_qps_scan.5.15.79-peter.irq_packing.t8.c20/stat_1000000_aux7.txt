# started on Sat Apr  1 00:32:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       122,824,993      LLC-load-misses           #   22.89% of all LL-cache accesses  (49.97%)
       152,043,034      LLC-load-misses           #    4.51% of all LL-cache accesses  (49.98%)
       536,596,411      LLC-loads                 #    1.854 M/sec                    (49.98%)
     3,374,121,925      LLC-loads                 #   11.659 M/sec                    (41.67%)
         2,545,999      LLC-store-misses          #    8.797 K/sec                    (25.01%)
       290,425,885      LLC-store-misses          #    1.004 M/sec                    (33.35%)
       200,283,095      LLC-stores                #  692.056 K/sec                    (25.01%)
     2,614,109,899      LLC-stores                #    9.033 M/sec                    (33.35%)
        289,403.15 msec task-clock                #    8.000 CPUs utilized          
   114,986,488,072      cycles:u                  #    0.397 GHz                      (41.68%)
   521,496,974,781      cycles:k                  #    1.802 GHz                      (41.67%)
    74,634,473,688      instructions:u            #    0.65  insn per cycle           (49.99%)
   413,870,466,273      instructions:k            #    0.79  insn per cycle           (49.98%)

      36.176466895 seconds time elapsed

