# started on Fri Mar 31 23:02:30 2023


 Performance counter stats for 'CPU(s) 0-7':

       124,273,140      LLC-load-misses           #   23.36% of all LL-cache accesses  (50.00%)
       154,054,186      LLC-load-misses           #    4.55% of all LL-cache accesses  (50.00%)
       532,020,490      LLC-loads                 #    1.842 M/sec                    (50.00%)
     3,384,812,689      LLC-loads                 #   11.721 M/sec                    (41.67%)
         2,482,949      LLC-store-misses          #    8.598 K/sec                    (25.00%)
       266,734,438      LLC-store-misses          #  923.659 K/sec                    (33.33%)
       193,343,543      LLC-stores                #  669.518 K/sec                    (25.00%)
     2,610,260,814      LLC-stores                #    9.039 M/sec                    (33.33%)
        288,780.16 msec task-clock                #    8.000 CPUs utilized          
   116,043,527,819      cycles:u                  #    0.402 GHz                      (41.66%)
   521,695,344,055      cycles:k                  #    1.807 GHz                      (41.66%)
    76,816,374,494      instructions:u            #    0.66  insn per cycle           (49.99%)
   418,871,364,505      instructions:k            #    0.80  insn per cycle           (49.99%)

      36.098549256 seconds time elapsed

