# started on Sat Apr  1 02:09:23 2023


 Performance counter stats for 'CPU(s) 0-7':

        88,359,481      LLC-load-misses           #   21.01% of all LL-cache accesses  (49.97%)
       110,384,042      LLC-load-misses           #    3.87% of all LL-cache accesses  (49.98%)
       420,505,684      LLC-loads                 #    1.446 M/sec                    (50.00%)
     2,855,150,659      LLC-loads                 #    9.816 M/sec                    (41.68%)
         2,299,644      LLC-store-misses          #    7.906 K/sec                    (25.02%)
       107,691,069      LLC-store-misses          #  370.240 K/sec                    (33.36%)
       159,680,789      LLC-stores                #  548.980 K/sec                    (25.01%)
     2,000,802,040      LLC-stores                #    6.879 M/sec                    (33.34%)
        290,868.16 msec task-clock                #    8.000 CPUs utilized          
    97,141,213,949      cycles:u                  #    0.334 GHz                      (41.67%)
   480,014,020,273      cycles:k                  #    1.650 GHz                      (41.65%)
    58,847,630,454      instructions:u            #    0.61  insn per cycle           (49.98%)
   358,132,220,076      instructions:k            #    0.75  insn per cycle           (49.97%)

      36.359620910 seconds time elapsed

