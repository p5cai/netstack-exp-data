# started on Sat Apr  1 00:00:08 2023


 Performance counter stats for 'CPU(s) 0-7':

        10,885,329      LLC-load-misses           #   18.68% of all LL-cache accesses  (49.98%)
        19,709,657      LLC-load-misses           #    3.22% of all LL-cache accesses  (49.98%)
        58,273,462      LLC-loads                 #  201.079 K/sec                    (49.98%)
       612,351,634      LLC-loads                 #    2.113 M/sec                    (41.67%)
           115,571      LLC-store-misses          #  398.791 /sec                     (25.01%)
         9,521,694      LLC-store-misses          #   32.856 K/sec                    (33.35%)
        25,957,913      LLC-stores                #   89.571 K/sec                    (25.01%)
       338,157,636      LLC-stores                #    1.167 M/sec                    (33.35%)
        289,803.19 msec task-clock                #    8.000 CPUs utilized          
    17,657,107,554      cycles:u                  #    0.061 GHz                      (41.68%)
   144,777,199,160      cycles:k                  #    0.500 GHz                      (41.67%)
     7,959,993,461      instructions:u            #    0.45  insn per cycle           (50.00%)
    90,551,358,831      instructions:k            #    0.63  insn per cycle           (49.99%)

      36.226463471 seconds time elapsed

