# started on Sat Apr  1 01:11:33 2023


 Performance counter stats for 'CPU(s) 0-7':

        44,016,235      LLC-load-misses           #   20.46% of all LL-cache accesses  (49.98%)
        61,879,482      LLC-load-misses           #    3.68% of all LL-cache accesses  (50.00%)
       215,100,536      LLC-loads                 #  737.822 K/sec                    (50.01%)
     1,681,838,548      LLC-loads                 #    5.769 M/sec                    (41.69%)
         2,114,035      LLC-store-misses          #    7.251 K/sec                    (25.01%)
        43,135,308      LLC-store-misses          #  147.960 K/sec                    (33.34%)
       102,334,531      LLC-stores                #  351.020 K/sec                    (24.99%)
     1,140,587,843      LLC-stores                #    3.912 M/sec                    (33.32%)
        291,534.43 msec task-clock                #    8.000 CPUs utilized          
    59,230,885,873      cycles:u                  #    0.203 GHz                      (41.65%)
   360,558,962,803      cycles:k                  #    1.237 GHz                      (41.65%)
    30,416,598,289      instructions:u            #    0.51  insn per cycle           (49.98%)
   240,937,843,317      instructions:k            #    0.67  insn per cycle           (49.98%)

      36.442890642 seconds time elapsed

