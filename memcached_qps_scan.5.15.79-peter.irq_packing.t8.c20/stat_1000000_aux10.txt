# started on Sat Apr  1 01:25:21 2023


 Performance counter stats for 'CPU(s) 0-7':

       122,560,420      LLC-load-misses           #   22.61% of all LL-cache accesses  (49.97%)
       151,582,031      LLC-load-misses           #    4.48% of all LL-cache accesses  (49.98%)
       542,036,146      LLC-loads                 #    1.878 M/sec                    (50.00%)
     3,384,612,575      LLC-loads                 #   11.730 M/sec                    (41.68%)
         2,460,738      LLC-store-misses          #    8.528 K/sec                    (25.02%)
       263,578,849      LLC-store-misses          #  913.451 K/sec                    (33.36%)
       199,378,324      LLC-stores                #  690.960 K/sec                    (25.01%)
     2,598,575,133      LLC-stores                #    9.006 M/sec                    (33.34%)
        288,552.75 msec task-clock                #    8.000 CPUs utilized          
   114,271,060,000      cycles:u                  #    0.396 GHz                      (41.66%)
   522,645,652,629      cycles:k                  #    1.811 GHz                      (41.65%)
    75,159,707,951      instructions:u            #    0.66  insn per cycle           (49.98%)
   416,293,806,298      instructions:k            #    0.80  insn per cycle           (49.97%)

      36.070188651 seconds time elapsed

