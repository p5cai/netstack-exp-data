# started on Fri Mar 31 22:53:37 2023


 Performance counter stats for 'CPU(s) 0-7':

        77,296,526      LLC-load-misses           #   21.16% of all LL-cache accesses  (49.99%)
       102,557,094      LLC-load-misses           #    3.94% of all LL-cache accesses  (50.00%)
       365,236,115      LLC-loads                 #    1.265 M/sec                    (50.00%)
     2,601,957,980      LLC-loads                 #    9.011 M/sec                    (41.67%)
           462,932      LLC-store-misses          #    1.603 K/sec                    (25.00%)
       109,467,768      LLC-store-misses          #  379.112 K/sec                    (33.34%)
       144,131,111      LLC-stores                #  499.159 K/sec                    (25.00%)
     1,815,258,715      LLC-stores                #    6.287 M/sec                    (33.34%)
        288,747.85 msec task-clock                #    8.000 CPUs utilized          
    88,129,550,113      cycles:u                  #    0.305 GHz                      (41.67%)
   457,093,492,041      cycles:k                  #    1.583 GHz                      (41.66%)
    51,696,629,236      instructions:u            #    0.59  insn per cycle           (50.00%)
   332,015,968,681      instructions:k            #    0.73  insn per cycle           (50.00%)

      36.094494536 seconds time elapsed

