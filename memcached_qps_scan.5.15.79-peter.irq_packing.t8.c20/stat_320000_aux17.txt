# started on Sat Apr  1 03:15:06 2023


 Performance counter stats for 'CPU(s) 0-7':

        44,102,168      LLC-load-misses           #   20.35% of all LL-cache accesses  (50.00%)
        62,548,784      LLC-load-misses           #    3.73% of all LL-cache accesses  (50.00%)
       216,752,867      LLC-loads                 #  750.542 K/sec                    (50.01%)
     1,676,948,813      LLC-loads                 #    5.807 M/sec                    (41.68%)
         2,148,206      LLC-store-misses          #    7.439 K/sec                    (25.00%)
        43,656,308      LLC-store-misses          #  151.167 K/sec                    (33.33%)
        97,695,546      LLC-stores                #  338.286 K/sec                    (25.00%)
     1,136,964,177      LLC-stores                #    3.937 M/sec                    (33.33%)
        288,795.30 msec task-clock                #    8.000 CPUs utilized          
    60,904,874,757      cycles:u                  #    0.211 GHz                      (41.66%)
   361,433,482,857      cycles:k                  #    1.252 GHz                      (41.66%)
    30,647,619,798      instructions:u            #    0.50  insn per cycle           (49.99%)
   240,757,463,073      instructions:k            #    0.67  insn per cycle           (49.99%)

      36.100498952 seconds time elapsed

