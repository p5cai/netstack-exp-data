# started on Fri Mar 31 23:35:28 2023


 Performance counter stats for 'CPU(s) 0-7':

       118,249,201      LLC-load-misses           #   21.68% of all LL-cache accesses  (49.98%)
       138,746,410      LLC-load-misses           #    4.05% of all LL-cache accesses  (49.98%)
       545,378,891      LLC-loads                 #    1.887 M/sec                    (49.98%)
     3,429,259,320      LLC-loads                 #   11.864 M/sec                    (41.66%)
         2,517,568      LLC-store-misses          #    8.710 K/sec                    (25.01%)
       136,763,651      LLC-store-misses          #  473.165 K/sec                    (33.35%)
       196,224,245      LLC-stores                #  678.883 K/sec                    (25.01%)
     2,417,076,488      LLC-stores                #    8.362 M/sec                    (33.34%)
        289,040.05 msec task-clock                #    8.000 CPUs utilized          
   116,749,923,278      cycles:u                  #    0.404 GHz                      (41.68%)
   517,580,105,308      cycles:k                  #    1.791 GHz                      (41.67%)
    75,403,377,209      instructions:u            #    0.65  insn per cycle           (50.00%)
   415,980,273,698      instructions:k            #    0.80  insn per cycle           (49.99%)

      36.131089697 seconds time elapsed

