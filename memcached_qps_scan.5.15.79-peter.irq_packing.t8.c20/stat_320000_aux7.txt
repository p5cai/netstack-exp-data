# started on Sat Apr  1 00:18:39 2023


 Performance counter stats for 'CPU(s) 0-7':

        44,105,151      LLC-load-misses           #   20.08% of all LL-cache accesses  (49.98%)
        61,318,941      LLC-load-misses           #    3.62% of all LL-cache accesses  (49.98%)
       219,654,032      LLC-loads                 #  761.985 K/sec                    (49.98%)
     1,693,656,365      LLC-loads                 #    5.875 M/sec                    (41.67%)
         2,153,004      LLC-store-misses          #    7.469 K/sec                    (25.01%)
        42,833,052      LLC-store-misses          #  148.589 K/sec                    (33.35%)
       104,518,105      LLC-stores                #  362.576 K/sec                    (25.01%)
     1,139,384,199      LLC-stores                #    3.953 M/sec                    (33.35%)
        288,265.63 msec task-clock                #    8.000 CPUs utilized          
    59,468,567,590      cycles:u                  #    0.206 GHz                      (41.68%)
   360,338,047,934      cycles:k                  #    1.250 GHz                      (41.67%)
    30,532,969,827      instructions:u            #    0.51  insn per cycle           (50.00%)
   241,400,336,270      instructions:k            #    0.67  insn per cycle           (49.99%)

      36.034267293 seconds time elapsed

