# started on Sat Apr  1 14:32:36 2023


 Performance counter stats for 'CPU(s) 4-11':

        88,487,853      LLC-load-misses           #   18.66% of all LL-cache accesses  (49.99%)
       923,982,613      LLC-load-misses           #   28.00% of all LL-cache accesses  (50.00%)
       474,280,549      LLC-loads                 #    1.646 M/sec                    (50.01%)
     3,299,693,133      LLC-loads                 #   11.455 M/sec                    (41.68%)
         5,366,965      LLC-store-misses          #   18.632 K/sec                    (25.00%)
       532,079,536      LLC-store-misses          #    1.847 M/sec                    (33.33%)
       176,874,522      LLC-stores                #  614.023 K/sec                    (25.00%)
     1,492,642,323      LLC-stores                #    5.182 M/sec                    (33.33%)
        288,058.30 msec task-clock                #    8.000 CPUs utilized          
    82,867,041,841      cycles:u                  #    0.288 GHz                      (41.66%)
   556,052,332,088      cycles:k                  #    1.930 GHz                      (41.65%)
    48,935,395,064      instructions:u            #    0.59  insn per cycle           (49.98%)
   293,763,621,252      instructions:k            #    0.53  insn per cycle           (49.98%)

      36.008237956 seconds time elapsed

