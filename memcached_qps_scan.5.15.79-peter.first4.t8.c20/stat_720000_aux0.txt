# started on Thu Mar 30 07:37:23 2023


 Performance counter stats for 'CPU(s) 4-11':

        93,771,437      LLC-load-misses           #   19.52% of all LL-cache accesses  (49.99%)
       995,723,752      LLC-load-misses           #   30.48% of all LL-cache accesses  (50.00%)
       480,378,524      LLC-loads                 #    1.657 M/sec                    (50.00%)
     3,266,651,280      LLC-loads                 #   11.268 M/sec                    (41.67%)
         5,839,798      LLC-store-misses          #   20.144 K/sec                    (25.00%)
       570,831,451      LLC-store-misses          #    1.969 M/sec                    (33.34%)
       194,365,775      LLC-stores                #  670.461 K/sec                    (25.00%)
     1,511,703,671      LLC-stores                #    5.215 M/sec                    (33.33%)
        289,898.91 msec task-clock                #    8.000 CPUs utilized          
    81,319,312,704      cycles:u                  #    0.281 GHz                      (41.67%)
   565,249,211,042      cycles:k                  #    1.950 GHz                      (41.67%)
    51,374,629,208      instructions:u            #    0.63  insn per cycle           (50.00%)
   304,501,861,028      instructions:k            #    0.54  insn per cycle           (50.00%)

      36.238463278 seconds time elapsed

