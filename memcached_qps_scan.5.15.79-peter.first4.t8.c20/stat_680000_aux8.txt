# started on Sat Apr  1 14:56:33 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,897,612      LLC-load-misses           #   19.90% of all LL-cache accesses  (49.97%)
       991,353,954      LLC-load-misses           #   29.92% of all LL-cache accesses  (49.97%)
       486,800,739      LLC-loads                 #    1.687 M/sec                    (49.98%)
     3,313,023,024      LLC-loads                 #   11.479 M/sec                    (41.67%)
         8,585,355      LLC-store-misses          #   29.746 K/sec                    (25.02%)
       583,370,329      LLC-store-misses          #    2.021 M/sec                    (33.35%)
       194,135,804      LLC-stores                #  672.635 K/sec                    (25.01%)
     1,525,817,179      LLC-stores                #    5.287 M/sec                    (33.35%)
        288,619.67 msec task-clock                #    8.000 CPUs utilized          
    83,314,840,027      cycles:u                  #    0.289 GHz                      (41.68%)
   563,695,306,785      cycles:k                  #    1.953 GHz                      (41.67%)
    52,915,942,483      instructions:u            #    0.64  insn per cycle           (49.99%)
   306,435,325,652      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.079070055 seconds time elapsed

