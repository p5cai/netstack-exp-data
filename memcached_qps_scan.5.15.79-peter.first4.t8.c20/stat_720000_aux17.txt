# started on Sat Apr  1 16:34:21 2023


 Performance counter stats for 'CPU(s) 4-11':

        93,316,883      LLC-load-misses           #   19.24% of all LL-cache accesses  (49.97%)
     1,000,039,225      LLC-load-misses           #   30.33% of all LL-cache accesses  (49.99%)
       485,032,596      LLC-loads                 #    1.677 M/sec                    (50.00%)
     3,297,578,926      LLC-loads                 #   11.399 M/sec                    (41.68%)
         5,784,399      LLC-store-misses          #   19.995 K/sec                    (25.02%)
       584,346,660      LLC-store-misses          #    2.020 M/sec                    (33.36%)
       186,084,932      LLC-stores                #  643.240 K/sec                    (25.00%)
     1,515,952,122      LLC-stores                #    5.240 M/sec                    (33.33%)
        289,293.33 msec task-clock                #    8.000 CPUs utilized          
    82,682,117,482      cycles:u                  #    0.286 GHz                      (41.66%)
   563,926,788,214      cycles:k                  #    1.949 GHz                      (41.65%)
    51,694,431,285      instructions:u            #    0.63  insn per cycle           (49.98%)
   303,759,699,812      instructions:k            #    0.54  insn per cycle           (49.97%)

      36.162792689 seconds time elapsed

