# started on Sat Apr  1 15:19:30 2023


 Performance counter stats for 'CPU(s) 4-11':

        93,719,098      LLC-load-misses           #   18.95% of all LL-cache accesses  (49.97%)
       996,340,273      LLC-load-misses           #   30.07% of all LL-cache accesses  (49.98%)
       494,445,983      LLC-loads                 #    1.713 M/sec                    (49.99%)
     3,313,797,513      LLC-loads                 #   11.482 M/sec                    (41.68%)
         5,777,598      LLC-store-misses          #   20.019 K/sec                    (25.02%)
       586,868,631      LLC-store-misses          #    2.033 M/sec                    (33.35%)
       192,477,655      LLC-stores                #  666.908 K/sec                    (25.01%)
     1,520,515,970      LLC-stores                #    5.268 M/sec                    (33.35%)
        288,611.96 msec task-clock                #    8.000 CPUs utilized          
    83,604,370,756      cycles:u                  #    0.290 GHz                      (41.67%)
   564,277,232,974      cycles:k                  #    1.955 GHz                      (41.66%)
    53,009,178,080      instructions:u            #    0.63  insn per cycle           (49.99%)
   305,474,890,481      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.077547602 seconds time elapsed

