# started on Sat Apr  1 16:41:48 2023


 Performance counter stats for 'CPU(s) 4-11':

        95,929,405      LLC-load-misses           #   19.14% of all LL-cache accesses  (49.99%)
       971,046,188      LLC-load-misses           #   28.68% of all LL-cache accesses  (50.00%)
       501,156,610      LLC-loads                 #    1.733 M/sec                    (50.00%)
     3,386,359,827      LLC-loads                 #   11.713 M/sec                    (41.67%)
         8,763,789      LLC-store-misses          #   30.312 K/sec                    (25.01%)
       571,522,919      LLC-store-misses          #    1.977 M/sec                    (33.34%)
       192,372,072      LLC-stores                #  665.363 K/sec                    (25.00%)
     1,536,147,781      LLC-stores                #    5.313 M/sec                    (33.34%)
        289,123.31 msec task-clock                #    8.000 CPUs utilized          
    83,566,747,747      cycles:u                  #    0.289 GHz                      (41.67%)
   562,813,142,321      cycles:k                  #    1.947 GHz                      (41.67%)
    51,085,134,725      instructions:u            #    0.61  insn per cycle           (50.00%)
   301,083,269,311      instructions:k            #    0.53  insn per cycle           (50.00%)

      36.141540543 seconds time elapsed

