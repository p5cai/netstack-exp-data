# started on Sat Apr  1 13:51:31 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,448,958      LLC-load-misses           #   19.82% of all LL-cache accesses  (49.99%)
       965,819,065      LLC-load-misses           #   29.18% of all LL-cache accesses  (50.00%)
       486,595,503      LLC-loads                 #    1.689 M/sec                    (50.01%)
     3,309,737,402      LLC-loads                 #   11.490 M/sec                    (41.68%)
         8,691,403      LLC-store-misses          #   30.172 K/sec                    (25.01%)
       580,017,300      LLC-store-misses          #    2.013 M/sec                    (33.34%)
       202,151,412      LLC-stores                #  701.754 K/sec                    (24.99%)
     1,528,067,832      LLC-stores                #    5.305 M/sec                    (33.32%)
        288,065.76 msec task-clock                #    8.000 CPUs utilized          
    83,736,779,291      cycles:u                  #    0.291 GHz                      (41.66%)
   563,314,608,401      cycles:k                  #    1.956 GHz                      (41.65%)
    51,576,516,401      instructions:u            #    0.62  insn per cycle           (49.98%)
   304,378,607,300      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.008842279 seconds time elapsed

