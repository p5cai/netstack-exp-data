# started on Thu Mar 30 07:44:50 2023


 Performance counter stats for 'CPU(s) 4-11':

        95,392,748      LLC-load-misses           #   19.60% of all LL-cache accesses  (49.98%)
       944,081,990      LLC-load-misses           #   28.47% of all LL-cache accesses  (49.98%)
       486,772,736      LLC-loads                 #    1.682 M/sec                    (49.98%)
     3,316,111,472      LLC-loads                 #   11.457 M/sec                    (41.67%)
         8,719,235      LLC-store-misses          #   30.125 K/sec                    (25.01%)
       543,949,915      LLC-store-misses          #    1.879 M/sec                    (33.35%)
       196,266,772      LLC-stores                #  678.111 K/sec                    (25.01%)
     1,534,774,940      LLC-stores                #    5.303 M/sec                    (33.34%)
        289,431.65 msec task-clock                #    8.000 CPUs utilized          
    85,359,149,953      cycles:u                  #    0.295 GHz                      (41.68%)
   560,641,429,289      cycles:k                  #    1.937 GHz                      (41.67%)
    51,105,483,366      instructions:u            #    0.60  insn per cycle           (49.99%)
   299,386,349,735      instructions:k            #    0.53  insn per cycle           (49.98%)

      36.180016583 seconds time elapsed

