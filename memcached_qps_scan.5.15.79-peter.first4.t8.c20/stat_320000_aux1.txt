# started on Thu Mar 30 07:40:43 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,644,066      LLC-load-misses           #   15.21% of all LL-cache accesses  (49.98%)
       583,273,531      LLC-load-misses           #   24.06% of all LL-cache accesses  (49.99%)
       359,363,393      LLC-loads                 #    1.239 M/sec                    (50.01%)
     2,424,362,386      LLC-loads                 #    8.359 M/sec                    (41.69%)
         6,015,161      LLC-store-misses          #   20.740 K/sec                    (25.02%)
       283,702,102      LLC-store-misses          #  978.203 K/sec                    (33.35%)
       139,648,131      LLC-stores                #  481.506 K/sec                    (24.99%)
       990,390,899      LLC-stores                #    3.415 M/sec                    (33.32%)
        290,023.61 msec task-clock                #    8.000 CPUs utilized          
    66,242,515,102      cycles:u                  #    0.228 GHz                      (41.65%)
   443,711,612,897      cycles:k                  #    1.530 GHz                      (41.65%)
    30,946,535,057      instructions:u            #    0.47  insn per cycle           (49.98%)
   220,057,533,434      instructions:k            #    0.50  insn per cycle           (49.98%)

      36.254073441 seconds time elapsed

