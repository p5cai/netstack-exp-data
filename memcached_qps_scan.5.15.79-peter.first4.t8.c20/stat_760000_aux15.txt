# started on Sat Apr  1 16:13:50 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,238,907      LLC-load-misses           #   19.51% of all LL-cache accesses  (49.98%)
       968,695,560      LLC-load-misses           #   29.54% of all LL-cache accesses  (49.99%)
       493,296,722      LLC-loads                 #    1.710 M/sec                    (50.01%)
     3,279,308,414      LLC-loads                 #   11.367 M/sec                    (41.69%)
         8,867,776      LLC-store-misses          #   30.737 K/sec                    (25.02%)
       571,578,134      LLC-store-misses          #    1.981 M/sec                    (33.35%)
       194,395,537      LLC-stores                #  673.804 K/sec                    (25.00%)
     1,510,777,731      LLC-stores                #    5.237 M/sec                    (33.32%)
        288,504.57 msec task-clock                #    8.000 CPUs utilized          
    87,406,251,923      cycles:u                  #    0.303 GHz                      (41.65%)
   559,503,576,204      cycles:k                  #    1.939 GHz                      (41.64%)
    51,354,914,830      instructions:u            #    0.59  insn per cycle           (49.97%)
   303,253,137,958      instructions:k            #    0.54  insn per cycle           (49.97%)

      36.064181840 seconds time elapsed

