# started on Sat Apr  1 16:27:00 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,750,558      LLC-load-misses           #   15.25% of all LL-cache accesses  (50.00%)
       569,412,370      LLC-load-misses           #   23.83% of all LL-cache accesses  (50.01%)
       358,976,194      LLC-loads                 #    1.243 M/sec                    (50.01%)
     2,389,734,517      LLC-loads                 #    8.275 M/sec                    (41.68%)
         6,026,438      LLC-store-misses          #   20.867 K/sec                    (25.00%)
       283,971,615      LLC-store-misses          #  983.268 K/sec                    (33.33%)
       134,133,101      LLC-stores                #  464.444 K/sec                    (25.00%)
       992,481,179      LLC-stores                #    3.437 M/sec                    (33.33%)
        288,803.90 msec task-clock                #    8.000 CPUs utilized          
    66,506,165,881      cycles:u                  #    0.230 GHz                      (41.66%)
   442,200,124,693      cycles:k                  #    1.531 GHz                      (41.66%)
    30,071,622,543      instructions:u            #    0.45  insn per cycle           (49.99%)
   220,181,026,464      instructions:k            #    0.50  insn per cycle           (49.99%)

      36.101561665 seconds time elapsed

