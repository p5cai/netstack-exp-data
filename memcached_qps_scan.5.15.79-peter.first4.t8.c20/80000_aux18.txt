#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       51.1    10.6    29.5    40.4    42.0    50.0    60.1    64.9    80.0
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      519296.1 27611.5 369246.3 471478.9 477246.2 522204.8 561477.5 566386.6 610740.3

Total QPS = 79974.9 (2399247 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX  587235817 bytes :   18.7 MB/s
TX  101302143 bytes :    3.2 MB/s
