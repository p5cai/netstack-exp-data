# started on Sat Apr  1 16:03:08 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,345,873      LLC-load-misses           #   19.46% of all LL-cache accesses  (49.98%)
       976,798,806      LLC-load-misses           #   29.75% of all LL-cache accesses  (50.00%)
       495,125,873      LLC-loads                 #    1.719 M/sec                    (50.01%)
     3,282,868,188      LLC-loads                 #   11.396 M/sec                    (41.69%)
         8,850,010      LLC-store-misses          #   30.721 K/sec                    (25.01%)
       574,932,250      LLC-store-misses          #    1.996 M/sec                    (33.34%)
       197,001,902      LLC-stores                #  683.850 K/sec                    (24.99%)
     1,522,044,957      LLC-stores                #    5.283 M/sec                    (33.32%)
        288,077.52 msec task-clock                #    8.000 CPUs utilized          
    85,100,886,097      cycles:u                  #    0.295 GHz                      (41.65%)
   562,245,939,513      cycles:k                  #    1.952 GHz                      (41.65%)
    52,093,883,170      instructions:u            #    0.61  insn per cycle           (49.98%)
   303,907,560,604      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.010862159 seconds time elapsed

