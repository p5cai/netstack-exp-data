# started on Thu Mar 30 07:41:34 2023


 Performance counter stats for 'CPU(s) 4-11':

        80,309,937      LLC-load-misses           #   16.95% of all LL-cache accesses  (49.98%)
       810,232,928      LLC-load-misses           #   26.81% of all LL-cache accesses  (49.98%)
       473,751,450      LLC-loads                 #    1.643 M/sec                    (49.99%)
     3,021,738,792      LLC-loads                 #   10.482 M/sec                    (41.66%)
         7,424,365      LLC-store-misses          #   25.753 K/sec                    (25.01%)
       420,594,248      LLC-store-misses          #    1.459 M/sec                    (33.35%)
       178,660,906      LLC-stores                #  619.735 K/sec                    (25.01%)
     1,335,419,551      LLC-stores                #    4.632 M/sec                    (33.34%)
        288,286.20 msec task-clock                #    8.000 CPUs utilized          
    81,523,937,092      cycles:u                  #    0.283 GHz                      (41.68%)
   529,849,074,550      cycles:k                  #    1.838 GHz                      (41.67%)
    43,360,287,977      instructions:u            #    0.53  insn per cycle           (50.00%)
   271,392,274,724      instructions:k            #    0.51  insn per cycle           (49.99%)

      36.036861920 seconds time elapsed

