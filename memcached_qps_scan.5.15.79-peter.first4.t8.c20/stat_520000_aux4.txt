# started on Sat Apr  1 14:09:39 2023


 Performance counter stats for 'CPU(s) 4-11':

        85,793,202      LLC-load-misses           #   17.70% of all LL-cache accesses  (49.99%)
       862,601,870      LLC-load-misses           #   27.47% of all LL-cache accesses  (49.99%)
       484,644,824      LLC-loads                 #    1.679 M/sec                    (49.99%)
     3,140,477,132      LLC-loads                 #   10.878 M/sec                    (41.66%)
         7,416,568      LLC-store-misses          #   25.690 K/sec                    (25.01%)
       478,756,042      LLC-store-misses          #    1.658 M/sec                    (33.34%)
       176,318,752      LLC-stores                #  610.745 K/sec                    (25.01%)
     1,412,448,828      LLC-stores                #    4.893 M/sec                    (33.34%)
        288,694.78 msec task-clock                #    8.000 CPUs utilized          
    87,143,925,992      cycles:u                  #    0.302 GHz                      (41.68%)
   543,529,718,494      cycles:k                  #    1.883 GHz                      (41.67%)
    48,470,463,391      instructions:u            #    0.56  insn per cycle           (50.00%)
   281,255,818,101      instructions:k            #    0.52  insn per cycle           (49.99%)

      36.087702627 seconds time elapsed

