# started on Sat Apr  1 14:23:37 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,469,569      LLC-load-misses           #   19.27% of all LL-cache accesses  (49.98%)
       978,532,900      LLC-load-misses           #   29.59% of all LL-cache accesses  (49.99%)
       500,584,702      LLC-loads                 #    1.735 M/sec                    (50.01%)
     3,307,224,899      LLC-loads                 #   11.464 M/sec                    (41.69%)
         8,676,746      LLC-store-misses          #   30.078 K/sec                    (25.02%)
       580,659,651      LLC-store-misses          #    2.013 M/sec                    (33.35%)
       194,883,193      LLC-stores                #  675.561 K/sec                    (24.99%)
     1,538,473,472      LLC-stores                #    5.333 M/sec                    (33.32%)
        288,475.97 msec task-clock                #    8.000 CPUs utilized          
    83,462,974,104      cycles:u                  #    0.289 GHz                      (41.65%)
   563,809,258,656      cycles:k                  #    1.954 GHz                      (41.65%)
    51,618,150,576      instructions:u            #    0.62  insn per cycle           (49.98%)
   304,435,283,958      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.060728271 seconds time elapsed

