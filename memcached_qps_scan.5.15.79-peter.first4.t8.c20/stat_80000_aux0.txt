# started on Thu Mar 30 07:29:09 2023


 Performance counter stats for 'CPU(s) 4-11':

        12,797,483      LLC-load-misses           #   12.54% of all LL-cache accesses  (49.97%)
       184,634,758      LLC-load-misses           #   20.35% of all LL-cache accesses  (49.98%)
       102,022,654      LLC-loads                 #  353.973 K/sec                    (49.99%)
       907,128,546      LLC-loads                 #    3.147 M/sec                    (41.67%)
           834,685      LLC-store-misses          #    2.896 K/sec                    (25.02%)
        76,027,155      LLC-store-misses          #  263.781 K/sec                    (33.35%)
        40,563,884      LLC-stores                #  140.739 K/sec                    (25.01%)
       313,702,056      LLC-stores                #    1.088 M/sec                    (33.35%)
        288,221.28 msec task-clock                #    8.000 CPUs utilized          
    22,525,634,590      cycles:u                  #    0.078 GHz                      (41.67%)
   177,488,536,585      cycles:k                  #    0.616 GHz                      (41.66%)
     9,774,849,606      instructions:u            #    0.43  insn per cycle           (49.99%)
    86,582,634,382      instructions:k            #    0.49  insn per cycle           (49.98%)

      36.028755395 seconds time elapsed

