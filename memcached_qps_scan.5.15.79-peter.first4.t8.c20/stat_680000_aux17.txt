# started on Sat Apr  1 16:32:48 2023


 Performance counter stats for 'CPU(s) 4-11':

        94,978,815      LLC-load-misses           #   19.29% of all LL-cache accesses  (49.98%)
       952,122,937      LLC-load-misses           #   29.13% of all LL-cache accesses  (49.99%)
       492,428,071      LLC-loads                 #    1.707 M/sec                    (50.01%)
     3,268,316,106      LLC-loads                 #   11.329 M/sec                    (41.69%)
         8,437,296      LLC-store-misses          #   29.245 K/sec                    (25.02%)
       559,631,519      LLC-store-misses          #    1.940 M/sec                    (33.35%)
       193,792,306      LLC-stores                #  671.717 K/sec                    (24.99%)
     1,497,943,363      LLC-stores                #    5.192 M/sec                    (33.32%)
        288,502.74 msec task-clock                #    8.000 CPUs utilized          
    83,649,537,938      cycles:u                  #    0.290 GHz                      (41.65%)
   562,933,890,361      cycles:k                  #    1.951 GHz                      (41.65%)
    50,886,737,297      instructions:u            #    0.61  insn per cycle           (49.97%)
   300,663,747,434      instructions:k            #    0.53  insn per cycle           (49.97%)

      36.063930747 seconds time elapsed

