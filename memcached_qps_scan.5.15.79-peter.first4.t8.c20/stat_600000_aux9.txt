# started on Sat Apr  1 15:05:33 2023


 Performance counter stats for 'CPU(s) 4-11':

        94,293,507      LLC-load-misses           #   18.98% of all LL-cache accesses  (49.98%)
       978,716,181      LLC-load-misses           #   29.53% of all LL-cache accesses  (50.00%)
       496,793,157      LLC-loads                 #    1.720 M/sec                    (50.01%)
     3,313,827,263      LLC-loads                 #   11.471 M/sec                    (41.69%)
         8,614,056      LLC-store-misses          #   29.819 K/sec                    (25.02%)
       576,342,957      LLC-store-misses          #    1.995 M/sec                    (33.35%)
       194,714,117      LLC-stores                #  674.026 K/sec                    (24.99%)
     1,509,821,617      LLC-stores                #    5.226 M/sec                    (33.32%)
        288,882.15 msec task-clock                #    8.000 CPUs utilized          
    84,852,235,297      cycles:u                  #    0.294 GHz                      (41.65%)
   562,015,061,939      cycles:k                  #    1.945 GHz                      (41.65%)
    50,054,588,947      instructions:u            #    0.59  insn per cycle           (49.97%)
   296,897,091,921      instructions:k            #    0.53  insn per cycle           (49.97%)

      36.111514465 seconds time elapsed

