#type       avg     std     min     5th    10th    50th    90th    95th    99th
read      286.4   260.4    32.4    63.7    77.7   206.5   596.7   778.7  1237.4
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3673089.7 63966.0 3306343.1 3480974.3 3498644.2 3640003.6 3781362.9 3799032.9 3813168.8

Total QPS = 559984.9 (16799548 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4116306937 bytes :  130.9 MB/s
TX  709397099 bytes :   22.6 MB/s
