# started on Sat Apr  1 14:24:28 2023


 Performance counter stats for 'CPU(s) 4-11':

        96,095,636      LLC-load-misses           #   19.31% of all LL-cache accesses  (49.98%)
       988,156,563      LLC-load-misses           #   30.05% of all LL-cache accesses  (49.99%)
       497,571,556      LLC-loads                 #    1.720 M/sec                    (50.01%)
     3,288,655,942      LLC-loads                 #   11.370 M/sec                    (41.69%)
         8,517,218      LLC-store-misses          #   29.446 K/sec                    (25.02%)
       580,124,025      LLC-store-misses          #    2.006 M/sec                    (33.35%)
       194,498,812      LLC-stores                #  672.432 K/sec                    (24.99%)
     1,519,971,518      LLC-stores                #    5.255 M/sec                    (33.32%)
        289,246.76 msec task-clock                #    8.000 CPUs utilized          
    83,310,136,039      cycles:u                  #    0.288 GHz                      (41.65%)
   563,857,623,224      cycles:k                  #    1.949 GHz                      (41.65%)
    51,585,040,997      instructions:u            #    0.62  insn per cycle           (49.98%)
   304,376,217,306      instructions:k            #    0.54  insn per cycle           (49.98%)

      36.156989475 seconds time elapsed

