# started on Thu Mar 30 07:18:27 2023


 Performance counter stats for 'CPU(s) 4-11':

        12,727,962      LLC-load-misses           #   13.18% of all LL-cache accesses  (49.98%)
       185,159,029      LLC-load-misses           #   20.67% of all LL-cache accesses  (49.99%)
        96,562,384      LLC-loads                 #  334.716 K/sec                    (50.01%)
       895,949,563      LLC-loads                 #    3.106 M/sec                    (41.69%)
           792,051      LLC-store-misses          #    2.745 K/sec                    (25.02%)
        75,330,743      LLC-store-misses          #  261.120 K/sec                    (33.35%)
        38,850,413      LLC-stores                #  134.668 K/sec                    (24.99%)
       310,884,855      LLC-stores                #    1.078 M/sec                    (33.32%)
        288,490.87 msec task-clock                #    8.000 CPUs utilized          
    21,223,685,533      cycles:u                  #    0.074 GHz                      (41.65%)
   175,546,442,459      cycles:k                  #    0.608 GHz                      (41.65%)
     7,965,330,180      instructions:u            #    0.38  insn per cycle           (49.98%)
    86,879,898,695      instructions:k            #    0.49  insn per cycle           (49.98%)

      36.062456029 seconds time elapsed

