#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1605.3   541.1    32.4   436.5   673.6  1817.1  2040.1  2139.0  2247.8
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3936969.6 255529.9 3636977.4 3503288.2 3538854.6 3831832.6 4438866.5 4527202.1 4597870.7

Total QPS = 601558.8 (18046764 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4416710405 bytes :  140.4 MB/s
TX  761981417 bytes :   24.2 MB/s
