# started on Sat Apr  1 15:08:48 2023


 Performance counter stats for 'CPU(s) 4-11':

        93,719,567      LLC-load-misses           #   19.69% of all LL-cache accesses  (49.99%)
       990,915,089      LLC-load-misses           #   29.85% of all LL-cache accesses  (50.00%)
       475,908,436      LLC-loads                 #    1.646 M/sec                    (50.01%)
     3,319,785,572      LLC-loads                 #   11.479 M/sec                    (41.68%)
         5,853,029      LLC-store-misses          #   20.239 K/sec                    (25.00%)
       584,141,219      LLC-store-misses          #    2.020 M/sec                    (33.34%)
       193,311,556      LLC-stores                #  668.443 K/sec                    (25.00%)
     1,523,681,268      LLC-stores                #    5.269 M/sec                    (33.33%)
        289,196.84 msec task-clock                #    8.000 CPUs utilized          
    81,953,489,916      cycles:u                  #    0.283 GHz                      (41.66%)
   564,833,623,116      cycles:k                  #    1.953 GHz                      (41.66%)
    51,623,286,096      instructions:u            #    0.63  insn per cycle           (49.99%)
   305,856,069,142      instructions:k            #    0.54  insn per cycle           (49.99%)

      36.150778082 seconds time elapsed

