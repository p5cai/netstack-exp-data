# started on Thu Mar  2 17:05:11 2023


 Performance counter stats for 'CPU(s) 0-1':

       127,333,136      LLC-load-misses           #    9.71% of all LL-cache hits     (49.99%)
         6,612,255      LLC-load-misses           #   56.57% of all LL-cache hits     (49.99%)
     1,311,083,107      LLC-loads                 #   20.633 M/sec                    (49.99%)
        11,688,016      LLC-loads                 #    0.184 M/sec                    (41.66%)
         1,321,451      LLC-store-misses          #    0.021 M/sec                    (25.00%)
            47,298      LLC-store-misses          #    0.744 K/sec                    (33.34%)
       588,216,871      LLC-stores                #    9.257 M/sec                    (25.00%)
           276,895      LLC-stores                #    0.004 M/sec                    (33.34%)
         63,542.43 msec task-clock                #    2.000 CPUs utilized          
   164,855,076,191      cycles:u                  #    2.594 GHz                      (41.67%)
       800,586,593      cycles:k                  #    0.013 GHz                      (41.67%)
   216,567,958,033      instructions:u            #    1.31  insn per cycle           (50.01%)
     1,424,841,513      instructions:k            #    1.78  insn per cycle           (50.00%)

      31.772395877 seconds time elapsed

