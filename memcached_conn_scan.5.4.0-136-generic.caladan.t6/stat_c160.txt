# started on Thu Mar  2 17:27:53 2023


 Performance counter stats for 'CPU(s) 0-5':

     1,248,847,835      LLC-load-misses           #   20.62% of all LL-cache hits     (50.00%)
         2,067,592      LLC-load-misses           #   28.30% of all LL-cache hits     (50.00%)
     6,056,519,533      LLC-loads                 #   31.670 M/sec                    (50.00%)
         7,306,788      LLC-loads                 #    0.038 M/sec                    (41.67%)
       261,617,886      LLC-store-misses          #    1.368 M/sec                    (25.00%)
           836,652      LLC-store-misses          #    0.004 M/sec                    (33.33%)
     2,436,286,423      LLC-stores                #   12.740 M/sec                    (25.00%)
         1,071,166      LLC-stores                #    0.006 M/sec                    (33.33%)
        191,235.97 msec task-clock                #    6.000 CPUs utilized          
   487,080,597,431      cycles:u                  #    2.547 GHz                      (41.66%)
     1,054,354,050      cycles:k                  #    0.006 GHz                      (41.66%)
   465,042,512,747      instructions:u            #    0.95  insn per cycle           (50.00%)
       498,925,583      instructions:k            #    0.47  insn per cycle           (50.00%)

      31.874186228 seconds time elapsed

