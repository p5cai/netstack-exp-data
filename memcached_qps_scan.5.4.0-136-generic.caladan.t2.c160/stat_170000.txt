# started on Mon Feb 27 09:54:14 2023


 Performance counter stats for 'CPU(s) 0-1':

        97,605,089      LLC-load-misses                                               (41.64%)
         3,657,344      LLC-load-misses                                               (41.65%)
         2,269,550      mem_load_uops_retired.llc_miss                                     (33.33%)
    58,370,265,498      mem_uops_retired.all_loads                                     (16.68%)
   158,917,545,877      cycles                                                        (25.02%)
   139,544,412,916      cycles:u                                                      (25.02%)
    19,236,560,414      cycles:k                                                      (25.02%)
   212,524,175,017      instructions              #    1.34  insn per cycle         
                                                  #    0.33  stalled cycles per insn  (33.34%)
   193,881,404,649      instructions:u            #    1.39  insn per cycle           (41.67%)
    18,657,686,578      instructions:k            #    0.97  insn per cycle           (50.00%)
    69,989,441,408      stalled-cycles-frontend   #   44.04% frontend cycles idle     (49.98%)
    43,818,749,763      stalled-cycles-backend    #   27.57% backend cycles idle      (49.97%)

      31.707288372 seconds time elapsed

