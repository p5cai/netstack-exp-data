#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       69.8   291.1    22.1    31.2    32.3    38.6    49.2    58.0   978.2
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      173349.8 16569.0 19237.2 149806.6 152991.8 172612.8 194520.5 198262.4 218241.8

Total QPS = 209996.6 (6299899 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 1555250108 bytes :   49.4 MB/s
TX  266049129 bytes :    8.5 MB/s
