# started on Wed Mar  1 12:24:42 2023


 Performance counter stats for 'CPU(s) 0-7':

       279,608,617      LLC-load-misses                                               (41.64%)
     1,191,064,740      LLC-load-misses                                               (41.64%)
        17,333,971      mem_load_uops_retired.llc_miss                                     (33.33%)
   101,779,696,650      mem_uops_retired.all_loads                                     (16.68%)
   644,498,438,887      cycles                                                        (25.02%)
    97,725,801,368      cycles:u                                                      (25.01%)
   546,850,623,643      cycles:k                                                      (25.01%)
   345,608,826,364      instructions              #    0.54  insn per cycle         
                                                  #    1.36  stalled cycles per insn  (33.35%)
    46,985,248,937      instructions:u            #    0.48  insn per cycle           (41.68%)
   298,835,033,946      instructions:k            #    0.55  insn per cycle           (50.00%)
   470,197,597,880      stalled-cycles-frontend   #   72.96% frontend cycles idle     (49.99%)
   377,188,978,699      stalled-cycles-backend    #   58.52% backend cycles idle      (49.98%)

      31.856426018 seconds time elapsed

