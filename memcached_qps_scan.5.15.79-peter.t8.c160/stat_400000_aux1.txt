# started on Wed Mar  1 11:16:36 2023


 Performance counter stats for 'CPU(s) 0-7':

       134,609,066      LLC-load-misses                                               (41.64%)
       456,574,137      LLC-load-misses                                               (41.64%)
         9,496,643      mem_load_uops_retired.llc_miss                                     (33.33%)
    97,598,255,564      mem_uops_retired.all_loads                                     (16.68%)
   599,910,495,520      cycles                                                        (25.02%)
    83,806,990,648      cycles:u                                                      (25.01%)
   516,336,680,842      cycles:k                                                      (25.01%)
   330,446,760,430      instructions              #    0.55  insn per cycle         
                                                  #    1.30  stalled cycles per insn  (33.35%)
    36,393,329,850      instructions:u            #    0.43  insn per cycle           (41.68%)
   294,131,922,919      instructions:k            #    0.57  insn per cycle           (50.00%)
   428,421,716,661      stalled-cycles-frontend   #   71.41% frontend cycles idle     (49.99%)
   340,677,638,870      stalled-cycles-backend    #   56.79% backend cycles idle      (49.98%)

      31.903048590 seconds time elapsed

