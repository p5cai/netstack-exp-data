# started on Wed Mar  1 11:53:33 2023


 Performance counter stats for 'CPU(s) 0-7':

       276,604,979      LLC-load-misses                                               (41.64%)
     1,185,207,995      LLC-load-misses                                               (41.65%)
        17,429,412      mem_load_uops_retired.llc_miss                                     (33.34%)
   101,235,252,167      mem_uops_retired.all_loads                                     (16.68%)
   644,778,122,261      cycles                                                        (25.02%)
    98,874,674,819      cycles:u                                                      (25.02%)
   546,882,402,307      cycles:k                                                      (25.01%)
   344,354,565,467      instructions              #    0.53  insn per cycle         
                                                  #    1.37  stalled cycles per insn  (33.34%)
    46,713,984,623      instructions:u            #    0.47  insn per cycle           (41.66%)
   297,535,294,887      instructions:k            #    0.54  insn per cycle           (49.99%)
   471,484,722,425      stalled-cycles-frontend   #   73.12% frontend cycles idle     (49.98%)
   378,279,910,672      stalled-cycles-backend    #   58.67% backend cycles idle      (49.97%)

      31.850365489 seconds time elapsed

