# started on Wed Mar  1 11:19:53 2023


 Performance counter stats for 'CPU(s) 0-7':

       184,322,391      LLC-load-misses                                               (41.65%)
       635,818,017      LLC-load-misses                                               (41.65%)
         4,344,836      mem_load_uops_retired.llc_miss                                     (33.32%)
   112,152,059,226      mem_uops_retired.all_loads                                     (16.67%)
   642,288,656,954      cycles                                                        (25.01%)
    93,350,496,227      cycles:u                                                      (25.01%)
   549,036,266,887      cycles:k                                                      (25.00%)
   375,595,844,218      instructions              #    0.58  insn per cycle         
                                                  #    1.20  stalled cycles per insn  (33.34%)
    45,151,124,471      instructions:u            #    0.48  insn per cycle           (41.67%)
   330,506,337,868      instructions:k            #    0.60  insn per cycle           (50.01%)
   450,512,823,522      stalled-cycles-frontend   #   70.14% frontend cycles idle     (50.00%)
   353,114,180,276      stalled-cycles-backend    #   54.98% backend cycles idle      (49.99%)

      31.863900008 seconds time elapsed

