# started on Thu Feb 16 14:53:23 2023


 Performance counter stats for 'CPU(s) 0-7':

       284,624,428      LLC-load-misses                                               (41.65%)
     1,244,814,617      LLC-load-misses                                               (41.66%)
        18,713,472      mem_load_uops_retired.llc_miss                                     (33.34%)
   107,309,070,275      mem_uops_retired.all_loads                                     (16.68%)
   645,007,363,599      cycles                                                        (25.03%)
   102,331,366,462      cycles:u                                                      (25.02%)
   543,152,661,734      cycles:k                                                      (25.00%)
   363,828,250,474      instructions              #    0.56  insn per cycle         
                                                  #    1.27  stalled cycles per insn  (33.33%)
    50,229,831,230      instructions:u            #    0.49  insn per cycle           (41.65%)
   313,566,125,482      instructions:k            #    0.58  insn per cycle           (49.98%)
   463,197,693,168      stalled-cycles-frontend   #   71.81% frontend cycles idle     (49.97%)
   368,941,464,127      stalled-cycles-backend    #   57.20% backend cycles idle      (49.96%)

      31.892312737 seconds time elapsed

