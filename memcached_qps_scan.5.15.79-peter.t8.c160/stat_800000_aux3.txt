# started on Wed Mar  1 11:55:54 2023


 Performance counter stats for 'CPU(s) 0-7':

       278,191,545      LLC-load-misses                                               (41.65%)
     1,188,402,828      LLC-load-misses                                               (41.67%)
        17,172,149      mem_load_uops_retired.llc_miss                                     (33.35%)
   101,877,966,364      mem_uops_retired.all_loads                                     (16.69%)
   645,602,510,463      cycles                                                        (25.02%)
    98,873,030,598      cycles:u                                                      (25.00%)
   546,318,310,016      cycles:k                                                      (24.99%)
   345,494,885,938      instructions              #    0.54  insn per cycle         
                                                  #    1.36  stalled cycles per insn  (33.32%)
    47,120,954,851      instructions:u            #    0.48  insn per cycle           (41.64%)
   298,408,201,893      instructions:k            #    0.55  insn per cycle           (49.97%)
   470,336,508,359      stalled-cycles-frontend   #   72.85% frontend cycles idle     (49.97%)
   377,123,263,488      stalled-cycles-backend    #   58.41% backend cycles idle      (49.97%)

      31.886522462 seconds time elapsed

