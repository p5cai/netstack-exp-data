# started on Wed Mar  1 12:05:50 2023


 Performance counter stats for 'CPU(s) 0-7':

       277,948,743      LLC-load-misses                                               (41.64%)
     1,193,859,617      LLC-load-misses                                               (41.66%)
        16,835,152      mem_load_uops_retired.llc_miss                                     (33.34%)
   102,094,247,146      mem_uops_retired.all_loads                                     (16.68%)
   644,718,679,853      cycles                                                        (25.02%)
    98,508,942,044      cycles:u                                                      (25.02%)
   546,715,411,740      cycles:k                                                      (25.01%)
   346,708,071,492      instructions              #    0.54  insn per cycle         
                                                  #    1.36  stalled cycles per insn  (33.34%)
    46,908,850,982      instructions:u            #    0.48  insn per cycle           (41.66%)
   299,706,378,078      instructions:k            #    0.55  insn per cycle           (49.99%)
   470,166,540,400      stalled-cycles-frontend   #   72.93% frontend cycles idle     (49.97%)
   376,753,525,698      stalled-cycles-backend    #   58.44% backend cycles idle      (49.96%)

      31.848254170 seconds time elapsed

