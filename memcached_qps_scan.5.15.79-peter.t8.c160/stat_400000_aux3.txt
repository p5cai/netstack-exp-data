# started on Wed Mar  1 11:46:12 2023


 Performance counter stats for 'CPU(s) 0-7':

       135,422,102      LLC-load-misses                                               (41.66%)
       459,893,752      LLC-load-misses                                               (41.67%)
         9,812,116      mem_load_uops_retired.llc_miss                                     (33.34%)
    97,790,112,501      mem_uops_retired.all_loads                                     (16.67%)
   598,165,710,087      cycles                                                        (25.00%)
    83,189,241,735      cycles:u                                                      (25.00%)
   515,059,140,275      cycles:k                                                      (25.00%)
   331,371,488,313      instructions              #    0.55  insn per cycle         
                                                  #    1.29  stalled cycles per insn  (33.33%)
    36,425,079,181      instructions:u            #    0.44  insn per cycle           (41.66%)
   295,019,943,667      instructions:k            #    0.57  insn per cycle           (49.99%)
   426,135,807,258      stalled-cycles-frontend   #   71.24% frontend cycles idle     (49.99%)
   338,801,771,542      stalled-cycles-backend    #   56.64% backend cycles idle      (49.99%)

      31.875824101 seconds time elapsed

