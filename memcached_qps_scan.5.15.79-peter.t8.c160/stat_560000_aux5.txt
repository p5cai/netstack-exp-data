# started on Wed Mar  1 12:19:51 2023


 Performance counter stats for 'CPU(s) 0-7':

       276,341,159      LLC-load-misses                                               (41.65%)
     1,197,301,642      LLC-load-misses                                               (41.65%)
        17,385,849      mem_load_uops_retired.llc_miss                                     (33.32%)
   101,842,789,837      mem_uops_retired.all_loads                                     (16.68%)
   644,749,165,677      cycles                                                        (25.01%)
    98,333,756,921      cycles:u                                                      (25.01%)
   546,744,355,133      cycles:k                                                      (25.01%)
   345,869,484,251      instructions              #    0.54  insn per cycle         
                                                  #    1.36  stalled cycles per insn  (33.34%)
    46,465,608,316      instructions:u            #    0.47  insn per cycle           (41.67%)
   299,692,782,397      instructions:k            #    0.55  insn per cycle           (50.01%)
   470,031,209,557      stalled-cycles-frontend   #   72.90% frontend cycles idle     (50.00%)
   377,130,228,453      stalled-cycles-backend    #   58.49% backend cycles idle      (49.99%)

      31.862622267 seconds time elapsed

