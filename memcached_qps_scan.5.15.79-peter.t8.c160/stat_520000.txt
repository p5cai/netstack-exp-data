# started on Thu Feb 16 14:51:15 2023


 Performance counter stats for 'CPU(s) 0-7':

       183,059,407      LLC-load-misses                                               (41.65%)
       656,441,983      LLC-load-misses                                               (41.65%)
         4,552,166      mem_load_uops_retired.llc_miss                                     (33.32%)
   112,733,897,931      mem_uops_retired.all_loads                                     (16.68%)
   641,258,976,097      cycles                                                        (25.01%)
    93,153,828,290      cycles:u                                                      (25.01%)
   547,968,994,106      cycles:k                                                      (25.01%)
   377,852,656,353      instructions              #    0.59  insn per cycle         
                                                  #    1.19  stalled cycles per insn  (33.34%)
    45,218,255,653      instructions:u            #    0.49  insn per cycle           (41.68%)
   332,675,932,931      instructions:k            #    0.61  insn per cycle           (50.01%)
   448,264,415,804      stalled-cycles-frontend   #   69.90% frontend cycles idle     (50.00%)
   352,183,122,697      stalled-cycles-backend    #   54.92% backend cycles idle      (49.99%)

      31.860825915 seconds time elapsed

