# started on Sun Apr  2 14:45:12 2023

#           time             counts unit events
     5.000173926          1,352,898      LLC-load-misses           #    7.17% of all LL-cache hits     (49.94%)
     5.000173926              8,947      LLC-load-misses           #    4.81% of all LL-cache hits     (50.02%)
     5.000173926         18,857,938      LLC-loads                 #    3.772 M/sec                    (50.08%)
     5.000173926            185,997      LLC-loads                 #    0.037 M/sec                    (41.76%)
     5.000173926            374,615      LLC-store-misses          #    0.075 M/sec                    (25.02%)
     5.000173926              9,230      LLC-store-misses          #    0.002 M/sec                    (33.34%)
     5.000173926          3,567,164      LLC-stores                #    0.713 M/sec                    (24.96%)
     5.000173926             31,912      LLC-stores                #    0.006 M/sec                    (33.28%)
     5.000173926           5,000.00 msec task-clock                #    1.000 CPUs utilized          
     5.000173926     13,410,770,773      cycles:u                  #    2.682 GHz                      (41.60%)
     5.000173926         21,815,836      cycles:k                  #    0.004 GHz                      (41.60%)
     5.000173926     21,518,860,949      instructions:u            #    1.60  insn per cycle           (49.92%)
     5.000173926          8,117,445      instructions:k            #    0.37  insn per cycle           (49.92%)
    10.000495554          4,311,354      LLC-load-misses           #    8.98% of all LL-cache hits     (49.92%)
    10.000495554              6,402      LLC-load-misses           #    2.61% of all LL-cache hits     (49.92%)
    10.000495554         77,145,252      LLC-loads                 #   15.429 M/sec                    (49.94%)
    10.000495554            304,287      LLC-loads                 #    0.061 M/sec                    (41.70%)
    10.000495554          1,239,871      LLC-store-misses          #    0.248 M/sec                    (25.12%)
    10.000495554                711      LLC-store-misses          #    0.142 K/sec                    (33.44%)
    10.000495554         15,884,311      LLC-stores                #    3.177 M/sec                    (25.02%)
    10.000495554             49,384      LLC-stores                #    0.010 M/sec                    (33.34%)
    10.000495554           5,000.15 msec task-clock                #    1.000 CPUs utilized          
    10.000495554     13,401,128,412      cycles:u                  #    2.680 GHz                      (41.66%)
    10.000495554         26,922,802      cycles:k                  #    0.005 GHz                      (41.60%)
    10.000495554     12,114,073,167      instructions:u            #    0.90  insn per cycle           (49.92%)
    10.000495554          7,600,565      instructions:k            #    0.31  insn per cycle           (49.92%)
    15.000741145          4,293,351      LLC-load-misses           #    7.43% of all LL-cache hits     (49.92%)
    15.000741145             10,863      LLC-load-misses           #    4.07% of all LL-cache hits     (49.92%)
    15.000741145         77,330,037      LLC-loads                 #   15.466 M/sec                    (49.92%)
    15.000741145            310,632      LLC-loads                 #    0.062 M/sec                    (41.60%)
    15.000741145          1,241,680      LLC-store-misses          #    0.248 M/sec                    (24.97%)
    15.000741145                713      LLC-store-misses          #    0.143 K/sec                    (33.37%)
    15.000741145         15,890,346      LLC-stores                #    3.178 M/sec                    (25.12%)
    15.000741145             49,277      LLC-stores                #    0.010 M/sec                    (33.44%)
    15.000741145           5,000.08 msec task-clock                #    1.000 CPUs utilized          
    15.000741145     13,399,706,460      cycles:u                  #    2.680 GHz                      (41.76%)
    15.000741145         27,197,736      cycles:k                  #    0.005 GHz                      (41.75%)
    15.000741145     12,137,292,482      instructions:u            #    0.91  insn per cycle           (50.07%)
    15.000741145          7,659,233      instructions:k            #    0.30  insn per cycle           (49.99%)
    20.001047537          4,314,662      LLC-load-misses           #    6.88% of all LL-cache hits     (50.07%)
    20.001047537              6,631      LLC-load-misses           #    2.38% of all LL-cache hits     (49.99%)
    20.001047537         77,395,195      LLC-loads                 #   15.479 M/sec                    (49.92%)
    20.001047537            312,782      LLC-loads                 #    0.063 M/sec                    (41.60%)
    20.001047537          1,232,390      LLC-store-misses          #    0.246 M/sec                    (24.96%)
    20.001047537                805      LLC-store-misses          #    0.161 K/sec                    (33.28%)
    20.001047537         15,945,751      LLC-stores                #    3.189 M/sec                    (24.97%)
    20.001047537             51,365      LLC-stores                #    0.010 M/sec                    (33.37%)
    20.001047537           5,000.14 msec task-clock                #    1.000 CPUs utilized          
    20.001047537     13,401,769,317      cycles:u                  #    2.680 GHz                      (41.76%)
    20.001047537         25,848,595      cycles:k                  #    0.005 GHz                      (41.76%)
    20.001047537     12,144,924,422      instructions:u            #    0.91  insn per cycle           (50.08%)
    20.001047537          7,581,631      instructions:k            #    0.30  insn per cycle           (50.08%)
    25.001285326          4,315,385      LLC-load-misses           #    6.58% of all LL-cache hits     (50.08%)
    25.001285326              5,842      LLC-load-misses           #    2.06% of all LL-cache hits     (50.08%)
    25.001285326         77,399,651      LLC-loads                 #   15.480 M/sec                    (50.08%)
    25.001285326            307,596      LLC-loads                 #    0.062 M/sec                    (41.60%)
    25.001285326          1,234,851      LLC-store-misses          #    0.247 M/sec                    (24.96%)
    25.001285326                823      LLC-store-misses          #    0.165 K/sec                    (33.28%)
    25.001285326         15,928,747      LLC-stores                #    3.186 M/sec                    (24.96%)
    25.001285326             50,116      LLC-stores                #    0.010 M/sec                    (33.28%)
    25.001285326           5,000.08 msec task-clock                #    1.000 CPUs utilized          
    25.001285326     13,401,126,481      cycles:u                  #    2.680 GHz                      (41.60%)
    25.001285326         26,849,688      cycles:k                  #    0.005 GHz                      (41.68%)
    25.001285326     12,150,105,320      instructions:u            #    0.91  insn per cycle           (50.08%)
    25.001285326          7,770,478      instructions:k            #    0.30  insn per cycle           (50.08%)
    30.001536814          4,303,840      LLC-load-misses           #    6.37% of all LL-cache hits     (50.08%)
    30.001536814              9,584      LLC-load-misses           #    3.32% of all LL-cache hits     (50.08%)
    30.001536814         77,380,820      LLC-loads                 #   15.476 M/sec                    (50.08%)
    30.001536814            312,667      LLC-loads                 #    0.063 M/sec                    (41.76%)
    30.001536814          1,236,020      LLC-store-misses          #    0.247 M/sec                    (24.96%)
    30.001536814                655      LLC-store-misses          #    0.131 K/sec                    (33.28%)
    30.001536814         15,961,785      LLC-stores                #    3.192 M/sec                    (24.96%)
    30.001536814             49,155      LLC-stores                #    0.010 M/sec                    (33.28%)
    30.001536814           5,000.09 msec task-clock                #    1.000 CPUs utilized          
    30.001536814     13,400,876,406      cycles:u                  #    2.680 GHz                      (41.60%)
    30.001536814         26,790,736      cycles:k                  #    0.005 GHz                      (41.60%)
    30.001536814     12,147,966,057      instructions:u            #    0.91  insn per cycle           (49.92%)
    30.001536814          7,634,589      instructions:k            #    0.29  insn per cycle           (50.00%)
    35.001791635          3,405,886      LLC-load-misses           #    5.11% of all LL-cache hits     (49.92%)
    35.001791635             11,207      LLC-load-misses           #    3.92% of all LL-cache hits     (49.99%)
    35.001791635         60,785,059      LLC-loads                 #   12.157 M/sec                    (50.07%)
    35.001791635            268,558      LLC-loads                 #    0.054 M/sec                    (41.76%)
    35.001791635            975,653      LLC-store-misses          #    0.195 M/sec                    (25.05%)
    35.001791635              2,706      LLC-store-misses          #    0.541 K/sec                    (33.37%)
    35.001791635         12,589,327      LLC-stores                #    2.518 M/sec                    (24.96%)
    35.001791635             44,524      LLC-stores                #    0.009 M/sec                    (33.28%)
    35.001791635           5,000.08 msec task-clock                #    1.000 CPUs utilized          
    35.001791635     13,405,504,537      cycles:u                  #    2.681 GHz                      (41.60%)
    35.001791635         24,565,193      cycles:k                  #    0.005 GHz                      (41.60%)
    35.001791635     14,507,639,685      instructions:u            #    1.08  insn per cycle           (49.92%)
    35.001791635          7,882,575      instructions:k            #    0.31  insn per cycle           (49.92%)
    36.074273987              1,615      LLC-load-misses           #    0.00% of all LL-cache hits     (49.32%)
    36.074273987              1,190      LLC-load-misses           #    0.47% of all LL-cache hits     (49.23%)
    36.074273987             45,056      LLC-loads                 #    0.010 M/sec                    (49.23%)
    36.074273987             11,181      LLC-loads                 #    0.002 M/sec                    (41.35%)
    36.074273987                  0      LLC-store-misses          #    0.000 K/sec                    (25.31%)
    36.074273987              1,038      LLC-store-misses          #    0.230 K/sec                    (33.89%)
    36.074273987                322      LLC-stores                #    0.071 K/sec                    (25.74%)
    36.074273987              3,202      LLC-stores                #    0.710 K/sec                    (34.03%)
    36.074273987           1,072.43 msec task-clock                #    0.214 CPUs utilized          
    36.074273987      2,877,719,456      cycles:u                  #    0.638 GHz                      (42.24%)
    36.074273987          4,575,445      cycles:k                  #    0.001 GHz                      (41.86%)
    36.074273987      5,258,608,419      instructions:u            #    0.44  insn per cycle           (50.07%)
    36.074273987          1,627,476      instructions:k            #    0.07  insn per cycle           (49.70%)
