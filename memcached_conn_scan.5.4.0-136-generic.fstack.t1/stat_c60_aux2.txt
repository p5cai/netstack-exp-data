# started on Sun Apr  2 13:05:28 2023

#           time             counts unit events
     5.000180179            316,632      LLC-load-misses           #   19.95% of all LL-cache hits     (49.98%)
     5.000180179             13,715      LLC-load-misses           #   14.42% of all LL-cache hits     (50.06%)
     5.000180179          1,587,054      LLC-loads                 #    0.317 M/sec                    (50.08%)
     5.000180179             95,138      LLC-loads                 #    0.019 M/sec                    (41.76%)
     5.000180179            285,615      LLC-store-misses          #    0.057 M/sec                    (24.98%)
     5.000180179              9,182      LLC-store-misses          #    0.002 M/sec                    (33.30%)
     5.000180179            578,484      LLC-stores                #    0.116 M/sec                    (24.96%)
     5.000180179             26,854      LLC-stores                #    0.005 M/sec                    (33.28%)
     5.000180179           5,000.06 msec task-clock                #    1.000 CPUs utilized          
     5.000180179     13,417,045,493      cycles:u                  #    2.683 GHz                      (41.60%)
     5.000180179         22,521,679      cycles:k                  #    0.005 GHz                      (41.60%)
     5.000180179     24,361,856,983      instructions:u            #    1.82  insn per cycle           (49.92%)
     5.000180179          7,721,878      instructions:k            #    0.34  insn per cycle           (49.92%)
    10.000481526          4,486,035      LLC-load-misses           #   11.45% of all LL-cache hits     (49.92%)
    10.000481526             10,573      LLC-load-misses           #    5.72% of all LL-cache hits     (49.92%)
    10.000481526         76,767,443      LLC-loads                 #   15.353 M/sec                    (49.97%)
    10.000481526            274,270      LLC-loads                 #    0.055 M/sec                    (41.73%)
    10.000481526          1,324,276      LLC-store-misses          #    0.265 M/sec                    (25.12%)
    10.000481526              1,656      LLC-store-misses          #    0.331 K/sec                    (33.44%)
    10.000481526         14,627,146      LLC-stores                #    2.925 M/sec                    (24.99%)
    10.000481526             43,779      LLC-stores                #    0.009 M/sec                    (33.31%)
    10.000481526           5,000.21 msec task-clock                #    1.000 CPUs utilized          
    10.000481526     13,405,156,281      cycles:u                  #    2.681 GHz                      (41.63%)
    10.000481526         27,723,341      cycles:k                  #    0.006 GHz                      (41.60%)
    10.000481526     13,392,905,841      instructions:u            #    1.00  insn per cycle           (49.92%)
    10.000481526          7,094,945      instructions:k            #    0.28  insn per cycle           (49.92%)
    15.000727478          4,908,981      LLC-load-misses           #    9.11% of all LL-cache hits     (49.92%)
    15.000727478             13,155      LLC-load-misses           #    5.93% of all LL-cache hits     (49.92%)
    15.000727478         83,323,647      LLC-loads                 #   16.664 M/sec                    (49.92%)
    15.000727478            296,271      LLC-loads                 #    0.059 M/sec                    (41.60%)
    15.000727478          1,461,262      LLC-store-misses          #    0.292 M/sec                    (25.01%)
    15.000727478              1,786      LLC-store-misses          #    0.357 K/sec                    (33.41%)
    15.000727478         15,940,726      LLC-stores                #    3.188 M/sec                    (25.12%)
    15.000727478             49,011      LLC-stores                #    0.010 M/sec                    (33.44%)
    15.000727478           5,000.15 msec task-clock                #    1.000 CPUs utilized          
    15.000727478     13,403,878,655      cycles:u                  #    2.681 GHz                      (41.76%)
    15.000727478         29,330,334      cycles:k                  #    0.006 GHz                      (41.71%)
    15.000727478     12,347,795,366      instructions:u            #    0.92  insn per cycle           (50.03%)
    15.000727478          7,471,632      instructions:k            #    0.28  insn per cycle           (49.95%)
    20.001007930          4,906,801      LLC-load-misses           #    8.00% of all LL-cache hits     (50.04%)
    20.001007930             14,918      LLC-load-misses           #    6.17% of all LL-cache hits     (49.96%)
    20.001007930         83,510,894      LLC-loads                 #   16.702 M/sec                    (49.92%)
    20.001007930            302,138      LLC-loads                 #    0.060 M/sec                    (41.60%)
    20.001007930          1,456,357      LLC-store-misses          #    0.291 M/sec                    (24.96%)
    20.001007930              2,391      LLC-store-misses          #    0.478 K/sec                    (33.28%)
    20.001007930         15,986,476      LLC-stores                #    3.197 M/sec                    (25.00%)
    20.001007930             47,664      LLC-stores                #    0.010 M/sec                    (33.40%)
    20.001007930           5,000.19 msec task-clock                #    1.000 CPUs utilized          
    20.001007930     13,403,825,153      cycles:u                  #    2.681 GHz                      (41.76%)
    20.001007930         28,506,240      cycles:k                  #    0.006 GHz                      (41.76%)
    20.001007930     12,357,941,727      instructions:u            #    0.92  insn per cycle           (50.08%)
    20.001007930          7,130,455      instructions:k            #    0.26  insn per cycle           (50.08%)
    25.001251056          4,878,652      LLC-load-misses           #    7.42% of all LL-cache hits     (50.08%)
    25.001251056              9,484      LLC-load-misses           #    3.73% of all LL-cache hits     (50.08%)
    25.001251056         83,400,592      LLC-loads                 #   16.680 M/sec                    (50.04%)
    25.001251056            303,932      LLC-loads                 #    0.061 M/sec                    (41.60%)
    25.001251056          1,458,248      LLC-store-misses          #    0.292 M/sec                    (24.96%)
    25.001251056              2,938      LLC-store-misses          #    0.588 K/sec                    (33.28%)
    25.001251056         16,024,644      LLC-stores                #    3.205 M/sec                    (24.96%)
    25.001251056             49,678      LLC-stores                #    0.010 M/sec                    (33.28%)
    25.001251056           5,000.15 msec task-clock                #    1.000 CPUs utilized          
    25.001251056     13,401,964,863      cycles:u                  #    2.680 GHz                      (41.64%)
    25.001251056         29,707,791      cycles:k                  #    0.006 GHz                      (41.72%)
    25.001251056     12,364,460,205      instructions:u            #    0.92  insn per cycle           (50.08%)
    25.001251056          7,169,888      instructions:k            #    0.26  insn per cycle           (50.08%)
    30.001495655          4,907,147      LLC-load-misses           #    7.14% of all LL-cache hits     (50.08%)
    30.001495655             13,982      LLC-load-misses           #    5.37% of all LL-cache hits     (50.08%)
    30.001495655         83,505,278      LLC-loads                 #   16.701 M/sec                    (50.08%)
    30.001495655            290,661      LLC-loads                 #    0.058 M/sec                    (41.73%)
    30.001495655          1,476,169      LLC-store-misses          #    0.295 M/sec                    (24.96%)
    30.001495655              1,177      LLC-store-misses          #    0.235 K/sec                    (33.28%)
    30.001495655         16,042,225      LLC-stores                #    3.208 M/sec                    (24.96%)
    30.001495655             47,006      LLC-stores                #    0.009 M/sec                    (33.28%)
    30.001495655           5,000.15 msec task-clock                #    1.000 CPUs utilized          
    30.001495655     13,404,139,116      cycles:u                  #    2.681 GHz                      (41.60%)
    30.001495655         28,651,328      cycles:k                  #    0.006 GHz                      (41.60%)
    30.001495655     12,370,226,351      instructions:u            #    0.92  insn per cycle           (49.95%)
    30.001495655          7,306,613      instructions:k            #    0.26  insn per cycle           (50.03%)
    35.001735554          4,908,425      LLC-load-misses           #    6.93% of all LL-cache hits     (49.95%)
    35.001735554             16,840      LLC-load-misses           #    6.33% of all LL-cache hits     (50.03%)
    35.001735554         83,382,776      LLC-loads                 #   16.676 M/sec                    (50.08%)
    35.001735554            299,261      LLC-loads                 #    0.060 M/sec                    (41.77%)
    35.001735554          1,463,973      LLC-store-misses          #    0.293 M/sec                    (25.01%)
    35.001735554                816      LLC-store-misses          #    0.163 K/sec                    (33.33%)
    35.001735554         16,029,287      LLC-stores                #    3.206 M/sec                    (24.96%)
    35.001735554             46,663      LLC-stores                #    0.009 M/sec                    (33.28%)
    35.001735554           5,000.15 msec task-clock                #    1.000 CPUs utilized          
    35.001735554     13,405,933,449      cycles:u                  #    2.681 GHz                      (41.60%)
    35.001735554         26,541,816      cycles:k                  #    0.005 GHz                      (41.60%)
    35.001735554     12,367,515,315      instructions:u            #    0.92  insn per cycle           (49.92%)
    35.001735554          7,067,712      instructions:k            #    0.26  insn per cycle           (49.92%)
    37.636160296            611,348      LLC-load-misses           #    0.97% of all LL-cache hits     (50.11%)
    37.636160296              9,066      LLC-load-misses           #    3.71% of all LL-cache hits     (50.00%)
    37.636160296          7,473,202      LLC-loads                 #    1.589 M/sec                    (49.89%)
    37.636160296             91,293      LLC-loads                 #    0.019 M/sec                    (41.54%)
    37.636160296            175,592      LLC-store-misses          #    0.037 M/sec                    (24.95%)
    37.636160296              4,552      LLC-store-misses          #    0.968 K/sec                    (33.30%)
    37.636160296          1,454,901      LLC-stores                #    0.309 M/sec                    (25.05%)
    37.636160296             17,144      LLC-stores                #    0.004 M/sec                    (33.40%)
    37.636160296           2,634.37 msec task-clock                #    0.527 CPUs utilized          
    37.636160296      7,066,365,929      cycles:u                  #    1.502 GHz                      (41.76%)
    37.636160296         13,988,281      cycles:k                  #    0.003 GHz                      (41.75%)
    37.636160296     11,343,208,307      instructions:u            #    0.90  insn per cycle           (50.11%)
    37.636160296          3,890,622      instructions:k            #    0.15  insn per cycle           (50.11%)
