# started on Sun Apr  2 13:24:08 2023

#           time             counts unit events
     5.000164490          1,197,559      LLC-load-misses           #    6.59% of all LL-cache hits     (49.99%)
     5.000164490             15,301      LLC-load-misses           #    9.42% of all LL-cache hits     (50.07%)
     5.000164490         18,183,685      LLC-loads                 #    3.637 M/sec                    (50.08%)
     5.000164490            162,459      LLC-loads                 #    0.032 M/sec                    (41.76%)
     5.000164490            372,022      LLC-store-misses          #    0.074 M/sec                    (24.97%)
     5.000164490             14,245      LLC-store-misses          #    0.003 M/sec                    (33.29%)
     5.000164490          3,539,468      LLC-stores                #    0.708 M/sec                    (24.96%)
     5.000164490             33,454      LLC-stores                #    0.007 M/sec                    (33.28%)
     5.000164490           4,999.94 msec task-clock                #    1.000 CPUs utilized          
     5.000164490     13,407,428,615      cycles:u                  #    2.682 GHz                      (41.60%)
     5.000164490         26,657,221      cycles:k                  #    0.005 GHz                      (41.60%)
     5.000164490     21,736,013,759      instructions:u            #    1.62  insn per cycle           (49.92%)
     5.000164490          7,848,072      instructions:k            #    0.29  insn per cycle           (49.92%)
    10.000502807          4,405,481      LLC-load-misses           #    9.01% of all LL-cache hits     (49.92%)
    10.000502807             13,198      LLC-load-misses           #    5.74% of all LL-cache hits     (49.92%)
    10.000502807         79,645,318      LLC-loads                 #   15.929 M/sec                    (49.99%)
    10.000502807            297,690      LLC-loads                 #    0.060 M/sec                    (41.75%)
    10.000502807          1,254,814      LLC-store-misses          #    0.251 M/sec                    (25.13%)
    10.000502807                681      LLC-store-misses          #    0.136 K/sec                    (33.44%)
    10.000502807         16,034,821      LLC-stores                #    3.207 M/sec                    (24.97%)
    10.000502807             44,130      LLC-stores                #    0.009 M/sec                    (33.29%)
    10.000502807           5,000.13 msec task-clock                #    1.000 CPUs utilized          
    10.000502807     13,399,323,186      cycles:u                  #    2.680 GHz                      (41.61%)
    10.000502807         26,003,661      cycles:k                  #    0.005 GHz                      (41.60%)
    10.000502807     12,291,924,713      instructions:u            #    0.92  insn per cycle           (49.92%)
    10.000502807          7,132,278      instructions:k            #    0.27  insn per cycle           (49.92%)
    15.000763485          4,392,244      LLC-load-misses           #    7.42% of all LL-cache hits     (49.92%)
    15.000763485             12,420      LLC-load-misses           #    4.86% of all LL-cache hits     (49.92%)
    15.000763485         79,819,659      LLC-loads                 #   15.964 M/sec                    (49.92%)
    15.000763485            306,203      LLC-loads                 #    0.061 M/sec                    (41.60%)
    15.000763485          1,259,903      LLC-store-misses          #    0.252 M/sec                    (25.02%)
    15.000763485              1,178      LLC-store-misses          #    0.236 K/sec                    (33.42%)
    15.000763485         16,109,350      LLC-stores                #    3.222 M/sec                    (25.12%)
    15.000763485             44,758      LLC-stores                #    0.009 M/sec                    (33.44%)
    15.000763485           5,000.05 msec task-clock                #    1.000 CPUs utilized          
    15.000763485     13,398,886,775      cycles:u                  #    2.680 GHz                      (41.76%)
    15.000763485         27,285,630      cycles:k                  #    0.005 GHz                      (41.70%)
    15.000763485     12,312,935,529      instructions:u            #    0.92  insn per cycle           (50.02%)
    15.000763485          7,091,501      instructions:k            #    0.27  insn per cycle           (49.94%)
    20.001028928          4,416,397      LLC-load-misses           #    6.85% of all LL-cache hits     (50.02%)
    20.001028928              8,153      LLC-load-misses           #    3.06% of all LL-cache hits     (49.94%)
    20.001028928         80,112,607      LLC-loads                 #   16.022 M/sec                    (49.92%)
    20.001028928            298,545      LLC-loads                 #    0.060 M/sec                    (41.60%)
    20.001028928          1,251,906      LLC-store-misses          #    0.250 M/sec                    (24.96%)
    20.001028928              1,075      LLC-store-misses          #    0.215 K/sec                    (33.28%)
    20.001028928         16,194,354      LLC-stores                #    3.239 M/sec                    (25.02%)
    20.001028928             43,478      LLC-stores                #    0.009 M/sec                    (33.42%)
    20.001028928           5,000.06 msec task-clock                #    1.000 CPUs utilized          
    20.001028928     13,401,908,660      cycles:u                  #    2.680 GHz                      (41.76%)
    20.001028928         24,549,794      cycles:k                  #    0.005 GHz                      (41.76%)
    20.001028928     12,334,149,406      instructions:u            #    0.92  insn per cycle           (50.08%)
    20.001028928          6,834,710      instructions:k            #    0.26  insn per cycle           (50.08%)
    25.001253306          4,430,633      LLC-load-misses           #    6.55% of all LL-cache hits     (50.08%)
    25.001253306             12,243      LLC-load-misses           #    4.48% of all LL-cache hits     (50.08%)
    25.001253306         80,217,872      LLC-loads                 #   16.043 M/sec                    (50.03%)
    25.001253306            302,349      LLC-loads                 #    0.060 M/sec                    (41.60%)
    25.001253306          1,261,812      LLC-store-misses          #    0.252 M/sec                    (24.96%)
    25.001253306                907      LLC-store-misses          #    0.181 K/sec                    (33.28%)
    25.001253306         16,211,934      LLC-stores                #    3.242 M/sec                    (24.96%)
    25.001253306             45,091      LLC-stores                #    0.009 M/sec                    (33.28%)
    25.001253306           5,000.02 msec task-clock                #    1.000 CPUs utilized          
    25.001253306     13,398,032,711      cycles:u                  #    2.680 GHz                      (41.65%)
    25.001253306         27,955,822      cycles:k                  #    0.006 GHz                      (41.73%)
    25.001253306     12,343,728,816      instructions:u            #    0.92  insn per cycle           (50.08%)
    25.001253306          7,390,404      instructions:k            #    0.28  insn per cycle           (50.08%)
    30.001512231          4,426,092      LLC-load-misses           #    6.35% of all LL-cache hits     (50.08%)
    30.001512231             16,892      LLC-load-misses           #    6.04% of all LL-cache hits     (50.08%)
    30.001512231         80,308,560      LLC-loads                 #   16.062 M/sec                    (50.08%)
    30.001512231            309,570      LLC-loads                 #    0.062 M/sec                    (41.71%)
    30.001512231          1,266,051      LLC-store-misses          #    0.253 M/sec                    (24.96%)
    30.001512231                904      LLC-store-misses          #    0.181 K/sec                    (33.28%)
    30.001512231         16,222,542      LLC-stores                #    3.244 M/sec                    (24.96%)
    30.001512231             43,287      LLC-stores                #    0.009 M/sec                    (33.28%)
    30.001512231           5,000.06 msec task-clock                #    1.000 CPUs utilized          
    30.001512231     13,403,360,298      cycles:u                  #    2.681 GHz                      (41.60%)
    30.001512231         25,167,438      cycles:k                  #    0.005 GHz                      (41.60%)
    30.001512231     12,350,028,636      instructions:u            #    0.92  insn per cycle           (49.97%)
    30.001512231          7,056,530      instructions:k            #    0.27  insn per cycle           (50.05%)
    35.001754294          3,564,467      LLC-load-misses           #    5.17% of all LL-cache hits     (49.97%)
    35.001754294             11,408      LLC-load-misses           #    4.13% of all LL-cache hits     (50.04%)
    35.001754294         64,098,365      LLC-loads                 #   12.820 M/sec                    (50.08%)
    35.001754294            258,750      LLC-loads                 #    0.052 M/sec                    (41.76%)
    35.001754294            998,761      LLC-store-misses          #    0.200 M/sec                    (25.00%)
    35.001754294              3,091      LLC-store-misses          #    0.618 K/sec                    (33.32%)
    35.001754294         12,907,577      LLC-stores                #    2.581 M/sec                    (24.96%)
    35.001754294             40,533      LLC-stores                #    0.008 M/sec                    (33.28%)
    35.001754294           5,000.03 msec task-clock                #    1.000 CPUs utilized          
    35.001754294     13,403,895,588      cycles:u                  #    2.681 GHz                      (41.60%)
    35.001754294         24,710,531      cycles:k                  #    0.005 GHz                      (41.60%)
    35.001754294     14,544,970,127      instructions:u            #    1.09  insn per cycle           (49.92%)
    35.001754294          7,119,513      instructions:k            #    0.27  insn per cycle           (49.92%)
    36.169099445              1,645      LLC-load-misses           #    0.00% of all LL-cache hits     (49.34%)
    36.169099445              2,413      LLC-load-misses           #    0.99% of all LL-cache hits     (49.34%)
    36.169099445             55,203      LLC-loads                 #    0.012 M/sec                    (49.52%)
    36.169099445             10,817      LLC-loads                 #    0.002 M/sec                    (41.64%)
    36.169099445                  7      LLC-store-misses          #    0.002 K/sec                    (25.53%)
    36.169099445              1,692      LLC-store-misses          #    0.374 K/sec                    (34.10%)
    36.169099445              1,311      LLC-stores                #    0.290 K/sec                    (25.47%)
    36.169099445              4,238      LLC-stores                #    0.937 K/sec                    (33.69%)
    36.169099445           1,167.29 msec task-clock                #    0.233 CPUs utilized          
    36.169099445      3,131,571,856      cycles:u                  #    0.693 GHz                      (41.92%)
    36.169099445          5,698,944      cycles:k                  #    0.001 GHz                      (41.57%)
    36.169099445      5,697,444,567      instructions:u            #    0.47  insn per cycle           (49.80%)
    36.169099445          1,718,459      instructions:k            #    0.07  insn per cycle           (49.45%)
