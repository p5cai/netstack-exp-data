#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       95.8    49.1    29.5    46.5    52.7    87.0   145.2   168.3   239.2
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5264721.7 74597.6 5324898.6 5102477.0 5127862.5 5330946.1 5534029.8 5559415.3 5579723.6

Total QPS = 800058.5 (24001756 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 5900940135 bytes :  187.6 MB/s
TX 1013481629 bytes :   32.2 MB/s
