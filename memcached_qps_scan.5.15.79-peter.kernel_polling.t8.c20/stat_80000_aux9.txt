# started on Fri Mar 31 18:03:17 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,403,253      LLC-load-misses           #   15.73% of all LL-cache accesses  (49.98%)
        31,886,139      LLC-load-misses           #   12.36% of all LL-cache accesses  (49.99%)
        72,504,155      LLC-loads                 #  249.965 K/sec                    (50.00%)
       257,941,220      LLC-loads                 #  889.276 K/sec                    (41.69%)
            41,956      LLC-store-misses          #  144.647 /sec                     (25.02%)
         5,712,964      LLC-store-misses          #   19.696 K/sec                    (33.35%)
        29,020,025      LLC-stores                #  100.049 K/sec                    (25.00%)
       108,612,590      LLC-stores                #  374.452 K/sec                    (33.33%)
        290,057.62 msec task-clock                #    8.000 CPUs utilized          
    14,719,154,148      cycles:u                  #    0.051 GHz                      (41.66%)
   634,591,457,503      cycles:k                  #    2.188 GHz                      (41.64%)
     7,763,028,995      instructions:u            #    0.53  insn per cycle           (49.97%)
 1,051,523,530,391      instructions:k            #    1.66  insn per cycle           (49.97%)

      36.258290204 seconds time elapsed

