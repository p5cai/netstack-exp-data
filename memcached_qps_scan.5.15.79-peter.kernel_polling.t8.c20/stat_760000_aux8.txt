# started on Fri Mar 31 17:54:28 2023


 Performance counter stats for 'CPU(s) 0-7':

       108,357,154      LLC-load-misses           #   18.59% of all LL-cache accesses  (49.98%)
       214,557,072      LLC-load-misses           #    9.27% of all LL-cache accesses  (50.00%)
       582,774,568      LLC-loads                 #    2.020 M/sec                    (50.01%)
     2,313,847,946      LLC-loads                 #    8.021 M/sec                    (41.69%)
         2,519,787      LLC-store-misses          #    8.735 K/sec                    (25.01%)
        67,811,732      LLC-store-misses          #  235.064 K/sec                    (33.34%)
       219,373,018      LLC-stores                #  760.439 K/sec                    (24.99%)
     1,186,104,588      LLC-stores                #    4.112 M/sec                    (33.32%)
        288,482.12 msec task-clock                #    8.000 CPUs utilized          
   108,546,362,105      cycles:u                  #    0.376 GHz                      (41.65%)
   539,702,323,436      cycles:k                  #    1.871 GHz                      (41.65%)
    67,654,302,164      instructions:u            #    0.62  insn per cycle           (49.98%)
   462,412,220,922      instructions:k            #    0.86  insn per cycle           (49.98%)

      36.061343026 seconds time elapsed

