# started on Fri Mar 31 18:48:02 2023


 Performance counter stats for 'CPU(s) 0-7':

       107,954,885      LLC-load-misses           #   18.03% of all LL-cache accesses  (49.97%)
       213,772,266      LLC-load-misses           #    9.19% of all LL-cache accesses  (49.98%)
       598,717,255      LLC-loads                 #    2.072 M/sec                    (49.99%)
     2,325,751,236      LLC-loads                 #    8.049 M/sec                    (41.68%)
         2,442,148      LLC-store-misses          #    8.452 K/sec                    (25.02%)
        69,671,338      LLC-store-misses          #  241.118 K/sec                    (33.36%)
       213,717,873      LLC-stores                #  739.633 K/sec                    (25.01%)
     1,186,929,862      LLC-stores                #    4.108 M/sec                    (33.34%)
        288,951.13 msec task-clock                #    8.000 CPUs utilized          
   110,971,779,409      cycles:u                  #    0.384 GHz                      (41.67%)
   538,920,722,454      cycles:k                  #    1.865 GHz                      (41.66%)
    68,512,174,489      instructions:u            #    0.62  insn per cycle           (49.98%)
   454,593,452,221      instructions:k            #    0.84  insn per cycle           (49.97%)

      36.119954848 seconds time elapsed

