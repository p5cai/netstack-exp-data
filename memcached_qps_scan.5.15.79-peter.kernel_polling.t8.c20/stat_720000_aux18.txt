# started on Fri Mar 31 21:00:42 2023


 Performance counter stats for 'CPU(s) 0-7':

       101,281,720      LLC-load-misses           #   18.98% of all LL-cache accesses  (49.98%)
       191,398,844      LLC-load-misses           #    8.77% of all LL-cache accesses  (49.99%)
       533,618,074      LLC-loads                 #    1.845 M/sec                    (50.01%)
     2,181,606,069      LLC-loads                 #    7.542 M/sec                    (41.69%)
           441,737      LLC-store-misses          #    1.527 K/sec                    (25.02%)
        61,243,574      LLC-store-misses          #  211.719 K/sec                    (33.35%)
       190,317,994      LLC-stores                #  657.930 K/sec                    (25.00%)
     1,113,519,293      LLC-stores                #    3.849 M/sec                    (33.32%)
        289,267.99 msec task-clock                #    8.000 CPUs utilized          
   105,525,025,455      cycles:u                  #    0.365 GHz                      (41.65%)
   543,393,208,612      cycles:k                  #    1.879 GHz                      (41.65%)
    66,133,876,978      instructions:u            #    0.63  insn per cycle           (49.98%)
   467,331,055,088      instructions:k            #    0.86  insn per cycle           (49.97%)

      36.159576149 seconds time elapsed

