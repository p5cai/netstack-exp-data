# started on Fri Mar 31 18:13:11 2023


 Performance counter stats for 'CPU(s) 0-7':

       111,802,481      LLC-load-misses           #   18.30% of all LL-cache accesses  (49.99%)
       204,131,805      LLC-load-misses           #    8.36% of all LL-cache accesses  (49.99%)
       610,784,164      LLC-loads                 #    2.110 M/sec                    (49.99%)
     2,441,543,682      LLC-loads                 #    8.434 M/sec                    (41.66%)
         2,363,977      LLC-store-misses          #    8.166 K/sec                    (25.01%)
        63,680,587      LLC-store-misses          #  219.979 K/sec                    (33.34%)
       214,446,703      LLC-stores                #  740.788 K/sec                    (25.00%)
     1,252,805,277      LLC-stores                #    4.328 M/sec                    (33.34%)
        289,484.61 msec task-clock                #    8.000 CPUs utilized          
   111,004,158,681      cycles:u                  #    0.383 GHz                      (41.67%)
   537,287,788,994      cycles:k                  #    1.856 GHz                      (41.67%)
    70,740,344,122      instructions:u            #    0.64  insn per cycle           (50.00%)
   451,871,401,754      instructions:k            #    0.84  insn per cycle           (49.99%)

      36.186666668 seconds time elapsed

