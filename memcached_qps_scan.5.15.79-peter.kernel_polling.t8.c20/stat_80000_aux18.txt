# started on Fri Mar 31 20:52:37 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,377,369      LLC-load-misses           #   17.54% of all LL-cache accesses  (50.00%)
        32,057,071      LLC-load-misses           #   12.42% of all LL-cache accesses  (50.00%)
        64,869,213      LLC-loads                 #  224.321 K/sec                    (50.01%)
       258,094,910      LLC-loads                 #  892.504 K/sec                    (41.68%)
            41,843      LLC-store-misses          #  144.695 /sec                     (25.00%)
         5,334,235      LLC-store-misses          #   18.446 K/sec                    (33.33%)
        28,913,553      LLC-stores                #   99.984 K/sec                    (25.00%)
       104,948,752      LLC-stores                #  362.918 K/sec                    (33.33%)
        289,180.69 msec task-clock                #    8.000 CPUs utilized          
    13,765,926,898      cycles:u                  #    0.048 GHz                      (41.66%)
   633,607,791,795      cycles:k                  #    2.191 GHz                      (41.66%)
     7,675,866,109      instructions:u            #    0.56  insn per cycle           (49.99%)
 1,053,421,494,763      instructions:k            #    1.66  insn per cycle           (49.99%)

      36.148654896 seconds time elapsed

