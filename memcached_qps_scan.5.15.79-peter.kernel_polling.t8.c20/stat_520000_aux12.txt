# started on Fri Mar 31 18:59:29 2023


 Performance counter stats for 'CPU(s) 0-7':

        73,951,226      LLC-load-misses           #   17.15% of all LL-cache accesses  (49.98%)
       155,575,678      LLC-load-misses           #    9.79% of all LL-cache accesses  (49.99%)
       431,255,916      LLC-loads                 #    1.496 M/sec                    (49.99%)
     1,588,345,845      LLC-loads                 #    5.509 M/sec                    (41.66%)
         1,964,978      LLC-store-misses          #    6.816 K/sec                    (25.01%)
        50,983,673      LLC-store-misses          #  176.840 K/sec                    (33.35%)
       168,257,573      LLC-stores                #  583.613 K/sec                    (25.01%)
       789,398,139      LLC-stores                #    2.738 M/sec                    (33.34%)
        288,303.38 msec task-clock                #    8.000 CPUs utilized          
    88,580,576,478      cycles:u                  #    0.307 GHz                      (41.68%)
   560,420,287,687      cycles:k                  #    1.944 GHz                      (41.67%)
    47,887,739,698      instructions:u            #    0.54  insn per cycle           (50.00%)
   561,898,278,444      instructions:k            #    1.00  insn per cycle           (49.99%)

      36.039003255 seconds time elapsed

