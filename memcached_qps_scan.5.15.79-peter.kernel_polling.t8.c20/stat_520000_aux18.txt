# started on Fri Mar 31 20:55:10 2023


 Performance counter stats for 'CPU(s) 0-7':

        73,451,166      LLC-load-misses           #   17.88% of all LL-cache accesses  (49.97%)
       145,820,681      LLC-load-misses           #    9.46% of all LL-cache accesses  (49.98%)
       410,752,984      LLC-loads                 #    1.420 M/sec                    (49.99%)
     1,541,817,367      LLC-loads                 #    5.329 M/sec                    (41.68%)
         1,996,412      LLC-store-misses          #    6.900 K/sec                    (25.02%)
        47,228,585      LLC-store-misses          #  163.224 K/sec                    (33.36%)
       166,932,643      LLC-stores                #  576.925 K/sec                    (25.02%)
       776,522,091      LLC-stores                #    2.684 M/sec                    (33.34%)
        289,349.07 msec task-clock                #    8.000 CPUs utilized          
    81,175,910,900      cycles:u                  #    0.281 GHz                      (41.67%)
   567,734,553,468      cycles:k                  #    1.962 GHz                      (41.66%)
    48,240,112,721      instructions:u            #    0.59  insn per cycle           (49.98%)
   608,344,452,311      instructions:k            #    1.07  insn per cycle           (49.97%)

      36.169671231 seconds time elapsed

