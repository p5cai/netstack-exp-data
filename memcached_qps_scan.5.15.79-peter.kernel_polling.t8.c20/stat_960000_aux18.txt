# started on Fri Mar 31 21:06:31 2023


 Performance counter stats for 'CPU(s) 0-7':

       133,960,156      LLC-load-misses           #   18.61% of all LL-cache accesses  (49.99%)
       249,094,890      LLC-load-misses           #    8.10% of all LL-cache accesses  (49.99%)
       719,869,836      LLC-loads                 #    2.490 M/sec                    (49.99%)
     3,075,458,310      LLC-loads                 #   10.638 M/sec                    (41.66%)
         2,523,785      LLC-store-misses          #    8.730 K/sec                    (25.01%)
        56,461,385      LLC-store-misses          #  195.307 K/sec                    (33.34%)
       225,753,609      LLC-stores                #  780.910 K/sec                    (25.01%)
     1,591,103,530      LLC-stores                #    5.504 M/sec                    (33.34%)
        289,090.30 msec task-clock                #    8.000 CPUs utilized          
   122,086,327,079      cycles:u                  #    0.422 GHz                      (41.67%)
   525,563,694,137      cycles:k                  #    1.818 GHz                      (41.67%)
    82,040,967,050      instructions:u            #    0.67  insn per cycle           (50.00%)
   459,184,574,991      instructions:k            #    0.87  insn per cycle           (49.99%)

      36.137372236 seconds time elapsed

