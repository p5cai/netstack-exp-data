# started on Fri Mar 31 17:10:36 2023


 Performance counter stats for 'CPU(s) 0-7':

        45,922,749      LLC-load-misses           #   18.56% of all LL-cache accesses  (49.97%)
       104,729,359      LLC-load-misses           #   10.78% of all LL-cache accesses  (49.98%)
       247,448,865      LLC-loads                 #  858.541 K/sec                    (49.99%)
       971,724,619      LLC-loads                 #    3.371 M/sec                    (41.68%)
         2,171,368      LLC-store-misses          #    7.534 K/sec                    (25.02%)
        31,514,282      LLC-store-misses          #  109.341 K/sec                    (33.35%)
       115,319,899      LLC-stores                #  400.110 K/sec                    (25.01%)
       472,744,246      LLC-stores                #    1.640 M/sec                    (33.35%)
        288,220.36 msec task-clock                #    8.000 CPUs utilized          
    54,921,825,107      cycles:u                  #    0.191 GHz                      (41.67%)
   593,646,319,509      cycles:k                  #    2.060 GHz                      (41.66%)
    30,449,489,499      instructions:u            #    0.55  insn per cycle           (49.98%)
   776,070,253,415      instructions:k            #    1.31  insn per cycle           (49.97%)

      36.028647957 seconds time elapsed

