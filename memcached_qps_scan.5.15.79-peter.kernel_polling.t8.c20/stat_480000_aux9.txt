# started on Fri Mar 31 18:04:59 2023


 Performance counter stats for 'CPU(s) 0-7':

        67,537,385      LLC-load-misses           #   16.76% of all LL-cache accesses  (50.00%)
       134,158,787      LLC-load-misses           #    9.42% of all LL-cache accesses  (50.00%)
       402,982,629      LLC-loads                 #    1.394 M/sec                    (50.00%)
     1,424,177,297      LLC-loads                 #    4.925 M/sec                    (41.67%)
         2,193,689      LLC-store-misses          #    7.586 K/sec                    (25.00%)
        43,212,788      LLC-store-misses          #  149.437 K/sec                    (33.33%)
       157,926,765      LLC-stores                #  546.139 K/sec                    (25.00%)
       713,321,031      LLC-stores                #    2.467 M/sec                    (33.33%)
        289,169.64 msec task-clock                #    8.000 CPUs utilized          
    80,318,645,555      cycles:u                  #    0.278 GHz                      (41.66%)
   568,173,388,660      cycles:k                  #    1.965 GHz                      (41.66%)
    44,496,581,944      instructions:u            #    0.55  insn per cycle           (49.99%)
   619,967,315,702      instructions:k            #    1.09  insn per cycle           (49.99%)

      36.147280351 seconds time elapsed

