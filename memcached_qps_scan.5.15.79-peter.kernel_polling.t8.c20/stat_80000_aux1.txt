# started on Wed Mar 29 17:26:53 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,522,754      LLC-load-misses           #   16.18% of all LL-cache accesses  (50.00%)
        34,137,375      LLC-load-misses           #   12.30% of all LL-cache accesses  (50.00%)
        71,226,918      LLC-loads                 #  246.642 K/sec                    (50.01%)
       277,649,361      LLC-loads                 #  961.436 K/sec                    (41.67%)
            69,848      LLC-store-misses          #  241.868 /sec                     (25.00%)
         5,430,221      LLC-store-misses          #   18.804 K/sec                    (33.33%)
        29,628,688      LLC-stores                #  102.597 K/sec                    (25.00%)
       109,149,599      LLC-stores                #  377.960 K/sec                    (33.33%)
        288,786.16 msec task-clock                #    8.000 CPUs utilized          
    14,815,047,522      cycles:u                  #    0.051 GHz                      (41.66%)
   633,884,707,855      cycles:k                  #    2.195 GHz                      (41.66%)
     8,667,917,556      instructions:u            #    0.59  insn per cycle           (49.99%)
 1,043,256,724,410      instructions:k            #    1.65  insn per cycle           (49.99%)

      36.099493274 seconds time elapsed

