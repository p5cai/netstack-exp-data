# started on Fri Mar 31 20:10:55 2023


 Performance counter stats for 'CPU(s) 0-7':

        73,954,945      LLC-load-misses           #   18.40% of all LL-cache accesses  (49.99%)
       155,206,166      LLC-load-misses           #    9.87% of all LL-cache accesses  (49.99%)
       401,953,832      LLC-loads                 #    1.392 M/sec                    (49.99%)
     1,571,928,011      LLC-loads                 #    5.445 M/sec                    (41.66%)
         1,978,867      LLC-store-misses          #    6.854 K/sec                    (25.01%)
        47,521,958      LLC-store-misses          #  164.603 K/sec                    (33.34%)
       166,740,985      LLC-stores                #  577.546 K/sec                    (25.00%)
       776,429,560      LLC-stores                #    2.689 M/sec                    (33.34%)
        288,706.09 msec task-clock                #    8.000 CPUs utilized          
    82,651,638,552      cycles:u                  #    0.286 GHz                      (41.67%)
   565,546,357,836      cycles:k                  #    1.959 GHz                      (41.67%)
    48,179,356,410      instructions:u            #    0.58  insn per cycle           (50.00%)
   598,806,162,257      instructions:k            #    1.06  insn per cycle           (49.99%)

      36.089319227 seconds time elapsed

