#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       66.4    27.2    26.8    37.5    40.3    60.6    99.3   113.6   146.8
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4470721.2 71630.1 4400742.6 4217188.7 4238434.0 4408396.3 4578358.5 4599603.8 4707848.5

Total QPS = 680167.9 (20405054 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 5007847813 bytes :  159.2 MB/s
TX  861568557 bytes :   27.4 MB/s
