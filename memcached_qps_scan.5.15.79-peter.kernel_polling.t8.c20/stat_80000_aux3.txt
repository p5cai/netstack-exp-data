# started on Wed Mar 29 18:03:55 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,565,233      LLC-load-misses           #   15.63% of all LL-cache accesses  (49.99%)
        34,671,255      LLC-load-misses           #   12.55% of all LL-cache accesses  (50.00%)
        74,002,504      LLC-loads                 #  255.951 K/sec                    (50.00%)
       276,288,155      LLC-loads                 #  955.591 K/sec                    (41.67%)
            54,307      LLC-store-misses          #  187.830 /sec                     (25.00%)
         6,145,418      LLC-store-misses          #   21.255 K/sec                    (33.34%)
        30,236,248      LLC-stores                #  104.577 K/sec                    (25.00%)
       109,786,888      LLC-stores                #  379.717 K/sec                    (33.33%)
        289,127.90 msec task-clock                #    8.000 CPUs utilized          
    14,828,037,898      cycles:u                  #    0.051 GHz                      (41.67%)
   633,639,675,309      cycles:k                  #    2.192 GHz                      (41.67%)
     7,776,031,064      instructions:u            #    0.52  insn per cycle           (50.00%)
 1,044,155,673,112      instructions:k            #    1.65  insn per cycle           (49.99%)

      36.142218839 seconds time elapsed

