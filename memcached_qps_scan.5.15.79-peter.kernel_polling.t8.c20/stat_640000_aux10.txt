# started on Fri Mar 31 18:27:02 2023


 Performance counter stats for 'CPU(s) 0-7':

        90,972,020      LLC-load-misses           #   18.27% of all LL-cache accesses  (49.98%)
       185,279,774      LLC-load-misses           #    9.51% of all LL-cache accesses  (49.98%)
       498,052,081      LLC-loads                 #    1.723 M/sec                    (49.98%)
     1,949,160,326      LLC-loads                 #    6.744 M/sec                    (41.66%)
         2,347,537      LLC-store-misses          #    8.122 K/sec                    (25.01%)
        58,539,993      LLC-store-misses          #  202.535 K/sec                    (33.35%)
       180,541,422      LLC-stores                #  624.633 K/sec                    (25.01%)
       977,324,518      LLC-stores                #    3.381 M/sec                    (33.35%)
        289,035.82 msec task-clock                #    8.000 CPUs utilized          
    95,538,124,153      cycles:u                  #    0.331 GHz                      (41.68%)
   552,435,913,879      cycles:k                  #    1.911 GHz                      (41.67%)
    58,126,817,458      instructions:u            #    0.61  insn per cycle           (50.00%)
   518,526,612,892      instructions:k            #    0.94  insn per cycle           (49.99%)

      36.130538246 seconds time elapsed

