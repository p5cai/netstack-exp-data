# started on Fri Mar 31 20:28:42 2023


 Performance counter stats for 'CPU(s) 0-7':

        75,124,175      LLC-load-misses           #   17.47% of all LL-cache accesses  (49.97%)
       155,737,988      LLC-load-misses           #    9.70% of all LL-cache accesses  (49.98%)
       430,114,820      LLC-loads                 #    1.491 M/sec                    (50.00%)
     1,605,508,176      LLC-loads                 #    5.564 M/sec                    (41.68%)
         2,079,828      LLC-store-misses          #    7.208 K/sec                    (25.02%)
        48,679,619      LLC-store-misses          #  168.696 K/sec                    (33.36%)
       171,504,475      LLC-stores                #  594.337 K/sec                    (25.01%)
       780,286,026      LLC-stores                #    2.704 M/sec                    (33.34%)
        288,564.29 msec task-clock                #    8.000 CPUs utilized          
    83,057,517,495      cycles:u                  #    0.288 GHz                      (41.66%)
   565,028,540,547      cycles:k                  #    1.958 GHz                      (41.65%)
    48,163,232,285      instructions:u            #    0.58  insn per cycle           (49.98%)
   594,303,282,516      instructions:k            #    1.05  insn per cycle           (49.97%)

      36.071614775 seconds time elapsed

