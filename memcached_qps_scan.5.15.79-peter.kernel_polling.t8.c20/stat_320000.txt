# started on Wed Mar 29 11:28:31 2023


 Performance counter stats for 'CPU(s) 0-7':

        44,991,869      LLC-load-misses           #   17.62% of all LL-cache accesses  (49.99%)
        98,672,367      LLC-load-misses           #    9.82% of all LL-cache accesses  (49.99%)
       255,355,686      LLC-loads                 #  880.948 K/sec                    (49.99%)
     1,004,329,402      LLC-loads                 #    3.465 M/sec                    (41.66%)
         2,150,695      LLC-store-misses          #    7.420 K/sec                    (25.01%)
        28,714,447      LLC-store-misses          #   99.062 K/sec                    (33.34%)
       119,034,894      LLC-stores                #  410.657 K/sec                    (25.00%)
       483,582,463      LLC-stores                #    1.668 M/sec                    (33.34%)
        289,864.60 msec task-clock                #    8.000 CPUs utilized          
    56,254,998,438      cycles:u                  #    0.194 GHz                      (41.67%)
   592,383,934,493      cycles:k                  #    2.044 GHz                      (41.67%)
    32,214,365,498      instructions:u            #    0.57  insn per cycle           (50.00%)
   763,646,077,733      instructions:k            #    1.29  insn per cycle           (50.00%)

      36.234803831 seconds time elapsed

