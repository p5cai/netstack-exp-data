# started on Fri Mar 31 18:18:08 2023


 Performance counter stats for 'CPU(s) 0-7':

       132,333,525      LLC-load-misses           #   18.35% of all LL-cache accesses  (49.98%)
       259,282,590      LLC-load-misses           #    8.34% of all LL-cache accesses  (50.00%)
       721,266,020      LLC-loads                 #    2.497 M/sec                    (50.01%)
     3,108,529,411      LLC-loads                 #   10.761 M/sec                    (41.69%)
         2,204,664      LLC-store-misses          #    7.632 K/sec                    (25.01%)
        25,421,733      LLC-store-misses          #   88.004 K/sec                    (33.34%)
       227,329,102      LLC-stores                #  786.962 K/sec                    (24.99%)
     1,611,642,193      LLC-stores                #    5.579 M/sec                    (33.32%)
        288,869.14 msec task-clock                #    8.000 CPUs utilized          
   125,220,600,330      cycles:u                  #    0.433 GHz                      (41.65%)
   522,963,845,789      cycles:k                  #    1.810 GHz                      (41.65%)
    83,086,187,326      instructions:u            #    0.66  insn per cycle           (49.98%)
   460,664,557,632      instructions:k            #    0.88  insn per cycle           (49.98%)

      36.109732661 seconds time elapsed

