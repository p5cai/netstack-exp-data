#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1171.6    48.1   163.9  1064.9  1079.3  1181.3  1256.4  1265.8  1335.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6281531.1 181271.7 5857388.4 5712453.3 5840105.9 6364438.9 6678974.9 6718291.9 6749745.5

Total QPS = 955072.2 (28652166 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 7045794309 bytes :  224.0 MB/s
TX 1209731233 bytes :   38.5 MB/s
