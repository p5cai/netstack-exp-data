#type       avg     std     min     5th    10th    50th    90th    95th    99th
read      432.0   341.1    29.5    81.8   102.8   289.3   988.9  1138.2  1249.9
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6056478.5 83536.6 5857388.4 5617176.4 5649552.0 5908557.3 6310824.8 6534216.8 6712930.5

Total QPS = 920208.4 (27606257 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6779854952 bytes :  215.5 MB/s
TX 1165621761 bytes :   37.1 MB/s
