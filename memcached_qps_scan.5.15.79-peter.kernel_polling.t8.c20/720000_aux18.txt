#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       78.4    35.6    29.5    42.6    47.0    72.4   115.3   131.7   177.6
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      4722025.7 81966.7 4400742.6 4435711.6 4621860.4 4824185.4 5026510.3 5051800.9 5072033.4

Total QPS = 720092.1 (21602766 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 5297856633 bytes :  168.4 MB/s
TX  912169393 bytes :   29.0 MB/s
