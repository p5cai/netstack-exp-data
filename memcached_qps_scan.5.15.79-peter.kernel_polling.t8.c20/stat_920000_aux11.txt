# started on Fri Mar 31 18:52:07 2023


 Performance counter stats for 'CPU(s) 0-7':

       130,975,630      LLC-load-misses           #   19.05% of all LL-cache accesses  (49.98%)
       260,364,820      LLC-load-misses           #    8.71% of all LL-cache accesses  (49.99%)
       687,611,925      LLC-loads                 #    2.385 M/sec                    (49.99%)
     2,988,133,956      LLC-loads                 #   10.364 M/sec                    (41.66%)
         2,593,730      LLC-store-misses          #    8.996 K/sec                    (25.01%)
        81,571,458      LLC-store-misses          #  282.930 K/sec                    (33.34%)
       224,612,474      LLC-stores                #  779.066 K/sec                    (25.01%)
     1,523,350,794      LLC-stores                #    5.284 M/sec                    (33.34%)
        288,310.04 msec task-clock                #    8.000 CPUs utilized          
   121,955,713,353      cycles:u                  #    0.423 GHz                      (41.67%)
   525,640,177,703      cycles:k                  #    1.823 GHz                      (41.67%)
    79,258,655,483      instructions:u            #    0.65  insn per cycle           (50.00%)
   446,232,655,164      instructions:k            #    0.85  insn per cycle           (49.99%)

      36.039841242 seconds time elapsed

