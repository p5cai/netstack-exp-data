# started on Fri Mar 31 17:09:45 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,424,670      LLC-load-misses           #   19.00% of all LL-cache accesses  (49.99%)
        33,403,223      LLC-load-misses           #   12.85% of all LL-cache accesses  (50.00%)
        60,114,203      LLC-loads                 #  208.118 K/sec                    (50.01%)
       259,941,808      LLC-loads                 #  899.929 K/sec                    (41.69%)
            42,610      LLC-store-misses          #  147.518 /sec                     (25.01%)
         5,765,974      LLC-store-misses          #   19.962 K/sec                    (33.34%)
        29,663,537      LLC-stores                #  102.696 K/sec                    (24.99%)
       106,758,169      LLC-stores                #  369.601 K/sec                    (33.32%)
        288,846.88 msec task-clock                #    8.000 CPUs utilized          
    14,302,590,500      cycles:u                  #    0.050 GHz                      (41.65%)
   633,523,628,592      cycles:k                  #    2.193 GHz                      (41.65%)
     7,691,628,762      instructions:u            #    0.54  insn per cycle           (49.98%)
 1,051,182,970,432      instructions:k            #    1.66  insn per cycle           (49.98%)

      36.106956255 seconds time elapsed

