# started on Fri Mar 31 20:00:17 2023


 Performance counter stats for 'CPU(s) 0-7':

       114,484,985      LLC-load-misses           #   18.38% of all LL-cache accesses  (49.99%)
       222,306,003      LLC-load-misses           #    8.97% of all LL-cache accesses  (49.99%)
       622,750,000      LLC-loads                 #    2.151 M/sec                    (50.00%)
     2,477,300,269      LLC-loads                 #    8.557 M/sec                    (41.66%)
         2,540,895      LLC-store-misses          #    8.777 K/sec                    (25.01%)
        75,141,923      LLC-store-misses          #  259.560 K/sec                    (33.34%)
       210,465,111      LLC-stores                #  727.001 K/sec                    (25.00%)
     1,264,481,756      LLC-stores                #    4.368 M/sec                    (33.34%)
        289,497.84 msec task-clock                #    8.000 CPUs utilized          
   112,552,345,098      cycles:u                  #    0.389 GHz                      (41.67%)
   536,431,704,875      cycles:k                  #    1.853 GHz                      (41.67%)
    70,408,179,564      instructions:u            #    0.63  insn per cycle           (50.00%)
   449,425,564,237      instructions:k            #    0.84  insn per cycle           (50.00%)

      36.188283807 seconds time elapsed

