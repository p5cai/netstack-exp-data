# started on Fri Mar 31 17:31:43 2023


 Performance counter stats for 'CPU(s) 0-7':

        79,454,746      LLC-load-misses           #   18.14% of all LL-cache accesses  (50.00%)
       165,670,077      LLC-load-misses           #    9.82% of all LL-cache accesses  (50.01%)
       438,096,110      LLC-loads                 #    1.513 M/sec                    (50.01%)
     1,687,543,132      LLC-loads                 #    5.827 M/sec                    (41.68%)
           379,687      LLC-store-misses          #    1.311 K/sec                    (25.00%)
        52,594,340      LLC-store-misses          #  181.618 K/sec                    (33.33%)
       177,093,203      LLC-stores                #  611.536 K/sec                    (25.00%)
       839,887,805      LLC-stores                #    2.900 M/sec                    (33.33%)
        289,587.32 msec task-clock                #    8.000 CPUs utilized          
    90,909,005,303      cycles:u                  #    0.314 GHz                      (41.66%)
   558,420,014,504      cycles:k                  #    1.928 GHz                      (41.66%)
    51,194,007,678      instructions:u            #    0.56  insn per cycle           (49.99%)
   554,806,457,475      instructions:k            #    0.99  insn per cycle           (49.99%)

      36.199495410 seconds time elapsed

