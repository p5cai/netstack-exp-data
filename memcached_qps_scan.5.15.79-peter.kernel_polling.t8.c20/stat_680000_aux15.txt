# started on Fri Mar 31 19:57:06 2023


 Performance counter stats for 'CPU(s) 0-7':

        96,375,115      LLC-load-misses           #   18.59% of all LL-cache accesses  (50.00%)
       196,096,052      LLC-load-misses           #    9.61% of all LL-cache accesses  (50.00%)
       518,421,995      LLC-loads                 #    1.795 M/sec                    (50.00%)
     2,040,883,072      LLC-loads                 #    7.068 M/sec                    (41.67%)
         2,310,383      LLC-store-misses          #    8.001 K/sec                    (25.00%)
        65,691,029      LLC-store-misses          #  227.498 K/sec                    (33.34%)
       197,322,286      LLC-stores                #  683.358 K/sec                    (25.00%)
     1,036,169,134      LLC-stores                #    3.588 M/sec                    (33.33%)
        288,753.72 msec task-clock                #    8.000 CPUs utilized          
    99,683,143,750      cycles:u                  #    0.345 GHz                      (41.67%)
   549,181,518,989      cycles:k                  #    1.902 GHz                      (41.66%)
    61,646,294,339      instructions:u            #    0.62  insn per cycle           (50.00%)
   501,437,548,802      instructions:k            #    0.91  insn per cycle           (50.00%)

      36.095266216 seconds time elapsed

