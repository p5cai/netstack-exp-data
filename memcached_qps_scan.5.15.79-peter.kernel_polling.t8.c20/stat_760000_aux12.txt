# started on Fri Mar 31 19:05:56 2023


 Performance counter stats for 'CPU(s) 0-7':

       109,281,486      LLC-load-misses           #   18.50% of all LL-cache accesses  (49.99%)
       214,587,047      LLC-load-misses           #    9.28% of all LL-cache accesses  (49.99%)
       590,822,930      LLC-loads                 #    2.049 M/sec                    (49.99%)
     2,312,718,098      LLC-loads                 #    8.021 M/sec                    (41.66%)
         2,504,648      LLC-store-misses          #    8.687 K/sec                    (25.01%)
        70,333,449      LLC-store-misses          #  243.932 K/sec                    (33.34%)
       204,859,389      LLC-stores                #  710.499 K/sec                    (25.00%)
     1,183,063,462      LLC-stores                #    4.103 M/sec                    (33.34%)
        288,331.74 msec task-clock                #    8.000 CPUs utilized          
   107,799,280,785      cycles:u                  #    0.374 GHz                      (41.67%)
   541,576,416,854      cycles:k                  #    1.878 GHz                      (41.67%)
    67,725,036,863      instructions:u            #    0.63  insn per cycle           (50.00%)
   462,725,448,426      instructions:k            #    0.85  insn per cycle           (49.99%)

      36.042573947 seconds time elapsed

