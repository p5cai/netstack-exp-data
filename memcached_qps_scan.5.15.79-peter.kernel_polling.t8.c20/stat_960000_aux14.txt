# started on Fri Mar 31 19:46:32 2023


 Performance counter stats for 'CPU(s) 0-7':

       133,092,442      LLC-load-misses           #   18.65% of all LL-cache accesses  (49.98%)
       274,994,865      LLC-load-misses           #    8.90% of all LL-cache accesses  (49.99%)
       713,679,146      LLC-loads                 #    2.464 M/sec                    (50.01%)
     3,090,817,039      LLC-loads                 #   10.671 M/sec                    (41.69%)
         2,511,331      LLC-store-misses          #    8.670 K/sec                    (25.02%)
        46,734,725      LLC-store-misses          #  161.349 K/sec                    (33.35%)
       229,546,725      LLC-stores                #  792.499 K/sec                    (24.99%)
     1,583,552,441      LLC-stores                #    5.467 M/sec                    (33.32%)
        289,649.31 msec task-clock                #    8.000 CPUs utilized          
   123,360,142,715      cycles:u                  #    0.426 GHz                      (41.65%)
   524,842,964,710      cycles:k                  #    1.812 GHz                      (41.64%)
    81,850,463,870      instructions:u            #    0.66  insn per cycle           (49.97%)
   455,875,881,556      instructions:k            #    0.87  insn per cycle           (49.97%)

      36.207229921 seconds time elapsed

