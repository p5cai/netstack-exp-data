# started on Fri Mar 31 17:00:07 2023


 Performance counter stats for 'CPU(s) 0-7':

       102,111,865      LLC-load-misses           #   17.48% of all LL-cache accesses  (49.98%)
       206,628,123      LLC-load-misses           #    9.24% of all LL-cache accesses  (49.99%)
       584,027,654      LLC-loads                 #    2.025 M/sec                    (50.01%)
     2,236,317,519      LLC-loads                 #    7.752 M/sec                    (41.69%)
           543,359      LLC-store-misses          #    1.884 K/sec                    (25.02%)
        68,858,443      LLC-store-misses          #  238.696 K/sec                    (33.35%)
       210,554,431      LLC-stores                #  729.881 K/sec                    (24.99%)
     1,128,945,707      LLC-stores                #    3.913 M/sec                    (33.32%)
        288,477.95 msec task-clock                #    8.000 CPUs utilized          
   108,324,566,086      cycles:u                  #    0.376 GHz                      (41.65%)
   539,264,572,943      cycles:k                  #    1.869 GHz                      (41.65%)
    63,860,497,559      instructions:u            #    0.59  insn per cycle           (49.98%)
   456,452,332,591      instructions:k            #    0.85  insn per cycle           (49.98%)

      36.060834755 seconds time elapsed

