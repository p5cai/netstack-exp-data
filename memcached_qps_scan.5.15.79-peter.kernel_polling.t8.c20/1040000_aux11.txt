#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1174.3    44.9   163.9  1067.5  1085.2  1189.6  1256.9  1265.3  1287.7
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6292488.6 162538.8 5857388.4 5756640.7 5928480.8 6394596.8 6692452.9 6729684.9 6892761.0

Total QPS = 954011.6 (28620351 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 7059478527 bytes :  224.4 MB/s
TX 1208651267 bytes :   38.4 MB/s
