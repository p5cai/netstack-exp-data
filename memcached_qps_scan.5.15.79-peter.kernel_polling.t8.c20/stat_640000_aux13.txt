# started on Fri Mar 31 19:20:36 2023


 Performance counter stats for 'CPU(s) 0-7':

        89,947,176      LLC-load-misses           #   18.19% of all LL-cache accesses  (49.96%)
       186,255,332      LLC-load-misses           #    9.69% of all LL-cache accesses  (49.98%)
       494,511,614      LLC-loads                 #    1.709 M/sec                    (49.99%)
     1,923,102,014      LLC-loads                 #    6.647 M/sec                    (41.68%)
         2,366,387      LLC-store-misses          #    8.179 K/sec                    (25.02%)
        60,520,190      LLC-store-misses          #  209.167 K/sec                    (33.36%)
       198,316,649      LLC-stores                #  685.413 K/sec                    (25.02%)
       977,421,350      LLC-stores                #    3.378 M/sec                    (33.34%)
        289,338.84 msec task-clock                #    8.000 CPUs utilized          
    98,698,235,810      cycles:u                  #    0.341 GHz                      (41.67%)
   550,008,190,517      cycles:k                  #    1.901 GHz                      (41.66%)
    58,033,972,177      instructions:u            #    0.59  insn per cycle           (49.98%)
   515,146,573,348      instructions:k            #    0.94  insn per cycle           (49.97%)

      36.168479888 seconds time elapsed

