# started on Wed Mar 29 18:13:42 2023


 Performance counter stats for 'CPU(s) 0-7':

       113,749,979      LLC-load-misses           #   17.49% of all LL-cache accesses  (49.97%)
       218,316,010      LLC-load-misses           #    8.75% of all LL-cache accesses  (49.98%)
       650,530,345      LLC-loads                 #    2.251 M/sec                    (49.99%)
     2,495,540,574      LLC-loads                 #    8.635 M/sec                    (41.67%)
         2,459,570      LLC-store-misses          #    8.510 K/sec                    (25.01%)
        75,672,494      LLC-store-misses          #  261.827 K/sec                    (33.35%)
       214,000,006      LLC-stores                #  740.441 K/sec                    (25.01%)
     1,261,428,250      LLC-stores                #    4.365 M/sec                    (33.35%)
        289,017.12 msec task-clock                #    8.000 CPUs utilized          
   111,287,888,103      cycles:u                  #    0.385 GHz                      (41.68%)
   536,770,873,416      cycles:k                  #    1.857 GHz                      (41.66%)
    71,148,941,666      instructions:u            #    0.64  insn per cycle           (49.99%)
   449,206,865,551      instructions:k            #    0.84  insn per cycle           (49.98%)

      36.128159125 seconds time elapsed

