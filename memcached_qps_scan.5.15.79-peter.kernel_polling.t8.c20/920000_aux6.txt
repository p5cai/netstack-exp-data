#type       avg     std     min     5th    10th    50th    90th    95th    99th
read      362.4   284.2    29.5    82.1   102.3   258.5   816.3  1011.8  1194.2
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6075657.8 92088.2 5857388.4 5621124.6 5657448.5 5948039.8 6491953.5 6624781.2 6731043.3

Total QPS = 919983.8 (27599526 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6800137936 bytes :  216.2 MB/s
TX 1165305773 bytes :   37.0 MB/s
