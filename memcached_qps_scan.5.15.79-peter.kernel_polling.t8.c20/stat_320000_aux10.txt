# started on Fri Mar 31 18:22:06 2023


 Performance counter stats for 'CPU(s) 0-7':

        45,997,456      LLC-load-misses           #   17.09% of all LL-cache accesses  (49.98%)
       105,547,455      LLC-load-misses           #   10.83% of all LL-cache accesses  (49.98%)
       269,218,949      LLC-loads                 #  932.710 K/sec                    (49.99%)
       974,181,701      LLC-loads                 #    3.375 M/sec                    (41.67%)
         2,171,178      LLC-store-misses          #    7.522 K/sec                    (25.01%)
        30,295,003      LLC-store-misses          #  104.957 K/sec                    (33.35%)
       113,004,289      LLC-stores                #  391.504 K/sec                    (25.01%)
       475,930,141      LLC-stores                #    1.649 M/sec                    (33.35%)
        288,641.66 msec task-clock                #    8.000 CPUs utilized          
    54,008,229,853      cycles:u                  #    0.187 GHz                      (41.68%)
   595,483,081,594      cycles:k                  #    2.063 GHz                      (41.66%)
    31,067,596,205      instructions:u            #    0.58  insn per cycle           (49.99%)
   781,683,937,676      instructions:k            #    1.31  insn per cycle           (49.98%)

      36.081298294 seconds time elapsed

