# started on Fri Mar 31 18:48:54 2023


 Performance counter stats for 'CPU(s) 0-7':

       113,562,878      LLC-load-misses           #   18.26% of all LL-cache accesses  (49.99%)
       220,561,489      LLC-load-misses           #    8.92% of all LL-cache accesses  (50.00%)
       622,069,086      LLC-loads                 #    2.154 M/sec                    (50.01%)
     2,472,592,348      LLC-loads                 #    8.560 M/sec                    (41.69%)
         2,468,404      LLC-store-misses          #    8.546 K/sec                    (25.01%)
        71,487,337      LLC-store-misses          #  247.487 K/sec                    (33.34%)
       210,083,463      LLC-stores                #  727.302 K/sec                    (24.99%)
     1,258,599,558      LLC-stores                #    4.357 M/sec                    (33.32%)
        288,853.19 msec task-clock                #    8.000 CPUs utilized          
   114,420,179,545      cycles:u                  #    0.396 GHz                      (41.65%)
   534,275,935,381      cycles:k                  #    1.850 GHz                      (41.65%)
    72,381,175,656      instructions:u            #    0.63  insn per cycle           (49.98%)
   443,147,860,880      instructions:k            #    0.83  insn per cycle           (49.98%)

      36.107734120 seconds time elapsed

