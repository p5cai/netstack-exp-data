# started on Sat Apr  1 20:20:07 2023


 Performance counter stats for 'CPU(s) 4-11':

        84,340,907      LLC-load-misses           #   20.23% of all LL-cache accesses  (49.97%)
       372,038,390      LLC-load-misses           #   20.47% of all LL-cache accesses  (49.98%)
       416,808,835      LLC-loads                 #    1.445 M/sec                    (50.00%)
     1,817,902,759      LLC-loads                 #    6.300 M/sec                    (41.68%)
         8,166,893      LLC-store-misses          #   28.304 K/sec                    (25.02%)
       183,971,500      LLC-store-misses          #  637.582 K/sec                    (33.36%)
       156,817,177      LLC-stores                #  543.475 K/sec                    (25.01%)
       906,243,820      LLC-stores                #    3.141 M/sec                    (33.33%)
        288,545.44 msec task-clock                #    8.000 CPUs utilized          
    81,723,871,503      cycles:u                  #    0.283 GHz                      (41.66%)
   566,754,561,753      cycles:k                  #    1.964 GHz                      (41.65%)
    48,098,873,064      instructions:u            #    0.59  insn per cycle           (49.98%)
   554,280,415,951      instructions:k            #    0.98  insn per cycle           (49.97%)

      36.069257708 seconds time elapsed

