# started on Thu Mar 30 08:12:54 2023


 Performance counter stats for 'CPU(s) 4-11':

        88,903,089      LLC-load-misses           #   20.10% of all LL-cache accesses  (49.99%)
       395,240,938      LLC-load-misses           #   19.96% of all LL-cache accesses  (50.00%)
       442,301,688      LLC-loads                 #    1.529 M/sec                    (50.01%)
     1,979,974,027      LLC-loads                 #    6.846 M/sec                    (41.69%)
         6,431,732      LLC-store-misses          #   22.237 K/sec                    (25.01%)
       199,440,236      LLC-store-misses          #  689.540 K/sec                    (33.34%)
       164,498,832      LLC-stores                #  568.735 K/sec                    (24.99%)
       987,412,143      LLC-stores                #    3.414 M/sec                    (33.32%)
        289,236.45 msec task-clock                #    8.000 CPUs utilized          
    85,421,117,781      cycles:u                  #    0.295 GHz                      (41.65%)
   562,825,885,967      cycles:k                  #    1.946 GHz                      (41.65%)
    51,571,574,542      instructions:u            #    0.60  insn per cycle           (49.98%)
   512,356,318,008      instructions:k            #    0.91  insn per cycle           (49.98%)

      36.155635229 seconds time elapsed

