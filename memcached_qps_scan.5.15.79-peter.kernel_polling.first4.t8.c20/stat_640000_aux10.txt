# started on Sat Apr  1 19:28:56 2023


 Performance counter stats for 'CPU(s) 4-11':

       102,775,463      LLC-load-misses           #   20.52% of all LL-cache accesses  (49.97%)
       453,502,749      LLC-load-misses           #   20.12% of all LL-cache accesses  (49.98%)
       500,892,031      LLC-loads                 #    1.736 M/sec                    (49.99%)
     2,253,542,533      LLC-loads                 #    7.809 M/sec                    (41.68%)
         9,694,236      LLC-store-misses          #   33.593 K/sec                    (25.02%)
       226,693,991      LLC-store-misses          #  785.564 K/sec                    (33.36%)
       184,091,174      LLC-stores                #  637.932 K/sec                    (25.01%)
     1,133,397,144      LLC-stores                #    3.928 M/sec                    (33.34%)
        288,574.88 msec task-clock                #    8.000 CPUs utilized          
    93,649,946,838      cycles:u                  #    0.325 GHz                      (41.67%)
   555,058,928,255      cycles:k                  #    1.923 GHz                      (41.65%)
    57,563,229,069      instructions:u            #    0.61  insn per cycle           (49.98%)
   464,111,475,861      instructions:k            #    0.84  insn per cycle           (49.97%)

      36.072919369 seconds time elapsed

