# started on Sat Apr  1 21:26:00 2023


 Performance counter stats for 'CPU(s) 4-11':

       136,885,932      LLC-load-misses           #   20.43% of all LL-cache accesses  (49.99%)
       574,951,244      LLC-load-misses           #   17.62% of all LL-cache accesses  (50.00%)
       669,893,072      LLC-loads                 #    2.317 M/sec                    (50.00%)
     3,263,317,185      LLC-loads                 #   11.287 M/sec                    (41.67%)
        11,820,697      LLC-store-misses          #   40.884 K/sec                    (25.00%)
       220,616,669      LLC-store-misses          #  763.042 K/sec                    (33.34%)
       206,320,171      LLC-stores                #  713.595 K/sec                    (25.00%)
     1,626,697,068      LLC-stores                #    5.626 M/sec                    (33.34%)
        289,127.94 msec task-clock                #    8.000 CPUs utilized          
   113,277,172,733      cycles:u                  #    0.392 GHz                      (41.67%)
   534,234,260,861      cycles:k                  #    1.848 GHz                      (41.67%)
    74,711,185,470      instructions:u            #    0.66  insn per cycle           (50.00%)
   418,184,005,507      instructions:k            #    0.78  insn per cycle           (50.00%)

      36.142082060 seconds time elapsed

