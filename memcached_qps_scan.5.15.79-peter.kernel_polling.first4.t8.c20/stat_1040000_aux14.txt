# started on Sat Apr  1 20:52:10 2023


 Performance counter stats for 'CPU(s) 4-11':

       131,867,649      LLC-load-misses           #   20.28% of all LL-cache accesses  (49.98%)
       596,460,866      LLC-load-misses           #   18.69% of all LL-cache accesses  (49.98%)
       650,306,450      LLC-loads                 #    2.253 M/sec                    (49.98%)
     3,191,996,988      LLC-loads                 #   11.058 M/sec                    (41.66%)
         8,771,849      LLC-store-misses          #   30.389 K/sec                    (25.01%)
       234,816,329      LLC-store-misses          #  813.505 K/sec                    (33.35%)
       187,837,951      LLC-stores                #  650.752 K/sec                    (25.01%)
     1,609,964,814      LLC-stores                #    5.578 M/sec                    (33.35%)
        288,647.59 msec task-clock                #    8.000 CPUs utilized          
   114,267,382,059      cycles:u                  #    0.396 GHz                      (41.68%)
   534,096,461,150      cycles:k                  #    1.850 GHz                      (41.67%)
    73,316,921,160      instructions:u            #    0.64  insn per cycle           (50.00%)
   412,889,245,905      instructions:k            #    0.77  insn per cycle           (49.99%)

      36.081956800 seconds time elapsed

