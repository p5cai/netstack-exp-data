# started on Sat Apr  1 18:55:02 2023


 Performance counter stats for 'CPU(s) 4-11':

       112,187,477      LLC-load-misses           #   20.35% of all LL-cache accesses  (49.99%)
       501,397,952      LLC-load-misses           #   19.51% of all LL-cache accesses  (50.00%)
       551,271,275      LLC-loads                 #    1.906 M/sec                    (50.01%)
     2,569,332,031      LLC-loads                 #    8.884 M/sec                    (41.68%)
         7,557,456      LLC-store-misses          #   26.131 K/sec                    (25.01%)
       247,081,554      LLC-store-misses          #  854.304 K/sec                    (33.34%)
       179,934,091      LLC-stores                #  622.136 K/sec                    (24.99%)
     1,295,374,061      LLC-stores                #    4.479 M/sec                    (33.32%)
        289,219.69 msec task-clock                #    8.000 CPUs utilized          
   102,465,137,025      cycles:u                  #    0.354 GHz                      (41.65%)
   546,210,344,334      cycles:k                  #    1.889 GHz                      (41.65%)
    63,489,749,367      instructions:u            #    0.62  insn per cycle           (49.98%)
   425,262,593,728      instructions:k            #    0.78  insn per cycle           (49.98%)

      36.153505603 seconds time elapsed

