# started on Sat Apr  1 19:05:45 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,605,853      LLC-load-misses           #   20.31% of all LL-cache accesses  (49.98%)
       259,120,859      LLC-load-misses           #   22.35% of all LL-cache accesses  (50.00%)
       268,823,696      LLC-loads                 #  930.516 K/sec                    (50.01%)
     1,159,442,373      LLC-loads                 #    4.013 M/sec                    (41.69%)
         6,609,083      LLC-store-misses          #   22.877 K/sec                    (25.02%)
       141,402,869      LLC-store-misses          #  489.457 K/sec                    (33.35%)
       109,624,468      LLC-stores                #  379.458 K/sec                    (24.99%)
       568,628,752      LLC-stores                #    1.968 M/sec                    (33.32%)
        288,897.34 msec task-clock                #    8.000 CPUs utilized          
    53,905,810,262      cycles:u                  #    0.187 GHz                      (41.65%)
   595,510,805,901      cycles:k                  #    2.061 GHz                      (41.64%)
    30,631,314,129      instructions:u            #    0.57  insn per cycle           (49.97%)
   744,012,365,527      instructions:k            #    1.25  insn per cycle           (49.97%)

      36.113253021 seconds time elapsed

