# started on Sat Apr  1 21:43:21 2023


 Performance counter stats for 'CPU(s) 4-11':

       133,663,509      LLC-load-misses           #   20.24% of all LL-cache accesses  (49.99%)
       556,848,473      LLC-load-misses           #   17.41% of all LL-cache accesses  (50.00%)
       660,255,679      LLC-loads                 #    2.286 M/sec                    (50.01%)
     3,199,337,238      LLC-loads                 #   11.078 M/sec                    (41.68%)
        11,917,768      LLC-store-misses          #   41.268 K/sec                    (25.00%)
       214,275,295      LLC-store-misses          #  741.978 K/sec                    (33.33%)
       202,945,609      LLC-stores                #  702.746 K/sec                    (25.00%)
     1,594,265,401      LLC-stores                #    5.521 M/sec                    (33.33%)
        288,789.47 msec task-clock                #    8.000 CPUs utilized          
   115,114,836,102      cycles:u                  #    0.399 GHz                      (41.66%)
   533,452,121,914      cycles:k                  #    1.847 GHz                      (41.66%)
    73,037,128,256      instructions:u            #    0.63  insn per cycle           (49.99%)
   409,522,939,982      instructions:k            #    0.77  insn per cycle           (49.99%)

      36.099771582 seconds time elapsed

