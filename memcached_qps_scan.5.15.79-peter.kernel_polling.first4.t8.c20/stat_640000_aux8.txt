# started on Sat Apr  1 18:52:38 2023


 Performance counter stats for 'CPU(s) 4-11':

       103,920,165      LLC-load-misses           #   19.84% of all LL-cache accesses  (50.00%)
       449,236,295      LLC-load-misses           #   19.61% of all LL-cache accesses  (50.01%)
       523,735,206      LLC-loads                 #    1.816 M/sec                    (50.01%)
     2,290,682,124      LLC-loads                 #    7.942 M/sec                    (41.68%)
         9,682,802      LLC-store-misses          #   33.571 K/sec                    (25.00%)
       226,698,554      LLC-store-misses          #  785.988 K/sec                    (33.33%)
       186,739,882      LLC-stores                #  647.447 K/sec                    (25.00%)
     1,136,166,497      LLC-stores                #    3.939 M/sec                    (33.33%)
        288,424.88 msec task-clock                #    8.000 CPUs utilized          
   100,374,063,086      cycles:u                  #    0.348 GHz                      (41.66%)
   549,093,111,930      cycles:k                  #    1.904 GHz                      (41.66%)
    58,350,730,636      instructions:u            #    0.58  insn per cycle           (49.99%)
   447,704,258,427      instructions:k            #    0.82  insn per cycle           (49.99%)

      36.054169345 seconds time elapsed

