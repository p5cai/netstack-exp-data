# started on Sat Apr  1 16:58:45 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,727,128      LLC-load-misses           #   21.24% of all LL-cache accesses  (49.97%)
       256,271,849      LLC-load-misses           #   21.99% of all LL-cache accesses  (49.98%)
       257,679,937      LLC-loads                 #  892.952 K/sec                    (49.99%)
     1,165,497,482      LLC-loads                 #    4.039 M/sec                    (41.68%)
         6,607,499      LLC-store-misses          #   22.897 K/sec                    (25.02%)
       139,238,899      LLC-store-misses          #  482.512 K/sec                    (33.36%)
       108,179,505      LLC-stores                #  374.880 K/sec                    (25.02%)
       567,577,900      LLC-stores                #    1.967 M/sec                    (33.34%)
        288,570.80 msec task-clock                #    8.000 CPUs utilized          
    55,885,873,650      cycles:u                  #    0.194 GHz                      (41.67%)
   593,553,045,438      cycles:k                  #    2.057 GHz                      (41.66%)
    30,435,968,101      instructions:u            #    0.54  insn per cycle           (49.98%)
   728,515,939,895      instructions:k            #    1.23  insn per cycle           (49.97%)

      36.072445353 seconds time elapsed

