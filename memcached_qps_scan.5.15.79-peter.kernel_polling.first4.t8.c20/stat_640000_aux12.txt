# started on Sat Apr  1 20:05:15 2023


 Performance counter stats for 'CPU(s) 4-11':

       103,070,590      LLC-load-misses           #   19.97% of all LL-cache accesses  (49.99%)
       450,738,182      LLC-load-misses           #   20.03% of all LL-cache accesses  (50.00%)
       516,118,901      LLC-loads                 #    1.787 M/sec                    (50.01%)
     2,250,245,898      LLC-loads                 #    7.791 M/sec                    (41.68%)
         9,757,369      LLC-store-misses          #   33.784 K/sec                    (25.01%)
       219,700,558      LLC-store-misses          #  760.692 K/sec                    (33.34%)
       189,234,672      LLC-stores                #  655.206 K/sec                    (25.00%)
     1,135,432,891      LLC-stores                #    3.931 M/sec                    (33.33%)
        288,816.85 msec task-clock                #    8.000 CPUs utilized          
    94,543,147,909      cycles:u                  #    0.327 GHz                      (41.66%)
   553,640,965,824      cycles:k                  #    1.917 GHz                      (41.66%)
    57,587,754,016      instructions:u            #    0.61  insn per cycle           (49.99%)
   463,790,274,291      instructions:k            #    0.84  insn per cycle           (49.99%)

      36.103186411 seconds time elapsed

