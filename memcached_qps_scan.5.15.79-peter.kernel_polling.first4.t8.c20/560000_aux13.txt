#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       59.8    21.0    26.8    37.0    39.4    55.6    85.0    96.7   123.6
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3689033.6 65208.1 3636977.4 3485973.0 3504224.2 3650233.5 3796242.9 3814494.0 4119653.6

Total QPS = 559908.0 (16797242 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4136436409 bytes :  131.5 MB/s
TX  709262529 bytes :   22.5 MB/s
