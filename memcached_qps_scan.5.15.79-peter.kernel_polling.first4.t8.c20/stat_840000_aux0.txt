# started on Thu Mar 30 08:19:36 2023


 Performance counter stats for 'CPU(s) 4-11':

       132,830,387      LLC-load-misses           #   20.83% of all LL-cache accesses  (49.99%)
       549,593,943      LLC-load-misses           #   17.54% of all LL-cache accesses  (50.00%)
       637,604,646      LLC-loads                 #    2.202 M/sec                    (50.01%)
     3,133,560,181      LLC-loads                 #   10.821 M/sec                    (41.68%)
        11,350,567      LLC-store-misses          #   39.197 K/sec                    (25.00%)
       239,151,102      LLC-store-misses          #  825.854 K/sec                    (33.34%)
       188,998,575      LLC-stores                #  652.664 K/sec                    (25.00%)
     1,557,664,452      LLC-stores                #    5.379 M/sec                    (33.33%)
        289,580.40 msec task-clock                #    8.000 CPUs utilized          
   110,401,549,492      cycles:u                  #    0.381 GHz                      (41.66%)
   537,917,771,478      cycles:k                  #    1.858 GHz                      (41.66%)
    72,105,738,171      instructions:u            #    0.65  insn per cycle           (49.99%)
   408,397,719,779      instructions:k            #    0.76  insn per cycle           (49.99%)

      36.198638342 seconds time elapsed

