# started on Sat Apr  1 20:48:03 2023


 Performance counter stats for 'CPU(s) 4-11':

       132,788,135      LLC-load-misses           #   20.42% of all LL-cache accesses  (49.97%)
       568,518,528      LLC-load-misses           #   17.70% of all LL-cache accesses  (49.97%)
       650,360,109      LLC-loads                 #    2.253 M/sec                    (49.98%)
     3,212,664,037      LLC-loads                 #   11.131 M/sec                    (41.67%)
         8,987,095      LLC-store-misses          #   31.139 K/sec                    (25.02%)
       226,021,379      LLC-store-misses          #  783.120 K/sec                    (33.35%)
       189,023,831      LLC-stores                #  654.931 K/sec                    (25.01%)
     1,602,009,983      LLC-stores                #    5.551 M/sec                    (33.35%)
        288,616.36 msec task-clock                #    8.000 CPUs utilized          
   114,641,689,581      cycles:u                  #    0.397 GHz                      (41.68%)
   534,658,419,720      cycles:k                  #    1.852 GHz                      (41.67%)
    74,140,694,933      instructions:u            #    0.65  insn per cycle           (49.99%)
   415,732,080,523      instructions:k            #    0.78  insn per cycle           (49.98%)

      36.078097828 seconds time elapsed

