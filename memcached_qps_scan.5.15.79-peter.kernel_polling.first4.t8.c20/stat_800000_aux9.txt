# started on Sat Apr  1 19:14:54 2023


 Performance counter stats for 'CPU(s) 4-11':

       126,584,355      LLC-load-misses           #   20.18% of all LL-cache accesses  (50.00%)
       532,904,361      LLC-load-misses           #   18.15% of all LL-cache accesses  (50.00%)
       627,171,039      LLC-loads                 #    2.172 M/sec                    (50.00%)
     2,935,696,761      LLC-loads                 #   10.166 M/sec                    (41.67%)
        11,081,500      LLC-store-misses          #   38.376 K/sec                    (25.00%)
       246,623,498      LLC-store-misses          #  854.064 K/sec                    (33.34%)
       199,854,311      LLC-stores                #  692.101 K/sec                    (25.00%)
     1,466,048,176      LLC-stores                #    5.077 M/sec                    (33.33%)
        288,764.64 msec task-clock                #    8.000 CPUs utilized          
   110,236,321,697      cycles:u                  #    0.382 GHz                      (41.67%)
   538,122,824,501      cycles:k                  #    1.864 GHz                      (41.66%)
    69,602,948,994      instructions:u            #    0.63  insn per cycle           (50.00%)
   410,490,487,448      instructions:k            #    0.76  insn per cycle           (49.99%)

      36.096642345 seconds time elapsed

