# started on Sat Apr  1 19:36:22 2023


 Performance counter stats for 'CPU(s) 4-11':

       131,863,175      LLC-load-misses           #   20.61% of all LL-cache accesses  (49.98%)
       561,077,805      LLC-load-misses           #   17.90% of all LL-cache accesses  (49.99%)
       639,875,108      LLC-loads                 #    2.216 M/sec                    (49.99%)
     3,134,332,239      LLC-loads                 #   10.857 M/sec                    (41.66%)
        11,357,163      LLC-store-misses          #   39.340 K/sec                    (25.01%)
       215,260,441      LLC-store-misses          #  745.648 K/sec                    (33.34%)
       198,514,569      LLC-stores                #  687.642 K/sec                    (25.01%)
     1,561,219,775      LLC-stores                #    5.408 M/sec                    (33.34%)
        288,688.99 msec task-clock                #    8.000 CPUs utilized          
   117,149,687,239      cycles:u                  #    0.406 GHz                      (41.68%)
   531,145,274,016      cycles:k                  #    1.840 GHz                      (41.67%)
    72,145,618,851      instructions:u            #    0.62  insn per cycle           (50.00%)
   403,779,421,235      instructions:k            #    0.76  insn per cycle           (49.99%)

      36.087163488 seconds time elapsed

