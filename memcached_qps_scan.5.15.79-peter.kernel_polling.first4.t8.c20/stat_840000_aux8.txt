# started on Sat Apr  1 18:57:35 2023


 Performance counter stats for 'CPU(s) 4-11':

       132,261,744      LLC-load-misses           #   20.62% of all LL-cache accesses  (49.99%)
       548,836,496      LLC-load-misses           #   17.65% of all LL-cache accesses  (50.00%)
       641,356,080      LLC-loads                 #    2.220 M/sec                    (50.01%)
     3,109,460,799      LLC-loads                 #   10.764 M/sec                    (41.69%)
        11,330,912      LLC-store-misses          #   39.226 K/sec                    (25.01%)
       242,839,270      LLC-store-misses          #  840.665 K/sec                    (33.34%)
       191,804,307      LLC-stores                #  663.992 K/sec                    (24.99%)
     1,552,011,480      LLC-stores                #    5.373 M/sec                    (33.32%)
        288,865.58 msec task-clock                #    8.000 CPUs utilized          
   112,425,353,837      cycles:u                  #    0.389 GHz                      (41.65%)
   537,052,745,569      cycles:k                  #    1.859 GHz                      (41.65%)
    72,165,101,903      instructions:u            #    0.64  insn per cycle           (49.98%)
   413,583,559,228      instructions:k            #    0.77  insn per cycle           (49.98%)

      36.109275525 seconds time elapsed

