# started on Sat Apr  1 20:19:16 2023


 Performance counter stats for 'CPU(s) 4-11':

        78,489,952      LLC-load-misses           #   20.20% of all LL-cache accesses  (50.00%)
       347,199,644      LLC-load-misses           #   20.68% of all LL-cache accesses  (50.00%)
       388,514,541      LLC-loads                 #    1.345 M/sec                    (50.01%)
     1,679,292,339      LLC-loads                 #    5.815 M/sec                    (41.68%)
         8,172,410      LLC-store-misses          #   28.298 K/sec                    (25.00%)
       172,818,444      LLC-store-misses          #  598.406 K/sec                    (33.33%)
       150,342,460      LLC-stores                #  520.580 K/sec                    (25.00%)
       833,532,633      LLC-stores                #    2.886 M/sec                    (33.33%)
        288,797.74 msec task-clock                #    8.000 CPUs utilized          
    76,483,651,026      cycles:u                  #    0.265 GHz                      (41.66%)
   571,697,832,251      cycles:k                  #    1.980 GHz                      (41.66%)
    44,418,249,265      instructions:u            #    0.58  insn per cycle           (49.99%)
   589,071,401,940      instructions:k            #    1.03  insn per cycle           (49.99%)

      36.100786950 seconds time elapsed

