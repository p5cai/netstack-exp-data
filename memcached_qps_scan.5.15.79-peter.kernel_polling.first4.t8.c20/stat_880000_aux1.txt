# started on Thu Mar 30 08:39:31 2023


 Performance counter stats for 'CPU(s) 4-11':

       130,632,793      LLC-load-misses           #   20.27% of all LL-cache accesses  (49.99%)
       566,181,563      LLC-load-misses           #   17.81% of all LL-cache accesses  (50.00%)
       644,588,844      LLC-loads                 #    2.220 M/sec                    (50.01%)
     3,178,318,056      LLC-loads                 #   10.945 M/sec                    (41.69%)
         8,571,936      LLC-store-misses          #   29.520 K/sec                    (25.00%)
       221,187,628      LLC-store-misses          #  761.724 K/sec                    (33.34%)
       192,633,917      LLC-stores                #  663.391 K/sec                    (24.99%)
     1,599,890,037      LLC-stores                #    5.510 M/sec                    (33.32%)
        290,377.60 msec task-clock                #    8.000 CPUs utilized          
   113,422,293,751      cycles:u                  #    0.391 GHz                      (41.65%)
   536,119,771,538      cycles:k                  #    1.846 GHz                      (41.65%)
    73,667,666,934      instructions:u            #    0.65  insn per cycle           (49.98%)
   410,099,066,185      instructions:k            #    0.76  insn per cycle           (49.98%)

      36.298303821 seconds time elapsed

