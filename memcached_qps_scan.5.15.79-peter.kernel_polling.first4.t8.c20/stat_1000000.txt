# started on Thu Mar 30 08:05:35 2023


 Performance counter stats for 'CPU(s) 4-11':

       133,718,000      LLC-load-misses           #   20.40% of all LL-cache accesses  (50.00%)
       562,081,582      LLC-load-misses           #   17.55% of all LL-cache accesses  (50.00%)
       655,384,942      LLC-loads                 #    2.269 M/sec                    (50.01%)
     3,202,462,525      LLC-loads                 #   11.089 M/sec                    (41.68%)
        11,733,217      LLC-store-misses          #   40.628 K/sec                    (25.00%)
       216,279,034      LLC-store-misses          #  748.894 K/sec                    (33.33%)
       194,111,085      LLC-stores                #  672.134 K/sec                    (25.00%)
     1,588,837,962      LLC-stores                #    5.502 M/sec                    (33.33%)
        288,798.07 msec task-clock                #    8.000 CPUs utilized          
   113,095,379,133      cycles:u                  #    0.392 GHz                      (41.66%)
   535,093,799,201      cycles:k                  #    1.853 GHz                      (41.66%)
    73,076,760,175      instructions:u            #    0.65  insn per cycle           (49.99%)
   408,236,444,793      instructions:k            #    0.76  insn per cycle           (49.99%)

      36.100864415 seconds time elapsed

