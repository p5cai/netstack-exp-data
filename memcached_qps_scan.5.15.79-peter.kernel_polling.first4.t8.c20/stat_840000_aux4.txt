# started on Sat Apr  1 17:45:04 2023


 Performance counter stats for 'CPU(s) 4-11':

       130,327,563      LLC-load-misses           #   20.36% of all LL-cache accesses  (49.99%)
       572,478,869      LLC-load-misses           #   18.41% of all LL-cache accesses  (50.00%)
       640,028,289      LLC-loads                 #    2.216 M/sec                    (50.01%)
     3,110,217,465      LLC-loads                 #   10.769 M/sec                    (41.68%)
        11,249,139      LLC-store-misses          #   38.951 K/sec                    (25.00%)
       238,723,073      LLC-store-misses          #  826.602 K/sec                    (33.33%)
       198,511,927      LLC-stores                #  687.367 K/sec                    (25.00%)
     1,539,841,967      LLC-stores                #    5.332 M/sec                    (33.33%)
        288,800.61 msec task-clock                #    8.000 CPUs utilized          
   114,888,829,284      cycles:u                  #    0.398 GHz                      (41.66%)
   534,174,643,157      cycles:k                  #    1.850 GHz                      (41.66%)
    72,337,631,450      instructions:u            #    0.63  insn per cycle           (49.99%)
   399,445,987,745      instructions:k            #    0.75  insn per cycle           (49.99%)

      36.101169283 seconds time elapsed

