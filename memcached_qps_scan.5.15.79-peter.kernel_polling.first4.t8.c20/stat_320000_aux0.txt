# started on Thu Mar 30 08:09:38 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,884,756      LLC-load-misses           #   20.11% of all LL-cache accesses  (49.99%)
       255,860,871      LLC-load-misses           #   21.76% of all LL-cache accesses  (49.99%)
       272,981,040      LLC-loads                 #  945.527 K/sec                    (49.99%)
     1,175,879,206      LLC-loads                 #    4.073 M/sec                    (41.66%)
         6,793,374      LLC-store-misses          #   23.530 K/sec                    (25.01%)
       136,414,062      LLC-store-misses          #  472.499 K/sec                    (33.34%)
       110,051,598      LLC-stores                #  381.187 K/sec                    (25.00%)
       563,847,629      LLC-stores                #    1.953 M/sec                    (33.34%)
        288,707.70 msec task-clock                #    8.000 CPUs utilized          
    53,361,058,230      cycles:u                  #    0.185 GHz                      (41.67%)
   595,014,429,573      cycles:k                  #    2.061 GHz                      (41.67%)
    31,543,346,949      instructions:u            #    0.59  insn per cycle           (50.00%)
   736,535,776,285      instructions:k            #    1.24  insn per cycle           (49.99%)

      36.089573840 seconds time elapsed

