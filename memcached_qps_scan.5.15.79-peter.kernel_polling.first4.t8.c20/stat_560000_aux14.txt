# started on Sat Apr  1 20:39:49 2023


 Performance counter stats for 'CPU(s) 4-11':

        88,672,046      LLC-load-misses           #   20.16% of all LL-cache accesses  (50.00%)
       401,925,363      LLC-load-misses           #   20.38% of all LL-cache accesses  (50.00%)
       439,828,952      LLC-loads                 #    1.521 M/sec                    (50.00%)
     1,971,760,492      LLC-loads                 #    6.819 M/sec                    (41.67%)
         6,230,896      LLC-store-misses          #   21.549 K/sec                    (25.00%)
       202,291,373      LLC-store-misses          #  699.612 K/sec                    (33.34%)
       164,780,601      LLC-stores                #  569.883 K/sec                    (25.00%)
       987,160,963      LLC-stores                #    3.414 M/sec                    (33.33%)
        289,148.10 msec task-clock                #    8.000 CPUs utilized          
    83,809,031,804      cycles:u                  #    0.290 GHz                      (41.66%)
   564,487,708,461      cycles:k                  #    1.952 GHz                      (41.66%)
    51,638,569,462      instructions:u            #    0.62  insn per cycle           (50.00%)
   526,514,520,940      instructions:k            #    0.93  insn per cycle           (49.99%)

      36.144578204 seconds time elapsed

