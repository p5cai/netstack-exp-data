# started on Sat Apr  1 20:56:22 2023


 Performance counter stats for 'CPU(s) 4-11':

        85,328,963      LLC-load-misses           #   20.94% of all LL-cache accesses  (50.00%)
       374,127,697      LLC-load-misses           #   20.49% of all LL-cache accesses  (50.00%)
       407,480,166      LLC-loads                 #    1.413 M/sec                    (50.00%)
     1,825,466,082      LLC-loads                 #    6.330 M/sec                    (41.67%)
         8,061,129      LLC-store-misses          #   27.953 K/sec                    (25.00%)
       191,633,748      LLC-store-misses          #  664.517 K/sec                    (33.34%)
       156,570,229      LLC-stores                #  542.929 K/sec                    (25.00%)
       908,556,033      LLC-stores                #    3.151 M/sec                    (33.33%)
        288,380.47 msec task-clock                #    8.000 CPUs utilized          
    80,822,706,424      cycles:u                  #    0.280 GHz                      (41.66%)
   567,493,654,000      cycles:k                  #    1.968 GHz                      (41.66%)
    47,803,161,412      instructions:u            #    0.59  insn per cycle           (50.00%)
   553,264,929,351      instructions:k            #    0.97  insn per cycle           (50.00%)

      36.048602638 seconds time elapsed

