# started on Sat Apr  1 18:11:20 2023


 Performance counter stats for 'CPU(s) 4-11':

        54,866,473      LLC-load-misses           #   20.20% of all LL-cache accesses  (49.97%)
       256,806,826      LLC-load-misses           #   22.07% of all LL-cache accesses  (49.98%)
       271,592,398      LLC-loads                 #  942.483 K/sec                    (50.00%)
     1,163,775,345      LLC-loads                 #    4.039 M/sec                    (41.68%)
         6,699,218      LLC-store-misses          #   23.248 K/sec                    (25.02%)
       136,650,198      LLC-store-misses          #  474.205 K/sec                    (33.36%)
       105,246,521      LLC-stores                #  365.228 K/sec                    (25.01%)
       565,765,217      LLC-stores                #    1.963 M/sec                    (33.34%)
        288,166.78 msec task-clock                #    8.000 CPUs utilized          
    54,536,448,398      cycles:u                  #    0.189 GHz                      (41.66%)
   594,012,613,599      cycles:k                  #    2.061 GHz                      (41.65%)
    30,941,070,660      instructions:u            #    0.57  insn per cycle           (49.98%)
   734,458,287,678      instructions:k            #    1.24  insn per cycle           (49.97%)

      36.021959661 seconds time elapsed

