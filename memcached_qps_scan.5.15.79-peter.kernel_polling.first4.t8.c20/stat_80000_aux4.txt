# started on Sat Apr  1 17:34:14 2023


 Performance counter stats for 'CPU(s) 4-11':

        13,276,292      LLC-load-misses           #   20.96% of all LL-cache accesses  (49.99%)
        79,155,841      LLC-load-misses           #   24.71% of all LL-cache accesses  (49.99%)
        63,355,059      LLC-loads                 #  219.129 K/sec                    (50.00%)
       320,320,330      LLC-loads                 #    1.108 M/sec                    (41.66%)
         1,100,072      LLC-store-misses          #    3.805 K/sec                    (25.01%)
        33,696,881      LLC-store-misses          #  116.549 K/sec                    (33.34%)
        28,531,705      LLC-stores                #   98.684 K/sec                    (25.00%)
       140,109,481      LLC-stores                #  484.604 K/sec                    (33.34%)
        289,121.68 msec task-clock                #    8.000 CPUs utilized          
    13,723,443,921      cycles:u                  #    0.047 GHz                      (41.67%)
   634,729,727,906      cycles:k                  #    2.195 GHz                      (41.67%)
     7,714,404,165      instructions:u            #    0.56  insn per cycle           (50.00%)
 1,042,015,248,620      instructions:k            #    1.64  insn per cycle           (50.00%)

      36.141315832 seconds time elapsed

