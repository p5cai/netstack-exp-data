# started on Thu Mar 30 08:01:28 2023


 Performance counter stats for 'CPU(s) 4-11':

       132,633,190      LLC-load-misses           #   20.68% of all LL-cache accesses  (50.00%)
       547,547,441      LLC-load-misses           #   17.38% of all LL-cache accesses  (50.00%)
       641,465,401      LLC-loads                 #    2.219 M/sec                    (50.00%)
     3,151,153,605      LLC-loads                 #   10.898 M/sec                    (41.67%)
        11,238,798      LLC-store-misses          #   38.869 K/sec                    (25.00%)
       236,280,177      LLC-store-misses          #  817.173 K/sec                    (33.34%)
       209,295,457      LLC-stores                #  723.847 K/sec                    (25.00%)
     1,568,477,325      LLC-stores                #    5.425 M/sec                    (33.33%)
        289,143.39 msec task-clock                #    8.000 CPUs utilized          
   111,822,614,617      cycles:u                  #    0.387 GHz                      (41.67%)
   537,801,288,945      cycles:k                  #    1.860 GHz                      (41.66%)
    73,234,910,030      instructions:u            #    0.65  insn per cycle           (50.00%)
   410,830,460,863      instructions:k            #    0.76  insn per cycle           (50.00%)

      36.143959229 seconds time elapsed

