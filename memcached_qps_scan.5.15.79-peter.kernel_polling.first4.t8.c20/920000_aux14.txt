#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1301.0    42.5   149.0  1183.5  1210.9  1319.3  1385.0  1393.2  1450.6
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5651093.0 102499.2 5324898.6 5173798.1 5270504.6 5764650.2 6067554.7 6105417.7 6135708.2

Total QPS = 861500.2 (25845008 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6346873359 bytes :  201.8 MB/s
TX 1091318806 bytes :   34.7 MB/s
