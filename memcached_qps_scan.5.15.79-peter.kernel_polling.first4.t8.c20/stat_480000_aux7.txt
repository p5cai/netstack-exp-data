# started on Sat Apr  1 18:30:17 2023


 Performance counter stats for 'CPU(s) 4-11':

        78,752,492      LLC-load-misses           #   20.57% of all LL-cache accesses  (49.98%)
       354,818,345      LLC-load-misses           #   21.30% of all LL-cache accesses  (49.99%)
       382,927,268      LLC-loads                 #    1.326 M/sec                    (50.00%)
     1,666,019,058      LLC-loads                 #    5.767 M/sec                    (41.69%)
         8,274,052      LLC-store-misses          #   28.642 K/sec                    (25.02%)
       180,328,327      LLC-store-misses          #  624.236 K/sec                    (33.35%)
       159,103,722      LLC-stores                #  550.764 K/sec                    (25.00%)
       832,184,469      LLC-stores                #    2.881 M/sec                    (33.33%)
        288,878.48 msec task-clock                #    8.000 CPUs utilized          
    74,130,955,120      cycles:u                  #    0.257 GHz                      (41.65%)
   574,983,946,229      cycles:k                  #    1.990 GHz                      (41.65%)
    44,450,073,018      instructions:u            #    0.60  insn per cycle           (49.98%)
   594,608,965,078      instructions:k            #    1.03  insn per cycle           (49.98%)

      36.110896868 seconds time elapsed

