# started on Sat Apr  1 21:06:08 2023


 Performance counter stats for 'CPU(s) 4-11':

       133,144,612      LLC-load-misses           #   20.40% of all LL-cache accesses  (49.97%)
       564,901,731      LLC-load-misses           #   17.48% of all LL-cache accesses  (49.98%)
       652,715,431      LLC-loads                 #    2.258 M/sec                    (49.99%)
     3,230,933,452      LLC-loads                 #   11.179 M/sec                    (41.68%)
         9,108,801      LLC-store-misses          #   31.517 K/sec                    (25.02%)
       223,204,535      LLC-store-misses          #  772.307 K/sec                    (33.35%)
       202,257,786      LLC-stores                #  699.830 K/sec                    (25.01%)
     1,618,930,215      LLC-stores                #    5.602 M/sec                    (33.35%)
        289,009.97 msec task-clock                #    8.000 CPUs utilized          
   111,355,344,625      cycles:u                  #    0.385 GHz                      (41.67%)
   536,033,163,722      cycles:k                  #    1.855 GHz                      (41.66%)
    73,962,613,332      instructions:u            #    0.66  insn per cycle           (49.99%)
   417,264,615,737      instructions:k            #    0.78  insn per cycle           (49.97%)

      36.127383986 seconds time elapsed

