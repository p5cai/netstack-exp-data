#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1292.9    49.2   180.3  1174.1  1192.6  1306.7  1381.9  1391.3  1398.8
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5703709.6 155294.2 5324898.6 5186866.5 5296641.5 5780041.7 6070633.0 6106956.9 6136016.0

Total QPS = 866079.1 (25982373 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6390049158 bytes :  203.1 MB/s
TX 1097113699 bytes :   34.9 MB/s
