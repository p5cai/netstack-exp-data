# started on Sat Apr  1 21:06:59 2023


 Performance counter stats for 'CPU(s) 4-11':

       136,294,221      LLC-load-misses           #   20.59% of all LL-cache accesses  (49.98%)
       570,520,190      LLC-load-misses           #   17.56% of all LL-cache accesses  (50.00%)
       661,796,084      LLC-loads                 #    2.297 M/sec                    (50.01%)
     3,248,953,072      LLC-loads                 #   11.278 M/sec                    (41.69%)
        12,139,608      LLC-store-misses          #   42.141 K/sec                    (25.01%)
       220,134,753      LLC-store-misses          #  764.166 K/sec                    (33.34%)
       211,384,929      LLC-stores                #  733.792 K/sec                    (24.99%)
     1,619,185,540      LLC-stores                #    5.621 M/sec                    (33.32%)
        288,072.00 msec task-clock                #    8.000 CPUs utilized          
   113,130,272,492      cycles:u                  #    0.393 GHz                      (41.65%)
   536,037,423,762      cycles:k                  #    1.861 GHz                      (41.65%)
    75,126,310,581      instructions:u            #    0.66  insn per cycle           (49.98%)
   419,159,452,093      instructions:k            #    0.78  insn per cycle           (49.98%)

      36.007628946 seconds time elapsed

