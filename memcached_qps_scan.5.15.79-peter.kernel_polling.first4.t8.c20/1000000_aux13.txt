#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1286.7    52.6   163.9  1170.4  1187.2  1301.1  1380.6  1390.6  1398.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5712049.6 182491.3 5324898.6 5161709.8 5246327.9 5747267.6 6072201.5 6112818.2 6266146.4

Total QPS = 870130.0 (26103902 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6388990644 bytes :  203.1 MB/s
TX 1102315354 bytes :   35.0 MB/s
