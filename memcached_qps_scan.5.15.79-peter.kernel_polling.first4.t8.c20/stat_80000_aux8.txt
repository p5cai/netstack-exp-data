# started on Sat Apr  1 18:46:47 2023


 Performance counter stats for 'CPU(s) 4-11':

        13,447,271      LLC-load-misses           #   21.20% of all LL-cache accesses  (49.99%)
        80,289,102      LLC-load-misses           #   25.28% of all LL-cache accesses  (50.00%)
        63,429,221      LLC-loads                 #  219.589 K/sec                    (50.01%)
       317,646,541      LLC-loads                 #    1.100 M/sec                    (41.69%)
         1,126,135      LLC-store-misses          #    3.899 K/sec                    (25.01%)
        33,652,629      LLC-store-misses          #  116.504 K/sec                    (33.34%)
        27,343,115      LLC-stores                #   94.661 K/sec                    (24.99%)
       139,067,945      LLC-stores                #  481.447 K/sec                    (33.32%)
        288,853.88 msec task-clock                #    8.000 CPUs utilized          
    14,167,811,740      cycles:u                  #    0.049 GHz                      (41.65%)
   633,738,589,223      cycles:k                  #    2.194 GHz                      (41.65%)
     7,780,991,603      instructions:u            #    0.55  insn per cycle           (49.98%)
 1,040,715,163,815      instructions:k            #    1.64  insn per cycle           (49.98%)

      36.107803786 seconds time elapsed

