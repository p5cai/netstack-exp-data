#type       avg     std     min     5th    10th    50th    90th    95th    99th
read       63.1    24.7    26.8    36.9    39.6    57.8    92.8   106.4   139.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3971439.1 66588.0 3636977.4 3828979.5 3848293.3 4002804.5 4157315.7 4176629.5 4192080.7

Total QPS = 600040.8 (18001230 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4446354783 bytes :  141.3 MB/s
TX  760033161 bytes :   24.2 MB/s
