# started on Sat Apr  1 20:10:14 2023


 Performance counter stats for 'CPU(s) 4-11':

       131,986,684      LLC-load-misses           #   20.27% of all LL-cache accesses  (49.99%)
       550,301,955      LLC-load-misses           #   17.87% of all LL-cache accesses  (50.00%)
       651,042,552      LLC-loads                 #    2.254 M/sec                    (50.02%)
     3,080,068,613      LLC-loads                 #   10.663 M/sec                    (41.69%)
        11,380,451      LLC-store-misses          #   39.397 K/sec                    (25.01%)
       235,425,962      LLC-store-misses          #  815.008 K/sec                    (33.34%)
       208,015,162      LLC-stores                #  720.116 K/sec                    (24.99%)
     1,533,535,910      LLC-stores                #    5.309 M/sec                    (33.32%)
        288,863.53 msec task-clock                #    8.000 CPUs utilized          
   113,650,597,785      cycles:u                  #    0.393 GHz                      (41.65%)
   534,750,889,901      cycles:k                  #    1.851 GHz                      (41.65%)
    71,763,390,194      instructions:u            #    0.63  insn per cycle           (49.98%)
   410,647,402,098      instructions:k            #    0.77  insn per cycle           (49.98%)

      36.109020615 seconds time elapsed

