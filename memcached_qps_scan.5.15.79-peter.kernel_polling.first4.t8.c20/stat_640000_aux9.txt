# started on Sat Apr  1 19:10:45 2023


 Performance counter stats for 'CPU(s) 4-11':

       103,797,290      LLC-load-misses           #   20.33% of all LL-cache accesses  (49.98%)
       449,207,465      LLC-load-misses           #   19.94% of all LL-cache accesses  (49.99%)
       510,514,781      LLC-loads                 #    1.770 M/sec                    (50.01%)
     2,252,929,534      LLC-loads                 #    7.809 M/sec                    (41.69%)
         9,564,614      LLC-store-misses          #   33.152 K/sec                    (25.02%)
       224,165,017      LLC-store-misses          #  776.990 K/sec                    (33.35%)
       189,701,629      LLC-stores                #  657.535 K/sec                    (24.99%)
     1,134,642,527      LLC-stores                #    3.933 M/sec                    (33.32%)
        288,504.32 msec task-clock                #    8.000 CPUs utilized          
    97,886,789,350      cycles:u                  #    0.339 GHz                      (41.65%)
   550,814,851,580      cycles:k                  #    1.909 GHz                      (41.64%)
    57,715,059,404      instructions:u            #    0.59  insn per cycle           (49.97%)
   460,560,667,576      instructions:k            #    0.84  insn per cycle           (49.97%)

      36.064126501 seconds time elapsed

