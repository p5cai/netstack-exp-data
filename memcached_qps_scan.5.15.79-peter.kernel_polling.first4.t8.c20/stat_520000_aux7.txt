# started on Sat Apr  1 18:31:08 2023


 Performance counter stats for 'CPU(s) 4-11':

        84,359,002      LLC-load-misses           #   20.37% of all LL-cache accesses  (49.98%)
       379,794,726      LLC-load-misses           #   20.99% of all LL-cache accesses  (49.99%)
       414,037,563      LLC-loads                 #    1.436 M/sec                    (49.99%)
     1,809,221,876      LLC-loads                 #    6.276 M/sec                    (41.66%)
         8,151,372      LLC-store-misses          #   28.274 K/sec                    (25.01%)
       194,785,395      LLC-store-misses          #  675.640 K/sec                    (33.34%)
       168,542,718      LLC-stores                #  584.613 K/sec                    (25.01%)
       905,858,171      LLC-stores                #    3.142 M/sec                    (33.34%)
        288,297.78 msec task-clock                #    8.000 CPUs utilized          
    79,896,616,015      cycles:u                  #    0.277 GHz                      (41.68%)
   568,644,197,778      cycles:k                  #    1.972 GHz                      (41.67%)
    49,689,091,249      instructions:u            #    0.62  insn per cycle           (50.00%)
   558,378,092,744      instructions:k            #    0.98  insn per cycle           (49.99%)

      36.038281566 seconds time elapsed

