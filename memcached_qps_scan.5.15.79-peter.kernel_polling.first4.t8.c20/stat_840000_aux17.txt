# started on Sat Apr  1 21:40:56 2023


 Performance counter stats for 'CPU(s) 4-11':

       131,613,868      LLC-load-misses           #   20.66% of all LL-cache accesses  (49.98%)
       544,120,636      LLC-load-misses           #   17.40% of all LL-cache accesses  (49.99%)
       636,960,063      LLC-loads                 #    2.205 M/sec                    (50.00%)
     3,127,563,619      LLC-loads                 #   10.827 M/sec                    (41.69%)
        11,220,143      LLC-store-misses          #   38.841 K/sec                    (25.02%)
       236,966,577      LLC-store-misses          #  820.302 K/sec                    (33.35%)
       204,402,528      LLC-stores                #  707.576 K/sec                    (25.00%)
     1,555,340,142      LLC-stores                #    5.384 M/sec                    (33.32%)
        288,877.28 msec task-clock                #    8.000 CPUs utilized          
   112,442,800,595      cycles:u                  #    0.389 GHz                      (41.65%)
   536,328,929,548      cycles:k                  #    1.857 GHz                      (41.65%)
    72,392,700,115      instructions:u            #    0.64  insn per cycle           (49.98%)
   411,690,952,631      instructions:k            #    0.77  insn per cycle           (49.98%)

      36.110723625 seconds time elapsed

