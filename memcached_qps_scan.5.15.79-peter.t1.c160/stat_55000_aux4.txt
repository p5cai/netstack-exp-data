# started on Wed Mar  1 10:44:44 2023


 Performance counter stats for 'CPU(s) 0-0':

        15,761,236      LLC-load-misses                                               (41.65%)
        46,601,039      LLC-load-misses                                               (41.65%)
           699,401      mem_load_uops_retired.llc_miss                                     (33.32%)
    13,357,937,132      mem_uops_retired.all_loads                                     (16.67%)
    76,237,619,415      cycles                                                        (25.01%)
    11,133,197,263      cycles:u                                                      (25.01%)
    65,110,406,590      cycles:k                                                      (25.01%)
    44,813,292,882      instructions              #    0.59  insn per cycle         
                                                  #    1.19  stalled cycles per insn  (33.34%)
     5,053,029,820      instructions:u            #    0.45  insn per cycle           (41.68%)
    39,771,810,353      instructions:k            #    0.61  insn per cycle           (50.01%)
    53,118,397,990      stalled-cycles-frontend   #   69.67% frontend cycles idle     (50.00%)
    41,746,582,277      stalled-cycles-backend    #   54.76% backend cycles idle      (49.99%)

      33.302600467 seconds time elapsed

