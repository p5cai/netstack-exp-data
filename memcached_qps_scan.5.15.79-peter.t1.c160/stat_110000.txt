# started on Thu Feb 16 15:04:41 2023


 Performance counter stats for 'CPU(s) 0-0':

        30,427,209      LLC-load-misses                                               (41.65%)
       172,213,669      LLC-load-misses                                               (41.65%)
           685,988      mem_load_uops_retired.llc_miss                                     (33.32%)
    14,431,274,227      mem_uops_retired.all_loads                                     (16.67%)
    81,626,331,135      cycles                                                        (25.01%)
    11,380,827,262      cycles:u                                                      (25.01%)
    70,118,793,714      cycles:k                                                      (25.01%)
    48,487,138,608      instructions              #    0.59  insn per cycle         
                                                  #    1.18  stalled cycles per insn  (33.34%)
     5,689,311,625      instructions:u            #    0.50  insn per cycle           (41.68%)
    42,786,399,984      instructions:k            #    0.61  insn per cycle           (50.01%)
    57,145,895,151      stalled-cycles-frontend   #   70.01% frontend cycles idle     (49.99%)
    45,353,310,921      stalled-cycles-backend    #   55.56% backend cycles idle      (49.98%)

      31.861783788 seconds time elapsed

