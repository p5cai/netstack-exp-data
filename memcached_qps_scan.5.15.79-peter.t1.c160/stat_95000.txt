# started on Thu Feb 16 15:03:05 2023


 Performance counter stats for 'CPU(s) 0-0':

        30,405,784      LLC-load-misses                                               (41.66%)
       170,781,839      LLC-load-misses                                               (41.67%)
           673,725      mem_load_uops_retired.llc_miss                                     (33.35%)
    14,352,431,387      mem_uops_retired.all_loads                                     (16.68%)
    81,527,810,266      cycles                                                        (25.01%)
    11,335,149,880      cycles:u                                                      (25.00%)
    70,214,889,963      cycles:k                                                      (24.99%)
    48,464,954,472      instructions              #    0.59  insn per cycle         
                                                  #    1.18  stalled cycles per insn  (33.32%)
     5,677,025,414      instructions:u            #    0.50  insn per cycle           (41.65%)
    42,779,038,383      instructions:k            #    0.61  insn per cycle           (49.98%)
    57,214,658,009      stalled-cycles-frontend   #   70.18% frontend cycles idle     (49.98%)
    45,453,632,695      stalled-cycles-backend    #   55.75% backend cycles idle      (49.98%)

      31.885424389 seconds time elapsed

