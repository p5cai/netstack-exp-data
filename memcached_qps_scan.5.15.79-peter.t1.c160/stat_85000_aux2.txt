# started on Wed Mar  1 10:19:33 2023


 Performance counter stats for 'CPU(s) 0-0':

        29,308,076      LLC-load-misses                                               (41.66%)
       157,537,429      LLC-load-misses                                               (41.67%)
           633,327      mem_load_uops_retired.llc_miss                                     (33.34%)
    14,027,453,722      mem_uops_retired.all_loads                                     (16.68%)
    81,304,399,826      cycles                                                        (25.02%)
    11,430,829,118      cycles:u                                                      (25.00%)
    69,930,322,916      cycles:k                                                      (24.99%)
    47,454,672,827      instructions              #    0.58  insn per cycle         
                                                  #    1.21  stalled cycles per insn  (33.32%)
     5,543,481,184      instructions:u            #    0.48  insn per cycle           (41.65%)
    41,919,833,378      instructions:k            #    0.60  insn per cycle           (49.97%)
    57,295,122,322      stalled-cycles-frontend   #   70.47% frontend cycles idle     (49.97%)
    45,510,453,886      stalled-cycles-backend    #   55.98% backend cycles idle      (49.97%)

      33.280903114 seconds time elapsed

