# started on Wed Mar  1 10:15:07 2023


 Performance counter stats for 'CPU(s) 0-0':

        26,882,927      LLC-load-misses                                               (41.66%)
       139,257,226      LLC-load-misses                                               (41.66%)
           563,609      mem_load_uops_retired.llc_miss                                     (33.33%)
    14,112,854,537      mem_uops_retired.all_loads                                     (16.67%)
    81,209,977,640      cycles                                                        (25.01%)
    11,122,252,483      cycles:u                                                      (25.00%)
    70,024,837,965      cycles:k                                                      (25.00%)
    47,530,055,871      instructions              #    0.59  insn per cycle         
                                                  #    1.20  stalled cycles per insn  (33.33%)
     5,401,268,702      instructions:u            #    0.49  insn per cycle           (41.67%)
    42,076,113,176      instructions:k            #    0.60  insn per cycle           (50.00%)
    57,062,183,486      stalled-cycles-frontend   #   70.26% frontend cycles idle     (50.00%)
    45,159,945,989      stalled-cycles-backend    #   55.61% backend cycles idle      (50.00%)

      33.261283235 seconds time elapsed

