# started on Wed Mar  1 10:00:28 2023


 Performance counter stats for 'CPU(s) 0-0':

        27,489,284      LLC-load-misses                                               (41.64%)
       141,866,410      LLC-load-misses                                               (41.65%)
           497,863      mem_load_uops_retired.llc_miss                                     (33.33%)
    13,962,600,945      mem_uops_retired.all_loads                                     (16.68%)
    81,112,515,646      cycles                                                        (25.02%)
    10,904,640,124      cycles:u                                                      (25.01%)
    70,123,270,204      cycles:k                                                      (25.01%)
    46,931,247,047      instructions              #    0.58  insn per cycle         
                                                  #    1.22  stalled cycles per insn  (33.35%)
     5,225,466,189      instructions:u            #    0.48  insn per cycle           (41.67%)
    41,687,526,189      instructions:k            #    0.59  insn per cycle           (50.00%)
    57,165,936,740      stalled-cycles-frontend   #   70.48% frontend cycles idle     (49.99%)
    45,367,178,658      stalled-cycles-backend    #   55.93% backend cycles idle      (49.97%)

      33.294734332 seconds time elapsed

