# started on Wed Mar  1 10:54:21 2023


 Performance counter stats for 'CPU(s) 0-0':

        29,575,464      LLC-load-misses                                               (41.64%)
       155,909,925      LLC-load-misses                                               (41.65%)
           571,961      mem_load_uops_retired.llc_miss                                     (33.33%)
    14,113,850,827      mem_uops_retired.all_loads                                     (16.68%)
    81,046,231,824      cycles                                                        (25.02%)
    11,366,726,333      cycles:u                                                      (25.02%)
    69,842,376,548      cycles:k                                                      (25.01%)
    47,697,879,651      instructions              #    0.59  insn per cycle         
                                                  #    1.20  stalled cycles per insn  (33.34%)
     5,598,132,041      instructions:u            #    0.49  insn per cycle           (41.67%)
    42,068,751,436      instructions:k            #    0.60  insn per cycle           (50.00%)
    57,039,461,060      stalled-cycles-frontend   #   70.38% frontend cycles idle     (49.98%)
    45,288,178,035      stalled-cycles-backend    #   55.88% backend cycles idle      (49.97%)

      33.290378216 seconds time elapsed

