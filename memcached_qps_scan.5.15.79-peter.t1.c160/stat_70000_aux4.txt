# started on Wed Mar  1 10:48:18 2023


 Performance counter stats for 'CPU(s) 0-0':

        27,380,190      LLC-load-misses                                               (41.66%)
       144,426,324      LLC-load-misses                                               (41.66%)
           567,535      mem_load_uops_retired.llc_miss                                     (33.33%)
    14,052,107,402      mem_uops_retired.all_loads                                     (16.67%)
    81,196,076,071      cycles                                                        (25.00%)
    11,090,444,402      cycles:u                                                      (25.00%)
    69,954,747,398      cycles:k                                                      (25.00%)
    47,245,476,125      instructions              #    0.58  insn per cycle         
                                                  #    1.21  stalled cycles per insn  (33.33%)
     5,362,992,182      instructions:u            #    0.48  insn per cycle           (41.67%)
    41,860,070,895      instructions:k            #    0.60  insn per cycle           (50.00%)
    57,102,121,050      stalled-cycles-frontend   #   70.33% frontend cycles idle     (50.00%)
    45,226,490,588      stalled-cycles-backend    #   55.70% backend cycles idle      (50.00%)

      33.309660879 seconds time elapsed

