# started on Wed Mar  1 10:43:55 2023


 Performance counter stats for 'CPU(s) 0-0':

        14,209,775      LLC-load-misses                                               (41.66%)
        42,262,671      LLC-load-misses                                               (41.68%)
           937,543      mem_load_uops_retired.llc_miss                                     (33.35%)
    12,481,088,362      mem_uops_retired.all_loads                                     (16.67%)
    73,233,486,892      cycles                                                        (25.00%)
    10,479,952,912      cycles:u                                                      (24.99%)
    62,989,605,614      cycles:k                                                      (24.99%)
    42,350,908,625      instructions              #    0.58  insn per cycle         
                                                  #    1.22  stalled cycles per insn  (33.32%)
     4,627,087,566      instructions:u            #    0.44  insn per cycle           (41.65%)
    37,676,853,944      instructions:k            #    0.60  insn per cycle           (49.99%)
    51,475,366,958      stalled-cycles-frontend   #   70.29% frontend cycles idle     (49.99%)
    40,716,504,429      stalled-cycles-backend    #   55.60% backend cycles idle      (49.99%)

      33.319387615 seconds time elapsed

