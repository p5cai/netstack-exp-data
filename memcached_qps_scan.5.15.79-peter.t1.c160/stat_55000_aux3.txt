# started on Wed Mar  1 10:28:33 2023


 Performance counter stats for 'CPU(s) 0-0':

        15,726,478      LLC-load-misses                                               (41.64%)
        46,503,000      LLC-load-misses                                               (41.65%)
           786,110      mem_load_uops_retired.llc_miss                                     (33.33%)
    13,405,238,894      mem_uops_retired.all_loads                                     (16.68%)
    75,603,335,247      cycles                                                        (25.01%)
    10,845,579,623      cycles:u                                                      (25.01%)
    64,785,593,748      cycles:k                                                      (25.01%)
    45,139,763,649      instructions              #    0.60  insn per cycle         
                                                  #    1.16  stalled cycles per insn  (33.35%)
     5,055,733,939      instructions:u            #    0.47  insn per cycle           (41.67%)
    40,105,695,155      instructions:k            #    0.62  insn per cycle           (50.00%)
    52,353,156,928      stalled-cycles-frontend   #   69.25% frontend cycles idle     (49.99%)
    40,982,599,081      stalled-cycles-backend    #   54.21% backend cycles idle      (49.98%)

      33.296924536 seconds time elapsed

