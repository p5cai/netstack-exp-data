# started on Wed Mar  1 10:13:05 2023


 Performance counter stats for 'CPU(s) 0-0':

        27,595,360      LLC-load-misses                                               (41.65%)
       147,959,886      LLC-load-misses                                               (41.66%)
           583,411      mem_load_uops_retired.llc_miss                                     (33.34%)
    13,925,060,430      mem_uops_retired.all_loads                                     (16.68%)
    81,376,547,258      cycles                                                        (25.02%)
    10,846,136,691      cycles:u                                                      (25.01%)
    70,405,364,584      cycles:k                                                      (25.00%)
    46,769,056,455      instructions              #    0.57  insn per cycle         
                                                  #    1.23  stalled cycles per insn  (33.32%)
     5,171,951,813      instructions:u            #    0.48  insn per cycle           (41.65%)
    41,588,673,864      instructions:k            #    0.59  insn per cycle           (49.98%)
    57,434,795,506      stalled-cycles-frontend   #   70.58% frontend cycles idle     (49.97%)
    45,580,165,978      stalled-cycles-backend    #   56.01% backend cycles idle      (49.97%)

      37.505488308 seconds time elapsed

