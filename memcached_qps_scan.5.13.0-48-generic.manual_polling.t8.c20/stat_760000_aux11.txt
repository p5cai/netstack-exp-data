# started on Sat Apr  1 11:09:46 2023


 Performance counter stats for 'CPU(s) 0-7':

       111,275,434      LLC-load-misses           #   19.41% of all LL-cache accesses  (50.00%)
       206,908,189      LLC-load-misses           #    9.11% of all LL-cache accesses  (50.00%)
       573,242,205      LLC-loads                 #    1.985 M/sec                    (50.00%)
     2,272,032,792      LLC-loads                 #    7.868 M/sec                    (41.67%)
         2,610,549      LLC-store-misses          #    9.040 K/sec                    (25.00%)
        76,626,784      LLC-store-misses          #  265.348 K/sec                    (33.34%)
       205,844,954      LLC-stores                #  712.813 K/sec                    (25.00%)
     1,243,744,014      LLC-stores                #    4.307 M/sec                    (33.33%)
        288,778.32 msec task-clock                #    8.000 CPUs utilized          
   107,774,295,216      cycles:u                  #    0.373 GHz                      (41.66%)
   539,217,590,268      cycles:k                  #    1.867 GHz                      (41.66%)
    67,523,423,084      instructions:u            #    0.63  insn per cycle           (49.99%)
   471,729,253,042      instructions:k            #    0.87  insn per cycle           (49.99%)

      36.098343474 seconds time elapsed

