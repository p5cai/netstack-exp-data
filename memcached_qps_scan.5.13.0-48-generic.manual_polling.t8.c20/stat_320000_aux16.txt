# started on Sat Apr  1 12:30:08 2023


 Performance counter stats for 'CPU(s) 0-7':

        47,635,396      LLC-load-misses           #   18.33% of all LL-cache accesses  (49.99%)
        98,457,943      LLC-load-misses           #   10.47% of all LL-cache accesses  (49.99%)
       259,811,787      LLC-loads                 #  897.563 K/sec                    (49.99%)
       940,071,246      LLC-loads                 #    3.248 M/sec                    (41.66%)
         2,254,494      LLC-store-misses          #    7.789 K/sec                    (25.01%)
        33,812,045      LLC-store-misses          #  116.809 K/sec                    (33.34%)
       104,342,964      LLC-stores                #  360.470 K/sec                    (25.01%)
       502,316,566      LLC-stores                #    1.735 M/sec                    (33.34%)
        289,463.52 msec task-clock                #    8.000 CPUs utilized          
    52,420,968,723      cycles:u                  #    0.181 GHz                      (41.67%)
   593,903,859,158      cycles:k                  #    2.052 GHz                      (41.67%)
    30,306,640,843      instructions:u            #    0.58  insn per cycle           (50.00%)
   775,038,206,303      instructions:k            #    1.30  insn per cycle           (49.99%)

      36.184027055 seconds time elapsed

