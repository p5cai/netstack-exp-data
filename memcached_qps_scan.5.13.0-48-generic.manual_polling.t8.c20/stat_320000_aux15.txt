# started on Sat Apr  1 12:12:27 2023


 Performance counter stats for 'CPU(s) 0-7':

        48,331,504      LLC-load-misses           #   18.97% of all LL-cache accesses  (50.00%)
       101,933,657      LLC-load-misses           #   10.83% of all LL-cache accesses  (50.00%)
       254,715,093      LLC-loads                 #  883.186 K/sec                    (50.01%)
       941,402,895      LLC-loads                 #    3.264 M/sec                    (41.68%)
         2,254,665      LLC-store-misses          #    7.818 K/sec                    (25.00%)
        36,983,457      LLC-store-misses          #  128.235 K/sec                    (33.33%)
       110,638,317      LLC-stores                #  383.622 K/sec                    (25.00%)
       506,272,300      LLC-stores                #    1.755 M/sec                    (33.33%)
        288,404.84 msec task-clock                #    8.000 CPUs utilized          
    54,571,913,030      cycles:u                  #    0.189 GHz                      (41.66%)
   593,702,817,520      cycles:k                  #    2.059 GHz                      (41.66%)
    30,298,496,144      instructions:u            #    0.56  insn per cycle           (49.99%)
   765,249,704,933      instructions:k            #    1.29  insn per cycle           (49.99%)

      36.051674711 seconds time elapsed

