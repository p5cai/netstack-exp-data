# started on Sat Apr  1 08:53:09 2023


 Performance counter stats for 'CPU(s) 0-7':

       137,256,488      LLC-load-misses           #   19.27% of all LL-cache accesses  (49.99%)
       272,910,449      LLC-load-misses           #    8.50% of all LL-cache accesses  (50.00%)
       712,365,083      LLC-loads                 #    2.466 M/sec                    (50.01%)
     3,212,568,090      LLC-loads                 #   11.122 M/sec                    (41.69%)
         2,621,034      LLC-store-misses          #    9.074 K/sec                    (25.00%)
        67,771,260      LLC-store-misses          #  234.628 K/sec                    (33.34%)
       205,935,717      LLC-stores                #  712.961 K/sec                    (24.99%)
     1,688,703,445      LLC-stores                #    5.846 M/sec                    (33.32%)
        288,845.74 msec task-clock                #    8.000 CPUs utilized          
   124,023,904,664      cycles:u                  #    0.429 GHz                      (41.65%)
   524,393,748,126      cycles:k                  #    1.815 GHz                      (41.65%)
    81,257,384,628      instructions:u            #    0.66  insn per cycle           (49.98%)
   460,652,251,822      instructions:k            #    0.88  insn per cycle           (49.98%)

      36.106483617 seconds time elapsed

