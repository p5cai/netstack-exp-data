# started on Wed Mar 29 16:10:50 2023


 Performance counter stats for 'CPU(s) 0-7':

        70,393,171      LLC-load-misses           #   19.19% of all LL-cache accesses  (49.98%)
       127,523,971      LLC-load-misses           #    9.46% of all LL-cache accesses  (49.98%)
       366,788,757      LLC-loads                 #    1.272 M/sec                    (49.98%)
     1,348,007,922      LLC-loads                 #    4.676 M/sec                    (41.67%)
         2,446,711      LLC-store-misses          #    8.487 K/sec                    (25.01%)
        46,012,118      LLC-store-misses          #  159.608 K/sec                    (33.35%)
       147,586,553      LLC-stores                #  511.952 K/sec                    (25.01%)
       751,824,154      LLC-stores                #    2.608 M/sec                    (33.34%)
        288,281.79 msec task-clock                #    8.000 CPUs utilized          
    75,952,593,586      cycles:u                  #    0.263 GHz                      (41.68%)
   571,887,262,686      cycles:k                  #    1.984 GHz                      (41.67%)
    44,541,307,218      instructions:u            #    0.59  insn per cycle           (50.00%)
   642,179,464,871      instructions:k            #    1.12  insn per cycle           (49.98%)

      36.036154857 seconds time elapsed

