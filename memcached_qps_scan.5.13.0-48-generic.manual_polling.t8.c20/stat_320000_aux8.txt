# started on Sat Apr  1 10:08:40 2023


 Performance counter stats for 'CPU(s) 0-7':

        47,709,843      LLC-load-misses           #   17.72% of all LL-cache accesses  (49.97%)
        97,594,342      LLC-load-misses           #   10.50% of all LL-cache accesses  (49.97%)
       269,271,606      LLC-loads                 #  933.066 K/sec                    (49.99%)
       929,090,714      LLC-loads                 #    3.219 M/sec                    (41.67%)
         2,256,566      LLC-store-misses          #    7.819 K/sec                    (25.02%)
        36,834,727      LLC-store-misses          #  127.638 K/sec                    (33.36%)
        99,924,546      LLC-stores                #  346.253 K/sec                    (25.02%)
       503,798,202      LLC-stores                #    1.746 M/sec                    (33.35%)
        288,587.93 msec task-clock                #    8.000 CPUs utilized          
    52,410,561,471      cycles:u                  #    0.182 GHz                      (41.67%)
   595,259,599,028      cycles:k                  #    2.063 GHz                      (41.66%)
    30,320,376,168      instructions:u            #    0.58  insn per cycle           (49.99%)
   779,104,669,557      instructions:k            #    1.31  insn per cycle           (49.97%)

      36.074686824 seconds time elapsed

