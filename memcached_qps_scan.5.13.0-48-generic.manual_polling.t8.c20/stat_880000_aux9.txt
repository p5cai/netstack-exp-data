# started on Sat Apr  1 10:37:34 2023


 Performance counter stats for 'CPU(s) 0-7':

       127,487,700      LLC-load-misses           #   19.62% of all LL-cache accesses  (50.00%)
       236,387,151      LLC-load-misses           #    8.41% of all LL-cache accesses  (50.00%)
       649,791,676      LLC-loads                 #    2.250 M/sec                    (50.01%)
     2,812,001,502      LLC-loads                 #    9.737 M/sec                    (41.67%)
           797,661      LLC-store-misses          #    2.762 K/sec                    (25.00%)
        80,999,468      LLC-store-misses          #  280.484 K/sec                    (33.34%)
       189,145,354      LLC-stores                #  654.971 K/sec                    (25.00%)
     1,508,282,652      LLC-stores                #    5.223 M/sec                    (33.33%)
        288,784.21 msec task-clock                #    8.000 CPUs utilized          
   119,244,737,733      cycles:u                  #    0.413 GHz                      (41.66%)
   528,291,984,907      cycles:k                  #    1.829 GHz                      (41.66%)
    75,806,941,573      instructions:u            #    0.64  insn per cycle           (49.99%)
   444,794,734,140      instructions:k            #    0.84  insn per cycle           (49.99%)

      36.097365745 seconds time elapsed

