#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1063.1   514.6    32.4   286.7   572.3  1161.5  1259.3  1271.5  1379.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6322509.1 307536.0 5857388.4 5674157.5 5763514.3 6357581.3 6982862.5 7208116.1 7388319.0

Total QPS = 969122.1 (29073664 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 7121683499 bytes :  226.4 MB/s
TX 1227543256 bytes :   39.0 MB/s
