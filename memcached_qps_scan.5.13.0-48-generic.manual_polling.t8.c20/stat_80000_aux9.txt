# started on Sat Apr  1 10:25:28 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,915,504      LLC-load-misses           #   18.31% of all LL-cache accesses  (49.97%)
        32,810,936      LLC-load-misses           #   12.94% of all LL-cache accesses  (49.99%)
        65,083,729      LLC-loads                 #  225.564 K/sec                    (50.00%)
       253,630,222      LLC-loads                 #  879.020 K/sec                    (41.68%)
            79,127      LLC-store-misses          #  274.235 /sec                     (25.02%)
         7,424,631      LLC-store-misses          #   25.732 K/sec                    (33.36%)
        24,975,501      LLC-stores                #   86.559 K/sec                    (25.00%)
       135,306,926      LLC-stores                #  468.941 K/sec                    (33.33%)
        288,537.42 msec task-clock                #    8.000 CPUs utilized          
    13,769,819,313      cycles:u                  #    0.048 GHz                      (41.66%)
   632,450,208,985      cycles:k                  #    2.192 GHz                      (41.65%)
     7,665,289,553      instructions:u            #    0.56  insn per cycle           (49.98%)
 1,017,852,192,659      instructions:k            #    1.61  insn per cycle           (49.97%)

      36.067959031 seconds time elapsed

