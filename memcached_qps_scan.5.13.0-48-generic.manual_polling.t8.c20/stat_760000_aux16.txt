# started on Sat Apr  1 12:38:11 2023


 Performance counter stats for 'CPU(s) 0-7':

       112,083,013      LLC-load-misses           #   19.27% of all LL-cache accesses  (49.98%)
       208,463,709      LLC-load-misses           #    9.06% of all LL-cache accesses  (49.99%)
       581,545,930      LLC-loads                 #    2.013 M/sec                    (50.00%)
     2,301,280,340      LLC-loads                 #    7.966 M/sec                    (41.69%)
         2,760,656      LLC-store-misses          #    9.556 K/sec                    (25.02%)
        84,733,116      LLC-store-misses          #  293.306 K/sec                    (33.35%)
       203,432,214      LLC-stores                #  704.186 K/sec                    (25.00%)
     1,250,137,102      LLC-stores                #    4.327 M/sec                    (33.33%)
        288,889.73 msec task-clock                #    8.000 CPUs utilized          
   108,311,908,758      cycles:u                  #    0.375 GHz                      (41.66%)
   538,921,530,198      cycles:k                  #    1.865 GHz                      (41.64%)
    67,504,510,631      instructions:u            #    0.62  insn per cycle           (49.97%)
   467,281,725,234      instructions:k            #    0.87  insn per cycle           (49.97%)

      36.112330897 seconds time elapsed

