# started on Sat Apr  1 10:44:51 2023


 Performance counter stats for 'CPU(s) 0-7':

        70,969,651      LLC-load-misses           #   18.79% of all LL-cache accesses  (49.97%)
       135,749,174      LLC-load-misses           #    9.73% of all LL-cache accesses  (49.98%)
       377,653,233      LLC-loads                 #    1.309 M/sec                    (49.99%)
     1,395,035,786      LLC-loads                 #    4.835 M/sec                    (41.68%)
         2,408,746      LLC-store-misses          #    8.348 K/sec                    (25.02%)
        49,174,061      LLC-store-misses          #  170.420 K/sec                    (33.36%)
       149,039,646      LLC-stores                #  516.519 K/sec                    (25.01%)
       755,224,123      LLC-stores                #    2.617 M/sec                    (33.34%)
        288,546.24 msec task-clock                #    8.000 CPUs utilized          
    75,943,213,271      cycles:u                  #    0.263 GHz                      (41.66%)
   571,225,707,824      cycles:k                  #    1.980 GHz                      (41.65%)
    44,567,748,400      instructions:u            #    0.59  insn per cycle           (49.98%)
   635,123,386,008      instructions:k            #    1.11  insn per cycle           (49.97%)

      36.069408095 seconds time elapsed

