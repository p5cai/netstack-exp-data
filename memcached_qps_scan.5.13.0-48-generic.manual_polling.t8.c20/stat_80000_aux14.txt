# started on Sat Apr  1 11:53:53 2023


 Performance counter stats for 'CPU(s) 0-7':

        12,166,701      LLC-load-misses           #   17.99% of all LL-cache accesses  (49.99%)
        33,881,979      LLC-load-misses           #   13.22% of all LL-cache accesses  (50.00%)
        67,649,108      LLC-loads                 #  234.285 K/sec                    (50.00%)
       256,214,181      LLC-loads                 #  887.329 K/sec                    (41.67%)
            87,589      LLC-store-misses          #  303.341 /sec                     (25.00%)
         8,428,896      LLC-store-misses          #   29.191 K/sec                    (33.34%)
        26,573,803      LLC-stores                #   92.031 K/sec                    (25.00%)
       130,895,073      LLC-stores                #  453.320 K/sec                    (33.33%)
        288,747.56 msec task-clock                #    8.000 CPUs utilized          
    13,907,907,068      cycles:u                  #    0.048 GHz                      (41.67%)
   632,495,191,994      cycles:k                  #    2.190 GHz                      (41.67%)
     7,653,054,094      instructions:u            #    0.55  insn per cycle           (50.00%)
 1,017,253,081,165      instructions:k            #    1.61  insn per cycle           (49.99%)

      36.093751035 seconds time elapsed

