# started on Sat Apr  1 09:40:28 2023


 Performance counter stats for 'CPU(s) 0-7':

       106,007,577      LLC-load-misses           #   18.87% of all LL-cache accesses  (49.99%)
       198,308,369      LLC-load-misses           #    9.40% of all LL-cache accesses  (50.00%)
       561,860,283      LLC-loads                 #    1.948 M/sec                    (50.01%)
     2,109,313,231      LLC-loads                 #    7.313 M/sec                    (41.68%)
           762,457      LLC-store-misses          #    2.643 K/sec                    (25.00%)
        77,704,343      LLC-store-misses          #  269.399 K/sec                    (33.33%)
       189,819,203      LLC-stores                #  658.098 K/sec                    (25.00%)
     1,162,834,443      LLC-stores                #    4.032 M/sec                    (33.33%)
        288,435.88 msec task-clock                #    8.000 CPUs utilized          
   103,327,105,382      cycles:u                  #    0.358 GHz                      (41.66%)
   543,172,200,992      cycles:k                  #    1.883 GHz                      (41.65%)
    64,240,745,833      instructions:u            #    0.62  insn per cycle           (49.98%)
   488,358,459,211      instructions:k            #    0.90  insn per cycle           (49.98%)

      36.054799152 seconds time elapsed

