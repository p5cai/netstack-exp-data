# started on Sat Apr  1 13:14:23 2023


 Performance counter stats for 'CPU(s) 0-7':

       117,621,602      LLC-load-misses           #   19.01% of all LL-cache accesses  (49.99%)
       221,031,932      LLC-load-misses           #    9.01% of all LL-cache accesses  (50.00%)
       618,829,793      LLC-loads                 #    2.140 M/sec                    (50.01%)
     2,453,631,820      LLC-loads                 #    8.484 M/sec                    (41.68%)
         2,697,715      LLC-store-misses          #    9.328 K/sec                    (25.01%)
        87,290,023      LLC-store-misses          #  301.816 K/sec                    (33.34%)
       191,950,757      LLC-stores                #  663.693 K/sec                    (24.99%)
     1,332,949,844      LLC-stores                #    4.609 M/sec                    (33.32%)
        289,216.40 msec task-clock                #    8.000 CPUs utilized          
   114,779,370,636      cycles:u                  #    0.397 GHz                      (41.66%)
   532,515,792,527      cycles:k                  #    1.841 GHz                      (41.65%)
    70,323,638,603      instructions:u            #    0.61  insn per cycle           (49.98%)
   448,591,499,027      instructions:k            #    0.84  insn per cycle           (49.98%)

      36.153133053 seconds time elapsed

