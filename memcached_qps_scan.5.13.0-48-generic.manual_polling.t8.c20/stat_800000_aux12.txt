# started on Sat Apr  1 11:28:16 2023


 Performance counter stats for 'CPU(s) 0-7':

       116,320,850      LLC-load-misses           #   18.36% of all LL-cache accesses  (49.98%)
       212,647,221      LLC-load-misses           #    8.79% of all LL-cache accesses  (49.98%)
       633,716,477      LLC-loads                 #    2.195 M/sec                    (49.98%)
     2,418,327,878      LLC-loads                 #    8.378 M/sec                    (41.67%)
         2,575,182      LLC-store-misses          #    8.921 K/sec                    (25.01%)
        75,530,984      LLC-store-misses          #  261.658 K/sec                    (33.35%)
       202,364,819      LLC-stores                #  701.041 K/sec                    (25.01%)
     1,320,892,888      LLC-stores                #    4.576 M/sec                    (33.34%)
        288,663.31 msec task-clock                #    8.000 CPUs utilized          
   112,754,180,971      cycles:u                  #    0.391 GHz                      (41.68%)
   534,406,088,763      cycles:k                  #    1.851 GHz                      (41.67%)
    70,509,899,517      instructions:u            #    0.63  insn per cycle           (49.99%)
   459,776,316,822      instructions:k            #    0.86  insn per cycle           (49.98%)

      36.083992829 seconds time elapsed

