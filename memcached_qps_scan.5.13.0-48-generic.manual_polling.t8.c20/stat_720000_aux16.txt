# started on Sat Apr  1 12:37:20 2023


 Performance counter stats for 'CPU(s) 0-7':

       106,212,735      LLC-load-misses           #   19.55% of all LL-cache accesses  (50.00%)
       200,341,542      LLC-load-misses           #    9.31% of all LL-cache accesses  (50.00%)
       543,362,342      LLC-loads                 #    1.881 M/sec                    (50.01%)
     2,151,469,105      LLC-loads                 #    7.450 M/sec                    (41.68%)
           854,063      LLC-store-misses          #    2.957 K/sec                    (25.00%)
        81,422,583      LLC-store-misses          #  281.941 K/sec                    (33.33%)
       193,580,802      LLC-stores                #  670.309 K/sec                    (25.00%)
     1,163,932,429      LLC-stores                #    4.030 M/sec                    (33.33%)
        288,793.44 msec task-clock                #    8.000 CPUs utilized          
   103,472,067,562      cycles:u                  #    0.358 GHz                      (41.66%)
   543,975,781,870      cycles:k                  #    1.884 GHz                      (41.66%)
    64,162,181,258      instructions:u            #    0.62  insn per cycle           (49.99%)
   483,543,978,097      instructions:k            #    0.89  insn per cycle           (49.99%)

      36.099034451 seconds time elapsed

