# started on Sat Apr  1 13:08:39 2023


 Performance counter stats for 'CPU(s) 0-7':

        83,505,250      LLC-load-misses           #   19.86% of all LL-cache accesses  (49.97%)
       160,099,412      LLC-load-misses           #    9.99% of all LL-cache accesses  (49.98%)
       420,538,376      LLC-loads                 #    1.457 M/sec                    (49.99%)
     1,603,335,620      LLC-loads                 #    5.556 M/sec                    (41.68%)
           635,033      LLC-store-misses          #    2.201 K/sec                    (25.02%)
        63,907,137      LLC-store-misses          #  221.466 K/sec                    (33.36%)
       153,087,498      LLC-stores                #  530.514 K/sec                    (25.01%)
       883,051,156      LLC-stores                #    3.060 M/sec                    (33.34%)
        288,564.74 msec task-clock                #    8.000 CPUs utilized          
    84,538,058,999      cycles:u                  #    0.293 GHz                      (41.67%)
   562,779,826,863      cycles:k                  #    1.950 GHz                      (41.65%)
    51,240,321,135      instructions:u            #    0.61  insn per cycle           (49.98%)
   584,271,238,677      instructions:k            #    1.04  insn per cycle           (49.97%)

      36.070561176 seconds time elapsed

