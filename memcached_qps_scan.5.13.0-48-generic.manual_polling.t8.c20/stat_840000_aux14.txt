# started on Sat Apr  1 12:04:30 2023


 Performance counter stats for 'CPU(s) 0-7':

       125,019,730      LLC-load-misses           #   20.41% of all LL-cache accesses  (49.99%)
       223,792,966      LLC-load-misses           #    8.57% of all LL-cache accesses  (50.00%)
       612,571,021      LLC-loads                 #    2.121 M/sec                    (50.01%)
     2,611,029,206      LLC-loads                 #    9.040 M/sec                    (41.68%)
         2,871,246      LLC-store-misses          #    9.941 K/sec                    (25.00%)
        85,872,988      LLC-store-misses          #  297.312 K/sec                    (33.34%)
       204,949,246      LLC-stores                #  709.581 K/sec                    (24.99%)
     1,409,008,354      LLC-stores                #    4.878 M/sec                    (33.33%)
        288,831.18 msec task-clock                #    8.000 CPUs utilized          
   114,458,321,902      cycles:u                  #    0.396 GHz                      (41.66%)
   532,902,048,477      cycles:k                  #    1.845 GHz                      (41.65%)
    73,321,114,331      instructions:u            #    0.64  insn per cycle           (49.98%)
   449,172,665,923      instructions:k            #    0.84  insn per cycle           (49.98%)

      36.104970667 seconds time elapsed

