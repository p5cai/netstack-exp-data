# started on Sat Apr  1 11:10:37 2023


 Performance counter stats for 'CPU(s) 0-7':

       117,265,287      LLC-load-misses           #   19.57% of all LL-cache accesses  (50.00%)
       214,816,875      LLC-load-misses           #    8.86% of all LL-cache accesses  (50.00%)
       599,240,971      LLC-loads                 #    2.075 M/sec                    (50.01%)
     2,423,541,093      LLC-loads                 #    8.392 M/sec                    (41.68%)
         2,706,897      LLC-store-misses          #    9.373 K/sec                    (25.00%)
        78,335,229      LLC-store-misses          #  271.252 K/sec                    (33.33%)
       204,954,093      LLC-stores                #  709.697 K/sec                    (25.00%)
     1,326,926,694      LLC-stores                #    4.595 M/sec                    (33.33%)
        288,791.03 msec task-clock                #    8.000 CPUs utilized          
   111,353,539,200      cycles:u                  #    0.386 GHz                      (41.66%)
   535,906,033,515      cycles:k                  #    1.856 GHz                      (41.66%)
    70,504,831,244      instructions:u            #    0.63  insn per cycle           (49.99%)
   462,100,929,620      instructions:k            #    0.86  insn per cycle           (49.99%)

      36.099937022 seconds time elapsed

