# started on Sat Apr  1 11:56:27 2023


 Performance counter stats for 'CPU(s) 0-7':

        76,542,851      LLC-load-misses           #   17.86% of all LL-cache accesses  (49.99%)
       150,725,084      LLC-load-misses           #    9.97% of all LL-cache accesses  (50.00%)
       428,477,695      LLC-loads                 #    1.485 M/sec                    (50.01%)
     1,512,047,386      LLC-loads                 #    5.242 M/sec                    (41.69%)
         2,220,125      LLC-store-misses          #    7.696 K/sec                    (25.01%)
        54,037,453      LLC-store-misses          #  187.327 K/sec                    (33.34%)
       152,417,387      LLC-stores                #  528.374 K/sec                    (24.99%)
       816,796,385      LLC-stores                #    2.832 M/sec                    (33.32%)
        288,465.16 msec task-clock                #    8.000 CPUs utilized          
    82,953,138,830      cycles:u                  #    0.288 GHz                      (41.65%)
   565,786,288,779      cycles:k                  #    1.961 GHz                      (41.65%)
    47,934,277,725      instructions:u            #    0.58  insn per cycle           (49.98%)
   601,443,895,637      instructions:k            #    1.06  insn per cycle           (49.98%)

      36.059224853 seconds time elapsed

