# started on Wed Mar 29 16:09:08 2023


 Performance counter stats for 'CPU(s) 0-7':

        11,823,582      LLC-load-misses           #   17.98% of all LL-cache accesses  (49.97%)
        30,534,680      LLC-load-misses           #   11.81% of all LL-cache accesses  (49.98%)
        65,747,747      LLC-loads                 #  227.791 K/sec                    (49.99%)
       258,629,644      LLC-loads                 #  896.052 K/sec                    (41.67%)
            98,371      LLC-store-misses          #  340.818 /sec                     (25.01%)
         5,865,313      LLC-store-misses          #   20.321 K/sec                    (33.35%)
        26,514,945      LLC-stores                #   91.864 K/sec                    (25.01%)
       136,423,319      LLC-stores                #  472.654 K/sec                    (33.35%)
        288,632.41 msec task-clock                #    8.000 CPUs utilized          
    14,133,950,169      cycles:u                  #    0.049 GHz                      (41.68%)
   631,006,835,037      cycles:k                  #    2.186 GHz                      (41.66%)
     7,641,784,562      instructions:u            #    0.54  insn per cycle           (49.99%)
 1,016,014,787,987      instructions:k            #    1.61  insn per cycle           (49.98%)

      36.079065440 seconds time elapsed

