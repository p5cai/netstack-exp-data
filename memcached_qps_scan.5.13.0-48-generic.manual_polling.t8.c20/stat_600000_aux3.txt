# started on Sat Apr  1 08:44:15 2023


 Performance counter stats for 'CPU(s) 0-7':

        88,140,208      LLC-load-misses           #   18.94% of all LL-cache accesses  (49.97%)
       167,476,743      LLC-load-misses           #    9.61% of all LL-cache accesses  (49.98%)
       465,277,244      LLC-loads                 #    1.610 M/sec                    (49.99%)
     1,742,503,716      LLC-loads                 #    6.030 M/sec                    (41.68%)
         2,480,583      LLC-store-misses          #    8.584 K/sec                    (25.02%)
        66,139,593      LLC-store-misses          #  228.866 K/sec                    (33.35%)
       158,928,975      LLC-stores                #  549.950 K/sec                    (25.01%)
       946,907,271      LLC-stores                #    3.277 M/sec                    (33.35%)
        288,987.80 msec task-clock                #    8.000 CPUs utilized          
    91,289,065,698      cycles:u                  #    0.316 GHz                      (41.67%)
   555,327,969,595      cycles:k                  #    1.922 GHz                      (41.66%)
    54,799,121,794      instructions:u            #    0.60  insn per cycle           (49.99%)
   555,440,027,467      instructions:k            #    1.00  insn per cycle           (49.97%)

      36.124455765 seconds time elapsed

