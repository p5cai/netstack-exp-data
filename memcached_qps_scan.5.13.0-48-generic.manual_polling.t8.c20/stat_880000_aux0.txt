# started on Wed Mar 29 16:00:24 2023


 Performance counter stats for 'CPU(s) 0-7':

       127,105,952      LLC-load-misses           #   19.99% of all LL-cache accesses  (49.97%)
       218,296,861      LLC-load-misses           #    7.85% of all LL-cache accesses  (49.98%)
       635,730,366      LLC-loads                 #    2.203 M/sec                    (49.98%)
     2,780,289,765      LLC-loads                 #    9.633 M/sec                    (41.67%)
           763,695      LLC-store-misses          #    2.646 K/sec                    (25.02%)
        78,237,102      LLC-store-misses          #  271.064 K/sec                    (33.35%)
       177,995,969      LLC-stores                #  616.694 K/sec                    (25.01%)
     1,506,079,269      LLC-stores                #    5.218 M/sec                    (33.35%)
        288,629.16 msec task-clock                #    8.000 CPUs utilized          
   120,670,321,735      cycles:u                  #    0.418 GHz                      (41.68%)
   526,105,042,065      cycles:k                  #    1.823 GHz                      (41.66%)
    75,883,672,278      instructions:u            #    0.63  insn per cycle           (49.99%)
   452,395,721,918      instructions:k            #    0.86  insn per cycle           (49.98%)

      36.077996907 seconds time elapsed

