# started on Sat Apr  1 10:56:56 2023


 Performance counter stats for 'CPU(s) 0-7':

       138,861,235      LLC-load-misses           #   19.57% of all LL-cache accesses  (49.99%)
       274,547,292      LLC-load-misses           #    8.23% of all LL-cache accesses  (50.01%)
       709,597,823      LLC-loads                 #    2.454 M/sec                    (50.01%)
     3,335,413,279      LLC-loads                 #   11.534 M/sec                    (41.68%)
         2,811,136      LLC-store-misses          #    9.721 K/sec                    (25.00%)
        71,822,972      LLC-store-misses          #  248.356 K/sec                    (33.33%)
       207,553,423      LLC-stores                #  717.698 K/sec                    (25.00%)
     1,694,014,211      LLC-stores                #    5.858 M/sec                    (33.33%)
        289,193.25 msec task-clock                #    8.000 CPUs utilized          
   122,072,968,460      cycles:u                  #    0.422 GHz                      (41.66%)
   524,019,048,012      cycles:k                  #    1.812 GHz                      (41.66%)
    81,107,859,388      instructions:u            #    0.66  insn per cycle           (49.99%)
   460,002,609,413      instructions:k            #    0.88  insn per cycle           (49.99%)

      36.150246500 seconds time elapsed

