# started on Sat Apr  1 10:38:25 2023


 Performance counter stats for 'CPU(s) 0-7':

       130,733,931      LLC-load-misses           #   18.97% of all LL-cache accesses  (49.99%)
       258,697,235      LLC-load-misses           #    8.70% of all LL-cache accesses  (49.99%)
       689,248,812      LLC-loads                 #    2.384 M/sec                    (49.99%)
     2,972,315,755      LLC-loads                 #   10.282 M/sec                    (41.66%)
         2,574,010      LLC-store-misses          #    8.904 K/sec                    (25.01%)
        57,541,488      LLC-store-misses          #  199.050 K/sec                    (33.34%)
       193,721,974      LLC-stores                #  670.131 K/sec                    (25.01%)
     1,589,174,791      LLC-stores                #    5.497 M/sec                    (33.34%)
        289,080.83 msec task-clock                #    8.000 CPUs utilized          
   122,160,653,348      cycles:u                  #    0.423 GHz                      (41.67%)
   524,537,876,582      cycles:k                  #    1.815 GHz                      (41.67%)
    78,356,000,297      instructions:u            #    0.64  insn per cycle           (50.00%)
   447,611,347,349      instructions:k            #    0.85  insn per cycle           (49.99%)

      36.136262000 seconds time elapsed

