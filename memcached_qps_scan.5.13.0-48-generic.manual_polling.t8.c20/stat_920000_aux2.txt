# started on Wed Mar 29 16:51:10 2023


 Performance counter stats for 'CPU(s) 0-7':

       131,190,867      LLC-load-misses           #   19.34% of all LL-cache accesses  (50.00%)
       229,405,228      LLC-load-misses           #    8.01% of all LL-cache accesses  (50.00%)
       678,324,512      LLC-loads                 #    2.349 M/sec                    (50.00%)
     2,865,743,416      LLC-loads                 #    9.924 M/sec                    (41.67%)
         2,589,898      LLC-store-misses          #    8.969 K/sec                    (25.00%)
        75,196,139      LLC-store-misses          #  260.407 K/sec                    (33.34%)
       202,331,522      LLC-stores                #  700.682 K/sec                    (25.00%)
     1,562,693,224      LLC-stores                #    5.412 M/sec                    (33.33%)
        288,763.81 msec task-clock                #    8.000 CPUs utilized          
   119,456,970,564      cycles:u                  #    0.414 GHz                      (41.66%)
   527,919,680,954      cycles:k                  #    1.828 GHz                      (41.66%)
    79,000,827,843      instructions:u            #    0.66  insn per cycle           (49.99%)
   461,071,762,845      instructions:k            #    0.87  insn per cycle           (50.00%)

      36.096704583 seconds time elapsed

