# started on Sat Apr  1 13:06:19 2023


 Performance counter stats for 'CPU(s) 0-7':

        72,468,328      LLC-load-misses           #   19.39% of all LL-cache accesses  (49.99%)
       139,241,741      LLC-load-misses           #   10.19% of all LL-cache accesses  (50.00%)
       373,799,430      LLC-loads                 #    1.294 M/sec                    (50.00%)
     1,366,439,760      LLC-loads                 #    4.732 M/sec                    (41.67%)
         2,503,270      LLC-store-misses          #    8.668 K/sec                    (25.00%)
        54,735,856      LLC-store-misses          #  189.542 K/sec                    (33.33%)
       144,639,217      LLC-stores                #  500.864 K/sec                    (25.00%)
       752,956,157      LLC-stores                #    2.607 M/sec                    (33.33%)
        288,779.32 msec task-clock                #    8.000 CPUs utilized          
    75,253,707,659      cycles:u                  #    0.261 GHz                      (41.66%)
   571,034,275,091      cycles:k                  #    1.977 GHz                      (41.66%)
    44,593,839,518      instructions:u            #    0.59  insn per cycle           (49.99%)
   640,977,020,360      instructions:k            #    1.12  insn per cycle           (49.99%)

      36.098492397 seconds time elapsed

