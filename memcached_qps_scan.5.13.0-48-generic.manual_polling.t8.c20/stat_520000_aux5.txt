# started on Sat Apr  1 09:17:18 2023


 Performance counter stats for 'CPU(s) 0-7':

        76,985,653      LLC-load-misses           #   19.82% of all LL-cache accesses  (49.99%)
       147,514,142      LLC-load-misses           #   10.09% of all LL-cache accesses  (49.99%)
       388,387,576      LLC-loads                 #    1.345 M/sec                    (50.00%)
     1,461,865,820      LLC-loads                 #    5.063 M/sec                    (41.66%)
         2,145,351      LLC-store-misses          #    7.430 K/sec                    (25.01%)
        56,900,462      LLC-store-misses          #  197.072 K/sec                    (33.34%)
       158,582,405      LLC-stores                #  549.243 K/sec                    (25.00%)
       816,223,719      LLC-stores                #    2.827 M/sec                    (33.34%)
        288,729.06 msec task-clock                #    8.000 CPUs utilized          
    79,879,363,509      cycles:u                  #    0.277 GHz                      (41.67%)
   568,280,464,941      cycles:k                  #    1.968 GHz                      (41.67%)
    47,976,498,241      instructions:u            #    0.60  insn per cycle           (50.00%)
   615,813,459,056      instructions:k            #    1.08  insn per cycle           (50.00%)

      36.092004651 seconds time elapsed

