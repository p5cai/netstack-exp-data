# started on Sat Apr  1 08:57:52 2023


 Performance counter stats for 'CPU(s) 0-7':

        48,500,747      LLC-load-misses           #   18.54% of all LL-cache accesses  (50.00%)
        98,931,127      LLC-load-misses           #   10.63% of all LL-cache accesses  (50.00%)
       261,587,137      LLC-loads                 #  905.790 K/sec                    (50.01%)
       930,398,024      LLC-loads                 #    3.222 M/sec                    (41.68%)
         2,281,749      LLC-store-misses          #    7.901 K/sec                    (25.00%)
        37,396,062      LLC-store-misses          #  129.490 K/sec                    (33.33%)
       100,448,638      LLC-stores                #  347.820 K/sec                    (25.00%)
       507,137,171      LLC-stores                #    1.756 M/sec                    (33.33%)
        288,794.50 msec task-clock                #    8.000 CPUs utilized          
    59,543,543,704      cycles:u                  #    0.206 GHz                      (41.66%)
   586,977,545,296      cycles:k                  #    2.033 GHz                      (41.66%)
    30,253,868,188      instructions:u            #    0.51  insn per cycle           (49.99%)
   733,275,366,338      instructions:k            #    1.25  insn per cycle           (49.99%)

      36.100661104 seconds time elapsed

