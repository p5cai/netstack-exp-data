# started on Sat Apr  1 09:34:05 2023


 Performance counter stats for 'CPU(s) 0-7':

        71,231,440      LLC-load-misses           #   19.15% of all LL-cache accesses  (49.99%)
       136,637,811      LLC-load-misses           #   10.04% of all LL-cache accesses  (49.99%)
       371,872,583      LLC-loads                 #    1.288 M/sec                    (49.99%)
     1,361,354,301      LLC-loads                 #    4.715 M/sec                    (41.66%)
         2,413,820      LLC-store-misses          #    8.361 K/sec                    (25.01%)
        50,060,322      LLC-store-misses          #  173.399 K/sec                    (33.34%)
       144,754,323      LLC-stores                #  501.401 K/sec                    (25.01%)
       750,403,886      LLC-stores                #    2.599 M/sec                    (33.34%)
        288,699.59 msec task-clock                #    8.000 CPUs utilized          
    77,260,872,874      cycles:u                  #    0.268 GHz                      (41.67%)
   569,685,252,775      cycles:k                  #    1.973 GHz                      (41.67%)
    44,526,968,149      instructions:u            #    0.58  insn per cycle           (50.00%)
   639,755,969,569      instructions:k            #    1.12  insn per cycle           (49.99%)

      36.089105399 seconds time elapsed

