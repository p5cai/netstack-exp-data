# started on Wed Mar 29 16:43:57 2023


 Performance counter stats for 'CPU(s) 0-7':

        95,755,371      LLC-load-misses           #   19.57% of all LL-cache accesses  (49.97%)
       171,170,678      LLC-load-misses           #    9.38% of all LL-cache accesses  (49.98%)
       489,254,627      LLC-loads                 #    1.693 M/sec                    (49.99%)
     1,824,751,956      LLC-loads                 #    6.315 M/sec                    (41.68%)
         2,736,914      LLC-store-misses          #    9.471 K/sec                    (25.02%)
        74,141,262      LLC-store-misses          #  256.576 K/sec                    (33.36%)
       166,840,371      LLC-stores                #  577.374 K/sec                    (25.01%)
     1,018,274,575      LLC-stores                #    3.524 M/sec                    (33.34%)
        288,963.94 msec task-clock                #    8.000 CPUs utilized          
    94,576,340,740      cycles:u                  #    0.327 GHz                      (41.67%)
   552,839,992,687      cycles:k                  #    1.913 GHz                      (41.65%)
    58,107,298,086      instructions:u            #    0.61  insn per cycle           (49.98%)
   538,812,447,971      instructions:k            #    0.97  insn per cycle           (49.97%)

      36.121513726 seconds time elapsed

