# started on Wed Mar 29 16:14:53 2023


 Performance counter stats for 'CPU(s) 0-7':

        93,467,851      LLC-load-misses           #   19.24% of all LL-cache accesses  (49.98%)
       163,665,136      LLC-load-misses           #    8.89% of all LL-cache accesses  (49.98%)
       485,859,530      LLC-loads                 #    1.677 M/sec                    (49.98%)
     1,840,665,523      LLC-loads                 #    6.352 M/sec                    (41.67%)
         2,521,584      LLC-store-misses          #    8.701 K/sec                    (25.01%)
        60,326,022      LLC-store-misses          #  208.173 K/sec                    (33.35%)
       185,316,941      LLC-stores                #  639.491 K/sec                    (25.01%)
     1,016,968,952      LLC-stores                #    3.509 M/sec                    (33.35%)
        289,788.10 msec task-clock                #    8.000 CPUs utilized          
    97,739,902,003      cycles:u                  #    0.337 GHz                      (41.68%)
   549,075,994,005      cycles:k                  #    1.895 GHz                      (41.67%)
    57,953,215,046      instructions:u            #    0.59  insn per cycle           (50.00%)
   532,351,296,656      instructions:k            #    0.97  insn per cycle           (49.98%)

      36.224971719 seconds time elapsed

