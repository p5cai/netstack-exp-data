# started on Sat Apr  1 08:49:07 2023


 Performance counter stats for 'CPU(s) 0-7':

       116,031,299      LLC-load-misses           #   19.24% of all LL-cache accesses  (50.00%)
       212,639,303      LLC-load-misses           #    8.94% of all LL-cache accesses  (50.00%)
       603,103,186      LLC-loads                 #    2.088 M/sec                    (50.00%)
     2,377,316,955      LLC-loads                 #    8.232 M/sec                    (41.67%)
         2,654,628      LLC-store-misses          #    9.193 K/sec                    (25.00%)
        81,219,819      LLC-store-misses          #  281.251 K/sec                    (33.33%)
       193,262,951      LLC-stores                #  669.238 K/sec                    (25.00%)
     1,313,434,805      LLC-stores                #    4.548 M/sec                    (33.33%)
        288,780.67 msec task-clock                #    8.000 CPUs utilized          
   111,935,394,554      cycles:u                  #    0.388 GHz                      (41.66%)
   535,309,724,897      cycles:k                  #    1.854 GHz                      (41.66%)
    70,562,789,004      instructions:u            #    0.63  insn per cycle           (49.99%)
   461,981,864,912      instructions:k            #    0.86  insn per cycle           (49.99%)

      36.098393024 seconds time elapsed

