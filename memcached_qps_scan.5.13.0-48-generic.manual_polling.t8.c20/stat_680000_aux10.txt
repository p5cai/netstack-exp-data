# started on Sat Apr  1 10:49:44 2023


 Performance counter stats for 'CPU(s) 0-7':

       101,039,705      LLC-load-misses           #   19.29% of all LL-cache accesses  (49.97%)
       190,242,638      LLC-load-misses           #    9.48% of all LL-cache accesses  (49.98%)
       523,793,714      LLC-loads                 #    1.815 M/sec                    (50.00%)
     2,007,242,706      LLC-loads                 #    6.956 M/sec                    (41.68%)
         2,616,835      LLC-store-misses          #    9.068 K/sec                    (25.02%)
        72,020,425      LLC-store-misses          #  249.566 K/sec                    (33.36%)
       183,296,127      LLC-stores                #  635.160 K/sec                    (25.01%)
     1,091,861,018      LLC-stores                #    3.784 M/sec                    (33.34%)
        288,582.51 msec task-clock                #    8.000 CPUs utilized          
   101,556,159,932      cycles:u                  #    0.352 GHz                      (41.67%)
   545,634,616,249      cycles:k                  #    1.891 GHz                      (41.65%)
    61,239,409,488      instructions:u            #    0.60  insn per cycle           (49.98%)
   503,957,455,250      instructions:k            #    0.92  insn per cycle           (49.97%)

      36.073896315 seconds time elapsed

