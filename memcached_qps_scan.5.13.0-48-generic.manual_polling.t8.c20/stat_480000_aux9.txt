# started on Sat Apr  1 10:27:10 2023


 Performance counter stats for 'CPU(s) 0-7':

        71,298,274      LLC-load-misses           #   18.90% of all LL-cache accesses  (49.97%)
       136,823,011      LLC-load-misses           #    9.98% of all LL-cache accesses  (49.98%)
       377,171,171      LLC-loads                 #    1.305 M/sec                    (49.99%)
     1,371,161,932      LLC-loads                 #    4.745 M/sec                    (41.68%)
         2,358,685      LLC-store-misses          #    8.163 K/sec                    (25.02%)
        48,642,839      LLC-store-misses          #  168.342 K/sec                    (33.36%)
       137,911,609      LLC-stores                #  477.283 K/sec                    (25.01%)
       753,191,985      LLC-stores                #    2.607 M/sec                    (33.34%)
        288,951.70 msec task-clock                #    8.000 CPUs utilized          
    76,730,190,560      cycles:u                  #    0.266 GHz                      (41.67%)
   571,001,845,246      cycles:k                  #    1.976 GHz                      (41.65%)
    44,597,977,903      instructions:u            #    0.58  insn per cycle           (49.98%)
   635,729,505,832      instructions:k            #    1.11  insn per cycle           (49.97%)

      36.120031933 seconds time elapsed

