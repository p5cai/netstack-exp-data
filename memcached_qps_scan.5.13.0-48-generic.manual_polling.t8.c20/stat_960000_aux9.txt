# started on Sat Apr  1 10:39:17 2023


 Performance counter stats for 'CPU(s) 0-7':

       131,970,321      LLC-load-misses           #   18.82% of all LL-cache accesses  (50.00%)
       269,174,255      LLC-load-misses           #    8.88% of all LL-cache accesses  (50.00%)
       701,141,187      LLC-loads                 #    2.425 M/sec                    (50.01%)
     3,030,765,954      LLC-loads                 #   10.481 M/sec                    (41.68%)
         2,512,465      LLC-store-misses          #    8.688 K/sec                    (25.00%)
        49,093,433      LLC-store-misses          #  169.767 K/sec                    (33.33%)
       200,243,355      LLC-stores                #  692.449 K/sec                    (25.00%)
     1,639,366,438      LLC-stores                #    5.669 M/sec                    (33.33%)
        289,181.31 msec task-clock                #    8.000 CPUs utilized          
   123,215,864,736      cycles:u                  #    0.426 GHz                      (41.66%)
   523,832,475,607      cycles:k                  #    1.811 GHz                      (41.66%)
    79,350,053,160      instructions:u            #    0.64  insn per cycle           (49.99%)
   450,505,418,520      instructions:k            #    0.86  insn per cycle           (49.99%)

      36.148808627 seconds time elapsed

