# started on Sat Apr  1 09:30:52 2023


 Performance counter stats for 'CPU(s) 0-7':

       135,944,128      LLC-load-misses           #   19.42% of all LL-cache accesses  (49.98%)
       274,273,690      LLC-load-misses           #    8.48% of all LL-cache accesses  (49.98%)
       699,932,509      LLC-loads                 #    2.428 M/sec                    (49.99%)
     3,234,833,258      LLC-loads                 #   11.221 M/sec                    (41.67%)
           508,335      LLC-store-misses          #    1.763 K/sec                    (25.01%)
        52,715,462      LLC-store-misses          #  182.860 K/sec                    (33.35%)
       196,230,309      LLC-stores                #  680.687 K/sec                    (25.01%)
     1,706,419,015      LLC-stores                #    5.919 M/sec                    (33.34%)
        288,282.57 msec task-clock                #    8.000 CPUs utilized          
   123,861,929,483      cycles:u                  #    0.430 GHz                      (41.68%)
   523,833,324,652      cycles:k                  #    1.817 GHz                      (41.67%)
    81,596,588,945      instructions:u            #    0.66  insn per cycle           (49.99%)
   464,825,190,833      instructions:k            #    0.89  insn per cycle           (49.98%)

      36.035504896 seconds time elapsed

