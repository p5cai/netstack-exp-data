# started on Sat Apr  1 10:10:22 2023


 Performance counter stats for 'CPU(s) 0-7':

        76,928,020      LLC-load-misses           #   18.08% of all LL-cache accesses  (49.97%)
       147,672,137      LLC-load-misses           #    9.87% of all LL-cache accesses  (49.98%)
       425,505,212      LLC-loads                 #    1.475 M/sec                    (50.00%)
     1,496,504,623      LLC-loads                 #    5.187 M/sec                    (41.68%)
         2,123,367      LLC-store-misses          #    7.359 K/sec                    (25.02%)
        60,812,032      LLC-store-misses          #  210.762 K/sec                    (33.36%)
       148,630,089      LLC-stores                #  515.121 K/sec                    (25.01%)
       821,277,594      LLC-stores                #    2.846 M/sec                    (33.33%)
        288,534.20 msec task-clock                #    8.000 CPUs utilized          
    80,977,260,651      cycles:u                  #    0.281 GHz                      (41.66%)
   567,450,781,651      cycles:k                  #    1.967 GHz                      (41.65%)
    48,007,586,188      instructions:u            #    0.59  insn per cycle           (49.98%)
   612,576,872,198      instructions:k            #    1.08  insn per cycle           (49.97%)

      36.067944018 seconds time elapsed

