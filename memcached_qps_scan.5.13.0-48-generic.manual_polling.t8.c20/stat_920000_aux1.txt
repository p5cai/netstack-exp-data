# started on Wed Mar 29 16:22:05 2023


 Performance counter stats for 'CPU(s) 0-7':

       130,768,906      LLC-load-misses           #   19.39% of all LL-cache accesses  (50.00%)
       234,322,027      LLC-load-misses           #    8.24% of all LL-cache accesses  (50.00%)
       674,286,843      LLC-loads                 #    2.332 M/sec                    (50.00%)
     2,845,111,345      LLC-loads                 #    9.840 M/sec                    (41.67%)
         2,566,100      LLC-store-misses          #    8.875 K/sec                    (25.00%)
        63,276,829      LLC-store-misses          #  218.843 K/sec                    (33.34%)
       189,520,367      LLC-stores                #  655.455 K/sec                    (25.00%)
     1,555,197,125      LLC-stores                #    5.379 M/sec                    (33.33%)
        289,143.16 msec task-clock                #    8.000 CPUs utilized          
   120,221,205,723      cycles:u                  #    0.416 GHz                      (41.67%)
   526,219,087,373      cycles:k                  #    1.820 GHz                      (41.66%)
    78,889,362,854      instructions:u            #    0.66  insn per cycle           (50.00%)
   461,302,090,034      instructions:k            #    0.88  insn per cycle           (50.00%)

      36.144089203 seconds time elapsed

