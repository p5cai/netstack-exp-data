# started on Wed Mar 29 16:21:14 2023


 Performance counter stats for 'CPU(s) 0-7':

       127,997,523      LLC-load-misses           #   20.02% of all LL-cache accesses  (49.97%)
       219,774,691      LLC-load-misses           #    8.11% of all LL-cache accesses  (49.98%)
       639,387,826      LLC-loads                 #    2.210 M/sec                    (49.99%)
     2,711,192,298      LLC-loads                 #    9.369 M/sec                    (41.67%)
           855,227      LLC-store-misses          #    2.956 K/sec                    (25.02%)
        82,570,010      LLC-store-misses          #  285.349 K/sec                    (33.35%)
       179,280,709      LLC-stores                #  619.566 K/sec                    (25.01%)
     1,486,694,312      LLC-stores                #    5.138 M/sec                    (33.35%)
        289,365.06 msec task-clock                #    8.000 CPUs utilized          
   117,266,980,891      cycles:u                  #    0.405 GHz                      (41.67%)
   531,022,877,867      cycles:k                  #    1.835 GHz                      (41.66%)
    76,221,230,455      instructions:u            #    0.65  insn per cycle           (49.99%)
   458,587,264,648      instructions:k            #    0.86  insn per cycle           (49.98%)

      36.170558503 seconds time elapsed

