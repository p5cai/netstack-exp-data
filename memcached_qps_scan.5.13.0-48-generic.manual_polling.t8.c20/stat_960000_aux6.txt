# started on Sat Apr  1 09:46:12 2023


 Performance counter stats for 'CPU(s) 0-7':

       135,272,471      LLC-load-misses           #   19.04% of all LL-cache accesses  (49.98%)
       264,465,661      LLC-load-misses           #    8.62% of all LL-cache accesses  (49.98%)
       710,353,264      LLC-loads                 #    2.461 M/sec                    (49.98%)
     3,066,632,336      LLC-loads                 #   10.624 M/sec                    (41.67%)
         2,489,843      LLC-store-misses          #    8.626 K/sec                    (25.01%)
        60,789,971      LLC-store-misses          #  210.594 K/sec                    (33.35%)
       190,207,221      LLC-stores                #  658.932 K/sec                    (25.01%)
     1,643,882,271      LLC-stores                #    5.695 M/sec                    (33.34%)
        288,659.70 msec task-clock                #    8.000 CPUs utilized          
   121,441,503,035      cycles:u                  #    0.421 GHz                      (41.68%)
   525,904,130,282      cycles:k                  #    1.822 GHz                      (41.67%)
    80,795,934,051      instructions:u            #    0.67  insn per cycle           (50.00%)
   459,674,687,086      instructions:k            #    0.87  insn per cycle           (49.99%)

      36.083656959 seconds time elapsed

