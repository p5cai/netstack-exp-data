# started on Sat Apr  1 09:07:38 2023


 Performance counter stats for 'CPU(s) 0-7':

       122,859,067      LLC-load-misses           #   19.50% of all LL-cache accesses  (49.98%)
       222,890,275      LLC-load-misses           #    8.67% of all LL-cache accesses  (49.98%)
       629,997,020      LLC-loads                 #    2.174 M/sec                    (49.98%)
     2,569,444,824      LLC-loads                 #    8.866 M/sec                    (41.66%)
         2,742,614      LLC-store-misses          #    9.463 K/sec                    (25.01%)
        85,768,823      LLC-store-misses          #  295.947 K/sec                    (33.35%)
       219,408,561      LLC-stores                #  757.073 K/sec                    (25.01%)
     1,401,277,903      LLC-stores                #    4.835 M/sec                    (33.34%)
        289,811.78 msec task-clock                #    8.000 CPUs utilized          
   116,854,431,282      cycles:u                  #    0.403 GHz                      (41.68%)
   530,051,189,150      cycles:k                  #    1.829 GHz                      (41.67%)
    73,392,385,068      instructions:u            #    0.63  insn per cycle           (50.00%)
   450,912,573,335      instructions:k            #    0.85  insn per cycle           (49.99%)

      36.227467201 seconds time elapsed

