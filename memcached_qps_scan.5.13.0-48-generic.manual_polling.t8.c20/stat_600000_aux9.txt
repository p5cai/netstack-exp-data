# started on Sat Apr  1 10:30:22 2023


 Performance counter stats for 'CPU(s) 0-7':

        88,395,572      LLC-load-misses           #   18.99% of all LL-cache accesses  (49.99%)
       168,231,537      LLC-load-misses           #    9.69% of all LL-cache accesses  (50.00%)
       465,565,337      LLC-loads                 #    1.610 M/sec                    (50.01%)
     1,735,532,495      LLC-loads                 #    6.001 M/sec                    (41.68%)
         2,561,284      LLC-store-misses          #    8.856 K/sec                    (25.01%)
        61,179,852      LLC-store-misses          #  211.535 K/sec                    (33.34%)
       184,183,069      LLC-stores                #  636.830 K/sec                    (24.99%)
       948,583,574      LLC-stores                #    3.280 M/sec                    (33.32%)
        289,218.66 msec task-clock                #    8.000 CPUs utilized          
    91,230,070,564      cycles:u                  #    0.315 GHz                      (41.65%)
   555,751,662,817      cycles:k                  #    1.922 GHz                      (41.65%)
    54,744,578,144      instructions:u            #    0.60  insn per cycle           (49.98%)
   549,723,757,573      instructions:k            #    0.99  insn per cycle           (49.98%)

      36.153418291 seconds time elapsed

