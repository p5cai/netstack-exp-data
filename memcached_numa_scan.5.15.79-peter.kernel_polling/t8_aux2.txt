#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1320.8    46.2  1002.2  1204.6  1253.0  1328.9  1388.8  1396.3  1504.3
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      5571341.0 143654.4 5324898.6 5126624.2 5176156.8 5572417.6 6028720.8 6086000.8 6131824.8

Total QPS = 848958.7 (25468770 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 6251624645 bytes :  198.7 MB/s
TX 1075335123 bytes :   34.2 MB/s
