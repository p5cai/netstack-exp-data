# started on Sun Apr  2 01:20:34 2023

#           time             counts unit events
     5.004233697          4,794,976      LLC-load-misses           #   20.28% of all LL-cache accesses  (49.88%)
     5.004233697         21,491,643      LLC-load-misses           #   17.56% of all LL-cache accesses  (49.96%)
     5.004233697         23,645,092      LLC-loads                 #  590.592 K/sec                    (50.04%)
     5.004233697        122,355,608      LLC-loads                 #    3.056 M/sec                    (41.81%)
     5.004233697            313,195      LLC-store-misses          #    7.823 K/sec                    (25.11%)
     5.004233697         10,391,102      LLC-store-misses          #  259.542 K/sec                    (33.42%)
     5.004233697          7,455,309      LLC-stores                #  186.214 K/sec                    (24.94%)
     5.004233697         59,841,123      LLC-stores                #    1.495 M/sec                    (33.25%)
     5.004233697          40,036.28 msec task-clock                #    8.007 CPUs utilized          
     5.004233697      3,978,955,059      cycles:u                  #    0.099 GHz                      (41.56%)
     5.004233697     20,899,400,006      cycles:k                  #    0.522 GHz                      (41.56%)
     5.004233697      2,671,670,641      instructions:u            #    0.67  insn per cycle           (49.87%)
     5.004233697     16,496,554,454      instructions:k            #    0.79  insn per cycle           (49.87%)
    10.008228667         21,948,052      LLC-load-misses           #   20.34% of all LL-cache accesses  (49.88%)
    10.008228667         94,645,549      LLC-load-misses           #   17.43% of all LL-cache accesses  (49.88%)
    10.008228667        107,919,548      LLC-loads                 #    2.696 M/sec                    (49.88%)
    10.008228667        542,920,166      LLC-loads                 #   13.563 M/sec                    (41.57%)
    10.008228667          1,456,916      LLC-store-misses          #   36.395 K/sec                    (25.02%)
    10.008228667         36,729,664      LLC-store-misses          #  917.536 K/sec                    (33.41%)
    10.008228667         35,294,945      LLC-stores                #  881.695 K/sec                    (25.18%)
    10.008228667        268,869,083      LLC-stores                #    6.717 M/sec                    (33.49%)
    10.008228667          40,030.77 msec task-clock                #    8.006 CPUs utilized          
    10.008228667     18,528,317,362      cycles:u                  #    0.463 GHz                      (41.81%)
    10.008228667     89,027,259,783      cycles:k                  #    2.224 GHz                      (41.73%)
    10.008228667     12,275,382,666      instructions:u            #    0.66  insn per cycle           (50.04%)
    10.008228667     68,483,662,459      instructions:k            #    0.77  insn per cycle           (49.96%)
    15.012237487         21,960,556      LLC-load-misses           #   20.33% of all LL-cache accesses  (50.12%)
    15.012237487         95,091,746      LLC-load-misses           #   17.51% of all LL-cache accesses  (50.04%)
    15.012237487        108,029,820      LLC-loads                 #    2.699 M/sec                    (49.97%)
    15.012237487        543,219,613      LLC-loads                 #   13.570 M/sec                    (41.57%)
    15.012237487          1,458,950      LLC-store-misses          #   36.446 K/sec                    (24.94%)
    15.012237487         36,697,024      LLC-store-misses          #  916.719 K/sec                    (33.26%)
    15.012237487         35,282,368      LLC-stores                #  881.379 K/sec                    (24.94%)
    15.012237487        269,559,178      LLC-stores                #    6.734 M/sec                    (33.33%)
    15.012237487          40,030.85 msec task-clock                #    8.006 CPUs utilized          
    15.012237487     18,532,129,140      cycles:u                  #    0.463 GHz                      (41.72%)
    15.012237487     88,998,103,484      cycles:k                  #    2.223 GHz                      (41.80%)
    15.012237487     12,287,845,959      instructions:u            #    0.66  insn per cycle           (50.12%)
    15.012237487     68,613,650,127      instructions:k            #    0.77  insn per cycle           (50.12%)
    20.016225055         21,973,833      LLC-load-misses           #   20.33% of all LL-cache accesses  (50.11%)
    20.016225055         94,986,334      LLC-load-misses           #   17.48% of all LL-cache accesses  (50.12%)
    20.016225055        108,074,409      LLC-loads                 #    2.700 M/sec                    (50.12%)
    20.016225055        543,299,479      LLC-loads                 #   13.572 M/sec                    (41.73%)
    20.016225055          1,463,373      LLC-store-misses          #   36.556 K/sec                    (24.94%)
    20.016225055         36,736,927      LLC-store-misses          #  917.725 K/sec                    (33.26%)
    20.016225055         35,251,139      LLC-stores                #  880.608 K/sec                    (24.94%)
    20.016225055        269,430,485      LLC-stores                #    6.731 M/sec                    (33.25%)
    20.016225055          40,030.45 msec task-clock                #    8.006 CPUs utilized          
    20.016225055     18,535,122,226      cycles:u                  #    0.463 GHz                      (41.57%)
    20.016225055     89,019,775,955      cycles:k                  #    2.224 GHz                      (41.56%)
    20.016225055     12,293,635,858      instructions:u            #    0.66  insn per cycle           (49.95%)
    20.016225055     68,642,402,726      instructions:k            #    0.77  insn per cycle           (50.03%)
    25.020228355         21,947,576      LLC-load-misses           #   20.32% of all LL-cache accesses  (49.88%)
    25.020228355         94,557,910      LLC-load-misses           #   17.42% of all LL-cache accesses  (49.95%)
    25.020228355        108,002,850      LLC-loads                 #    2.698 M/sec                    (50.04%)
    25.020228355        542,805,812      LLC-loads                 #   13.560 M/sec                    (41.80%)
    25.020228355          1,454,043      LLC-store-misses          #   36.323 K/sec                    (25.11%)
    25.020228355         36,678,452      LLC-store-misses          #  916.260 K/sec                    (33.42%)
    25.020228355         35,272,987      LLC-stores                #  881.151 K/sec                    (24.95%)
    25.020228355        269,025,904      LLC-stores                #    6.721 M/sec                    (33.26%)
    25.020228355          40,030.60 msec task-clock                #    8.006 CPUs utilized          
    25.020228355     18,544,289,908      cycles:u                  #    0.463 GHz                      (41.57%)
    25.020228355     88,999,134,228      cycles:k                  #    2.223 GHz                      (41.56%)
    25.020228355     12,279,807,984      instructions:u            #    0.66  insn per cycle           (49.88%)
    25.020228355     68,603,434,991      instructions:k            #    0.77  insn per cycle           (49.88%)
    30.024229795         21,963,656      LLC-load-misses           #   20.34% of all LL-cache accesses  (49.88%)
    30.024229795         94,908,432      LLC-load-misses           #   17.51% of all LL-cache accesses  (49.88%)
    30.024229795        107,993,006      LLC-loads                 #    2.698 M/sec                    (49.88%)
    30.024229795        541,918,727      LLC-loads                 #   13.537 M/sec                    (41.57%)
    30.024229795          1,456,834      LLC-store-misses          #   36.393 K/sec                    (25.02%)
    30.024229795         36,706,758      LLC-store-misses          #  916.958 K/sec                    (33.41%)
    30.024229795         35,348,697      LLC-stores                #  883.033 K/sec                    (25.18%)
    30.024229795        268,975,992      LLC-stores                #    6.719 M/sec                    (33.49%)
    30.024229795          40,031.01 msec task-clock                #    8.006 CPUs utilized          
    30.024229795     18,554,342,478      cycles:u                  #    0.463 GHz                      (41.81%)
    30.024229795     89,003,759,850      cycles:k                  #    2.223 GHz                      (41.73%)
    30.024229795     12,297,772,978      instructions:u            #    0.66  insn per cycle           (50.04%)
    30.024229795     68,637,725,214      instructions:k            #    0.77  insn per cycle           (49.96%)
    35.028254297         17,265,717      LLC-load-misses           #   20.33% of all LL-cache accesses  (50.12%)
    35.028254297         74,810,633      LLC-load-misses           #   17.56% of all LL-cache accesses  (50.04%)
    35.028254297         84,939,079      LLC-loads                 #    2.122 M/sec                    (49.97%)
    35.028254297        426,080,585      LLC-loads                 #   10.643 M/sec                    (41.57%)
    35.028254297          1,134,551      LLC-store-misses          #   28.341 K/sec                    (24.94%)
    35.028254297         28,990,131      LLC-store-misses          #  724.174 K/sec                    (33.26%)
    35.028254297         27,442,622      LLC-stores                #  685.517 K/sec                    (24.94%)
    35.028254297        210,057,755      LLC-stores                #    5.247 M/sec                    (33.33%)
    35.028254297          40,032.00 msec task-clock                #    8.006 CPUs utilized          
    35.028254297     14,487,409,452      cycles:u                  #    0.362 GHz                      (41.72%)
    35.028254297     69,782,441,400      cycles:k                  #    1.743 GHz                      (41.80%)
    35.028254297      9,626,188,412      instructions:u            #    0.66  insn per cycle           (50.12%)
    35.028254297     53,852,650,241      instructions:k            #    0.77  insn per cycle           (50.12%)
    36.161132317             24,392      LLC-load-misses           #   18.79% of all LL-cache accesses  (49.74%)
    36.161132317            282,297      LLC-load-misses           #   26.63% of all LL-cache accesses  (50.10%)
    36.161132317            129,833      LLC-loads                 #   14.326 K/sec                    (50.46%)
    36.161132317          1,059,899      LLC-loads                 #  116.952 K/sec                    (42.37%)
    36.161132317                 23      LLC-store-misses          #    2.538 /sec                     (25.43%)
    36.161132317            361,476      LLC-store-misses          #   39.886 K/sec                    (33.66%)
    36.161132317             10,177      LLC-stores                #    1.123 K/sec                    (24.83%)
    36.161132317            478,546      LLC-stores                #   52.804 K/sec                    (32.95%)
    36.161132317           9,062.72 msec task-clock                #    1.813 CPUs utilized          
    36.161132317          8,080,908      cycles:u                  #    0.001 GHz                      (41.06%)
    36.161132317        245,731,655      cycles:k                  #    0.027 GHz                      (40.71%)
    36.161132317         19,038,880      instructions:u            #    2.36  insn per cycle           (49.15%)
    36.161132317         47,425,691      instructions:k            #    0.19  insn per cycle           (49.39%)
