# started on Tue Feb 28 15:51:19 2023


 Performance counter stats for 'CPU(s) 0-7':

       140,932,996      LLC-load-misses                                               (41.65%)
       310,401,939      LLC-load-misses                                               (41.65%)
         6,409,883      mem_load_uops_retired.llc_miss                                     (33.32%)
   126,984,723,458      mem_uops_retired.all_loads                                     (16.67%)
   599,219,396,529      cycles                                                        (25.01%)
   105,238,291,538      cycles:u                                                      (25.01%)
   494,018,158,827      cycles:k                                                      (25.00%)
   434,829,408,387      instructions              #    0.73  insn per cycle         
                                                  #    0.90  stalled cycles per insn  (33.34%)
    61,583,959,592      instructions:u            #    0.59  insn per cycle           (41.67%)
   373,408,316,457      instructions:k            #    0.76  insn per cycle           (50.01%)
   390,451,797,678      stalled-cycles-frontend   #   65.16% frontend cycles idle     (50.00%)
   294,396,476,509      stalled-cycles-backend    #   49.13% backend cycles idle      (49.99%)

      31.870187112 seconds time elapsed

