#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1066.9  1843.9    84.1   823.8   879.7  1046.3  1216.8  1293.2  1660.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      6928480.7 582304.2 5857388.4 5928480.8 6182080.5 6923361.5 7746353.7 8371390.2 8869780.0

Total QPS = 2127634.8 (63830362 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 15724804864 bytes :  499.9 MB/s
TX 2695221132 bytes :   85.7 MB/s
