# started on Thu Feb 16 15:22:13 2023


 Performance counter stats for 'CPU(s) 0-0':

        18,688,942      LLC-load-misses                                               (41.66%)
        63,055,947      LLC-load-misses                                               (41.66%)
           169,896      mem_load_uops_retired.llc_miss                                     (33.32%)
    21,092,824,604      mem_uops_retired.all_loads                                     (16.67%)
    81,692,075,092      cycles                                                        (25.00%)
    11,229,071,232      cycles:u                                                      (25.00%)
    70,529,082,513      cycles:k                                                      (25.00%)
    70,622,100,778      instructions              #    0.86  insn per cycle         
                                                  #    0.70  stalled cycles per insn  (33.33%)
     5,991,101,048      instructions:u            #    0.53  insn per cycle           (41.67%)
    64,593,314,924      instructions:k            #    0.92  insn per cycle           (50.00%)
    49,295,095,298      stalled-cycles-frontend   #   60.34% frontend cycles idle     (50.00%)
    36,707,979,335      stalled-cycles-backend    #   44.93% backend cycles idle      (50.00%)

      31.869878573 seconds time elapsed

