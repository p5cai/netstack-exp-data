# started on Sat Apr  1 23:03:22 2023

#           time             counts unit events
     5.003581002          1,580,016      LLC-load-misses           #   20.37% of all LL-cache accesses  (49.95%)
     5.003581002         16,017,441      LLC-load-misses           #   28.99% of all LL-cache accesses  (50.03%)
     5.003581002          7,758,234      LLC-loads                 #  387.625 K/sec                    (50.11%)
     5.003581002         55,257,694      LLC-loads                 #    2.761 M/sec                    (41.81%)
     5.003581002            115,469      LLC-store-misses          #    5.769 K/sec                    (25.03%)
     5.003581002          9,896,634      LLC-store-misses          #  494.466 K/sec                    (33.34%)
     5.003581002          2,855,880      LLC-stores                #  142.689 K/sec                    (24.94%)
     5.003581002         25,557,703      LLC-stores                #    1.277 M/sec                    (33.25%)
     5.003581002          20,014.77 msec task-clock                #    4.003 CPUs utilized          
     5.003581002      1,427,055,755      cycles:u                  #    0.071 GHz                      (41.57%)
     5.003581002     10,073,824,828      cycles:k                  #    0.503 GHz                      (41.56%)
     5.003581002      1,056,127,883      instructions:u            #    0.74  insn per cycle           (49.88%)
     5.003581002      5,408,861,611      instructions:k            #    0.54  insn per cycle           (49.88%)
    10.007584850          7,754,249      LLC-load-misses           #   19.93% of all LL-cache accesses  (49.88%)
    10.007584850         77,675,451      LLC-load-misses           #   28.84% of all LL-cache accesses  (49.88%)
    10.007584850         38,906,079      LLC-loads                 #    1.944 M/sec                    (49.88%)
    10.007584850        269,329,157      LLC-loads                 #   13.456 M/sec                    (41.65%)
    10.007584850            493,786      LLC-store-misses          #   24.670 K/sec                    (25.10%)
    10.007584850         44,782,290      LLC-store-misses          #    2.237 M/sec                    (33.49%)
    10.007584850         14,970,958      LLC-stores                #  747.976 K/sec                    (25.10%)
    10.007584850        122,515,697      LLC-stores                #    6.121 M/sec                    (33.42%)
    10.007584850          20,015.29 msec task-clock                #    4.003 CPUs utilized          
    10.007584850      6,674,118,907      cycles:u                  #    0.333 GHz                      (41.73%)
    10.007584850     47,102,672,871      cycles:k                  #    2.353 GHz                      (41.65%)
    10.007584850      4,330,147,529      instructions:u            #    0.65  insn per cycle           (49.96%)
    10.007584850     26,045,280,473      instructions:k            #    0.55  insn per cycle           (49.88%)
    15.011586099          7,729,327      LLC-load-misses           #   19.96% of all LL-cache accesses  (50.04%)
    15.011586099         77,678,247      LLC-load-misses           #   28.97% of all LL-cache accesses  (49.96%)
    15.011586099         38,720,317      LLC-loads                 #    1.935 M/sec                    (49.88%)
    15.011586099        268,166,469      LLC-loads                 #   13.398 M/sec                    (41.57%)
    15.011586099            491,072      LLC-store-misses          #   24.534 K/sec                    (24.94%)
    15.011586099         44,851,180      LLC-store-misses          #    2.241 M/sec                    (33.26%)
    15.011586099         14,964,666      LLC-stores                #  747.650 K/sec                    (25.02%)
    15.011586099        123,012,355      LLC-stores                #    6.146 M/sec                    (33.41%)
    15.011586099          20,015.61 msec task-clock                #    4.003 CPUs utilized          
    15.011586099      6,672,142,297      cycles:u                  #    0.333 GHz                      (41.80%)
    15.011586099     47,105,224,996      cycles:k                  #    2.353 GHz                      (41.80%)
    15.011586099      4,323,083,882      instructions:u            #    0.65  insn per cycle           (50.12%)
    15.011586099     26,111,873,166      instructions:k            #    0.55  insn per cycle           (50.12%)
    20.015584626          7,730,496      LLC-load-misses           #   19.97% of all LL-cache accesses  (50.12%)
    20.015584626         77,669,925      LLC-load-misses           #   28.94% of all LL-cache accesses  (50.12%)
    20.015584626         38,715,323      LLC-loads                 #    1.934 M/sec                    (50.12%)
    20.015584626        268,340,633      LLC-loads                 #   13.407 M/sec                    (41.65%)
    20.015584626            488,910      LLC-store-misses          #   24.427 K/sec                    (24.94%)
    20.015584626         44,801,147      LLC-store-misses          #    2.238 M/sec                    (33.26%)
    20.015584626         14,967,469      LLC-stores                #  747.806 K/sec                    (24.94%)
    20.015584626        122,847,559      LLC-stores                #    6.138 M/sec                    (33.25%)
    20.015584626          20,015.18 msec task-clock                #    4.003 CPUs utilized          
    20.015584626      6,666,737,246      cycles:u                  #    0.333 GHz                      (41.57%)
    20.015584626     47,095,139,963      cycles:k                  #    2.353 GHz                      (41.64%)
    20.015584626      4,318,878,354      instructions:u            #    0.65  insn per cycle           (50.03%)
    20.015584626     26,100,131,519      instructions:k            #    0.55  insn per cycle           (50.11%)
    25.019581809          7,735,864      LLC-load-misses           #   19.94% of all LL-cache accesses  (49.95%)
    25.019581809         77,707,326      LLC-load-misses           #   28.95% of all LL-cache accesses  (50.03%)
    25.019581809         38,794,083      LLC-loads                 #    1.938 M/sec                    (50.12%)
    25.019581809        268,381,576      LLC-loads                 #   13.409 M/sec                    (41.81%)
    25.019581809            491,590      LLC-store-misses          #   24.561 K/sec                    (25.03%)
    25.019581809         44,851,037      LLC-store-misses          #    2.241 M/sec                    (33.34%)
    25.019581809         14,911,203      LLC-stores                #  744.989 K/sec                    (24.94%)
    25.019581809        122,990,880      LLC-stores                #    6.145 M/sec                    (33.25%)
    25.019581809          20,015.32 msec task-clock                #    4.003 CPUs utilized          
    25.019581809      6,673,912,912      cycles:u                  #    0.333 GHz                      (41.57%)
    25.019581809     47,095,819,620      cycles:k                  #    2.353 GHz                      (41.56%)
    25.019581809      4,320,399,171      instructions:u            #    0.65  insn per cycle           (49.88%)
    25.019581809     26,109,487,482      instructions:k            #    0.55  insn per cycle           (49.88%)
    30.023578759          7,726,005      LLC-load-misses           #   19.97% of all LL-cache accesses  (49.88%)
    30.023578759         77,683,999      LLC-load-misses           #   28.98% of all LL-cache accesses  (49.88%)
    30.023578759         38,697,410      LLC-loads                 #    1.933 M/sec                    (49.88%)
    30.023578759        268,044,217      LLC-loads                 #   13.392 M/sec                    (41.65%)
    30.023578759            492,082      LLC-store-misses          #   24.585 K/sec                    (25.10%)
    30.023578759         44,999,144      LLC-store-misses          #    2.248 M/sec                    (33.49%)
    30.023578759         14,990,521      LLC-stores                #  748.947 K/sec                    (25.10%)
    30.023578759        122,991,277      LLC-stores                #    6.145 M/sec                    (33.42%)
    30.023578759          20,015.47 msec task-clock                #    4.003 CPUs utilized          
    30.023578759      6,667,466,946      cycles:u                  #    0.333 GHz                      (41.73%)
    30.023578759     47,110,307,718      cycles:k                  #    2.354 GHz                      (41.65%)
    30.023578759      4,319,448,454      instructions:u            #    0.65  insn per cycle           (49.96%)
    30.023578759     26,123,441,036      instructions:k            #    0.55  insn per cycle           (49.88%)
    35.027566990          6,012,620      LLC-load-misses           #   19.86% of all LL-cache accesses  (50.04%)
    35.027566990         60,585,290      LLC-load-misses           #   28.86% of all LL-cache accesses  (49.96%)
    35.027566990         30,269,775      LLC-loads                 #    1.512 M/sec                    (49.88%)
    35.027566990        209,909,346      LLC-loads                 #   10.487 M/sec                    (41.57%)
    35.027566990            382,311      LLC-store-misses          #   19.101 K/sec                    (24.94%)
    35.027566990         35,299,873      LLC-store-misses          #    1.764 M/sec                    (33.26%)
    35.027566990         11,666,599      LLC-stores                #  582.884 K/sec                    (25.02%)
    35.027566990         96,054,246      LLC-stores                #    4.799 M/sec                    (33.41%)
    35.027566990          20,015.32 msec task-clock                #    4.003 CPUs utilized          
    35.027566990      5,191,592,698      cycles:u                  #    0.259 GHz                      (41.80%)
    35.027566990     36,710,045,508      cycles:k                  #    1.834 GHz                      (41.80%)
    35.027566990      3,364,041,477      instructions:u            #    0.65  insn per cycle           (50.12%)
    35.027566990     20,346,301,604      instructions:k            #    0.55  insn per cycle           (50.11%)
    36.112948476              2,327      LLC-load-misses           #   58.26% of all LL-cache accesses  (50.10%)
    36.112948476            143,307      LLC-load-misses           #   28.31% of all LL-cache accesses  (50.47%)
    36.112948476              3,994      LLC-loads                 #  919.955 /sec                     (50.84%)
    36.112948476            506,194      LLC-loads                 #  116.594 K/sec                    (42.38%)
    36.112948476                 87      LLC-store-misses          #   20.039 /sec                     (25.21%)
    36.112948476            176,292      LLC-store-misses          #   40.606 K/sec                    (33.32%)
    36.112948476              1,156      LLC-stores                #  266.266 /sec                     (24.47%)
    36.112948476            231,062      LLC-stores                #   53.221 K/sec                    (32.58%)
    36.112948476           4,341.52 msec task-clock                #    0.868 CPUs utilized          
    36.112948476            760,740      cycles:u                  #    0.000 GHz                      (40.68%)
    36.112948476        113,768,850      cycles:k                  #    0.026 GHz                      (40.89%)
    36.112948476            509,888      instructions:u            #    0.67  insn per cycle           (49.36%)
    36.112948476         11,197,525      instructions:k            #    0.10  insn per cycle           (49.72%)
