#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     1912.6   166.5  1334.0  1712.0  1732.2  1890.3  2033.1  2077.1  2234.5
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      3853470.6 178051.8 3636977.4 3503750.1 3539778.4 3828449.5 4126166.1 4163380.7 4193152.3

Total QPS = 585545.7 (17566371 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 4310109954 bytes :  137.0 MB/s
TX  741792305 bytes :   23.6 MB/s
