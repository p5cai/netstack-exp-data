#type       avg     std     min     5th    10th    50th    90th    95th    99th
read     3714.6   106.6  3145.4  3396.4  3494.3  3774.0  3949.2  3971.1  3988.6
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      1984747.7 55676.7 1866344.5 1806866.4 1834243.1 2008187.0 2124183.3 2138682.9 2150282.5

Total QPS = 301402.8 (9042083 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 2218526922 bytes :   70.5 MB/s
TX  381919714 bytes :   12.1 MB/s
