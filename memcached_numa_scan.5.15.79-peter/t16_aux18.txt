#type       avg     std     min     5th    10th    50th    90th    95th    99th
read      924.9   201.4   123.1   782.6   800.4   889.5  1001.4  1411.0  1838.4
update      0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0     0.0
op_q        1.0     0.0     1.0     1.0     1.0     1.1     1.1     1.1     1.1
rx      7956631.2 406910.5 7087440.0 7143757.9 7449706.8 7972493.3 8756509.6 8875443.5 8970590.6

Total QPS = 1215304.3 (36459146 / 30.0s)

Misses = 0 (0.0%)
Skipped TXs = 0 (0.0%)

RX 8950450121 bytes :  284.5 MB/s
TX 1539604152 bytes :   48.9 MB/s
