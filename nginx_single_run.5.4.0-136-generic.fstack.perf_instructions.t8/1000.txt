Running 1m test @ http://192.168.199.1/plaintext
  8 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.41ms  462.69us   5.63ms   68.74%
    Req/Sec    81.86k    11.80k  101.64k    64.88%
  Latency Distribution
     50%    1.39ms
     75%    1.73ms
     90%    2.02ms
     99%    2.54ms
  39094528 requests in 1.00m, 5.86GB read
  Socket errors: success 1000, connect 0, read 0 (shutdown 0), write 0, timeout 0
Requests/sec: 650901.60
Transfer/sec:     99.91MB
