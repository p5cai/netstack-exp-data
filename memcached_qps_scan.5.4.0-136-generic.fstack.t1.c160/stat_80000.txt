# started on Wed Feb 22 16:08:41 2023


 Performance counter stats for 'CPU(s) 0-0':

        57,418,561      LLC-load-misses                                               (41.66%)
           474,703      LLC-load-misses                                               (41.66%)
           121,933      mem_load_uops_retired.llc_miss                                     (33.33%)
    55,022,586,051      mem_uops_retired.all_loads                                     (16.67%)
   122,008,261,299      cycles                                                        (25.00%)
   121,440,361,565      cycles:u                                                      (25.00%)
       542,071,881      cycles:k                                                      (25.00%)
   147,434,162,100      instructions              #    1.21  insn per cycle         
                                                  #    0.46  stalled cycles per insn  (33.34%)
   147,167,045,193      instructions:u            #    1.21  insn per cycle           (41.67%)
       253,182,904      instructions:k            #    0.47  insn per cycle           (50.00%)
    67,787,859,652      stalled-cycles-frontend   #   55.56% frontend cycles idle     (50.00%)
    33,549,800,563      stalled-cycles-backend    #   27.50% backend cycles idle      (50.00%)

      45.405171606 seconds time elapsed

